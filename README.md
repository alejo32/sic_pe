# PROYECTO SIC #

Este proyecto está en remodelación y se está actualizando.

### Tecnologías usadas ###

* [Java SE Development Kit 8](https://www.oracle.com/cl/java/technologies/javase/javase-jdk8-downloads.html)
* [Maven](https://maven.apache.org/)
* [Selenium](https://www.selenium.dev/)
* [Cucumber](https://cucumber.io/)
* [TestNG](https://testng.org/doc/)

### Integrantes ###

* Alejandro Contreras | acontrerass@ripley.com
* Yulin Tinco Bautista | ytinco-ext@ripley.com.pe

### Supervisores ###

* Eric Díaz Bizama | ediazb@ripley.com
* Edward jara Castro | ejarac@ripley.com
* Carla Llanos | cllanos@ripley.com.pe