# language: es
Característica: Carga Masiva de Ajuste de Stock
  Como usuario
  Quiero realizar ajuste de stock masivo a través del sistema SIC

  Antecedentes:
    Dado ingreso al sistema sic
    Y voy a la opcion Carga Masiva de Ajuste de Stock

  @SIC @SIC_CMAS @SIC_CMAS_01 @Regresion
  Esquema del escenario: Validar la correcta carga masiva de ajuste de stock disponible
    Dado tengo informacion de stock de "<cantidad_productos>" productos de la bodega "<bodega>"
    Y genero un archivo de carga masiva de ajuste de stock acotado
    Cuando selecciono la bodega "<bodega>"
    Y cargo el archivo de carga masiva
    Y doy click a realizar carga
    Entonces se valida la carga en estado "Completado"
    Y se valida lo mostrado en el resumen de ejecucion
    Y se validan los datos del ajuste en bd

    Ejemplos:
      | cantidad_productos | bodega |
      |                 10 |  20027 |

  @SIC @SIC_CMAS @SIC_CMAS_02 @Regresion
  Escenario: Validar que en el formulario el campo tipo de producto tenga el valor por defecto '1-Ripley'
    Entonces se valida que por defecto tenga el valor "1-Ripley"

  @SIC @SIC_CMAS @SIC_CMAS_03 
  Escenario: Realizar carga sin haber seleccionado un archivo
    Cuando doy click a realizar carga
    Entonces se valida mensaje de error "Debe elegir un archivo para procesar."

  @SIC @SIC_CMAS @SIC_CMAS_04 @Regresion
  Escenario: Validar que se muestre el nombre del archivo cargado en el campo Archivo
    Dado tengo informacion de stock de "10" productos de la bodega "20027"
    Y genero un archivo de carga masiva de ajuste de stock
    Cuando selecciono la bodega "20027"
    Y cargo el archivo de carga masiva
    Entonces se valida que se visualice el nombre del archivo seleccionado
    

  @SIC @SIC_CMAS @SIC_CMAS_05
  Esquema del escenario: Realizar una carga masiva con un archivo con extencion no permitida
    Dado tengo informacion de stock de "<cantidad_productos>" productos de la bodega "<bodega>"
    Y genero un archivo de carga masiva de ajuste de stock con extension "<extension>"
    Cuando selecciono la bodega "<bodega>"
    Y cargo el archivo de carga masiva
    Y doy click a realizar carga
    Entonces se valida mensaje de error "El formato a cargar debe ser csv."
    
    Ejemplos: 
      | cantidad_productos | bodega | extension |
      |                 10 |  20027 | .txt      |
      |                 10 |  20027 | .zip      |
      |                 10 |  20027 | .tar.gz   |
      |                 10 |  20027 | .xlsx     |

  @SIC @SIC_CMAS @SIC_CMAS_09 @Regresion
  Escenario: Realizar una carga masiva que termine en estado Completado Fallido
    Dado tengo informacion de stock de "10" productos de la bodega "20027"
    Y genero un archivo de carga masiva de ajuste de stock con errores en algunos registros
    Cuando selecciono la bodega "20027"
    Y cargo el archivo de carga masiva
    Y doy click a realizar carga
    Entonces se valida la carga en estado "Completado con Errores"
    Y se valida lo mostrado en el resumen de ejecucion
    Y se validan los datos del ajuste en bd

  @SIC @SIC_CMAS @SIC_CMAS_10 @Regresion
  Escenario: Realizar una carga masiva que termine en estado Fallido
    Dado tengo informacion de stock de "0" productos de la bodega "20027"
    Y genero un archivo de carga masiva de ajuste de stock
    Cuando selecciono la bodega "20027"
    Y cargo el archivo de carga masiva
    Y doy click a realizar carga
    Entonces se valida la carga en estado "Fallido"
    Y se valida lo mostrado en el resumen de ejecucion

  @SIC @SIC_CMAS @SIC_CMAS_11 @Regresion
  Escenario: Realizar una carga masiva donde se envie un valor no numerico en el campo stock
    Dado tengo informacion de stock de "10" productos de la bodega "20027"
    Y genero un archivo de carga masiva de ajuste de stock con stock "X"
    Cuando selecciono la bodega "20027"
    Y cargo el archivo de carga masiva
    Y doy click a realizar carga
    Entonces se valida la carga en estado "Fallido"
    Y se valida lo mostrado en el resumen de ejecucion
    Y se registra en bd el detalle con estado "F" mensaje "El campo Stock debe ser un número"

  @SIC @SIC_CMAS @SIC_CMAS_12 @Regresion
  Escenario: Realziar una carga masiva donde no se envie el campo sku
    Dado tengo informacion de stock de "10" productos de la bodega "20027"
    Y genero un archivo de carga masiva de ajuste de stock sin valor en el campo sku
    Cuando selecciono la bodega "20027"
    Y cargo el archivo de carga masiva
    Y doy click a realizar carga
    Entonces se valida la carga en estado "Fallido"
    Y se valida lo mostrado en el resumen de ejecucion
    Y se registra en bd el detalle con estado "F" mensaje "SKU no existe (Core)"

  @SIC @SIC_CMAS @SIC_CMAS_13 @Regresion
  Escenario: Realziar una carga masiva donde no se envie el campo stock
    Dado tengo informacion de stock de "10" productos de la bodega "20027"
    Y genero un archivo de carga masiva de ajuste de stock sin valor en el campo stock
    Cuando selecciono la bodega "20027"
    Y cargo el archivo de carga masiva
    Y doy click a realizar carga
    Entonces se valida la carga en estado "Fallido"
    Y se valida lo mostrado en el resumen de ejecucion
    Y se registra en bd el detalle con estado "F" mensaje "El campo Stock debe ser un número"

  @SIC @SIC_CMAS @SIC_CMAS_14 @REVISAR
  Escenario: Validar la correcta carga masiva de ajuste de stock tipo inventario
    Dado tengo informacion de stock de "10" productos de la bodega "20027"
    Y genero un archivo de carga masiva de ajuste de stock
    Cuando selecciono la bodega "20027"
    Y cargo el archivo de carga masiva
    Y doy click a realizar carga
    Entonces se valida la carga en estado "Fallido"
    Y se valida lo mostrado en el resumen de ejecucion
    Y se registra en bd el detalle con estado "F" mensaje "Las operaciones permitidas son A y S"

  @SIC @SIC_CMAS @SIC_CMAS_15 @Regresion
  Escenario: Realizar una carga masiva con tipo de operacion A y S
    Dado tengo informacion de stock de "10" productos de la bodega "20027"
    Y genero un archivo de carga masiva de ajuste de stock con adicion y sustraccion
    Cuando selecciono la bodega "20027"
    Y cargo el archivo de carga masiva
    Y doy click a realizar carga
    Entonces se valida la carga en estado "Completado"
    Y se valida lo mostrado en el resumen de ejecucion
    Y se validan los datos del ajuste en bd

  @SIC @SIC_CMAS @SIC_CMAS_16 @Regresion
  Escenario: Realizar una carga masiva con tipo de operacion incorrecta
    Dado tengo informacion de stock de "10" productos de la bodega "20027"
    Y genero un archivo de carga masiva de ajuste de stock con tipo de operacion "T"
    Cuando selecciono la bodega "20027"
    Y cargo el archivo de carga masiva
    Y doy click a realizar carga
    Entonces se valida la carga en estado "Fallido"
    Y se valida lo mostrado en el resumen de ejecucion
    Y se registra en bd el detalle con estado "F" mensaje "Las operaciones permitidas son A y S"

    