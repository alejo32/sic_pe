# language: es
Característica: Carga Masiva de Ajuste de Stock
  Como usuario
  Quiero realizar ajuste de stock masivo a través del sistema SIC

  Antecedentes:
  Dado ingreso al sistema sic
  Y voy a la opcion Carga Masiva de Ajuste de Stock

 	@SIC @SIC_CMAS @SIC_CMAS_GENERACION_DATA
    Esquema del escenario: Generar stock de 60000
    Dado tengo informacion de stock de "6" productos para la carga masiva de stock
    Y genero un archivo de carga masiva de ajuste de stock