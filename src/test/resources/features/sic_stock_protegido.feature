# language: es
@StockProtegido
Característica: Caso 5.3 - Acceder a opción Manetenedor para ingresar Stock Protegido

  Antecedentes: Caso 5.3 - Acceder a opción Mantenedor para ingresar stock protegido
    Dado ingreso al sistema sic
    Y voy a la opcion Mantenedor para ingresar stock de seguridad

  Escenario: Caso 5.4 - Realizamos busqueda por SKU para realizar su ajuste de Stock
    Dado selecciono la bodega "20027"
    Y selecciono departamento "D101"
    Cuando presiono el boton consultar stock protegido
    Entonces se despliega la grilla con registros

  @StockProtegidoEliminar
  Escenario: Caso 5.8 - Eliminamos evento activo
    Dado selecciono la bodega "20027"
    Y selecciono departamento "D101"
    Y presiono el boton consultar stock protegido
    Y se despliega la grilla con registros
    Cuando presiono el boton eliminar
    Entonces Se despliega Pop Up con mensaje de alerta para Confirmar Stock
    Y se despliega mensaje de alerta Confirmando

  @SIC @SIC_MSP @SIC_MSP_001 @Regresion
  Escenario: Caso SIC_MSP_001 - El campo Tipo de Producto presenta la opción Ripley seleccionada por defecto.
    Entonces se valida que por defecto tenga el valor "1-ripley"

  @SIC @SIC_MSP @SIC_MSP_002 @Regresion
  Escenario: Caso SIC_MSP_002 - El campo Departamento no presenta ningún valor seleccionado por defecto pero sí permite elegir uno.
    Dado existe el campo departamento
    Entonces el campo departamento se encuentra vacio
    Y me permite seleccionar el departamento "D101"

  @SIC @SIC_MSP @SIC_MSP_003 @Regresion
  Escenario: Caso SIC_MSP_003 - El campo Línea, no presenta ningún valor seleccionado por defecto y no permite elegir uno.
    Dado valido la existencia del campo linea
    Entonces no permite seleccionar un valor de linea

  @SIC @SIC_MSP @SIC_MSP_004 @Regresion
  Escenario: Caso SIC_MSP_004 - El campo SubLínea, no presenta ningún valor seleccionado por defecto y no permite elegir uno.
    Dado valido la existencia del campo sub linea
    Entonces no permite seleccionar un valor de sub linea

  @SIC @SIC_MSP @SIC_MSP_005 @Regresion
  Escenario: Caso SIC_MSP_005 - El campo SKU, no presenta ningún valor seleccionado por defecto y no permite elegir uno.
    Dado valido la existencia del campo cod venta
    Entonces no permite seleccionar un valor de cod venta

  @SIC @SIC_MSP @SIC_MSP_006 @Regresion
  Escenario: Caso SIC_MSP_006 - El botón Consultar se muestra inhabilitado por defecto
    Dado valido la existencia del boton consultar
    Entonces el boton consultar se encuentra deshabilitado

  @SIC @SIC_MSP @SIC_MSP_007 @Regresion
  Escenario: Caso SIC_MSP_007 - La grilla se muestra sin ningún registro por defecto.
    Dado valido la existencia grilla configuraciones
    Entonces la grilla se despliega sin registros

  @SIC @SIC_MSP @SIC_MSP_008 @Regresion
  Escenario: Caso SIC_MSP_008 - El botón Nuevo Evento se muestra inhabilitado por defecto
    Dado valido la existencia del boton nuevo evento
    Entonces el boton nuevo evento se encuentra deshabilitado

  @SIC @SIC_MSP @SIC_MSP_009 @Regresion
  Escenario: Caso SIC_MSP_009 - El botón Editar se muestra inhabilitado por defecto
    Dado valido la existencia del boton editar
    Entonces el boton editar se encuentra deshabilitado

  @SIC @SIC_MSP @SIC_MSP_010 @Regresion
  Escenario: Caso SIC_MSP_010 - El botón Eliminar se muestra inhabilitado por defecto
    Dado valido la existencia del boton eliminar
    Entonces el boton eliminar se encuentra deshabilitado

  @SIC @SIC_MSP @SIC_MSP_011 @Regresion
  Escenario: Caso SIC_MSP_011 - El usuario habiendo seleccionado el tipo de producto Ripley, selecciona un departamento
    Cuando selecciono por defecto tipo de producto ripley
    Entonces me permite seleccionar el departamento "D101"

  @SIC @SIC_MSP @SIC_MSP_012 @Regresion
  Escenario: Caso SIC_MSP_012 - El botón consultar se habilita al seleccionar tipo de producto y departamento
    Dado selecciono por defecto tipo de producto ripley
    Cuando ingreso el departamento "D101"
    Entonces el boton consultar se encuentra habilitado

  @SIC @SIC_MSP @SIC_MSP_013 @Regresion
  Escenario: Caso SIC_MSP_013 - El campo línea se habilita al seleccionar tipo de producto y departamento
    Dado selecciono la bodega "20027"
    Y selecciono por defecto tipo de producto ripley
    Cuando el campo departamento se encuentra vacio
    Y me permite seleccionar el departamento "D101"
    Entonces se habilita el combobox linea

  @SIC @SIC_MSP @SIC_MSP_014 @Regresion 
  Escenario: Caso SIC_MSP_014 - Visualizar en combo Departamento listado de departamentos almacenados en BD. Codigo Dpto - Nombre Dpto
    Dado selecciono la bodega "20027"
    Y selecciono por defecto tipo de producto ripley
    Cuando el campo departamento se encuentra vacio
    Entonces selecciono el combo departamento
    Y se despliegan todos los departamentos existentes

  @SIC @SIC_MSP @SIC_MSP_015 @Regresion
  Escenario: Caso SIC_MSP_015 - Visualizar en el combo de Línea el listado de Líneas almacenados en BD. Codigo Linea - Nombre Linea
    Cuando selecciono la bodega "20027"
    Y selecciono por defecto tipo de producto ripley
    Y selecciono departamento "D101"
    Entonces selecciono el combo linea

  @SIC @SIC_MSP @SIC_MSP_016 @Regresion
  Escenario: Caso SIC_MSP_016 - Se habilita el campo SubLínea al seleccionar una Línea
    Dado selecciono la bodega "20027"
    Y selecciono por defecto tipo de producto ripley
    Y selecciono departamento "D101"
    Cuando selecciono linea "001038 - Acc. textil"
    Entonces selecciono sublinea ""

  @SIC @SIC_MSP @SIC_MSP_017 @Regresion
  Escenario: Caso SIC_MSP_017 - Selecciona una línea y elimina el departamento seleccionado
    Dado selecciono la bodega "20027"
    Y selecciono por defecto tipo de producto ripley
    Y selecciono departamento "D101"
    Cuando selecciono linea "001038 - Acc. textil"
    Entonces elimino departamento seleccionado

  @SIC @SIC_MSP @SIC_MSP @SIC_MSP_018 @Regresion
  Escenario: Caso SIC_MSP_018 - Se inhabilita el campo sublinea al eliminar todas las líneas seleccionadas
    Dado selecciono la bodega "20027"
    Y selecciono por defecto tipo de producto ripley
    Y selecciono departamento "D101"
    Cuando selecciono linea "001038 - Acc. textil"
    Entonces selecciono sublinea "S002423 - Gorros"
    Y elimino lineas seleccionadas

  @SIC @SIC_MSP @SIC_MSP_019 @Regresion
  Escenario: Caso SIC_MSP_019 - Visualizar en combo SubLínea listado almacenados en la BD. Codigo SubLinea - Nombre SubLinea
    Dado selecciono la bodega "20027"
    Y selecciono por defecto tipo de producto ripley
    Y selecciono departamento "D101"
    Cuando selecciono linea "001038 - Acc. textil"
    Entonces selecciono sublinea "S002423 - Gorros"

  @SIC @SIC_MSP @SIC_MSP_020 @Regresion
  Escenario: Caso SIC_MSP_020 - Se habilita el campo SKU al seleccionar una sublínea
    Dado selecciono la bodega "20027"
    Y selecciono por defecto tipo de producto ripley
    Y selecciono departamento "D101"
    Cuando selecciono linea "001038 - Acc. textil"
    Y selecciono sublinea "S002423 - Gorros"
    Entonces se habilita campo codigo de venta

  @SIC @SIC_MSP @SIC_MSP_024 @Regresion
  Escenario: Caso SIC_MSP_024 - El usuario selecciona más de un sku
    Dado selecciono la bodega "20027"
    Y selecciono por defecto tipo de producto ripley
    Y selecciono departamento "D101"
    Y selecciono linea "001038 - Acc. textil"
    Cuando selecciono sublinea "S002423 - Gorros"
    Entonces selecciono codigo venta "2001014790008"
    Y selecciono codigo venta "2001011470002"

  @SIC @SIC_MSP @SIC_MSP_025 @Regresion
  Escenario: Caso SIC_MSP_025 - El usuario selecciona Departamento, da clic en el botón Consultar sin tener una bodega seleccionada.
    Dado selecciono por defecto tipo de producto ripley
    Cuando selecciono departamento "D101"
    Entonces presiono boton consultar
    Y se despliega mensaje de alerta debe seleccionar una bodega para continuar

  @SIC @SIC_MSP @SIC_MSP_026 @Regresion
  Escenario: Caso SIC_MSP_026 - El usuario seleccionar Línea, da clic en el botón Consultar sin tener una bodega seleccionada.
    Dado selecciono por defecto tipo de producto ripley
    Cuando selecciono departamento "D101"
    Y selecciono linea "001038 - Acc. textil"
    Entonces presiono boton consultar
    Y se despliega mensaje de alerta debe seleccionar una bodega para continuar

  @SIC @SIC_MSP @SIC_MSP_028 @Regresion
  Escenario: Caso SIC_MSP_028 - El usuario selecciona SubLínea, da clic en el botón Consultar sin tener una bodega seleccionada.
    Dado selecciono por defecto tipo de producto ripley
    Cuando selecciono departamento "D101"
    Y selecciono linea "001038 - Acc. textil"
    Y selecciono sublinea "S002423 - Gorros"
    Y selecciono codigo venta "2001014790008"
    Entonces presiono boton consultar
    Y se despliega mensaje de alerta debe seleccionar una bodega para continuar

  @SIC @SIC_MSP @SIC_MSP_029 @Regresion
  Escenario: Caso SIC_MSP_029 - El usuario da clic en el botón Consultar teniendo elegida un departamento
    Dado selecciono la bodega "20027"
    Y selecciono por defecto tipo de producto ripley
    Cuando selecciono departamento "D101"
    Y presiono boton consultar
    Entonces la grilla se despliega sin registros

  @SIC @SIC_MSP @SIC_MSP_030 @Regresion
  Escenario: Caso SIC_MSP_030 - El usuario da clic en el botón Consultar teniendo elegida un departamento y una línea
    Dado selecciono la bodega "20027"
    Y selecciono por defecto tipo de producto ripley
    Cuando selecciono departamento "D101"
    Y selecciono linea "001038 - Acc. textil"
    Y presiono boton consultar
    Entonces la grilla se despliega sin registros

  @SIC @SIC_MSP @SIC_MSP_031 @Regresion
  Escenario: Caso SIC_MSP_031 - El usuario da clic en el botón Consultar teniendo elegida un departamento, una línea y SubLínea
    Dado selecciono la bodega "20027"
    Y selecciono por defecto tipo de producto ripley
    Cuando selecciono departamento "D101"
    Y selecciono linea "001038 - Acc. textil"
    Y selecciono sublinea "S002423 - Gorros"
    Y presiono boton consultar
    Entonces la grilla se despliega sin registros

  @SIC @SIC_MSP @SIC_MSP_032 @Regresion
  Escenario: Caso SIC_MSP_032 - El usuario da clic en el botón Consultar teniendo elegida un departamento, una línea, SubLínea y un SKU
    Dado selecciono la bodega "20027"
    Y selecciono por defecto tipo de producto ripley
    Cuando selecciono departamento "D101"
    Y selecciono linea "001038 - Acc. textil"
    Y selecciono sublinea "S002423 - Gorros"
    Y selecciono codigo venta "2001014790008"
    Y presiono boton consultar
    Entonces la grilla se despliega sin registros

  @SIC @SIC_MSP @SIC_MSP_033 @Regresion
  Escenario: Caso SIC_MSP_033 - El usuario da clic en el botón Consultar teniendo elegida un departamento
    Dado selecciono la bodega "20027"
    Y selecciono por defecto tipo de producto ripley
    Cuando selecciono departamento "D103"
    Y presiono boton consultar
    Entonces se despliega la grilla con registros

  @SIC @SIC_MSP @SIC_MSP_034 @Regresion
  Escenario: Caso SIC_MSP_034 - El usuario da clic en el botón Consultar teniendo elegida un departamento y una Línea
    Dado selecciono la bodega "20027"
    Y selecciono por defecto tipo de producto ripley
    Cuando selecciono departamento "D103"
    Y selecciono linea "000283 - Audio hi-fi"
    Y presiono boton consultar
    Entonces se despliega la grilla con registros

  @SIC @SIC_MSP @SIC_MSP_035 @Regresion
  Escenario: Caso SIC_MSP_035 - El usuario da clic en el botón Consultar teniendo elegida un departamento, una Línea y una Sub Línea
    Dado selecciono la bodega "20027"
    Y selecciono por defecto tipo de producto ripley
    Cuando selecciono departamento "D103"
    Y selecciono linea "000283 - Audio hi-fi"
    Y selecciono sublinea "S000679 - Componentes"
    Y presiono boton consultar
    Entonces se despliega la grilla con registros

  @SIC @SIC_MSP @SIC_MSP_036 @Regresion
  Escenario: Caso SIC_MSP_036 - El usuario da clic en el botón Consultar teniendo elegida un departamento, una Línea, una Sub Línea y un SKU
    Dado selecciono la bodega "20027"
    Y selecciono por defecto tipo de producto ripley
    Cuando selecciono departamento "D103"
    Y selecciono linea "000283 - Audio hi-fi"
    Y selecciono sublinea "S000679 - Componentes"
    Y selecciono codigo venta "2001011450004"
    Y presiono boton consultar
    Entonces se despliega la grilla con registros

  @SIC @SIC_MSP @SIC_MSP_060 @Regresion
  Escenario: Caso SIC_MSP_060 - El usuario da clic en el botón Nuevo Evento habiendo realizado una consulta previamente.
    Dado selecciono la bodega "20027"
    Y selecciono por defecto tipo de producto ripley
    Cuando selecciono departamento "D103"
    Y presiono boton consultar
    Y se despliega la grilla con registros
    Y presiono el boton nuevo evento
    Entonces se despliega el formulario de configuracion

  @SIC @SIC_MSP @SIC_MSP_061 @Regresion
  Escenario: Caso SIC_MSP_061 - Realizar consulta y nuevo evento. El sistema no permite seleccionar una fecha anterior a la fecha actual
    Dado selecciono la bodega "20027"
    Y selecciono departamento "D103"
    Y presiono boton consultar
    Cuando presiono el boton nuevo evento
    Y se despliega el formulario de configuracion
    Y selecciono el campo inicio vigencia
    Entonces sistema no permite ingresar fecha menor a la actual

  @SIC @SIC_MSP @SIC_MSP_062 @Regresion
  Escenario: Caso SIC_MSP_062 - Realizar consulta y nuevo evento. El sistema permite seleccionar una fecha igual a la fecha actual
    Dado selecciono la bodega "20027"
    Y selecciono departamento "D103"
    Y presiono boton consultar
    Y presiono el boton nuevo evento
    Y se despliega el formulario de configuracion
    Cuando ingreso una fecha de inicio igual a la actual
    Entonces se muestra la fecha seleccionada

  @SIC @SIC_MSP @SIC_MSP_063 @Regresion
  Escenario: Caso SIC_MSP_063 - Realizar consulta y nuevo evento. El sistema permite seleccionar una fecha posterior a la fecha actual
    Dado selecciono la bodega "20027"
    Y selecciono departamento "D103"
    Y presiono boton consultar
    Y presiono el boton nuevo evento
    Y se despliega el formulario de configuracion
    Cuando ingreso una fecha de inicio mayor a la actual
    Entonces se muestra la fecha seleccionada

  @SIC @SIC_MSP @SIC_MSP_064 @Regresion
  Escenario: Caso SIC_MSP_064 - Realizar consulta y nuevo evento. El sistema no permite seleccionar una fecha anterior a la fecha inicio
    Dado selecciono la bodega "20027"
    Y selecciono departamento "D103"
    Y presiono boton consultar
    Y presiono el boton nuevo evento
    Y se despliega el formulario de configuracion
    Cuando ingreso una fecha de inicio mayor a la actual
    Y ingreso una fecha de termino igual a la actual
    Y presiono el boton guardar
    Entonces se muestra mensaje flotante "La fecha Fin Vigencia no puede ser menor a la fecha Inicio Vigencia."

  @SIC @SIC_MSP @SIC_MSP_065 @Regresion
  Escenario: Caso SIC_MSP_065 - Realizar consulta y nuevo evento. El sistema permite seleccionar una fecha posterior a la fecha inicio
    Dado selecciono la bodega "20027"
    Y selecciono departamento "D101"
    Y presiono el boton consultar stock protegido
    Cuando presiono el boton nuevo evento
    Y se despliega el formulario de configuracion
    Entonces ingreso una fecha de inicio correcta
    Y ingreso una fecha de termino correcta
    Y presiono el boton guardar

  @SIC @SIC_MSP @SIC_MSP_066 @Regresion
  Escenario: Caso SIC_MSP_066 -  Los campos de fecha inicio vigencia y fecha fin vigencia se inactivan cuando se selecciona casilla permanente
    Dado selecciono la bodega "20027"
    Y selecciono departamento "D103"
    Y presiono boton consultar
    Y presiono el boton nuevo evento
    Y se despliega el formulario de configuracion
    Cuando selecciono la casilla permanente
    Entonces el campo fecha inicio se deshabilita
    Y el campo fecha de termino se deshabilita

  @SIC @SIC_MSP @SIC_MSP_070 @Regresion
  Escenario: Caso SIC_MSP_070 - Guardar un nuevo evento sin ingresar Campos Obligatorios
    Dado selecciono la bodega "20027"
    Y selecciono departamento "D101"
    Y presiono el boton consultar stock protegido
    Cuando presiono el boton nuevo evento
    Y se despliega el formulario de configuracion
    Y presiono el boton guardar
    Entonces se muestra mensaje de validacion "Debe completar los campos obligatorios"

  @SIC @SIC_MSP @SIC_MSP_071 @Regresion
  Escenario: Caso SIC_MSP_071 - Guardar un nuevo evento sin ingresar todos los niveles de protección
    Dado selecciono la bodega "20027"
    Y selecciono departamento "D103"
    Y presiono boton consultar
    Y presiono el boton nuevo evento
    Y se despliega el formulario de configuracion
    Cuando ingreso una fecha de inicio mayor a la actual
    Y ingreso una fecha de termino correcta
    Y ingreso stock para el nivel uno
    Y ingreso stock para el nivel dos
    Y presiono el boton guardar
    Entonces se muestra mensaje flotante "Hay niveles de protección sin completar, éstos quedarán sin valores en dichos niveles."

  @SIC @SIC_MSP @SIC_MSP_072 @Regresion
  Escenario: Caso SIC_MSP_072 - Guardar un nuevo evento, sistema valida que existe una configuración
    Dado selecciono la bodega "20027"
    Y selecciono departamento "D101"
    Y presiono el boton consultar stock protegido
    Y se despliega la grilla con registros
    Cuando presiono el boton nuevo evento
    Y ingreso una fecha de inicio correcta
    Y ingreso una fecha de termino correcta
    Y ingreso stock para el nivel uno
    Y ingreso stock para el nivel dos
    Y ingreso stock para el nivel tres
    Y ingreso stock para el nivel cuatro
    Y ingreso stock para el nivel cinco
    Entonces presiono el boton guardar
    Y Se despliega mensaje de alerta indicando que ya existe una configuración

  @SIC @SIC_MSP @SIC_MSP_073 @Regresion
  Escenario: Caso SIC_MSP_073 - Guardar un nuevo evento. El sistema cerrará el mensaje de alerta, y se volverá al Formulario de Configuración de Stock de seguridad.
    Dado selecciono la bodega "20027"
    Y selecciono departamento "D101"
    Y presiono el boton consultar stock protegido
    Y se despliega la grilla con registros
    Cuando presiono el boton nuevo evento
    Y ingreso una fecha de inicio correcta
    Y ingreso una fecha de termino correcta
    Y ingreso stock para el nivel uno
    Y ingreso stock para el nivel dos
    Y ingreso stock para el nivel tres
    Y ingreso stock para el nivel cuatro
    Y ingreso stock para el nivel cinco
    Entonces presiono el boton guardar
    Y se despliega mensaje de alerta Confirmando

  @SIC @SIC_MSP @SIC_MSP_080 @Regresion
  Escenario: Caso SIC_MSP_080 - Guardar un nuevo evento. El sistema cerrará el mensaje de alerta, y se volverá al Formulario de Configuración.
    Dado selecciono la bodega "20027"
    Y selecciono departamento "D103"
    Y selecciono linea "000283 - Audio hi-fi"
    Y selecciono sublinea "S000679 - Componentes"
    Y selecciono codigo venta "2001011450004"
    Y presiono boton consultar
    Cuando presiono el boton nuevo evento
    Y ingreso una fecha de inicio correcta
    Y ingreso una fecha de termino correcta
    Y ingreso stock para el nivel uno
    Y ingreso stock para el nivel dos
    Y ingreso stock para el nivel tres
    Y ingreso stock para el nivel cuatro
    Y ingreso stock para el nivel cinco
    Y presiono el boton guardar
    Entonces se muestra mensaje flotante "Ya existe una configuración para la jerarquía"
    Y presiono el boton confirmar
    Y presiono el boton guardar
    Y se muestra mensaje flotante "Configuración actualizada"

  @SIC @SIC_MSP @SIC_MSP_084 @Regresion
  Escenario: Caso SIC_MSP_084 - El usuario da clic en el botón Cancelar del Formulario de Configuración de Stock Protegido.
    Dado selecciono la bodega "20027"
    Y selecciono departamento "D103"
    Y selecciono linea "000283 - Audio hi-fi"
    Y selecciono sublinea "S000679 - Componentes"
    Y selecciono codigo venta "2001011450004"
    Y presiono boton consultar
    Cuando presiono el boton nuevo evento
    Y ingreso una fecha de inicio correcta
    Y ingreso una fecha de termino correcta
    Y ingreso stock para el nivel uno
    Y ingreso stock para el nivel dos
    Y ingreso stock para el nivel tres
    Y ingreso stock para el nivel cuatro
    Y ingreso stock para el nivel cinco
    Entonces presiono el boton cancelar

  @SIC @SIC_MSP @SIC_MSP_087 @Regresion
  Escenario: Caso SIC_MSP_087 - El usuario da clic en el botón Eliminar de la grilla de Configuraciones Vigentes y habiendo seleccionado una configuración
    Dado selecciono la bodega "20027"
    Y selecciono departamento "D101"
    Y presiono el boton consultar stock protegido
    Y se despliega la grilla con registros
    Cuando selecciono el ultimo registro
    Y presiono el boton eliminar
    Entonces se muestra mensaje flotante "¿Está seguro que desea eliminar la configuración seleccionada?"
    
  @SIC_MSP @SIC_MSP_088 @Regresion
  Escenario: Caso SIC_MSP_088 - El usuario da clic en el botón Eliminar de la grilla de Configuraciones Vigentes, el usuario confirma la eliminación
    Dado selecciono la bodega "20027"
    Y selecciono departamento "D101"
    Y presiono el boton consultar stock protegido
    Y se despliega la grilla con registros
    Cuando presiono el boton eliminar
    Entonces Se despliega Pop Up con mensaje de alerta para Confirmar Stock
    Y se despliega mensaje de alerta Confirmando    

  @SIC @SIC_MSP @SIC_MSP_089 @Regresion
  Escenario: Caso SIC_MSP_089 - El usuario da clic en el botón Eliminar de la grilla de Configuraciones Vigentes, el usuario cancela la eliminación
    Dado selecciono la bodega "20027"
    Y selecciono departamento "D101"
    Y presiono el boton consultar stock protegido
    Y se despliega la grilla con registros
    Cuando selecciono el ultimo registro
    Y presiono el boton eliminar
    Y se muestra mensaje flotante "¿Está seguro que desea eliminar la configuración seleccionada?"
    Y presiono el boton cancelar
    Entonces se muestra mensaje flotante "Proceso fue cancelado."

  @SIC @SIC_MSP @SIC_MSP_090 @Regresion
  Escenario: Caso SIC_MSP_090 - El usuario da clic en el botón Editar de la grilla de Configuraciones Vigentes
    Dado selecciono la bodega "20027"
    Y selecciono departamento "D101"
    Y presiono el boton consultar stock protegido
    Y se despliega la grilla con registros
    Cuando Se presiona el Botón Editar
    Y se despliega el formulario de configuracion

  @SIC @SIC_MSP @SIC_MSP_091 @Regresion
  Escenario: Caso SIC_MSP_091 - El usuario da clic en el botón Editar de la grilla de Configuraciones Vigentes habiendo seleccionado una configuración en estado activo
    Dado selecciono la bodega "20027"
    Y selecciono departamento "D101"
    Y presiono el boton consultar stock protegido
    Y se despliega la grilla con registros
    Cuando Se presiona el Botón Editar
    Y se despliega el formulario de configuracion
    Entonces Editamos stock para el nivel uno
    Y Editamos stock para el nivel dos
    Entonces presiono el boton guardar

  @SIC @SIC_MSP @SIC_MSP_094 @Regresion
  Escenario: Caso SIC_MSP_094 - El usuario da clic en el botón Editar de la grilla de Configuraciones Vigentes habiendo seleccionado una configuración en estado activo y cancela edición
    Dado selecciono la bodega "20027"
    Y selecciono departamento "D101"
    Y presiono el boton consultar stock protegido
    Y se despliega la grilla con registros
    Cuando Se presiona el Botón Editar
    Y se despliega el formulario de configuracion
    Entonces Editamos stock para el nivel uno
    Y Editamos stock para el nivel dos
    Entonces Presionamos el Botón Cancelar

    
    
