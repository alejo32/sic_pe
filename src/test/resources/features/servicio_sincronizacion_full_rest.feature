# language: es
Característica: Sincronizacion Full
  Lista de casos de prueba para probar el correcto funcionamiento de Sincronizacion Full via REST

  @SFULL @SFULL_01 @SFULL_PMM @SFULL_F
  Esquema del escenario: PMM - Enviar sfull, los archivos comprimidos tienen información y formato diferente a csv
    Dado quiero realizar una sincronizacion full via REST para el sistema "<sistema_origen>"
    Y tengo informacion de "<cantidad_productos>" productos
    Y tengo informacion de "<cantidad_bodegas>" bodegas
    Cuando genero un archivo por bodega con extension incorrecta
    Y comprimo los archivos en formato ".zip"
    Y cargo el archivo sfull en el servidor sftp
    Y envio un request de sfull al api
    Entonces se registra en bd la cabecera con estado "F" mensaje "El archivo se encuentra vacío"

    Ejemplos: 
      | sistema_origen | cantidad_productos | cantidad_bodegas |
      | PMM            |                 25 |                4 |
      | B2B            |                 25 |                4 |
      | BGT            |                 25 |                4 |

  @SFULL @SFULL_04 @SFULL_PMM @SFULL_F
  Esquema del escenario: PMM - Se envia un archivo SFULL con extension incorrecta
    Dado quiero realizar una sincronizacion full via REST para el sistema "<sistema_origen>"
    Cuando genero un archivo sfull con extension "<extension>"
    Y cargo el archivo sfull en el servidor sftp
    Y envio un request de sfull al api
    Entonces se obtiene un response con mensaje "The filename must contain .csv|.tar|.gz|.zip"

    Ejemplos: 
      | sistema_origen | extension |
      | PMM            | .txt      |
      | B2B            | .rar      |
      | BGT            | .xlsx     |

  @SFULL @SFULL_07 @SFULL_PMM @SFULL_F
  Esquema del escenario: PMM - Se envia un archivo SFULL sin datos en la columna Stock
    Dado quiero realizar una sincronizacion full via REST para el sistema "<sistema_origen>"
    Y genero un archivo sfull con la columna stock vacia
    Cuando cargo el archivo sfull en el servidor sftp
    Y envio un request de sfull al api
    Entonces se registra en bd la cabecera con estado "F" mensaje ""
    Y se registra en bd el detalle con estado "F" mensaje "El campo Stock debe ser numérico"

    Ejemplos: 
      | sistema_origen |
      | PMM            |
      | B2B            |
      | BGT            |

  @SFULL @SFULL_10 @SFULL_PMM @SFULL_C
  Esquema del escenario: PMM - Se envia un archivo SFULL sin datos en la columna Seller
    Dado quiero realizar una sincronizacion full via REST para el sistema "<sistema_origen>"
    Y genero un archivo sfull con la columna seller vacia
    Cuando cargo el archivo sfull en el servidor sftp
    Y envio un request de sfull al api
    Entonces se registra en bd la cabecera con estado "C" mensaje ""
    Y se registra en bd el detalle con estado "C" mensaje ""

    Ejemplos: 
      | sistema_origen |
      | PMM            |
      | B2B            |
      | BGT            |

  @SFULL @SFULL_13 @SFULL_PMM @SFULL_F
  Esquema del escenario: PMM - Se envia un SFULL sin archivo
    Dado quiero realizar una sincronizacion full via REST para el sistema "<sistema_origen>"
    Cuando envio un request de sfull al api
    Entonces se registra en bd la cabecera con estado "F" mensaje "no se encontró en la ruta proporcionada..."
    Y no se registra en bd el detalle

    Ejemplos: 
      | sistema_origen |
      | PMM            |
      | B2B            |
      | BGT            |

  @SFULL @SFULL_16 @SFULL_PMM @SFULL_F
  Esquema del escenario: PMM - Se envía la misma cantidad para un producto
    Dado quiero realizar una sincronizacion full via REST para el sistema "<sistema_origen>"
    Y obtengo "<cantidad_registros>" registros con stock igual al actual
    Y genero un archivo sfull con los datos obtenidos
    Cuando cargo el archivo sfull en el servidor sftp
    Y envio un request de sfull al api
    Entonces se registra en bd la cabecera con estado "F" mensaje ""
    Y no se registra en bd el detalle

    Ejemplos: 
      | sistema_origen | cantidad_registros |
      | PMM            |                  5 |
      | B2B            |                  5 |
      | BGT            |                  5 |

  @SFULL @SFULL_19 @SFULL_PMM @SFULL_C
  Esquema del escenario: PMM - Se envía un SFULL de 65 millones de productos
    Dado quiero realizar una sincronizacion full via REST para el sistema "<sistema_origen>"
    Y tengo informacion de "<cantidad_productos>" productos
    Y tengo informacion de "<cantidad_bodegas>" bodegas
    Cuando genero un archivo csv por bodega
    Y comprimo los archivos en formato ".zip"
    Y cargo el archivo sfull en el servidor sftp

    Ejemplos: 
      | sistema_origen | cantidad_productos | cantidad_bodegas |
      | PMM            |             325000 |              200 |

  @SFULL @SFULL_22 @SFULL_PMM @SFULL_C @Regresion
  Esquema del escenario: BGT - Sincronización de 100 productos
    Dado quiero realizar una sincronizacion full via REST para el sistema "<sistema_origen>"
    Y tengo informacion de "<cantidad_productos>" productos
    Y tengo informacion de "<cantidad_bodegas>" bodegas
    Cuando genero un archivo csv por bodega
    Y comprimo los archivos en formato ".zip"
    Y cargo el archivo sfull en el servidor sftp
    Y envio un request de sfull al api
    Entonces se registra en bd la cabecera con estado "C" mensaje ""
    Y se registra en bd el detalle con estado "C" mensaje ""

    Ejemplos: 
      | sistema_origen | cantidad_productos | cantidad_bodegas |
      | PMM            |                 25 |                4 |
      | B2B            |                 25 |                4 |
      | BGT            |                 25 |                4 |

  @SFULL @SFULL_25 @SFULL_PMM @SFULL_CF
  Esquema del escenario: PMM - Se envía una transacción y su estado cambia a 'Completado con Errores'
    Dado quiero realizar una sincronizacion full via REST para el sistema "<sistema_origen>"
    Y obtengo "<cantidad_registros>" registros con stock igual y diferente
    Y genero un archivo sfull con los datos obtenidos
    Cuando cargo el archivo sfull en el servidor sftp
    Y envio un request de sfull al api
    Entonces se registra en bd la cabecera con estado "CF" mensaje ""
    Y se registra en bd el detalle con estado "C" mensaje ""

    Ejemplos: 
      | sistema_origen | cantidad_registros |
      | PMM            |                 10 |
      | B2B            |                 10 |
      | BGT            |                 10 |

  @SFULL @SFULL_28 @SFULL_PMM @SFULL_F
  Esquema del escenario: PMM - Se envía una transacción con un producto que no existe
    Dado quiero realizar una sincronizacion full via REST para el sistema "<sistema_origen>"
    Y tengo informacion de "<cantidad_productos>" productos que no existen
    Y tengo informacion de "<cantidad_bodegas>" bodegas
    Cuando genero un archivo csv por bodega
    Y comprimo los archivos en formato ".zip"
    Y cargo el archivo sfull en el servidor sftp
    Y envio un request de sfull al api
    Entonces se registra en bd la cabecera con estado "F" mensaje ""
    Y se registra en bd el detalle con estado "C" mensaje "SKU no existe."

    Ejemplos: 
      | sistema_origen | cantidad_productos | cantidad_bodegas |
      | PMM            |                 10 |                1 |
      | B2B            |                 10 |                1 |
      | BGT            |                 10 |                1 |

  @SFULL @SFULL_31 @SFULL_PMM @SFULL_F
  Esquema del escenario: PMM - Se envía una transacción con una bodega que no existe
    Dado quiero realizar una sincronizacion full via REST para el sistema "<sistema_origen>"
    Y tengo informacion de "<cantidad_productos>" productos
    Y tengo informacion de "<cantidad_bodegas>" bodega que no existe
    Cuando genero un archivo csv por bodega
    Y comprimo los archivos en formato ".zip"
    Y cargo el archivo sfull en el servidor sftp
    Y envio un request de sfull al api
    Entonces se registra en bd la cabecera con estado "F" mensaje ""
    Y se registra en bd el detalle con estado "F" mensaje "bodega no existe."

    Ejemplos: 
      | sistema_origen | cantidad_productos | cantidad_bodegas |
      | PMM            |                 10 |                1 |
      | B2B            |                 10 |                1 |
      | BGT            |                 10 |                1 |

  @SFULL @SFULL_34 @SFULL_PMM @SFULL_C
  Esquema del escenario: PMM - En la cabecera de la trama, no se envía el campo 'Usuario'
    Dado quiero realizar una sincronizacion full via REST para el sistema "<sistema_origen>"
    Y tengo informacion de "<cantidad_productos>" productos
    Y tengo informacion de "<cantidad_bodegas>" bodegas
    Cuando genero un archivo csv por bodega
    Y comprimo los archivos en formato ".zip"
    Y cargo el archivo sfull en el servidor sftp
    Y envio un request de sfull al api sin datos en la columna usuario
    Entonces se obtiene un response con mensaje de error "El campo usuario no puede estar vacio."

    Ejemplos: 
      | sistema_origen | cantidad_productos | cantidad_bodegas |
      | PMM            |                  2 |                2 |
      | B2B            |                  2 |                2 |
      | BGT            |                  2 |                2 |

  @SFULL @SFULL_37 @SFULL_PMM @SFULL_C
  Esquema del escenario: PMM - Se envia un archivo SFULL sin datos en la columna Sku
    Dado quiero realizar una sincronizacion full via REST para el sistema "<sistema_origen>"
    Y genero un archivo sfull con la columna sku vacia
    Cuando cargo el archivo sfull en el servidor sftp
    Y envio un request de sfull al api
    Entonces se registra en bd la cabecera con estado "F" mensaje ""
    Y se registra en bd el detalle con estado "F" mensaje "SKU no existe."

    Ejemplos: 
      | sistema_origen |
      | PMM            |
      | B2B            |
      | BGT            |
      
      
  @SFULL @SFULL_XX @SFULL_PMM @SFULL_C
  Esquema del escenario: PMM - Se envia un archivo SFULL sin datos en la columna Sku
    Dado quiero realizar una sincronizacion full via REST para el sistema "<sistema_origen>"
    Y genero un archivo sfull con la columna tipo inventario incorrecto
    Cuando cargo el archivo sfull en el servidor sftp
    Y envio un request de sfull al api
    Entonces se registra en bd la cabecera con estado "F" mensaje ""
    Y se registra en bd el detalle con estado "F" mensaje "SKU no existe."

    Ejemplos: 
      | sistema_origen |
      | PMM            |
      | B2B            |
      | BGT            |

  @SFULL @SFULL_40 @SFULL_PMM @SFULL_C
  Esquema del escenario: PMM - El archivo tiene el nombre correcto pero esta vacio
    Dado quiero realizar una sincronizacion full via REST para el sistema "<sistema_origen>"
    Y genero un archivo sfull sin datos
    Cuando cargo el archivo sfull en el servidor sftp
    Y envio un request de sfull al api
    Entonces se registra en bd la cabecera con estado "F" mensaje "El archivo se encuentra vacío"

    Ejemplos: 
      | sistema_origen |
      | PMM            |
      | B2B            |
      | BGT            |
