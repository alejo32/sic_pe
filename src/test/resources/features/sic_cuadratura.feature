# language: es
Característica: SIC - Reporte de Cuadratura Automático

  Antecedentes: 
    Dado ingreso al sistema sic
    Y voy a la opcion Reporte de Cuadratura Automatico

  @SIC_RCA @SIC_RCA_01
  Escenario: SIC - Validar que se muestren los ultimos 30 registros
    Cuando navego por las paginas
    Entonces valido que se muestren los ultimos registros

  @SIC_RCA @SIC_RCA_02
  Escenario: SIC - Validar que se realice la descarga del archivo
    Cuando doy clic en el ultimo reporte de cuadratura
    Entonces se descarga el reporte
