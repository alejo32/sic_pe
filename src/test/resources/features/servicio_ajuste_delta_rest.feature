# language: es
Característica: Ajuste delta vía REST
  Como usuario
  Quiero realizar ajustes de inventario con la finalidad de cubrir todas las casuisticas que puedan existir

  @SDELTA @SDELTA_REST @SDELTA_REST_01 @SDELTA_REST_PMM @SDELTA_REST_C @Regresion
  Esquema del escenario: Realizar ajuste delta de 1 producto
    Dado  Me conecto  a la BD para realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos
    Cuando envio un request de ajuste al api
    Entonces se registra en bd la cabecera con estado "C" mensaje ""
    Y se validan los datos del ajuste en bd
    Y se valida en SIC el inventario

    Ejemplos: 
      | sistema_origen | cantidad_productos |
      | BGT            |                  1 |

  @SDELTA @SDELTA_REST @SDELTA_REST_09 @SDELTA_REST_PMM @SDELTA_REST_C @Regresion
  Esquema del escenario: Realizar ajuste delta de un producto con stock 0
    Dado  Me conecto  a la BD para realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos con stock 0 con ajuste negativo
    Cuando envio un request de ajuste al api
    Entonces se registra en bd la cabecera con estado "C" mensaje ""
    Y se validan los datos del ajuste en bd
    Y se valida en SIC el inventario

    Ejemplos: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  1 |

  @SDELTA @SDELTA_REST @SDELTA_REST_13 @SDELTA_REST_PMM @SDELTA_REST_F
  Esquema del escenario: En la cabecera del ajuste, no se envía el campo 'Tipo Inventario'
    Dado  Me conecto  a la BD para realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos sin datos en el campo Tipo de Inventario
    Cuando envio un request de ajuste al api
    Entonces se registra en bd la cabecera con estado "F" mensaje ""
    Y se registra en bd el detalle con estado "F" mensaje "El campo inventoryType es obligatorio"

    Ejemplos: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  1 |

  @SDELTA @SDELTA_REST @SDELTA_REST_17 @SDELTA_REST_PMM @SDELTA_REST_C @Regresion
  Esquema del escenario: Realizar ajuste delta de 5 productos sin stock
    Dado  Me conecto  a la BD para realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos sin stock para la bodega "20027"
    Cuando envio un request de ajuste al api
    Entonces se registra en bd la cabecera con estado "C" mensaje ""
    Y se validan los datos del ajuste en bd
    Y se valida en SIC el inventario

    Ejemplos: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  5 |


  @SDELTA @SDELTA_REST @SDELTA_REST_21 @SDELTA_REST_PMM @SDELTA_REST_F
  Esquema del escenario: En la transacción de ajuste, se envía un producto que no existe
    Dado  Me conecto  a la BD para realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos que no existen
    Cuando envio un request de ajuste al api
    Entonces se registra en bd la cabecera con estado "F" mensaje ""
    Y se registra en bd el detalle con estado "F" mensaje "SKU no existe"

    Ejemplos: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  2 |

  @SDELTA @SDELTA_REST @SDELTA_REST_25 @SDELTA_REST_PMM @SDELTA_REST_F
  Esquema del escenario: En la cabecera del ajuste, no se envía el campo 'Tipo de Movimiento'
    Dado  Me conecto  a la BD para realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos sin datos en el campo Tipo de Movimiento
    Cuando envio un request de ajuste al api
    Entonces validar que no se registre en la bd

    Ejemplos: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  1 |

  @SDELTA @SDELTA_REST @SDELTA_REST_29 @SDELTA_REST_PMM @SDELTA_REST_F
  Esquema del escenario: En la cabecera del ajuste, no se envía el campo 'Usuario'
    Dado  Me conecto  a la BD para realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos sin datos en el campo Usuario
    Cuando envio un request de ajuste al api
    Entonces se registra en bd la cabecera con estado "F" mensaje "El campo usuario no puede estar vacio."
    Y no se registra en bd el detalle

    Ejemplos: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  1 |

  @SDELTA @SDELTA_REST @SDELTA_REST_33 @SDELTA_REST_PMM @SDELTA_REST_F
  Esquema del escenario: En la cabecera del ajuste, no se envía el campo 'Sistema Origen'
    Dado  Me conecto  a la BD para realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos sin datos en el campo Sistema Origen
    Cuando envio un request de ajuste al api
    Entonces validar que no se registre en la bd

    Ejemplos: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  1 |

  @SDELTA @SDELTA_REST @SDELTA_REST_37 @SDELTA_REST_PMM @SDELTA_REST_F
  Esquema del escenario: En la cabecera del ajuste, no se envía el campo 'Operacion'
    Dado Me conecto  a la BD para realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos sin datos en el campo Operacion
    Cuando envio un request de ajuste al api
    Entonces se registra en bd la cabecera con estado "C" mensaje ""
    Y se validan los datos del ajuste en bd
    Y se valida en SIC el inventario

    Ejemplos: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  1 |


  @SDELTA @SDELTA_REST @SDELTA_REST_41 @SDELTA_REST_PMM @SDELTA_REST_F
  Esquema del escenario: En la cabecera del ajuste, no se envía el campo 'Tipo Documento'
    Dado Me conecto  a la BD para realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos sin datos en el campo Documento de Transaccion
    Cuando envio un request de ajuste al api
    Entonces se registra en bd la cabecera con estado "F" mensaje ""
    Y se registra en bd el detalle con estado "F" mensaje "El campo transaction_doc es obligatorio"

    Ejemplos: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  1 |

  @SDELTA @SDELTA_REST @SDELTA_REST_45 @SDELTA_REST_PMM @SDELTA_REST_F
  Esquema del escenario: En la cabecera del ajuste, no se envía el campo 'Bodega'
    Dado  Me conecto  a la BD para realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos sin datos en el campo Bodega
    Cuando envio un request de ajuste al api
    Entonces se registra en bd la cabecera con estado "F" mensaje ""
    Y se registra en bd el detalle con estado "F" mensaje "El campo fulfillment es obligatorio"

    Ejemplos: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  1 |

  @SDELTA @SDELTA_REST @SDELTA_REST_49 @SDELTA_REST_PMM @SDELTA_REST_C
  Esquema del escenario: Realizar ajuste delta de 200 producto
    Dado  Me conecto  a la BD para realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos
    Cuando envio un request de ajuste al api
    Entonces se registra en bd la cabecera con estado "C" mensaje ""
    Y se validan los datos del ajuste en bd

    Ejemplos: 
      | sistema_origen | cantidad_productos |
      | PMM            |                200 |


  @SDELTA @SDELTA_REST @SDELTA_REST_53 @SDELTA_REST_PMM @SDELTA_REST_C
  Esquema del escenario: En el del ajuste, no se envía el campo 'SKU'
    Dado  Me conecto  a la BD para realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos
    Y borro el valor de el campo producto
    Cuando envio un request de ajuste al api
    Entonces se registra en bd la cabecera con estado "F" mensaje ""
    Y se registra en bd el detalle con estado "F" mensaje "El campo sku es obligatorio"

    Ejemplos: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  1 |


  @SDELTA @SDELTA_REST @SDELTA_REST_57 @SDELTA_REST_BGT @SDELTA_REST_F
  Esquema del escenario: En el detalle del ajuste, se envía una bodega que no existe
    Dado  Me conecto  a la BD para realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos
    Y asigno la bodega "<bodega_no_existe>"
    Cuando envio un request de ajuste al api
    Entonces se registra en bd la cabecera con estado "F" mensaje ""
    Y se registra en bd el detalle con estado "F" mensaje "Bodega no existe"

    Ejemplos: 
      | sistema_origen | cantidad_productos | bodega_no_existe |
      | PMM            |                  1 |            29912 |


  @SDELTA @SDELTA_REST @SDELTA_REST_61 @SDELTA_REST_PMM @SDELTA_REST_F
  Esquema del escenario: Se envía la misma cantidad para un producto
    Dado  Me conecto  a la BD para realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos
    Y asigno stock igual al actual
    Cuando envio un request de ajuste al api
    Entonces se registra en bd la cabecera con estado "F" mensaje ""
    Y no se registra en bd el detalle

    Ejemplos: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  1 |


  @SDELTA @SDELTA_REST @SDELTA_REST_65 @SDELTA_REST_BGT @SDELTA_REST_CF
  Esquema del escenario: Se envía una transacción y su estado cambia a 'Completado con Errores'
    Dado  Me conecto  a la BD para realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos con error en un registro
    Cuando envio un request de ajuste al api
    Entonces se registra en bd la cabecera con estado "CF" mensaje ""

    Ejemplos: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  1 |

