# language: es
Característica: SIC - Configuracion de Movimientos de Inventario
  Como usuario
  Quiero agregar, eliminar, actualizar y consultar en la pantalla de Configuración de Movimientos de Inventario

  Antecedentes: 
    Dado ingreso al sistema sic
    Y voy a la opcion Configuracion de Movimientos de Inventario

  @SIC @SIC_CMI @SIC_CMI_01
  Escenario: SIC - Validar que el filtro nombre del sistema tenga las todas las opciones
    Cuando doy click al combo nombre de sistema
    Entonces se verifica que se muestren todos los sistemas

  @SIC @SIC_CMI @SIC_CMI_02
  Escenario: SIC - Validar que el filtro Nombre del Sistema es obligatorio
    Dado el boton consultar esta deshabilitado
    Cuando selecciono el sistema "PMM"
    Entonces se habilita el boton consultar

  @SIC @SIC_CMI @SIC_CMI_03
  Escenario: SIC - Validar boton Limpiar
    Dado el boton consultar esta deshabilitado
    Cuando selecciono el sistema "PMM"
    Y doy click en consultar
    Y doy click en limpiar
    Entonces se limpia el resultado
    Y los botones se deshabilitan a excepcion del boton limpiar

  @SIC @SIC_CMI @SIC_CMI_04 @Regresion
  Escenario: SIC - Verificar boton Consultar
    Dado el boton consultar esta deshabilitado
    Cuando selecciono el sistema "PMM"
    Y doy click en consultar
    Entonces muestra las configuraciones asociadas al sistema

  @SIC @SIC_CMI @SIC_CMI_05
  Escenario: SIC - Validar que el botón Eliminar exista y esté bloqueado mientras no selecciones ninguna configuración
    Dado el boton eliminar esta deshabilitado
    Cuando selecciono el sistema "PMM"
    Y doy click en consultar
    Entonces se habilita el boton eliminar

  @SIC @SIC_CMI @SIC_CMI_06 @Regresion
  Escenario: SIC - Validar el filtro Tipo de Transaccion
    Cuando selecciono el sistema "PMM"
    Y doy click en consultar
    Y doy click en el combo tipo de transaccion
    Entonces se verifica que se muestren todos los tipos de transaccion

  @SIC @SIC_CMI @SIC_CMI_07 @Regresion
  Escenario: SIC - Validar el filtro Tipo de Inventario
    Cuando selecciono el sistema "PMM"
    Y doy click en consultar
    Y doy click en el combo tipo de inventario
    Entonces se verifica que se muestren todos los tipos de inventario

  @SIC @SIC_CMI @SIC_CMI_08
  Escenario: SIC - Validar que el botón Agregar se habilite solo si tengo un sistema definido, tipo de actualizacion y tipo de inventario.
    Dado el boton agregar esta deshabilitado
    Cuando selecciono el sistema "PMM"
    Y doy click en consultar
    Y selecciono tipo de transaccion "ADJUST"
    Y selecciono tipo de inventario 1
    Entonces se habilita el boton agregar

  @SIC @SIC_CMI @SIC_CMI_09
  Escenario: SIC - Validar que pueda registrar configuraciones para el sistema B2B
    Dado habilito configuracion para tipo de transaccion "ADJUST" tipo de inventario "1" para el sistema "B2B"
    Cuando selecciono el sistema "B2B"
    Y doy click en consultar
    Y selecciono tipo de transaccion "ADJUST"
    Y selecciono tipo de inventario 1
    Y doy click en agregar
    Entonces muestra mensaje de confirmacion "Ya existe la configuración ingresada, ¿desea actualizarla?"
    Y al dar continuar muestra mensaje "Operación realizada con éxito."

  @SIC @SIC_CMI @SIC_CMI_10
  Escenario: SIC - Validar el filtro Tipo de Inventario
    Entonces se verifica que exista la columna "Tipo de Transacción"
    Y se verifica que exista la columna "Tipo de Inventario"

  @ELIMINADOS
  Esquema del escenario: SIC - Validar que pueda registrar configuraciones para el sistema B2B
    Dado deshabilito configuracion para tipo de transaccion "<tipo_ajuste>" tipo de inventario "<tipo_inventario>" para el sistema "<sistema_origen>"
    Cuando selecciono el sistema "<sistema_origen>"
    Y doy click en consultar
    Y selecciono tipo de transaccion "<tipo_ajuste>"
    Y selecciono tipo de inventario <tipo_inventario>
    Y doy click en agregar
    Entonces muestra mensaje de confirmacion "Existe una configuración inactiva, ¿desea reactivarla?"
    Y al dar continuar muestra mensaje "Operación realizada con éxito."

    Ejemplos: 
      | tipo_ajuste | tipo_inventario | sistema_origen |
      | ADJUST      |               1 | PMM            |
      | ADJUST      |               1 | B2B            |
      | ADJUST      |               1 | BO             |
      | ADJUST      |               1 | BGT            |
