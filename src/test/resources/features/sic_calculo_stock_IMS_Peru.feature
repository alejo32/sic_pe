# language: es

Característica: Como usuario SIC quiero Realizar ajuste de inventario para validar el envio del stock vendible al IMS

 Antecedentes: 
    Dado ingreso al sistema sic con usuario administrador Peru

  @calculo_stock_vemdible_IMS_PE_CP01 @RegresionIMS_PE
  Esquema del escenario: Realizar ajuste incremental de inventario del stock disponible al producto con atributo de despacho DD y validar el calculo del stock vendible enviado a IMS
    Dado que en la tabla de metodo de despacho tenga la configuracion requerida "<STS>","<DD>","<SFS>","<PU>" para el producto"<codigo_venta>" en la bodega "<codigo_bodega>"
    Y que el producto"<codigo_venta>" exista en la tabla del whitelist en la bodega "<codigo_bodega>" 
    Y que el producto"<codigo_venta>" exista en la tabla de publicados 
    Y que el producto"<codigo_venta>" en la bodega "<codigo_bodega>" tenga valor umbral "<Umbral>" y los metodos "<STS>", "<DD>", "<SFS>","<PU>"  en la tabla de stock ripley
    Y voy a la opcion Mantenedor para Ingresar Ajuste de Inventario
    Y realizo la consulta de stock disponible "<tipo_inventario>" actual del producto "<codigo_venta>" en la bodega "<codigo_bodega>"
    Y realizo ajuste ingresando el stock a ajustar "<stock_a_ajustar>" en la bodega "<codigo_bodega>"
    Y obtener informacion del restos de bodegas para el producto"<codigo_venta>"
    Cuando ingreso al sistema IMS Peru
    Y voy a la opcion sku_controller
    Y ingresar el producto"<codigo_venta>" y las credenciales de IMS Peru
    Entonces Valida que el  stock vendible de sic se haya informado a IMS Peru

    Ejemplos: 
      | codigo_bodega | codigo_venta  | tipo_inventario | tipo_actualizacion | stock_a_ajustar |Umbral|STS|DD	|SFS|PU	|
      |         20026 | 2001179636081 |               1 | Incrementar        |               2 |10000 |0  |1	|0	|0	|
    
@calculo_stock_vemdible_IMS_PE_CP02 @RegresionIMS_PE
  Esquema del escenario: Realizar ajuste incremental de inventario del stock disponible al producto con atributo de despacho STS y validar el calculo del stock vendible enviado a IMS
Dado que en la tabla de metodo de despacho tenga la configuracion requerida "<STS>","<DD>","<SFS>","<PU>" para el producto"<codigo_venta>" en la bodega "<codigo_bodega>"
    Y que el producto"<codigo_venta>" exista en la tabla del whitelist en la bodega "<codigo_bodega>" 
    Y que el producto"<codigo_venta>" exista en la tabla de publicados 
    Y que el producto"<codigo_venta>" en la bodega "<codigo_bodega>" tenga valor umbral "<Umbral>" y los metodos "<STS>", "<DD>", "<SFS>","<PU>"  en la tabla de stock ripley
    Y voy a la opcion Mantenedor para Ingresar Ajuste de Inventario
    Y realizo la consulta de stock disponible "<tipo_inventario>" actual del producto "<codigo_venta>" en la bodega "<codigo_bodega>"
    Y realizo ajuste ingresando el stock a ajustar "<stock_a_ajustar>" en la bodega "<codigo_bodega>"
    Y obtener informacion del restos de bodegas para el producto"<codigo_venta>"
    Cuando ingreso al sistema IMS Peru
    Y voy a la opcion sku_controller
    Y ingresar el producto"<codigo_venta>" y las credenciales de IMS Peru
    Entonces Valida que el  stock vendible de sic se haya informado a IMS Peru

    Ejemplos: 
      | codigo_bodega | codigo_venta  | tipo_inventario | tipo_actualizacion | stock_a_ajustar |Umbral|STS|DD	|SFS|PU	|
      |         20025 | 2024760430004 |               1 | Incrementar        |               2 |10000 |1	|0	|0	|1	|

  @calculo_stock_vemdible_IMS_PE_STS_01 @RegresionIMS_PE
  Esquema del escenario: Realizar ajuste incremental de inventario del stock disponible al producto con atributo de despacho STS y validar el calculo del stock vendible enviado a IMS
    Dado que en la tabla de metodo de despacho tenga la configuracion requerida "<STS>","<DD>","<SFS>","<PU>" para el producto"<codigo_venta>" en la bodega "<codigo_bodega>"
    Y que el producto"<codigo_venta>" exista en la tabla del whitelist en la bodega "<codigo_bodega>"
    Y que el producto"<codigo_venta>" exista en la tabla de publicados
    Y que el producto"<codigo_venta>" en la bodega "<codigo_bodega>" tenga valor umbral "<Umbral>" y los metodos "<STS>", "<DD>", "<SFS>","<PU>"  en la tabla de stock ripley
    Y voy a la opcion Mantenedor para Ingresar Ajuste de Inventario
    Y realizo la consulta de stock disponible "<tipo_inventario>" actual del producto "<codigo_venta>" en la bodega "<codigo_bodega>"
    Y realizo ajuste ingresando el stock a ajustar "<stock_a_ajustar>" en la bodega "<codigo_bodega>"
    Y obtener informacion del restos de bodegas para el producto"<codigo_venta>"
    Cuando ingreso al sistema IMS Peru
    Y voy a la opcion sku_controller
    Y ingresar el producto"<codigo_venta>" y las credenciales de IMS Peru
    Entonces Valida que el  stock vendible de sic se haya informado a IMS Peru

    Ejemplos:
      | codigo_bodega | codigo_venta  | tipo_inventario | tipo_actualizacion | stock_a_ajustar |Umbral|STS|DD	|SFS|PU	|
      |         2003  | 2024760430004 |               1 | Incrementar        |               3 |10000 |1	|0	|0	|1	|

  @calculo_stock_vemdible_IMS_PE_CP03 @RegresionIMS_PE
  Esquema del escenario: Realizar ajuste incremental de inventario del stock disponible al producto con atributo de despacho SFS y validar el calculo del stock vendible enviado a IMS
    Dado que en la tabla de metodo de despacho tenga la configuracion requerida "<STS>","<DD>","<SFS>","<PU>" para el producto"<codigo_venta>" en la bodega "<codigo_bodega>"
    Y que el producto"<codigo_venta>" exista en la tabla del whitelist en la bodega "<codigo_bodega>" 
    Y que el producto"<codigo_venta>" exista en la tabla de publicados 
    Y que el producto"<codigo_venta>" en la bodega "<codigo_bodega>" tenga valor umbral "<Umbral>" y los metodos "<STS>", "<DD>", "<SFS>","<PU>"  en la tabla de stock ripley
    Y voy a la opcion Mantenedor para Ingresar Ajuste de Inventario
    Y realizo la consulta de stock disponible "<tipo_inventario>" actual del producto "<codigo_venta>" en la bodega "<codigo_bodega>"
    Y realizo ajuste ingresando el stock a ajustar "<stock_a_ajustar>" en la bodega "<codigo_bodega>"
    Y obtener informacion del restos de bodegas para el producto"<codigo_venta>"
    Cuando ingreso al sistema IMS Peru
    Y voy a la opcion sku_controller
    Y ingresar el producto"<codigo_venta>" y las credenciales de IMS Peru
    Entonces Valida que el  stock vendible de sic se haya informado a IMS Peru

    Ejemplos: 
      | codigo_bodega | codigo_venta  | tipo_inventario | tipo_actualizacion | stock_a_ajustar |Umbral|STS|DD	|SFS|PU	|
      |         20030 | 2024760160000 |               1 | Incrementar        |               1 |10000 |0	|0	|1	|1	|
      
      
  @calculo_stock_vemdible_IMS_PE_CP04 
  Esquema del escenario: Realizar ajuste incremental de inventario del stock disponible al producto con atributo de despacho STS-SFS y validar el calculo del stock vendible enviado a IMS
    Dado que en la tabla de metodo de despacho tenga la configuracion requerida "<STS>","<DD>","<SFS>","<PU>" para el producto"<codigo_venta>" en la bodega "<codigo_bodega>"
    Y que el producto"<codigo_venta>" exista en la tabla del whitelist en la bodega "<codigo_bodega>" 
    Y que el producto"<codigo_venta>" exista en la tabla de publicados 
    Y que el producto"<codigo_venta>" en la bodega "<codigo_bodega>" tenga valor umbral "<Umbral>" y los metodos "<STS>", "<DD>", "<SFS>","<PU>"  en la tabla de stock ripley
    Y voy a la opcion Mantenedor para Ingresar Ajuste de Inventario
    Y realizo la consulta de stock disponible "<tipo_inventario>" actual del producto "<codigo_venta>" en la bodega "<codigo_bodega>"
    Y realizo ajuste ingresando el stock a ajustar "<stock_a_ajustar>" en la bodega "<codigo_bodega>"
    Y obtener informacion del restos de bodegas para el producto"<codigo_venta>"
    Cuando ingreso al sistema IMS Peru
    Y voy a la opcion sku_controller
    Y ingresar el producto"<codigo_venta>" y las credenciales de IMS Peru
    Entonces Valida que el  stock vendible de sic se haya informado a IMS Peru

    Ejemplos: 
      | codigo_bodega | codigo_venta  | tipo_inventario | tipo_actualizacion | stock_a_ajustar |Umbral|STS|DD	|SFS|PU	|
      |         20027 | 2001197217507 |               1 | Incrementar        |               1 |10000 |1	|0	|1	|1	|
      

@calculo_stock_vemdible_IMS_PE_CP05 
  Esquema del escenario: Realizar ajuste incremental de inventario del stock disponible al producto con atributo de despacho STS + DD y validar el calculo del stock vendible enviado a IMS
Dado que en la tabla de metodo de despacho tenga la configuracion requerida "<STS>","<DD>","<SFS>","<PU>" para el producto"<codigo_venta>" en la bodega "<codigo_bodega>"
    Y que el producto"<codigo_venta>" exista en la tabla del whitelist en la bodega "<codigo_bodega>" 
    Y que el producto"<codigo_venta>" exista en la tabla de publicados 
    Y que el producto"<codigo_venta>" en la bodega "<codigo_bodega>" tenga valor umbral "<Umbral>" y los metodos "<STS>", "<DD>", "<SFS>","<PU>"  en la tabla de stock ripley
    Y voy a la opcion Mantenedor para Ingresar Ajuste de Inventario
    Y realizo la consulta de stock disponible "<tipo_inventario>" actual del producto "<codigo_venta>" en la bodega "<codigo_bodega>"
    Y realizo ajuste ingresando el stock a ajustar "<stock_a_ajustar>" en la bodega "<codigo_bodega>"
    Y obtener informacion del restos de bodegas para el producto"<codigo_venta>"
    Cuando ingreso al sistema IMS Peru
    Y voy a la opcion sku_controller
    Y ingresar el producto"<codigo_venta>" y las credenciales de IMS Peru
    Entonces Valida que el  stock vendible de sic se haya informado a IMS Peru

    Ejemplos: 
      | codigo_bodega | codigo_venta  | tipo_inventario | tipo_actualizacion | stock_a_ajustar |Umbral|STS|DD	|SFS|PU	|
      |         20026 | 2001197217538 |               1 | Incrementar        |               1 |10000 |0	|1	|0	|0	|
      |         20025 | 2001197217538 |               1 | Incrementar        |               2 |10000 |1	|0	|0	|1	|
      
  @calculo_stock_vemdible_IMS_PE_CP06
  Esquema del escenario: Realizar ajuste incremental de inventario del stock disponible al producto con atributo de despacho SFS + DD y validar el calculo del stock vendible enviado a IMS
    Dado que en la tabla de metodo de despacho tenga la configuracion requerida "<STS>","<DD>","<SFS>","<PU>" para el producto"<codigo_venta>" en la bodega "<codigo_bodega>"
    Y que el producto"<codigo_venta>" exista en la tabla del whitelist en la bodega "<codigo_bodega>" 
    Y que el producto"<codigo_venta>" exista en la tabla de publicados 
    Y que el producto"<codigo_venta>" en la bodega "<codigo_bodega>" tenga valor umbral "<Umbral>" y los metodos "<STS>", "<DD>", "<SFS>","<PU>"  en la tabla de stock ripley
    Y voy a la opcion Mantenedor para Ingresar Ajuste de Inventario
    Y realizo la consulta de stock disponible "<tipo_inventario>" actual del producto "<codigo_venta>" en la bodega "<codigo_bodega>"
    Y realizo ajuste ingresando el stock a ajustar "<stock_a_ajustar>" en la bodega "<codigo_bodega>"
    Y obtener informacion del restos de bodegas para el producto"<codigo_venta>"
    Cuando ingreso al sistema IMS Peru
    Y voy a la opcion sku_controller
    Y ingresar el producto"<codigo_venta>" y las credenciales de IMS Peru
    Entonces Valida que el  stock vendible de sic se haya informado a IMS Peru

    Ejemplos: 
      | codigo_bodega | codigo_venta  | tipo_inventario | tipo_actualizacion | stock_a_ajustar |Umbral|STS|DD	|SFS|PU	|
      |         20026 | 2001207703587 |               1 | Incrementar        |               1 |10000 |0	|1	|0	|0	|
      |         20030 | 2001207703587 |               1 | Incrementar        |               2 |10000 |0	|0	|1	|1	|   
      
      
   @calculo_stock_vemdible_IMS_PE_CP07
  Esquema del escenario: Realizar ajuste incremental de inventario del stock disponible al producto con atributo de despacho STS-SFS + DD y validar el calculo del stock vendible enviado a IMS
    Dado que en la tabla de metodo de despacho tenga la configuracion requerida "<STS>","<DD>","<SFS>","<PU>" para el producto"<codigo_venta>" en la bodega "<codigo_bodega>"
    Y que el producto"<codigo_venta>" exista en la tabla del whitelist en la bodega "<codigo_bodega>" 
    Y que el producto"<codigo_venta>" exista en la tabla de publicados 
    Y que el producto"<codigo_venta>" en la bodega "<codigo_bodega>" tenga valor umbral "<Umbral>" y los metodos "<STS>", "<DD>", "<SFS>","<PU>"  en la tabla de stock ripley
    Y voy a la opcion Mantenedor para Ingresar Ajuste de Inventario
    Y realizo la consulta de stock disponible "<tipo_inventario>" actual del producto "<codigo_venta>" en la bodega "<codigo_bodega>"
    Y realizo ajuste ingresando el stock a ajustar "<stock_a_ajustar>" en la bodega "<codigo_bodega>"
    Y obtener informacion del restos de bodegas para el producto"<codigo_venta>"
    Cuando ingreso al sistema IMS Peru
    Y voy a la opcion sku_controller
    Y ingresar el producto"<codigo_venta>" y las credenciales de IMS Peru
    Entonces Valida que el  stock vendible de sic se haya informado a IMS Peru

    Ejemplos: 
      | codigo_bodega | codigo_venta  | tipo_inventario | tipo_actualizacion | stock_a_ajustar |Umbral|STS|DD	|SFS|PU	|
      |         20026 | 2001207703594 |               1 | Incrementar        |               1 |10000 |0	|1	|0	|0	|
      |         20027 | 2001207703594 |               1 | Incrementar        |               1 |10000 |1	|0	|1	|1	| 
      
      
       