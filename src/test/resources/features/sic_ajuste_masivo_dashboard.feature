# language: es

Característica: Carga Masiva de Ajuste de Stock
  Como usuario
  Quiero realizar ajuste de stock masivo a través del sistema SIC

  Antecedentes:
    Dado ingreso al sistema sic con perfil administrador
    Y voy a la opcion Carga Masiva de Ajuste de Stock

  @CMAS_Apartir_de_archivo
  Escenario: Validar la correcta carga masiva de ajuste de stock disponible
    Cuando cargo el archivo de Carga Masiva de Ajuste de Stock acotado
    Y doy click a realizar carga
    Entonces se valida la carga en estado "Completado"

