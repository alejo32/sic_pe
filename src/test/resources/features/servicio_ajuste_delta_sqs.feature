# language: es
Característica: Ajuste delta vía SQS
  Como usuario
  Quiero realizar ajustes de inventario con la finalidad de cubrir todas las casuisticas que puedan existir

  @SDELTA @SDELTA_SQS @SDELTA_SQS_01 @SDELTA_SQS_PMM @SDELTA_SQS_C @Regresion
  Esquema del escenario: Realizar ajuste delta de 1 producto
    Dado quiero realizar un ajuste delta via SQS para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos
   # Cuando envio un request de ajuste via SQS
    Cuando envio un request de ajuste via SQS "<Cola_Standar>"
    Entonces se registra en bd la cabecera con estado "C" mensaje ""
    Y se validan los datos del delta en bd

    Ejemplos: 

      | sistema_origen | cantidad_productos |Cola_Standar|
      | PMM            | 1                  |https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsic-sqs-stock-pmm-qa-pe/send-receive|


  @SDELTA @SDELTA_SQS @SDELTA_SQS_09 @SDELTA_SQS_PMM @SDELTA_SQS_SinOrigen
  Esquema del escenario: En la cabecera del ajuste, no se envía el campo 'Sistema Origen'
    Dado quiero realizar un ajuste delta via SQS para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos sin datos en el campo Sistema Origen
    Cuando envio un request de ajuste via SQS "<Cola_Standar>"
    Entonces validar que no se registre en la bd

    Ejemplos:

      | sistema_origen | cantidad_productos |Cola_Standar|
      | PMM            | 1                  |https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsic-sqs-stock-pmm-qa-pe/send-receive|


  @SDELTA @SDELTA_SQS @SDELTA_SQS_13 @SDELTA_SQS_PMM @SDELTA_SQS_SinEstado
  Esquema del escenario: En la cabecera del ajuste, no se envía el campo 'Tipo de Movimiento'
    Dado quiero realizar un ajuste delta via SQS para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos sin datos en el campo Tipo de Movimiento
    Cuando envio un request de ajuste via SQS "<Cola_Standar>"
    Entonces validar que no se registre en la bd

    Ejemplos:

      | sistema_origen | cantidad_productos |Cola_Standar|
      | PMM            | 1                  |https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsic-sqs-stock-pmm-qa-pe/send-receive|


  @SDELTA @SDELTA_SQS @SDELTA_SQS_17 @SDELTA_REST_PMM @SDELTA_SQS_SinUsuario
  Esquema del escenario: En la cabecera del ajuste, no se envía el campo 'Usuario'
    Dado quiero realizar un ajuste delta via SQS para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos sin datos en el campo Usuario
    Cuando envio un request de ajuste via SQS "<Cola_Standar>"
    Entonces se registra en bd la cabecera con estado "F" mensaje "El campo user es obligatorio"
    Y no se registra en bd el detalle

    Ejemplos:

      | sistema_origen | cantidad_productos |Cola_Standar|
      | PMM            | 1                  |https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsic-sqs-stock-pmm-qa-pe/send-receive|


  @SDELTA @SDELTA_SQS @SDELTA_SQS_21 @SDELTA_REST_PMM @SDELTA_SQS_No_existe_producto
  Esquema del escenario: En la transacción de ajuste, se envía un producto que no existe
    Dado quiero realizar un ajuste delta via SQS para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos que no existen
    Cuando envio un request de ajuste via SQS "<Cola_Standar>"
    Entonces se registra en bd la cabecera con estado "F" mensaje ""
    Y se registra en bd el detalle con estado "F" mensaje "SKU no existe"

    Ejemplos:

      | sistema_origen | cantidad_productos |Cola_Standar|
      | PMM            | 1                  |https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsic-sqs-stock-pmm-qa-pe/send-receive|


  @SDELTA @SDELTA_SQS @SDELTA_SQS_25 @SDELTA_REST_PMM @SDELTA_SQS_Con_stokc_cero @Regresion
  Esquema del escenario: Realizar ajuste delta de un producto con stock 0
    Dado quiero realizar un ajuste delta via SQS para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos con stock cero
    Cuando envio un request de ajuste via SQS "<Cola_Standar>"
    Entonces se registra en bd la cabecera con estado "C" mensaje ""
    Y se validan los datos del delta en bd
    Y se valida en SIC el inventario

    Ejemplos:

      | sistema_origen | cantidad_productos |Cola_Standar|
      | PMM            | 1                  |https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsic-sqs-stock-pmm-qa-pe/send-receive|


  @SDELTA @SDELTA_SQS @SDELTA_SQS_29 @SDELTA_REST_PMM @SDELTA_SQS_Sin_tipo_documento @_I
  Esquema del escenario: En la cabecera del ajuste, no se envía el campo 'Tipo Documento'
    Dado quiero realizar un ajuste delta via SQS para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos sin datos en el campo Documento de Transaccion
    Cuando envio un request de ajuste via SQS "<Cola_Standar>"
    Entonces se registra en bd la cabecera con estado "F" mensaje ""
   # Y se registra en bd el detalle con estado "F" mensaje "No se encontro el campo documento de transaccion"
    Y se registra en bd el detalle con estado "F" mensaje "El campo transaction_doc es obligatorio"

    Ejemplos:

      | sistema_origen | cantidad_productos |Cola_Standar|
      | PMM            | 1                  |https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsic-sqs-stock-pmm-qa-pe/send-receive|


  @SDELTA @SDELTA_SQS @SDELTA_SQS_33 @SDELTA_REST_PMM @SDELTA_SQS_sin_bodega
  Esquema del escenario: En la cabecera del ajuste, no se envía el campo 'Bodega'
    Dado quiero realizar un ajuste delta via SQS para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos sin datos en el campo Bodega
    Cuando envio un request de ajuste via SQS "<Cola_Standar>"
    Entonces se registra en bd la cabecera con estado "F" mensaje ""
    Y se registra en bd el detalle con estado "F" mensaje "El campo fulfillment es obligatorio"

    Ejemplos:

      | sistema_origen | cantidad_productos |Cola_Standar|
      | PMM            | 1                  |https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsic-sqs-stock-pmm-qa-pe/send-receive|

  @SDELTA @SDELTA_SQS @SDELTA_SQS_37 @SDELTA_REST_PMM @SDELTA_SQS_sin_tipo_inventario
  Esquema del escenario: En la cabecera del ajuste, no se envía el campo 'Tipo Inventario'
    Dado quiero realizar un ajuste delta via SQS para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos sin datos en el campo Tipo de Inventario
    Cuando envio un request de ajuste via SQS "<Cola_Standar>"
    Entonces se registra en bd la cabecera con estado "F" mensaje ""
    Y se registra en bd el detalle con estado "F" mensaje "El campo inventoryType es obligatorio"

    Ejemplos:

      | sistema_origen | cantidad_productos |Cola_Standar|
      | PMM            | 1                  |https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsic-sqs-stock-pmm-qa-pe/send-receive|


  @SDELTA @SDELTA_SQS @SDELTA_SQS_41 @SDELTA_REST_PMM @SDELTA_SQS_sin_SKU
  Esquema del escenario: En el ajuste, no se envía el campo 'SKU'
    Dado quiero realizar un ajuste delta via SQS para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos
    Y borro el valor de el campo producto
    Cuando envio un request de ajuste via SQS "<Cola_Standar>"
    Y se registra en bd el detalle con estado "F" mensaje "El campo sku es obligatorio"


    Ejemplos:

      | sistema_origen | cantidad_productos |Cola_Standar|
      | PMM            | 1                  |https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsic-sqs-stock-pmm-qa-pe/send-receive|


  @SDELTA @SDELTA_SQS @SDELTA_SQS_49 @SDELTA_SQS_PMM @SDELTA_SQS_C
  Esquema del escenario: Realizar ajuste delta de 20 productos
    Dado quiero realizar un ajuste delta via SQS para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos
    Cuando envio un request de ajuste via SQS "<Cola_Standar>"
    Entonces se registra en bd la cabecera con estado "C" mensaje ""
    Y se validan los datos del delta en bd

    Ejemplos:

      | sistema_origen | cantidad_productos |Cola_Standar|
      | PMM            | 1                  |https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsic-sqs-stock-pmm-qa-pe/send-receive|


  @SDELTA @SDELTA_SQS @SDELTA_SQS_53  @SDELTA_SQS_estado_CF
  Esquema del escenario: Se envía una transacción y se espera como estado 'Completado con Errores'
    Dado quiero realizar un ajuste delta via SQS para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos con error en un registro
    Cuando envio un request de ajuste via SQS "<Cola_Standar>"
    Entonces se registra en bd la cabecera con estado "CF" mensaje ""

    Ejemplos:

      | sistema_origen | cantidad_productos |Cola_Standar|
      | PMM            | 1                  |https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsic-sqs-stock-pmm-qa-pe/send-receive|

