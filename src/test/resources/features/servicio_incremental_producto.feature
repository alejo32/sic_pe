# language: es
Característica: Incremental de Productos

  @SIC @SIC_INP @SIC_INP_01
  Escenario: Se envia una trama de 90 productos, la información es la misma que la almacenada en BD.
    Dado tengo informacion de jerarquia de "90" productos
    Y preparo la trama de incremental de productos
    Cuando envio un request de incremental de productos via SQS
    Entonces se valida en bd que el incremental de productos no ha realizado cambios

  @SIC @SIC_INP @SIC_INP_02 @Regresion
  Escenario: Se envia una trama de 5 productos, la información es diferente a la almacenada en BD.
    Dado tengo informacion de jerarquia de "5" productos
    Y agrego el sufijo "-AUT" a la descripcion del producto
    Y preparo la trama de incremental de productos
    Cuando envio un request de incremental de productos via SQS
    Entonces se valida en bd los cambios realizados por el incremental del productos

  @SIC @SIC_INP @SIC_INP_03
  Escenario: Se envian 2 tramas de 5 productos, la información es diferente a la almacenada en BD.
    Dado tengo "2" tramas de "5" productos con el sufijo "-AUT" a la descripcion del producto
    Cuando envio varios request de incremental de productos via SQS
    Entonces se valida en bd los cambios realizados por el incremental del productos

  @SIC @SIC_INP @SIC_INP_04
  Escenario: Se envia una trama de 2 productos con el campo source errado
    Dado tengo informacion de jerarquia de "2" productos
    Y agrego el sufijo "-AUT" a la descripcion del producto
    Y preparo la trama de incremental de productos con el campo source erroneo
    Cuando envio un request de incremental de productos via SQS
    Entonces se valida en bd que el incremental de productos no ha realizado cambios

  @SIC @SIC_INP @SIC_INP_05
  Escenario: Se envia una trama de 1 productos con el campo Departamento errado
    Dado tengo informacion de jerarquia de "1" productos
    Y modifico el departamento al valor "F819"
    Y preparo la trama de incremental de productos
    Cuando envio un request de incremental de productos via SQS
    Entonces se valida en bd los cambios realizados por el incremental del productos

  @SIC @SIC_INP @SIC_INP_06
  Escenario: Se envia una trama de 1 productos con el campo ItemId errado
    Dado tengo informacion de jerarquia de "1" productos
    Y modifico el itemId al valor "9991199"
    Y preparo la trama de incremental de productos
    Cuando envio un request de incremental de productos via SQS
    Entonces se valida en bd que el incremental de productos no ha realizado cambios

  @SIC @SIC_INP @SIC_INP_07
  Escenario: Se envia una trama de 1 productos con el campo Style errado
    Dado tengo informacion de jerarquia de "1" productos
    Y modifico el style al valor "9999991999999"
    Y preparo la trama de incremental de productos
    Cuando envio un request de incremental de productos via SQS
    Entonces se valida en bd los cambios realizados por el incremental del productos

  @SIC @SIC_INP @SIC_INP_08
  Escenario: Se envia una trama de 1 productos con el campo SubLinea errado
    Dado tengo informacion de jerarquia de "1" productos
    Y modifico la sublinea al valor "SL99199"
    Y preparo la trama de incremental de productos
    Cuando envio un request de incremental de productos via SQS
    Entonces se valida en bd los cambios realizados por el incremental del productos

  @SIC @SIC_INP @SIC_INP_09
  Escenario: Se envia una trama de 1 productos con el campo Linea errado
    Dado tengo informacion de jerarquia de "1" productos
    Y modifico la linea al valor "9999"
    Y preparo la trama de incremental de productos
    Cuando envio un request de incremental de productos via SQS
    Entonces se valida en bd los cambios realizados por el incremental del productos

  @SIC @SIC_INP @SIC_INP_10 @Regresion
  Escenario: Actualizar la informacion de departamento de 10 productos
    Dado tengo informacion de jerarquia de "10" productos
    Y modifico el departamento al valor "D111"
    Y preparo la trama de incremental de productos
    Cuando envio un request de incremental de productos via SQS
    Entonces se valida en bd los cambios realizados por el incremental del productos

  @SIC @SIC_INP @SIC_INP_11 @Regresion
  Escenario: Actualizar la informacion de linea de 10 productos
    Dado tengo informacion de jerarquia de "10" productos
    Y modifico la linea al valor "000175"
    Y preparo la trama de incremental de productos
    Cuando envio un request de incremental de productos via SQS
    Entonces se valida en bd los cambios realizados por el incremental del productos

  @SIC @SIC_INP @SIC_INP_12 @Regresion
  Escenario: Actualizar la informacion de sublinea de 10 productos
    Dado tengo informacion de jerarquia de "10" productos
    Y modifico la sublinea al valor "S013692"
    Y preparo la trama de incremental de productos
    Cuando envio un request de incremental de productos via SQS
    Entonces se valida en bd los cambios realizados por el incremental del productos
    
