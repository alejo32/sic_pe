# language: es
Característica: Ajuste de inventario vía REST
  Como usuario
  Quiero realizar ajustes de inventario con la finalidad de cubrir todas las casuisticas que puedan existir

  @ADJUST @ADJUST_REST @ADJUST_REST_01 @ADJUST_REST_PMM @ADJUST_REST_C @Regresion
  Esquema del escenario: Realizar ajuste de inventario
    Dado Me conecto  a la BD para realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos con tipo de operacion "<tipo_operacion>"
   # Y valido el stock antes de realizar el ajuste
    Cuando envio un request de ajuste al api
    Entonces se registra en bd la cabecera con estado "C" mensaje ""
    Y se validan los datos del ajuste en bd
    Y se valida en SIC el inventario

    Ejemplos: 
      | sistema_origen | cantidad_productos |tipo_operacion|
      | PMM            |                 2  |A             |
      | PMM            |                 2  |S             |

  @ADJUST @ADJUST_REST @ADJUST_REST_09 @ADJUST_REST_PMM @ADJUST_REST_Negativo
  Esquema del escenario: Realizar ajuste de inventario de un producto con stock 0
    Dado Me conecto  a la BD para realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos con stock 0 con ajuste negativo
    Cuando envio un request de ajuste al api
    Entonces se registra en bd la cabecera con estado "C" mensaje ""
    Y se validan los datos del ajuste en bd
    Y se valida en SIC el inventario

    Ejemplos: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  1 |

  @ADJUST @ADJUST_REST @ADJUST_REST_13 @ADJUST_REST_PMM @ADJUST_REST_C
  Esquema del escenario: Ajuste de 1 registro de producto con la operacion de sustracción.
    Dado Me conecto  a la BD para realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos con ajuste negativo
    Cuando envio un request de ajuste al api
    Entonces se registra en bd la cabecera con estado "C" mensaje ""
    Y se validan los datos del ajuste en bd
    Y se valida en SIC el inventario

    Ejemplos: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  1 |

  @ADJUST @ADJUST_REST @ADJUST_REST_17 @ADJUST_REST_PMM @ADJUST_REST_Sin_stock
  Esquema del escenario: Realizar ajuste de inventario de  productos sin stock en SKU_STOCK_RIPLEY
    Dado Me conecto  a la BD para realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos sin stock para la bodega "20027" y operacion "<tipo_operacion>"
    Cuando envio un request de ajuste al api
    Entonces se registra en bd la cabecera con estado "C" mensaje ""
    Y se validan los datos del ajuste en bd
    Y se valida en SIC el inventario

    Ejemplos: 
      | sistema_origen | cantidad_productos |tipo_operacion|
      | PMM            |                  2 |A             |

  @ADJUST @ADJUST_REST @ADJUST_REST_21 @ADJUST_REST_PMM @ADJUST_REST_operacion_incorrecto
  Esquema del escenario: Realizar ajuste de inventario con tipo de operacion incorrecto
    Dado Me conecto  a la BD para realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos con Operacion "<tipo_operacion>"
    Cuando envio un request de ajuste al api
    Entonces se registra en bd la cabecera con estado "F" mensaje ""
    Y se registra en bd el detalle con estado "F" mensaje "Las operaciones permitidas son A y S"

    Ejemplos: 
      | sistema_origen | cantidad_productos | tipo_operacion |
      | PMM            |                  1 | L              |

  @ADJUST @ADJUST_REST @ADJUST_REST_25 @ADJUST_REST_PMM @ADJUST_REST_No_existe_SKU
  Esquema del escenario: En la transacción de ajuste, se envía un producto que no existe
    Dado Me conecto  a la BD para realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos que no existen
    Cuando envio un request de ajuste al api
    Entonces se registra en bd la cabecera con estado "F" mensaje ""
    Y se registra en bd el detalle con estado "F" mensaje "SKU no existe"

    Ejemplos: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  2 |

  @ADJUST @ADJUST_REST @ADJUST_REST_29 @ADJUST_REST_PMM @ADJUST_REST_Sin_tipo_movimiento
  Esquema del escenario: En la cabecera del ajuste, no se envía el campo 'Tipo de Movimiento'
    Dado Me conecto  a la BD para realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos sin datos en el campo Tipo de Movimiento
    Cuando envio un request de ajuste al api
    Entonces validar que no se registre en la bd

    Ejemplos: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  1 |

  @ADJUST @ADJUST_REST @ADJUST_REST_33 @ADJUST_REST_PMM @ADJUST_REST_sin_usuario
  Esquema del escenario: En la cabecera del ajuste, no se envía el campo 'Usuario'
    Dado Me conecto  a la BD para realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos sin datos en el campo Usuario
    Cuando envio un request de ajuste al api
    Entonces se registra en bd la cabecera con estado "F" mensaje "El campo usuario no puede estar vacio."
    Y no se registra en bd el detalle

    Ejemplos: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  1 |

  @ADJUST @ADJUST_REST @ADJUST_REST_37 @ADJUST_REST_PMM @ADJUST_REST_sin_SistemaOrigen
  Esquema del escenario: En la cabecera del ajuste, no se envía el campo 'Sistema Origen'
    Dado Me conecto  a la BD para realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos sin datos en el campo Sistema Origen
    Cuando envio un request de ajuste al api
    Entonces validar que no se registre en la bd

    Ejemplos: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  1 |

  @ADJUST @ADJUST_REST @ADJUST_REST_41 @ADJUST_REST_PMM @ADJUST_REST_sin_TipoDocumento
  Esquema del escenario: En la cabecera del ajuste, no se envía el campo 'Tipo Documento'
    Dado Me conecto  a la BD para realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos sin datos en el campo Documento de Transaccion
    Cuando envio un request de ajuste al api
    Entonces se registra en bd la cabecera con estado "F" mensaje ""
    Y se registra en bd el detalle con estado "F" mensaje "El campo transaction_doc es obligatorio"

    Ejemplos: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  1 |

  @ADJUST @ADJUST_REST @ADJUST_REST_45 @ADJUST_REST_PMM @ADJUST_REST_sin_idtrx
  Esquema del escenario: En la cabecera del ajuste, no se envía el campo 'IDTRX'
    Dado Me conecto  a la BD para realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos sin datos en el campo IDTRX
    Cuando envio un request de ajuste al api
    Entonces se registra en bd la cabecera con estado "F" mensaje ""
    Y se registra en bd el detalle con estado "F" mensaje "El campo idtrx es obligatorio"

    Ejemplos: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  1 |

  @ADJUST @ADJUST_REST @ADJUST_REST_49 @ADJUST_REST_PMM @ADJUST_REST_sin_bodega
  Esquema del escenario: En la cabecera del ajuste, no se envía el campo 'Bodega'
    Dado Me conecto  a la BD para realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos sin datos en el campo Bodega
    Cuando envio un request de ajuste al api
    Entonces se registra en bd la cabecera con estado "F" mensaje ""
    Y se registra en bd el detalle con estado "F" mensaje "El campo fulfillment es obligatorio"

    Ejemplos: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  1 |


  @ADJUST @ADJUST_REST @ADJUST_REST_53 @ADJUST_REST_PMM @ADJUST_REST_sin_TipoInventario
  Esquema del escenario: PMM - En la cabecera del ajuste, no se envía el campo 'Tipo Inventario'
    Dado Me conecto  a la BD para realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos sin datos en el campo Tipo de Inventario
    Cuando envio un request de ajuste al api
    Entonces se registra en bd la cabecera con estado "F" mensaje ""
    Y se registra en bd el detalle con estado "F" mensaje "El campo inventoryType es obligatorio"

    Ejemplos: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  1 |


  @ADJUST @ADJUST_REST @ADJUST_REST_57 @ADJUST_REST_BGT @ADJUST_REST_sin_Operacion
  Esquema del escenario: PMM - En el detalle del ajuste, no se envía el campo 'Operacion'
    Dado Me conecto  a la BD para realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos sin datos en el campo Operacion
    Cuando envio un request de ajuste al api
    Entonces se registra en bd la cabecera con estado "F" mensaje ""
    Y se registra en bd el detalle con estado "F" mensaje "El campo operation es obligatorio"

    Ejemplos: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  1 |

  @ADJUST @ADJUST_REST @ADJUST_REST_61 @ADJUST_REST_PMM @ADJUST_REST_EstadoDuplicado
  Esquema del escenario: PMM - En la transacción de ajuste, un registro posee el estado Duplicado
    Dado Me conecto  a la BD para realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos con id detalle duplicado
    Cuando envio un request de ajuste al api
    Entonces se registra en bd la cabecera con estado "F" mensaje ""
    Y se registra en bd el detalle con estado "D" mensaje ""

    Ejemplos: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  2 |


  @ADJUST @ADJUST_REST @ADJUST_REST_65 @ADJUST_REST_PMM @ADJUST_REST_ResultadoNegativo
  Esquema del escenario: PMM - Validar ajuste que de como resultado stock negativo
    Dado Me conecto  a la BD para realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos con stock 0 con ajuste negativo
    Cuando envio un request de ajuste al api
    Entonces se registra en bd la cabecera con estado "C" mensaje ""
    Y se validan los datos del ajuste en bd
    Y se valida en SIC el inventario

    Ejemplos: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  1 |


  @ADJUST @ADJUST_REST @ADJUST_REST_69 @ADJUST_REST_PMM @ADJUST_REST_ResultadoCero
  Esquema del escenario: PMM - Validar ajuste de inventario de como resultado 0 de stock
    Dado Me conecto  a la BD para realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos con stock igual al actual con ajuste negativo
    Cuando envio un request de ajuste al api
    Entonces se registra en bd la cabecera con estado "C" mensaje ""
    Y se validan los datos del ajuste en bd
    Y se valida en SIC el inventario

    Ejemplos: 
      | sistema_origen | cantidad_productos |
      | PMM            |                  1 |


  @ADJUST @ADJUST_REST @ADJUST_REST_73 @ADJUST_REST_PMM @ADJUST_REST_BodegaIncorrecta
  Esquema del escenario: PMM - En el detalle del ajuste, se envía una bodega que no existe
    Dado Me conecto  a la BD para realizar un ajuste de inventario via REST para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos
    Y asigno la bodega "<bodega_no_existe>"
    Cuando envio un request de ajuste al api
    Entonces se registra en bd la cabecera con estado "F" mensaje ""
    Y se registra en bd el detalle con estado "F" mensaje "Bodega no existe"

    Ejemplos: 
      | sistema_origen | cantidad_productos | bodega_no_existe |
      | PMM            |                  1 |            29912 |

