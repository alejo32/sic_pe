# language: es

Característica: Como usuario SIC quiero Realizar ajuste de inventario para validar el envio del stock vendible al IMS

  @Yulin
  Esquema del escenario: Realizar ajuste incremental de inventario del stock disponible al producto con atributo de despacho DD y validar el calculo del stock vendible enviado a IMS
    Dado ingreso al sistema sic con usuario administrador Chile
    Y que el metodo de despacho es DD para el producto"<codigo_venta>" en la bodega "<codigo_bodega>"
    Y que el producto"<codigo_venta>" exista en la tabla del whitelist en la bodega "<codigo_bodega>" 
    Y que el producto"<codigo_venta>" exista en la tabla de publicados 
    Y que el producto"<codigo_venta>" en la bodega "<codigo_bodega>" tenga valor umbral "<Umbral>" en la tabla de stock ripley
    Y voy a la opcion Mantenedor para Ingresar Ajuste de Inventario
    Y realizo la consulta de stock disponible "<tipo_inventario>" actual del producto "<codigo_venta>" en la bodega "<codigo_bodega>"
    Y realizo ajuste ingresando el stock a ajustar "<stock_a_ajustar>" en la bodega "<codigo_bodega>"
    Y obtener informacion del restos de bodegas para el producto"<codigo_venta>"
    Cuando ingreso al sistema IMS
    Y voy a la opcion sku_controller
    Y ingresar el producto"<codigo_venta>" y le doy click en el boton TryItOut para enviar las credenciales de IMS
    Y Valida que el  stock vendible de sic se haya informado a IMS

    Ejemplos: 
      | codigo_bodega | codigo_venta  | tipo_inventario | tipo_actualizacion | stock_a_ajustar |Umbral|
      |         10095 | 2000301920012 |               1 | Incrementar        |               1 |10000 |

  @calculo_stock_vemdible_IMS_CL_CP01 @RegresionIMS
  Esquema del escenario: Realizar ajuste incremental de inventario del stock disponible y validar el calculo del stock vendible enviado a IMS
    Dado ingreso al sistema sic con usuario administrador Chile
    Y voy a la opcion Mantenedor para Ingresar Ajuste de Inventario
    Y realizo la consulta de stock disponible "<tipo_inventario>" actual del producto "<codigo_venta>" en la bodega "<codigo_bodega>"
    Y realizo ajuste ingresando el stock a ajustar "<stock_a_ajustar>" en la bodega "<codigo_bodega>"
    Y voy a la opcion Consulta de Stock por Jerarquia de Productos
    Y realizo la consulta de stock vendible por jerarquia en SIC para el producto "<codigo_venta>"
    Y obtener informacion del restos de bodegas para el producto"<codigo_venta>"
    Cuando ingreso al sistema IMS
    Y voy a la opcion sku_controller
    Y ingresar el producto"<codigo_venta>" y le doy click en el boton TryItOut para enviar las credenciales de IMS
    Y Valida que el  stock vendible de sic se haya informado a IMS

    Ejemplos: 
      | codigo_bodega | codigo_venta  | tipo_inventario | tipo_actualizacion | stock_a_ajustar |
      |         10095 | 2000301920012 |               1 | Incrementar        |               3 |
	  	
   @calculo_stock_vemdible_IMS_CL_CP02 @RegresionIMS
  Esquema del escenario: Realizar ajuste incremental de inventario del stock disponible y validar el calculo del stock vendible enviado a IMS
    Dado ingreso al sistema sic con usuario administrador Chile
    Y voy a la opcion Mantenedor para Ingresar Ajuste de Inventario
    Y realizo la consulta de stock disponible "<tipo_inventario>" actual del producto "<codigo_venta>" en la bodega "<codigo_bodega>"
    Y realizo ajuste ingresando el stock a ajustar "<stock_a_ajustar>" en la bodega "<codigo_bodega>"
    Y voy a la opcion Consulta de Stock por Jerarquia de Productos
    Y realizo la consulta de stock vendible por jerarquia en SIC para el producto "<codigo_venta>"
    Y obtener informacion del restos de bodegas para el producto"<codigo_venta>"
    Cuando ingreso al sistema IMS
    Y voy a la opcion sku_controller
    Y ingresar el producto"<codigo_venta>" y le doy click en el boton TryItOut para enviar las credenciales de IMS
    Y Valida que el  stock vendible de sic se haya informado a IMS

    Ejemplos: 
      | codigo_bodega | codigo_venta  | tipo_inventario | tipo_actualizacion | stock_a_ajustar |
      |         10000 | 2064060970008 |               1 | Incrementar        |               3 |
      |         10000 | 2064060970008 |               1 | Incrementar        |               4 |
      |         10000 | 2064060970008 |               1 | Disminuir          |               2 |
      |         10000 | 2064060970008 |               1 | Disminuir          |               1 |
      |         10000 | 2064060970008 |               1 | Reemplazar         |             200 |
      
        @calculo_stock_vemdible_IMS_CL_CP03 @RegresionIMS
  Esquema del escenario: Realizar ajuste incremental de inventario del stock disponible y validar el calculo del stock vendible enviado a IMS
    Dado ingreso al sistema sic con usuario administrador Chile
    Y voy a la opcion Mantenedor para Ingresar Ajuste de Inventario
    Y realizo la consulta de stock disponible "<tipo_inventario>" actual del producto "<codigo_venta>" en la bodega "<codigo_bodega>"
    Y realizo ajuste ingresando el stock a ajustar "<stock_a_ajustar>"
    Y voy a la opcion Consulta de Stock por Jerarquia de Productos
    Y realizo la consulta de stock vendible por jerarquia en SIC para el producto "<codigo_venta>"
    Y obtener informacion del restos de bodegas para el producto"<codigo_venta>"
    Cuando ingreso al sistema IMS
    Y voy a la opcion sku_controller
    Y ingresar el producto"<codigo_venta>" y le doy click en el boton TryItOut para enviar las credenciales de IMS
    Y Valida que el  stock vendible de sic se haya informado a IMS

    Ejemplos: 
      | codigo_bodega | codigo_venta  | tipo_inventario | tipo_actualizacion | stock_a_ajustar |
	  	|         10003 | 2047240150001 |               1 | Incrementar        |               3 |
	 		|         10003 | 2047240150001 |               1 | Disminuir          |               1 |
	 	  |         10003 | 2047240150001 |               1 | Reemplazar         |             300 |
      
        @calculo_stock_vemdible_IMS_CL_CP04 @RegresionIMS
  Esquema del escenario: Realizar ajuste incremental de inventario del stock disponible y validar el calculo del stock vendible enviado a IMS
    Dado ingreso al sistema sic con usuario administrador Chile
    Y voy a la opcion Mantenedor para Ingresar Ajuste de Inventario
    Y realizo la consulta de stock disponible "<tipo_inventario>" actual del producto "<codigo_venta>" en la bodega "<codigo_bodega>"
    Y realizo ajuste ingresando el stock a ajustar "<stock_a_ajustar>"
    Y voy a la opcion Consulta de Stock por Jerarquia de Productos
    Y realizo la consulta de stock vendible por jerarquia en SIC para el producto "<codigo_venta>"
    Y obtener informacion del restos de bodegas para el producto"<codigo_venta>"
    Cuando ingreso al sistema IMS
    Y voy a la opcion sku_controller
    Y ingresar el producto"<codigo_venta>" y le doy click en el boton TryItOut para enviar las credenciales de IMS
    Y Valida que el  stock vendible de sic se haya informado a IMS

    Ejemplos: 
      | codigo_bodega | codigo_venta  | tipo_inventario | tipo_actualizacion | stock_a_ajustar |
	  	|         10029 | 2000303481559 |               1 | Incrementar        |               3 |
	  	|         10029 | 2000303481559 |               1 | Disminuir          |               1 |
	    |         10029 | 2000303481559 |               1 | Reemplazar         |             400 |
      
        @calculo_stock_vemdible_IMS_CL_CP05 @RegresionIMS
  Esquema del escenario: Realizar ajuste incremental de inventario del stock disponible y validar el calculo del stock vendible enviado a IMS
    Dado ingreso al sistema sic con usuario administrador Chile
    Y voy a la opcion Mantenedor para Ingresar Ajuste de Inventario
    Y realizo la consulta de stock disponible "<tipo_inventario>" actual del producto "<codigo_venta>" en la bodega "<codigo_bodega>"
    Y realizo ajuste ingresando el stock a ajustar "<stock_a_ajustar>"
    Y voy a la opcion Consulta de Stock por Jerarquia de Productos
    Y realizo la consulta de stock vendible por jerarquia en SIC para el producto "<codigo_venta>"
    Y obtener informacion del restos de bodegas para el producto"<codigo_venta>"
    Cuando ingreso al sistema IMS
    Y voy a la opcion sku_controller
    Y ingresar el producto"<codigo_venta>" y le doy click en el boton TryItOut para enviar las credenciales de IMS
    Y Valida que el  stock vendible de sic se haya informado a IMS

    Ejemplos: 
      | codigo_bodega | codigo_venta  | tipo_inventario | tipo_actualizacion | stock_a_ajustar |
	  	|         10095 | 2000302032196 |               1 | Incrementar        |               3 |
    	|         10095 | 2000302032196 |               1 | Disminuir          |               1 |   
      |         10095 | 2000302032196 |               1 | Reemplazar         |             500 |

     @calculo_IMS_CL
  Esquema del escenario: Realizar ajuste incremental de inventario del stock disponible y validar el calculo del stock vendible enviado a IMS
    Cuando ingreso al sistema IMS
    Y ingresar el producto"<codigo_venta>" y le doy click en el boton TryItOut para enviar las credenciales de IMS
    Y Valida que el  stock vendible de sic se haya informado a IMS



    Ejemplos: 
      | codigo_bodega | codigo_venta  | tipo_inventario | tipo_actualizacion | stock_a_ajustar | response_code |
      |         10000 | 2064060970008 |               1 | Incrementar        |               3 |           200 |