# language: es
Característica: SIC - Auditoria de Transacciones de Inventario
  Como usuario
  Quiero realizar consultas en la pantalla de Auditoria de Transacciones de invenario y validar el resultado con  base de datos

  Antecedentes: 
    Dado ingreso al sistema sic
    Y voy a la opcion Auditoria de Transacciones de Inventario

  @SIC_ATI @SIC_ATI_B2B_01
  Escenario: SIC - Realizar consulta del ajuste delta que se realizo desde B2B hacia SIC via archivo
    Dado tengo el arhivo de carga leo informacion del archivo de carga
    Cuando realizo la consulta de auditoria de la transacion con los filtros obligatorios
    Entonces valido el Stock enviado en el campo stock disponible de SIC

  @SIC @SIC_ATI @SIC_ATI_01
  Esquema del escenario: SIC - Realizar consulta utilizando todos los filtros para sku tipo inventario Stock Disponible
    Dado tengo una transaccion de un producto con tipo de inventario "1"
    Cuando realizo la consulta de auditoria
    Entonces se validan en bd los datos mostrados en la pantalla de auditoria

    Ejemplos: 
      | tipo_inventario |
      |               1 |

  @SIC @SIC_ATI @SIC_ATI_04 @Regresion
  Escenario: SIC - Realizar consulta utilizando todos los filtros por defecto
    Dado tengo una transaccion de un producto con tipo de inventario "1"
    Cuando realizo la consulta de auditoria solo con los filtros obligatorios
    Entonces se validan en bd los datos mostrados en la pantalla de auditoria

  @SIC @SIC_ATI @SIC_ATI_05 @Regresion
  Escenario: SIC - Realizar consulta sin ingresar codigo de venta
    Dado tengo una transaccion de un producto con tipo de inventario "1"
    Cuando realizo la consulta de auditoria sin ingresar codigo de venta
    Entonces muestra mensaje de error "El campo COD DE VENTA es obligatorio"

  @SIC @SIC_ATI @SIC_ATI_06 @Regresion
  Escenario: SIC - Realizar consulta sin ingresar la fecha desde
    Dado tengo una transaccion de un producto con tipo de inventario "1"
    Cuando realizo la consulta de auditoria sin ingresar fecha desde
    Entonces muestra mensaje de error "El campo Desde es obligatorio"

  @SIC @SIC_ATI @SIC_ATI_07 @Regresion
  Escenario: SIC - Realizar consulta sin ingresar la fecha hasta
    Dado tengo una transaccion de un producto con tipo de inventario "1"
    Cuando realizo la consulta de auditoria sin ingresar fecha hasta
    Entonces muestra mensaje de error "El campo Hasta es obligatorio"

  @SIC @SIC_ATI @SIC_ATI_08 @Regresion
  Escenario: SIC - Realizar consulta y limpiar resultados
    Dado tengo una transaccion de un producto con tipo de inventario "1"
    Cuando realizo la consulta de auditoria
    Y doy clic al boton Limpiar
    Entonces se limpian todos los filtros utilizados

  @SIC @SIC_ATI @SIC_ATI_09 @Regresion
  Esquema del escenario: SIC - Validar que se visualice las transacciones y ajustes de inventario enviadas de diferentes sistemas hacia SIC
    Dado tengo una transaccion de un producto con sistema origen "<sistema_origen>"
    Cuando realizo la consulta de auditoria
    Entonces se validan en bd los datos mostrados en la pantalla de auditoria

    Ejemplos: 
      | sistema_origen |
      | PMM            |
      | B2B            |
      | BO             |
      | BGT            |

  @SIC @SIC_ATI @SIC_ATI_13 @Regresion
  Escenario: SIC - Validar que se visualice el botón Exporta y se genere archivo en formato CSV Zipeado
    Dado tengo una transaccion de un producto con tipo de inventario "1"
    Cuando realizo la consulta de auditoria
    Y doy clic al boton Exportar
    Entonces validar la descarga del archivo

  @SIC @SIC_ATI @SIC_ATI_14 @Regresion
  Escenario: Validar el tipo de producto por defecto sea '1-Ripley'
    Entonces se valida que tenga por defecto el tipo de producto "1-Ripley"

  @SIC @SIC_ATI @SIC_ATI_15
  Escenario: SIC - Realizar una consulta, limpiar consulta y hacer click en exportar
    Dado tengo una transaccion de un producto con tipo de inventario "1"
    Cuando realizo la consulta de auditoria
    Y doy click en limpiar el resultado de auditoria
    Y doy clic al boton Exportar
    Entonces muestra mensaje de error "El campo COD DE VENTA es obligatorio"

  @SIC @SIC_ATI @SIC_ATI_16 @Regresion
  Escenario: SIC - Validar mensaje de error al consultar un rango de fechas mayor a 7 dias
    Cuando ingreso codigo de venta "2000000000001"
    Y selecciono rango de fechas mayor a siete dias
    Y doy click en consultar auditoria
    Entonces muestra mensaje de error "El rango de fechas, en la consulta, no puede ser mayor a 7 días."

  @SIC @SIC_ATI @SIC_ATI_17 @Regresion @Mal_Implementado
  Escenario: SIC - Realizar consulta utilizando todos los filtros para sku tipo inventario Stock Protegido
    Dado tengo una transaccion de un producto con tipo de inventario "3"
    Cuando realizo la consulta de auditoria
    Entonces se validan en bd los datos mostrados en la pantalla de auditoria

  @Pendiente
  Escenario: SIC - Validar la paginacion
    Dado tengo una transaccion de un producto con tipo de inventario "1"
    Cuando realizo la consulta de auditoria
    Entonces validar la cantidad de registros por página
    Y validar el total de registros mostrados
    Y validar el campo cantidad de registros

  @SIC @SIC_ATI @SIC_ATI_18 @Regresion
  Escenario: SIC - Realizar consulta utilizando todos los filtros para sku tipo inventario 
    Dado tengo una transaccion de un producto con tipo de inventario
      | tipo_inventario |
      |               1 |
      |               2 |
    Cuando realizo la consulta de auditoria solo con los filtros obligatorios
    Entonces se validan en bd los datos mostrados por tipo de inventario
    
  

    
 # @SIC @SIC_ATI @SIC_ATI_19
 # Esquema: SIC - Validar que se visualicen las transacciones y actualizaciones de stock enviado de B2B hacia SIC
 #  Dado tengo una transaccion de un producto con sistema origen "B2B"
 # Cuando realizo la consulta de auditoria
 # Entonces se validan en bd los datos mostrados en la pantalla de auditoria
    
    
    
    
    
