# language: es
Característica: Mantenedor para ingresar ajuste de inventario
  Como [Actor]
  Quiero [que va a hacer]
  Para [para que lo va a hacer]

  Antecedentes: 
    Dado ingreso al sistema sic
    Y voy a la opcion Mantenedor para Ingresar Ajuste de Inventario

  @SIC @SIC_MIAI @SIC_MIAI_01 @Regresion
  Esquema del escenario: Realizar consulta para un producto con los diferentes tipos de stock
    Dado tengo informacion de un producto con tipo de inventario "<tipo_inventario>"
    Cuando realizo la consulta de stock actual
    Entonces se muestra el stock actual

    Ejemplos: 
      | tipo_inventario |
      |               1 |
      |               2 |

  @SIC @SIC_MIAI @SIC_MIAI_03 @Regresion
  Esquema del escenario: Validar campo nuevo stock al seleccionar los diferentes tipos de actualizacion de stock disponible
    Dado tengo informacion de un producto con tipo de inventario "<tipo_inventario>"
    Y realizo la consulta de stock actual
    Cuando selecciono tipo de actualizacion "<tipo_actualizacion>"
    Y ingreso el stock a ajustar "<stock_a_ajustar>"
    Y ingreso la observacion "<Observacion>"
    Entonces se actualiza el valor del campo nuevo stock
    Y realizo click en Actualizar

    Ejemplos: 
      | tipo_inventario | tipo_actualizacion | stock_a_ajustar |Observacion|
      |               1 | Incrementar        |              15 |Automatizado-Mantenedor para ingresar ajuste de stock |
      |               1 | Disminuir          |              12 |Automatizado-Mantenedor para ingresar ajuste de stock |
      |               1 | Reemplazar         |             100 |Automatizado-Mantenedor para ingresar ajuste de stock |

  @SIC @SIC_MIAI @SIC_MIAI_06 @Regresion
  Escenario: Validar el combo Bodegas exista y sea obligatorio
    Cuando ingreso codigo venta "2000000000001"
    Y selecciono tipo de inventario "1"
    Y doy click a consultar
    Entonces se muestra mensaje flotante "Debe seleccionar una bodega para continuar"

  @SIC @SIC_MIAI @SIC_MIAI_07 @Regresion
  Escenario: Validar el campo codigo venta sea obligatorio
    Cuando selecciono tipo de inventario "1"
    Y selecciono una bodega al azar
    Y doy click a consultar
    Entonces se muestra mensaje "Complete todos los campos requeridos (*)"

  @SIC @SIC_MIAI @SIC_MIAI_08 @Regresion
  Escenario: Validar existencia de registro de stock del producto en SIC
    Cuando tengo informacion de un producto sin stock
    Y selecciono una bodega al azar
    Y realizo la consulta de stock actual
    Entonces se muestra mensaje "No se encontró stock para el cod de venta:"

  @SIC @SIC_MIAI @SIC_MIAI_09 @Regresion
  Escenario: Validar el campo Ajuste exista y sea obligatorio
    Cuando ingreso codigo venta "2000000000001"
    Y selecciono tipo de inventario "1"
    Y selecciono una bodega al azar
    Y completo el campo observacion
    Y no completo el campo ajuste
    Entonces el boton actualizar esta deshabilitado

  @SIC @SIC_MIAI @SIC_MIAI_11 @Regresion
  Escenario: Realizar incremento de stock de un producto con stock negativo
    Dado tengo informacion de un producto con stock negativo
    Y realizo la consulta de stock actual
    Cuando selecciono tipo de actualizacion "Incrementar"
    Y ingreso el stock a ajustar "15"
    Entonces se actualiza el valor del campo nuevo stock

  @SIC @SIC_MIAI @SIC_MIAI_12 @Regresion
  Escenario: Realizar incremento de stock de un producto con stock cero
    Dado tengo informacion de un producto con stock cero
    Y realizo la consulta de stock actual
    Cuando selecciono tipo de actualizacion "Incrementar"
    Y ingreso el stock a ajustar "15"
    Entonces se actualiza el valor del campo nuevo stock

  @SIC @SIC_MIAI @SIC_MIAI_13 @Regresion
  Escenario: Realizar reemplazo de stock de un producto con stock positivo
    Dado tengo informacion de un producto con stock positivo
    Y realizo la consulta de stock actual
    Cuando selecciono tipo de actualizacion "Reemplazar"
    Y ingreso el stock a ajustar "60"
    Entonces se actualiza el valor del campo nuevo stock

  @SIC @SIC_MIAI @SIC_MIAI_14 @Regresion
  Escenario: Validar el boton Limpiar
    Dado tengo informacion de un producto con stock positivo
    Y realizo la consulta de stock actual
    Y selecciono tipo de actualizacion "Reemplazar"
    Y ingreso el stock a ajustar "60"
    Cuando doy click a limpiar
    Entonces los campos vuelven a su valor inicial
