# language: es

Característica: B2B - Actualizar Stock tipo: único, excepción y diario vía carga de archivo y formulario
  	El usuario debe poder ingresar productos que han cambiado de stock vía archivo tipo XLS.
  	El sistema debe realizar la carga de datos y mostrar como resultado que el proceso se ejecutó correctamente.
  @B2B_Archivo_unico_opcion_mantenedor
  Esquema del escenario: B2B - Modificación de stock único mostrar carga de datos
    Dado ingreso a B2B con perfil Ripley con proveedor "<proveedor>"
    Y ingreso a la funcionalidad Mantencion de stock comprometido via archivo
    Cuando cargo un archivo XLS con productos que han cambiado de stock "<ruta>"
    Y doy click en enviar
    Y muestra resultado "El proceso se ejecuto correctamente"
    Y obtener data de carga
    Cuando ingreso al sistema sic con perfil administrador
    Y voy a la opcion Consulta de Stock por Jerarquia de Productos
    Y realizo la consulta de stock por jerarquia en SIC
    Y voy a la opcion Auditoria de Transacciones de Inventario
    Cuando realizo la consulta de auditoria de la transacion en SIC
    Entonces comparar stock base de B2B con stock disponible de SIC

    Ejemplos:
      |proveedor      |ruta |
      |ROSEN PERU S.A.|C:\\Automatizacion\\Inputs\\B2BCargaArchivo\\Actualizacion Stock Unico.xls|

  @B2B_Archivo_unico @regresionb2b
  Esquema del escenario: B2B - Modificación de stock único, excepción y diario vía archivo y mostrar carga de datos
  Dado ingreso a B2B con perfil Ripley con proveedor "<proveedor>"
  Y ingreso a la funcionalidad Modificacion de stock comprometido via archivo
  Cuando cargo un archivo XLS con productos que han cambiado de stock "<ruta>"
  Y doy click en enviar
  Y muestra resultado "El proceso se ejecuto correctamente"
  Y obtener data de carga
  Cuando ingreso al sistema sic con perfil administrador
  Y voy a la opcion Consulta de Stock por Jerarquia de Productos
  Y realizo la consulta de stock por jerarquia en SIC
  Y voy a la opcion Auditoria de Transacciones de Inventario
  Cuando realizo la consulta de auditoria de la transacion en SIC
  Entonces comparar stock base de B2B con stock disponible de SIC
  
  Ejemplos: 
  |proveedor      |ruta |
  |ROSEN PERU S.A.|C:\\Automatizacion\\Inputs\\B2BCargaArchivo\\Actualizacion Stock Unico.xls|
# |ROSEN PERU S.A.|C:\\Automatizacion\\Inputs\\B2BCargaArchivo\\Update Stock Unico.xlsx    |

#  |ROSEN PERU S.A.|C:\\Automatizacion\\Inputs\\B2BCargaArchivo\\Update Stock Excepcion.xlsx|
#  |ROSEN PERU S.A.|C:\\Automatizacion\\Inputs\\B2BCargaArchivo\\Update Stock Diario.xlsx   |


  
  @B2B_STK_UNICO_FORM @B2B_FORM @regresionb2b
  Esquema del escenario: B2B - Modificación de stock único o base vía formulario
  Dado ingreso a B2B con perfil Ripley con proveedor "<proveedor>"
  Y ingreso a la funcionalidad Modificacion de stock comprometido
  Cuando busco un sku unico "<sku>"
  Y ingreso una nueva cantidad base comprometida "<cantidad>"
  Y graba y muestra resultado "Grabacion Exitosa"
  Cuando ingreso al sistema sic
  Y voy a la opcion Auditoria de Transacciones de Inventario
  Cuando realizo la consulta de auditoria de la transacion "<codigo_venta>" y "<codigo_bodega>"
  Entonces compara stock base "<cantidad>" con stock disponible de SIC
  
  Ejemplos: 
  |codigo_bodega|proveedor      | sku	    | codigo_venta   | cantidad |																									     
  |2014         |MABE PERU SA   | 15561268  | 2003155612683  | 100      |

@B2B_STK_DIARIO_FORM @B2B_FORM @regresionb2b
  Esquema del escenario: B2B - Modificación de stock único vía formulario 
  Dado ingreso a B2B con perfil Ripley con proveedor "<proveedor>"
  Y ingreso a la funcionalidad Modificacion de stock comprometido
  Cuando busco un sku unico "<sku>"
  Y ingreso una nueva cantidad base comprometida "<cantidad>"
  Y graba y muestra resultado "Grabacion Exitosa"
  Cuando ingreso al sistema sic
  Y voy a la opcion Auditoria de Transacciones de Inventario
  Cuando realizo la consulta de auditoria de la transacion "<codigo_venta>" y "<codigo_bodega>"
  Entonces compara stock base "<cantidad>" con stock disponible de SIC
  
  Ejemplos: 
  |codigo_bodega|proveedor      | sku	    | codigo_venta   | cantidad |																									     
  |2080         |ROSEN PERU S.A.| 4701085 | 2047010850001  | 200      |
  
  
@B2B_STK_EXCEPCION_FORM @B2B_FORM @regresionb2b
  Esquema del escenario: B2B - Modificación de stock único vía formulario 
  Dado ingreso a B2B con perfil Ripley con proveedor "<proveedor>"
  Y ingreso a la funcionalidad Modificacion de stock comprometido
  Cuando busco un sku excepcion "<sku>" en la bodega "<codigo_bodega>"
  Y ingreso una nueva cantidad base comprometida "<StockBase>" y excepcion "<StockExcepcion>"
  Y graba y muestra resultado "Grabacion Exitosa"
  Cuando ingreso al sistema sic
  Y voy a la opcion Auditoria de Transacciones de Inventario
  Cuando realizo la consulta de auditoria de la transacion "<codigo_venta>" y "<codigo_bodega>"
  Entonces compara stock base "<StockExcepcion>" con stock disponible de SIC
  
    Ejemplos: 
  |codigo_bodega|proveedor      | sku	    | codigo_venta   | StockBase |StockExcepcion	|																								     
  |2084         |MABE PERU SA   |15561268   | 2003155612683  | 240       |45	     		|
  