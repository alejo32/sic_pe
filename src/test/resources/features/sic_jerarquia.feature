# language: es
Característica: SIC - Consulta de Stock por Jerarquia de Productos
  Como usuario
  Quiero realizar consultas de stock por jerarquia a traves del sistema SIC

  Antecedentes: 
    Dado ingreso al sistema sic
    Y voy a la opcion Consulta de Stock por Jerarquia de Productos

  @SIC @SIC_CSJ @SIC_CSJ_01 @Regresion
  Escenario: SIC - Validar que exista el filtro Tipo de Producto en la consulta y este seleccionado por defecto '1-Ripley'
    Entonces se valida que tenga por defecto el valor "1-Ripley"

  @SIC @SIC_CSJ @SIC_CSJ_02 @Regresion
  Escenario: SIC - Validar que exista el filtro Departamento en la consulta y sea obligatorio
    Cuando no selecciono un departamento
    Entonces el boton consultar esta deshabilitado

  @SIC @SIC_CSJ @SIC_CSJ_03 @Regresion
  Escenario: SIC - Validar que muestre las lineas correspondientes al departamento escogido
    Cuando selecciono el departamento "D101 - Acc. Mujer"
    Entonces se muestra la linea "001038 - ACC. TEXTIL"

  @SIC @SIC_CSJ @SIC_CSJ_04 @Regresion
  Escenario: SIC - Validar que muestre las lineas correspondientes a los multiples departamentos escogidos
    Cuando selecciono el departamento "D101 - Acc. Mujer"
    Y selecciono el departamento "D103 - Audio"
    Entonces se muestra la linea "000283 - AUDIO HI-FI"
    Y se muestra la linea "001038 - ACC. TEXTIL"

  @SIC @SIC_CSJ @SIC_CSJ_05 @Regresion
  Escenario: SIC - Validar que muestre las sublíneas correspondientes a los departamentos y lineas escogidos
    Cuando selecciono el departamento "D101 - Acc. Mujer"
    Y selecciono el departamento "D103 - Audio"
    Y selecciono la linea "000283 - AUDIO HI-FI"
    Y selecciono la linea "001038 - ACC. TEXTIL"
    Entonces se muestra la sublinea "S002423 - GORROS"
    Y se muestra la sublinea "S000681 - PARLANTES"

  @SIC @SIC_CSJ @SIC_CSJ_06 @Regresion
  Escenario: SIC - Validar que exista el filtro SKU en la consulta y que sea obligatorio
    Cuando no ingreso un sku
    Entonces el boton consultar esta deshabilitado

  @SIC @SIC_CSJ @SIC_CSJ_07 @Regresion
  Escenario: SIC - Validar que exista el filtro Tipo de Inventario en la consulta
    Cuando doy click al combo tipo de inventario
    Entonces se verifica se muestren todos los tipos de inventario

  @SIC @SIC_CSJ @SIC_CSJ_08 @Regresion
  Escenario: SIC - Validar que exista el filtro Tipo de Producto en la consulta y este seleccionado por defecto 'Stock mayor a:'
    Entonces se valida que el rango de stock tenga por defecto el valor "Stock mayor a:"

  @SIC @SIC_CSJ @SIC_CSJ_09 @Regresion
  Escenario: SIC - Validar que existan todas las opciones de rango de stock
    Cuando doy click al combo rango de stock
    Entonces se verifica que se muestren las opciones de rango de stock
      | Stock mayor a:         |
      | Stock menor a:         |
      | Stock mayor o igual a: |
      | Stock menor o igual a: |
      | Stock igual a:         |
      | Stock distinto a:      |

  @SIC @SIC_CSJ @SIC_CSJ_10 @Regresion
  Escenario: SIC - Validar todos los campos del resultado de la consulta en la grilla con stock mayor a 0
    Dado tengo informacion de stock por jerarquia de un producto con stock mayor a cero
    Cuando selecciono la bodega donde el producto tiene stock
    Y realizo la consulta
    Entonces se verifica el resultado mostrado con la base de datos

  @SIC @SIC_CSJ @SIC_CSJ_11 @Regresion
  Escenario: SIC - Validar todos los campos del resultado de la consulta en la grilla con stock menor a 0
    Dado tengo informacion de stock por jerarquia de un producto con stock menor a cero
    Cuando selecciono la bodega donde el producto tiene stock
    Y realizo la consulta
    Entonces se verifica el resultado mostrado con la base de datos

  @SIC @SIC_CSJ @SIC_CSJ_12 @Regresion
  Escenario: SIC - Validar todos los campos del resultado de la consulta en la grilla con stock igual a 0
    Dado tengo informacion de stock por jerarquia de un producto con stock igual a cero
    Cuando selecciono la bodega donde el producto tiene stock
    Y realizo la consulta
    Entonces se verifica el resultado mostrado con la base de datos

  @SIC @SIC_CSJ @SIC_CSJ_13 @Regresion
  Escenario: SIC - Validar todos los campos del resultado de la consulta en la grilla con stock mayor o igual a 10
    Dado tengo informacion de stock por jerarquia de un producto con stock mayor o igual a "10"
    Cuando selecciono la bodega donde el producto tiene stock
    Y realizo la consulta con el filtro mayor igual a
    Entonces se verifica el resultado mostrado con la base de datos

  @SIC @SIC_CSJ @SIC_CSJ_14 @Regresion
  Escenario: SIC - Validar todos los campos del resultado de la consulta en la grilla con stock menor o igual a 20
    Dado tengo informacion de stock por jerarquia de un producto con stock menor o igual a "20"
    Cuando selecciono la bodega donde el producto tiene stock
    Y realizo la consulta con el filtro menor igual a
    Entonces se verifica el resultado mostrado con la base de datos

  @SIC @SIC_CSJ @SIC_CSJ_15 @Regresion
  Escenario: SIC - Validar todos los campos del resultado de la consulta en la grilla con stock diferente a 10
    Dado tengo informacion de stock por jerarquia de un producto con stock diferente a "10"
    Cuando selecciono la bodega donde el producto tiene stock
    Y realizo la consulta con el filtro distinto a
    Entonces se verifica el resultado mostrado con la base de datos

  @SIC @SIC_CSJ @SIC_CSJ_16
  Escenario: SIC - Validar consulta de stock para un SKU
    Dado tengo informacion de stock por jerarquia de un producto con stock igual a cero
    Cuando selecciono la bodega donde el producto tiene stock
    Y realizo la consulta con filtro tipo inventario
    Entonces se verifica el resultado mostrado con la base de datos

  @SIC @SIC_CSJ @SIC_CSJ_17 @Regresion
  Esquema del escenario: SIC -
    Dado tengo informacion de stock por jerarquia de un producto con tipo de inventario "<tipo_inventario>"
    Cuando selecciono la bodega donde el producto tiene stock
    Y realizo la consulta con filtro tipo inventario
    Entonces se verifica el resultado mostrado con la base de datos

    Ejemplos: 
      | tipo_inventario |
      |               1 |
      |               2 |
