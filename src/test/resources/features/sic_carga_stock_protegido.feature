# language: es
Característica: Carga Masiva de Stock Protegido

  Antecedentes: 
    Dado ingreso al sistema sic
    Y voy a la opcion Carga Masiva de Stock Protegido

  @SIC @SIC_CMSP @SIC_CMSP_01
  Escenario: Realizar carga sin haber seleccionado un archivo
    Cuando doy click a realizar carga
    Entonces se valida mensaje de error "El formato a cargar debe ser csv."

  @SIC @SIC_CMSP @SIC_CMSP_02 @Regresion
  Escenario: Validar que se muestre el nombre del archivo cargado en el campo Archivo
    Dado tengo informacion de stock protegido
      | fec_ini             | fec_fin             | cod_venta     | bodega | nivel_1 | nivel_2 | nivel_3 | nivel_4 | nivel_5 |
      | 2020-02-04 10:00:00 | 2020-02-06 10:00:00 | 2013500230004 |  20027 |      10 |      20 |      30 |      40 |      50 |
    Y genero un archivo de carga masiva de stock protegido
    Cuando cargo el archivo de carga masiva de stock protegido
    Entonces se valida que se visualice el nombre del archivo seleccionado

  @SIC @SIC_CMSP @SIC_CMSP_03
  Esquema del escenario: Realizar una carga masiva con un archivo con extension no permitida
    Dado tengo informacion de stock protegido
      | fec_ini             | fec_fin             | cod_venta     | bodega | nivel_1 | nivel_2 | nivel_3 | nivel_4 | nivel_5 |
      | 2020-02-04 10:00:00 | 2020-02-06 10:00:00 | 2013500230004 |  20027 |      10 |      20 |      30 |      40 |      50 |
    Y genero un archivo de carga masiva de stock protegido con extension "<extension>"
    Y cargo el archivo de carga masiva de stock protegido
    Y doy click a realizar carga
    Entonces se valida mensaje de error "El formato a cargar debe ser csv."

    Ejemplos: 
      | extension |
      | .txt      |
      | .zip      |
      | .tar.gz   |
      | .xlsx     |
      | .tar      |

  @SIC @SIC_CMSP @SIC_CMSP_04 @Regresion
  Escenario: Validar que en el formulario el campo tipo de producto tenga el valor por defecto '1-Ripley'
    Entonces se valida que por defecto tenga el valor "1-Ripley"

  @SIC @SIC_CMSP @SIC_CMSP_05
  Escenario: Realizar una carga con un archivo vacio
    Dado genero un archivo de carga masiva de stock protegido vacio
    Cuando selecciono la bodega "20027"
    Y cargo el archivo de carga masiva de stock protegido
    Y doy click a realizar carga
    Entonces se muestra en la cabecera el detalle "Archivo sin ningún registro."

  @SIC @SIC_CMSP @SIC_CMSP_06 @Regresion
  Escenario: Validar que el formulario contenga el boton examinar
    Entonces se muestra el boton examinar

  @SIC @SIC_CMSP @SIC_CMSP_07 @Regresion
  Escenario: Realizar una carga masiva se obtienen errores a nivel de detalle
    Dado tengo informacion de stock protegido
      | fec_ini             | fec_fin             | cod_venta     | bodega | nivel_1 | nivel_2 | nivel_3 | nivel_4 | nivel_5 | Casos                             |
      | 2021-02-04 10:00:00 |                     | 2013500230004 |  20027 |      10 |      20 |      30 |      40 |      50 | Caso01 - Sin fecha fin            |
      |                     | 2021-02-04 10:00:00 | 2011834730009 |  20027 |      10 |      20 |      30 |      40 |      50 | Caso02 - Sin fecha inicio         |
      | 2021-02-04 10:00:00 | 2021-02-07 10:00:00 | 2001011570009 |        |      10 |      20 |      30 |      40 |      50 | Caso03 - Sin bodega               |
      | 2021-02-04 10:00:00 | 2021-02-07 10:00:00 | 2001011610002 |  20027 |         |         |         |         |         | Caso04 - Sin niveles              |
      | 2019-01-04 10:00:00 | 2019-01-07 10:00:00 | 2001012290005 |  20027 |      10 |      20 |      30 |      40 |      50 | Caso06 - Fechas pasadas           |
      | 2021-02-04 10:00:00 | 2021-02-03 10:00:00 | 2001012320009 |  20027 |      10 |      20 |      30 |      40 |      50 | Caso07 - Fecha Inicio mayor a fin |
      | 2021-02-04 10:00:00 | 2021-02-06 10:00:00 | 2001013250008 |  20027 | ABCC    |      20 |      30 |      40 |      50 | Caso08 - Nivel1 valor no numerico |
    Y genero un archivo de carga masiva de stock protegido
    Cuando selecciono la bodega "20027"
    Y cargo el archivo de carga masiva de stock protegido
    Y doy click a realizar carga
    Entonces se valida la carga en estado "Fallido"
    Y se muestra el detalle de los errores
      | PartNumber    | Estado  | Detalle                                                                                                                                  | Casos                             |
      | 2013500230004 | Fallido | Faltan campos obligatorios. ExpiryDate                                                                                                   | Caso01 - Sin fecha fin            |
      | 2011834730009 | Fallido | Faltan campos obligatorios. EffectiveDate                                                                                                | Caso02 - Sin fecha inicio         |
      | 2001011570009 | Fallido | Faltan campos obligatorios. LocationId                                                                                                   | Caso03 - Sin bodega               |
      | 2001011610002 | Fallido | Faltan campos obligatorios. InventoryProtection1, InventoryProtection2, InventoryProtection3, InventoryProtection4, InventoryProtection5 | Caso04 - Sin niveles              |
      | 2001012290005 | Fallido | Las fechas de inicio y fin de vigencia deben ser iguales o mayores al día de hoy.                                                        | Caso06 - Fechas pasadas           |
      | 2001012320009 | Fallido | La fecha Fin Vigencia debe ser mayor a la fecha Inicio Vigencia.                                                                         | Caso07 - Fecha Inicio mayor a fin |
      | 2001013250008 | Fallido | El valor ingresado no es un número LEVEL_PROTECTION1                                                                                     | Caso08 - Nivel1 valor no numerico |

  @SIC @SIC_CMSP @SIC_CMSP_08
  Escenario: La cabecera del archivo no tiene el formato definido.
    Dado tengo informacion de stock protegido
      | fec_ini             | fec_fin             | cod_venta     | bodega | nivel_1 | nivel_2 | nivel_3 | nivel_4 | nivel_5 |
      | 2020-02-04 10:00:00 | 2020-02-06 10:00:00 | 2013500230004 |  20027 |      10 |      20 |      30 |      40 |      50 |
    Y genero un archivo de carga masiva de stock protegido con nombres de cabecera incorrectos
    Cuando selecciono la bodega "20027"
    Y cargo el archivo de carga masiva de stock protegido
    Y doy click a realizar carga
    Entonces se muestra en la cabecera el detalle "La cabecera del archivo no tiene el formato definido."

  @SIC @SIC_CMSP @SIC_CMSP_09 @Regresion
  Escenario: Realizar una carga masiva en estado Completado con Errores
    Dado tengo informacion de stock protegido
      | fec_ini             | fec_fin             | cod_venta     | bodega | nivel_1 | nivel_2 | nivel_3 | nivel_4 | nivel_5 | Casos                             |
      |                     |                     | 2024752220002 |  20027 |     -10 |      20 |      30 |      40 |      50 | Caso01 - Carga Permanente         |
      | 2021-02-11 10:00:00 | 2021-02-14 10:00:00 | 2024752220002 |  20027 |       2 |      10 |      10 |      10 |      10 | Caso02 - Carga con fechas         |
      | 2021-02-10 10:00:00 | 2021-02-12 10:00:00 | 2024752220002 |  20027 |       2 |      10 |      10 |      10 |      10 | Caso03 - Traslape                 |
      | 2021-02-12 10:00:00 | 2021-02-13 10:00:00 | 2024752220002 |  20027 |       2 |      10 |      10 |      10 |      10 | Caso04 - Traslape incluido        |
      | 2021-02-10 10:00:00 | 2021-02-11 10:00:00 | 2024752220002 |  20027 |       2 |      10 |      10 |      10 |      10 | Caso05 - Traslape limite inferior |
      | 2021-02-14 10:00:00 | 2021-02-15 10:00:00 | 2024752220002 |  20027 |       2 |      10 |      10 |      10 |      10 | Caso06 - Traslape limite superior |
    Y genero un archivo de carga masiva de stock protegido
    Cuando selecciono la bodega "20027"
    Y cargo el archivo de carga masiva de stock protegido
    Y doy click a realizar carga
    Entonces se valida la carga en estado "Completado con Errores"

  @SIC @SIC_CMSP @SIC_CMSP_10 @Regresion
  Escenario: Realizar una carga masiva en estado Completado
    Dado tengo informacion de stock protegido
      | fec_ini             | fec_fin             | cod_venta     | bodega | nivel_1 | nivel_2 | nivel_3 | nivel_4 | nivel_5 | Casos                    |
      | 2021-03-01 10:00:00 | 2021-03-05 10:00:00 | 2024760040005 |  20027 |       2 |       5 |         |         |         | Caso01 - Producto sin SP |
      | 2021-03-01 10:00:00 | 2021-03-05 10:00:00 | 2024752220002 |  20027 |       2 |      10 |      10 |      10 |      10 | Caso02 - Producto con SP |
    Y genero un archivo de carga masiva de stock protegido
    Cuando selecciono la bodega "20027"
    Y cargo el archivo de carga masiva de stock protegido
    Y doy click a realizar carga
    Entonces se valida la carga en estado "Completado"
