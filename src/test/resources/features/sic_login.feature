# language: es
Característica: SIC - Inicio de Sesion

  @SIC @SIC_INI @SIC_INI_01 @Regresion
  Escenario: SIC - Validar que al momento de ingresar al sistema exitosamente se muestre en la pantalla Dashboard
    Cuando ingreso al sistema sic con perfil administrador
    Entonces se muestra la pantalla dashboard

  @SIC @SIC_INI @SIC_INI_02 @Regresion
  Escenario: SIC - Validar la opción de cerrar cesión y retorno a la pantalla del login
    Cuando ingreso al sistema sic con perfil administrador
    Y cierro sesion
    Entonces se muestra la pantalla login

  @SIC @SIC_INI @SIC_INI_03
  Escenario: SIC - Validar que cuando no se ingrese la contraseña en el login muestre un mensaje de validación
    Dado ingreso al login
    Cuando ingreso usuario "USERADM2"
    Y doy click a iniciar sesion
    Entonces muestra mensaje de clave "Contraseña es un campo requerido"

  @SIC @SIC_INI @SIC_INI_04
  Escenario: SIC - Validar que cuando se ingrese una contraseña incorrecta el sistema muestre un mensaje de error
    Dado ingreso al login
    Cuando ingreso usuario "USERADM2"
    Y ingreso password "111111"
    Y doy click a iniciar sesion
    Entonces muestra mensaje de validacion "Parece que tu nombre de usuario, contraseña o país no coinciden. Inténtalo de nuevo."

  @SIC @SIC_INI @SIC_INI_05
  Escenario: SIC - Verificar que cuando no se ingrese el usuario en el login muestre un mensaje de validación
    Dado ingreso al login
    Cuando ingreso password "111111"
    Y doy click a iniciar sesion
    Entonces muestra mensaje de usuario "Usuario es un campo requerido"

  @SIC @SIC_INI @SIC_INI_06
  Escenario: SIC - Verificar la existencia de los campos nombre de usuario, password y botón 'Iniciar Sesión'
    Cuando ingreso al login
    Entonces verifico que existe el campo usuario
    Y verifico que existe el campo password
    Y verifico que existe el boton iniciar sesion

  @SIC @SIC_INI @SIC_INI_07 @Regresion
  Escenario: SIC - Verificar mensaje de expiro de sesion luego de 30 minutos
    Cuando ingreso al sistema sic con perfil administrador
    Y espero 30 minutos
    Entonces se muestra mensaje de expiro se sesion
