# language: es
Característica: Ajuste de inventario vía SQS STANDAR
  Como usuario
  Quiero realizar ajustes de inventario con la finalidad de cubrir todas las casuisticas que puedan existir

  @ADJUST @ADJUST_SQS @ADJUST_SQS_01 @ADJUST_SQS_PMM @ADJUST_SQS_C @Regresion
  Esquema del escenario: Realizar ajuste de inventario de n productos en la cola standar PMM BT BO y validar en consulta de stock por jerarquia
    Dado quiero realizar un ajuste de inventario via SQS para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos y operacion "<operacion>"
    Cuando envio un request de ajuste via SQS "<Cola_Standar>"
    Entonces se registra en bd la cabecera con estado "C" mensaje ""
    Y se validan los datos del ajuste "<operacion>" en bd
    Y se valida en SIC el inventario

    Ejemplos: 
      | sistema_origen | cantidad_productos|operacion|Cola_Standar|
      | BGT            | 6                 |A        |https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsic-sqs-stock-bt-qa-pe/send-receive|
      | PMM            | 5                 |A        |https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsic-sqs-stock-pmm-qa-pe/send-receive|
      | BO             | 5                 |A        |https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsic-sqs-stock-bo-qa-pe/send-receive|
      | PMM            | 5                 |S        |https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsic-sqs-stock-pmm-qa-pe/send-receive|
      | BO             | 5                 |S        |https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsic-sqs-stock-bo-qa-pe/send-receive|
      | BGT            | 5                 |S        |https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsic-sqs-stock-bt-qa-pe/send-receive|


  @ADJUST @ADJUST_SQS @ADJUST_SQS_09 @ADJUST_SQS_PMM @ADJUST_SQS_SinSistemaOrigen
  Esquema del escenario: PMM - En la cabecera del ajuste, no se envía el campo 'Sistema Origen'
    Dado quiero realizar un ajuste de inventario via SQS para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos sin datos en el campo Sistema Origen
    Cuando envio un request de ajuste via SQS "<Cola_Standar>"
    Entonces validar que no se registre en la bd

    Ejemplos: 
      | sistema_origen | cantidad_productos |Cola_Standar|
      | BGT            | 1                  |https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsic-sqs-stock-bt-qa-pe/send-receive|
      | PMM            | 1                  |https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsic-sqs-stock-pmm-qa-pe/send-receive|
      | BO             | 1                  |https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsic-sqs-stock-bo-qa-pe/send-receive|

  @ADJUST @ADJUST_SQS @ADJUST_SQS_13 @ADJUST_SQS_PMM @ADJUST_SQS_SinTipoMovimiento
  Esquema del escenario: PMM - En la cabecera del ajuste, no se envía el campo 'Tipo de Movimiento'
    Dado quiero realizar un ajuste de inventario via SQS para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos sin datos en el campo Tipo de Movimiento
    Cuando envio un request de ajuste via SQS "<Cola_Standar>"
    Entonces validar que no se registre en la bd

    Ejemplos: 
      | sistema_origen | cantidad_productos |Cola_Standar|
      | BGT            | 1                  |https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsic-sqs-stock-bt-qa-pe/send-receive|
      | PMM            | 1                  |https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsic-sqs-stock-pmm-qa-pe/send-receive|
      | BO             | 1                  |https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsic-sqs-stock-bo-qa-pe/send-receive|

  @ADJUST @ADJUST_SQS @ADJUST_SQS_17 @ADJUST_SQS_PMM @ADJUST_SQS_SinUsuario
  Esquema del escenario: PMM - En la cabecera del ajuste, no se envía el campo 'Usuario'
    Dado quiero realizar un ajuste de inventario via SQS para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos sin datos en el campo Usuario
    Cuando envio un request de ajuste via SQS "<Cola_Standar>"
    Entonces se registra en bd la cabecera con estado "F" mensaje "El campo user es obligatorio"
    Y no se registra en bd el detalle

    Ejemplos: 
      | sistema_origen | cantidad_productos |Cola_Standar|
      | BGT            | 1                  |https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsic-sqs-stock-bt-qa-pe/send-receive|
      | PMM            | 1                  |https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsic-sqs-stock-pmm-qa-pe/send-receive|
      | BO             | 1                  |https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsic-sqs-stock-bo-qa-pe/send-receive|


  @ADJUST @ADJUST_SQS @ADJUST_SQS_21 @ADJUST_SQS_PMM @ADJUST_SQS_Operacion_incorrecta
  Esquema del escenario: PMM - Realizar ajuste de inventario con tipo de operacion incorrecto
    Dado quiero realizar un ajuste de inventario via SQS para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos con Operacion "<tipo_operacion>"
    Cuando envio un request de ajuste via SQS "<Cola_Standar>"
    Entonces se registra en bd la cabecera con estado "F" mensaje ""
    Y se registra en bd el detalle con estado "F" mensaje "Las operaciones permitidas son A y S"

    Ejemplos: 
      | sistema_origen | cantidad_productos | tipo_operacion  |Cola_Standar|
      | BGT            | 1                  |P                |https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsic-sqs-stock-bt-qa-pe/send-receive|
      | PMM            | 1                  |Q                |https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsic-sqs-stock-pmm-qa-pe/send-receive|
      | BO             | 1                  |L                |https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsic-sqs-stock-bo-qa-pe/send-receive|

  @ADJUST @ADJUST_SQS @ADJUST_SQS_25 @ADJUST_SQS_PMM @ADJUST_SQS_SKU_noExiste
  Esquema del escenario: PMM - En la transacción de ajuste, se envía un producto que no existe
    Dado quiero realizar un ajuste de inventario via SQS para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos que no existen con operacion "<tipo_operacion>"
    Cuando envio un request de ajuste via SQS "<Cola_Standar>"
    Entonces se registra en bd la cabecera con estado "F" mensaje ""
    Y se registra en bd el detalle con estado "F" mensaje "SKU no existe"

    Ejemplos: 
      | sistema_origen | cantidad_productos |tipo_operacion|Cola_Standar|
      | BGT            | 2                  |A             |https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsic-sqs-stock-bt-qa-pe/send-receive|
      | PMM            | 1                  |A             |https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsic-sqs-stock-pmm-qa-pe/send-receive|
      | BO             | 1                  |A             |https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsic-sqs-stock-bo-qa-pe/send-receive|

  @ADJUST @ADJUST_SQS @ADJUST_SQS_29 @ADJUST_SQS_PMM @ADJUST_SQS_StockCero
  Esquema del escenario: PMM - Realizar ajuste de inventario de un producto con stock 0
    Dado quiero realizar un ajuste de inventario via SQS para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos con stock 0 con "<operacion>"
    Cuando envio un request de ajuste via SQS "<Cola_Standar>"
    Entonces se registra en bd la cabecera con estado "C" mensaje ""
    Y se validan los datos del ajuste "<operacion>" en bd
    Y se valida en SIC el inventario

    Ejemplos: 

      | sistema_origen | cantidad_productos |operacion|Cola_Standar|
      | BGT            | 3                  |S        |https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsic-sqs-stock-bt-qa-pe/send-receive|
      | PMM            | 3                  |S        |https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsic-sqs-stock-pmm-qa-pe/send-receive|
      | BO             | 3                  |S        |https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsic-sqs-stock-bo-qa-pe/send-receive|



  @ADJUST @ADJUST_SQS @ADJUST_SQS_33 @ADJUST_SQS_PMM @ADJUST_SQS_idtrx_duplicado
  Esquema del escenario: PMM - Realizar un ajuste de inventario con IDTRX repetidos en el detalle
    Dado quiero realizar un ajuste de inventario via SQS para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos con id detalle duplicado
    #Cuando envio un request de ajuste via SQS
    Cuando envio un request de ajuste via SQS "<Cola_Standar>"
   # Entonces se registra en bd la cabecera con estado "F" mensaje ""
   # Y se registra en bd el detalle con estado "D" mensaje ""

    Ejemplos: 

      | sistema_origen | cantidad_productos |Cola_Standar|
      | BGT            | 2                  |https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsic-sqs-stock-bt-qa-pe/send-receive|


  @ADJUST @ADJUST_SQS @ADJUST_SQS_37 @ADJUST_SQS_PMM @ADJUST_SQS_Sin_TipoDocumento
  Esquema del escenario: PMM - En la cabecera del ajuste, no se envía el campo 'Tipo Documento'
    Dado quiero realizar un ajuste de inventario via SQS para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos sin datos en el campo Documento de Transaccion con operacion "<tipo_operacion>"
    #Y preparo la trama de ajuste con "<cantidad_productos>" productos con Operacion "<tipo_operacion>"
    Cuando envio un request de ajuste via SQS "<Cola_Standar>"
    Entonces se registra en bd la cabecera con estado "F" mensaje ""
    Y se registra en bd el detalle con estado "F" mensaje "El campo transaction_doc es obligatorio"

    Ejemplos:

      | sistema_origen | cantidad_productos |tipo_operacion|Cola_Standar|
      | BGT            | 2                  |A             |https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsic-sqs-stock-bt-qa-pe/send-receive|
      | PMM            | 1                  |A             |https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsic-sqs-stock-pmm-qa-pe/send-receive|
      | BO             | 1                  |A             |https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsic-sqs-stock-bo-qa-pe/send-receive|



  @ADJUST @ADJUST_SQS @ADJUST_SQS_41 @ADJUST_SQS_PMM @ADJUST_SQS_sin_idtrx
  Esquema del escenario: PMM - En la cabecera del ajuste, no se envía el campo 'IDTRX'
    Dado quiero realizar un ajuste de inventario via SQS para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos sin datos en el campo IDTRX
    Cuando envio un request de ajuste via SQS "<Cola_Standar>"
    Entonces se registra en bd la cabecera con estado "F" mensaje ""
    Y se registra en bd el detalle con estado "F" mensaje "El campo idtrx es obligatorio"

    Ejemplos:
      | sistema_origen | cantidad_productos |Cola_Standar|
      | BGT            | 2                  |https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsic-sqs-stock-bt-qa-pe/send-receive|
      | PMM            | 1                  |https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsic-sqs-stock-pmm-qa-pe/send-receive|
      | BO             | 1                  |https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsic-sqs-stock-bo-qa-pe/send-receive|




  @ADJUST @ADJUST_SQS @ADJUST_SQS_45 @ADJUST_SQS_PMM @ADJUST_SQS_sinBodega
  Esquema del escenario: PMM - En la cabecera del ajuste, no se envía el campo 'Bodega'
    Dado quiero realizar un ajuste de inventario via SQS para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos sin datos en el campo Bodega
    Cuando envio un request de ajuste via SQS "<Cola_Standar>"
    Entonces se registra en bd la cabecera con estado "F" mensaje ""
    Y se registra en bd el detalle con estado "F" mensaje "El campo fulfillment es obligatorio"

    Ejemplos:

      | sistema_origen | cantidad_productos |Cola_Standar|
      | BGT            | 2                  |https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsic-sqs-stock-bt-qa-pe/send-receive|
      | PMM            | 1                  |https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsic-sqs-stock-pmm-qa-pe/send-receive|
      | BO             | 1                  |https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsic-sqs-stock-bo-qa-pe/send-receive|



  @ADJUST @ADJUST_SQS @ADJUST_SQS_49 @ADJUST_SQS_PMM @ADJUST_SQS_sinTipoInventario
  Esquema del escenario: PMM - En la cabecera del ajuste, no se envía el campo 'Tipo Inventario'
    Dado quiero realizar un ajuste de inventario via SQS para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos sin datos en el campo Tipo de Inventario
    Cuando envio un request de ajuste via SQS "<Cola_Standar>"
    Entonces se registra en bd la cabecera con estado "F" mensaje ""
    Y se registra en bd el detalle con estado "F" mensaje "El campo inventoryType es obligatorio"

    Ejemplos:
      | sistema_origen | cantidad_productos |Cola_Standar|
      | BGT            | 2                  |https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsic-sqs-stock-bt-qa-pe/send-receive|
      | PMM            | 1                  |https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsic-sqs-stock-pmm-qa-pe/send-receive|
      | BO             | 1                  |https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsic-sqs-stock-bo-qa-pe/send-receive|



  @ADJUST @ADJUST_SQS @ADJUST_SQS_53 @ADJUST_SQS_PMM @ADJUST_SQS_SinOperacion
  Esquema del escenario: BGT - En el detalle del ajuste, no se envía el campo 'Operacion'
    Dado quiero realizar un ajuste de inventario via SQS para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos sin datos en el campo Operacion
    Cuando envio un request de ajuste via SQS "<Cola_Standar>"
    Entonces se registra en bd la cabecera con estado "F" mensaje ""
    Y se registra en bd el detalle con estado "F" mensaje "El campo operation es obligatorio"

    Ejemplos:

      | sistema_origen | cantidad_productos |Cola_Standar|
      | BGT            | 2                  |https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsic-sqs-stock-bt-qa-pe/send-receive|
      | PMM            | 1                  |https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsic-sqs-stock-pmm-qa-pe/send-receive|
      | BO             | 1                  |https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsic-sqs-stock-bo-qa-pe/send-receive|



  @ADJUST @ADJUST_SQS @ADJUST_SQS_57 @ADJUST_SQS_PMM @ADJUST_SQS_idtrx_repetido_otra_trama
  Esquema del escenario: PMM - En el detalle del ajuste, se envía el campo 'IDTRX' repetido de otra transaccion
    Dado quiero realizar un ajuste de inventario via SQS para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos
    Cuando envio un request de ajuste via SQS "<Cola_Standar>"
    Y envio nuevamente el mismo request de ajuste via SQS "<Cola_Standar>"
    Entonces se registra en bd la cabecera con estado "F" mensaje ""
    Y se registra en bd el detalle con estado "D" mensaje "Registro duplicado."

    Ejemplos:

      | sistema_origen | cantidad_productos |Cola_Standar|
      | BGT            | 2                  |https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsic-sqs-stock-bt-qa-pe/send-receive|


  @ADJUST @ADJUST_SQS @ADJUST_SQS_61 @ADJUST_SQS_PMM @ADJUST_SQS_SinSku
  Esquema del escenario: PMM - En el detalle del ajuste, no se envía el campo 'Sku'
    Dado quiero realizar un ajuste de inventario via SQS para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos
    Y borro el valor de el campo producto
    Cuando envio un request de ajuste via SQS "<Cola_Standar>"
    Entonces se registra en bd la cabecera con estado "F" mensaje ""
    Y se registra en bd el detalle con estado "F" mensaje "El campo sku es obligatorio"

    Ejemplos: 
      | sistema_origen | cantidad_productos |Cola_Standar|
      | BGT            | 2                  |https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsic-sqs-stock-bt-qa-pe/send-receive|
      | PMM            | 1                  |https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsic-sqs-stock-pmm-qa-pe/send-receive|
      | BO             | 1                  |https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsic-sqs-stock-bo-qa-pe/send-receive|



  @ADJUST @ADJUST_SQS @ADJUST_SQS_65 @ADJUST_SQS_PMM @ADJUST_SQS_EstadoCompletadoConErrores
  Esquema del escenario: PMM - Se envía una transacción y su estado cambia a 'Completado con Errores'
    Dado quiero realizar un ajuste de inventario via SQS para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos con error en un registro con tipo operacion "<operacion>"
    Cuando envio un request de ajuste via SQS "<Cola_Standar>"
    Entonces se registra en bd la cabecera con estado "CF" mensaje ""

    Ejemplos:

      | sistema_origen | cantidad_productos |operacion |Cola_Standar|
      | BGT            | 2                  | A        |https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsic-sqs-stock-bt-qa-pe/send-receive|
      | PMM            | 2                  | A        |https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsic-sqs-stock-pmm-qa-pe/send-receive|
      | BO             | 2                  | A        |https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsic-sqs-stock-bo-qa-pe/send-receive|


  @ADJUST @ADJUST_SQS @ADJUST_SQS_69 @ADJUST_REST_PMM @ADJUST_SQS_NoExisteBodega
  Esquema del escenario: PMM - En el detalle del ajuste, se envía una bodega que no existe
    Dado quiero realizar un ajuste de inventario via SQS para el sistema "<sistema_origen>"
    Y preparo la trama de ajuste con "<cantidad_productos>" productos
    Y asigno la bodega "<bodega_no_existe>"
    Cuando envio un request de ajuste via SQS "<Cola_Standar>"
    Entonces se registra en bd la cabecera con estado "F" mensaje ""
    Y se registra en bd el detalle con estado "F" mensaje "El campo fulfillment es obligatorio"

    Ejemplos: 
      | sistema_origen | cantidad_productos |bodega_no_existe|Cola_Standar|
      | BGT            | 2                  |            |https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsic-sqs-stock-bt-qa-pe/send-receive|
      | PMM            | 1                  |            |https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsic-sqs-stock-pmm-qa-pe/send-receive|
      | BO             | 1                  |            |https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F322525823727%2Fsic-sqs-stock-bo-qa-pe/send-receive|


