package corp.ripley.sicpe.runner;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
import org.testng.annotations.DataProvider;

@CucumberOptions(
		features = "src/test/resources/features",
		glue = "corp/ripley/sicpe/stepdefinition",
		tags = "@SDELTA_SQS_01",
		plugin = {
				"pretty",
				"html:target/cucumber-reports/cucumber-pretty",
				"json:target/cucumber-reports/json-reports/CucumberTestReport.json",
				"rerun:target/cucumber-reports/rerun-reports/rerun.txt"
		})
public class MainRunner extends AbstractTestNGCucumberTests {

	/* Descomentar para paralelismo, en data-provider-thread-count="3" de la suite testng
	se le da el n�mero de procesos paralelos*/
	/*@Override
	@DataProvider(parallel = true)
	public Object[][] scenarios() {
		return super.scenarios();
	}*/

}