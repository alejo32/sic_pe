package corp.ripley.sicpe.model.incrementalProducto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EIncrementalProductoSku {
	
	@JsonProperty("ItemId")
	public String itemId;
	
	@JsonProperty("Style")
	public String style;
	
	@JsonProperty("ProductSubClass")
	public String productSubClass;
	
	@JsonProperty("ProductClass")
	public String productClass;
	
	@JsonProperty("DepartmentNumber")
	public String departmentNumber;
	
	@JsonProperty("Description")
	public String description;
	
	@JsonProperty("CodVenta")
	public String codVenta;


}