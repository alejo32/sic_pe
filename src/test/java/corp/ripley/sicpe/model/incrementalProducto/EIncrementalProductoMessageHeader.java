package corp.ripley.sicpe.model.incrementalProducto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EIncrementalProductoMessageHeader {

	@JsonProperty("Source")
	public String source;
	
	@JsonProperty("Country")
	public String country;

}