package corp.ripley.sicpe.model.sic;

public final class EATTRIBUTES {
	public String FULFILLMENT; 
	public String CODE;
	public String TYPE;
	public String STS;
	public String DD;
	public String SFS;
	public String PU;
	public String MARK;
	public String ORDEN;
}
