package corp.ripley.sicpe.model.sic;

public class ResponseIMS {
	
	private String sku;
	private String tipoDespacho;
	private String bodegasRTConStock;
	private String tipoProducto;
	private String flagPublicado;
	private String bodegasPorStock;
	private String bodegasRCConStock;
	private String skuPadre;
	private String timestampCarga;
	private String flagservicioagregado;
	
	public ResponseIMS(){
		
	}
	
	
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	public String getTipoDespacho() {
		return tipoDespacho;
	}
	public void setTipoDespacho(String tipoDespacho) {
		this.tipoDespacho = tipoDespacho;
	}
	public String getBodegasRTConStock() {
		return bodegasRTConStock;
	}
	public void setBodegasRTConStock(String bodegasRTConStock) {
		this.bodegasRTConStock = bodegasRTConStock;
	}
	public String getTipoProducto() {
		return tipoProducto;
	}
	public void setTipoProducto(String tipoProducto) {
		this.tipoProducto = tipoProducto;
	}
	public String getFlagPublicado() {
		return flagPublicado;
	}
	public void setFlagPublicado(String flagPublicado) {
		this.flagPublicado = flagPublicado;
	}
	public String getBodegasPorStock() {
		return bodegasPorStock;
	}
	public void setBodegasPorStock(String bodegasPorStock) {
		this.bodegasPorStock = bodegasPorStock;
	}
	public String getBodegasRCConStock() {
		return bodegasRCConStock;
	}
	public void setBodegasRCConStock(String bodegasRCConStock) {
		this.bodegasRCConStock = bodegasRCConStock;
	}
	public String getSkuPadre() {
		return skuPadre;
	}
	public void setSkuPadre(String skuPadre) {
		this.skuPadre = skuPadre;
	}
	public String getTimestampCarga() {
		return timestampCarga;
	}
	public void setTimestampCarga(String timestampCarga) {
		this.timestampCarga = timestampCarga;
	}
	public String getFlagservicioagregado() {
		return flagservicioagregado;
	}
	public void setFlagservicioagregado(String flagservicioagregado) {
		this.flagservicioagregado = flagservicioagregado;
	}
	

}

/*{
"sku":"2064060970008",
"tiposDespacho":"RET_TIENDA,DESP_DOMICILIO",
"bodegasRTConStock":"010000,010034,010003,010029",
"tipoProducto":"R",
"flagPublicado":1,
"bodegasPorStock":"010000:11006,010034:9000,010003:9500,010039:20506,010029:1024",
"bodegasRCConStock":null,
"skuPadre":null,
"cantidadTotalSTS":"1004",
"timestampCarga":"1592821317.510627",
"flagservicioagregado":0
}
*/
