package corp.ripley.sicpe.model;

public class ETransactionDetail {

	public String idTrx;
	public String sku;
	public String fulfillmentCode;
	public String inventoryType;
    public String stock;
    public String prevStock;
    public String operation;
    public String cud;
    public String status;
    public String detail;

}