package corp.ripley.sicpe.stepdefinition;

import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Entonces;
import corp.ripley.sicpe.model.EConexionBD;
import corp.ripley.sicpe.util.DriverFactory;

public class PoC extends DriverFactory {

	
	@Cuando("^me conecto a bd oracle$")
	public void me_conecto_a_bd_oracle() throws Throwable {

		EConexionBD oEConexionBD = new EConexionBD();
		oEConexionBD.sshUser = GetSSHUser();
		oEConexionBD.sshPassword = GetSSHPpk();
		oEConexionBD.sshHost = GetSSHHost();
		oEConexionBD.sshPort = Integer.parseInt(GetSSHPort());
		
		oEConexionBD.dbHost = GetDBHost();
		oEConexionBD.dbUser = GetDBUser();
		oEConexionBD.dbPassword = GetDBPassword();

		oracleDb.ConnectarTunelSSH(oEConexionBD);
	}

	@Entonces("^muestra mensaje OK$")
	public void muestra_mensaje_OK() throws Throwable {
	  System.out.println("CONEXION EXITOSA");
	}

}
