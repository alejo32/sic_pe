package corp.ripley.sicpe.stepdefinition;

import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import corp.ripley.sicpe.model.EConexionBD;
import org.junit.Assert;
import corp.ripley.sicpe.util.DriverFactory;

import java.io.IOException;

public class LoginSteps extends DriverFactory {

	@Dado("^ingreso al sistema sic$")
	public void ingreso_al_sistema_sic() throws Exception {
		String usuario = GetSICUsuarioAdministrador();
		String password = GetSICPasswordAdministrador();

		loginPage.AbrirSIC();
		loginPage.Login(usuario, password);

		EConexionBD oEConexionBD = new EConexionBD();
		oEConexionBD.dbHost = GetDBHost();
		oEConexionBD.dbPort = Integer.parseInt(GetDBPort());
		oEConexionBD.dbUser = GetDBUser();
		oEConexionBD.dbPassword = GetDBPassword();
		database.ConnectarTunelSSH(oEConexionBD);
	}

	@Dado("^ingreso al sistema sic con perfil administrador$")
	public void ingreso_al_sistema_sic_con_perfil_administrador() throws Exception {
		String usuario = GetSICUsuarioAdministrador();
		String password = GetSICPasswordAdministrador();

		loginPage.AbrirSIC();
		loginPage.Login(usuario, password);
	}


	//calculo stock IMS inicio
	@Dado("^ingreso al sistema sic con usuario administrador Peru$")
	public void ingreso_al_sistema_sic_con_usuario_administrador_Peru() throws Exception {
				
		loginPage.AbrirSIC();
		String usuario = GetSICUsuarioAdministrador();
		String password = GetSICPasswordAdministrador();
		loginPage.Login(usuario, password);
		
		EConexionBD oEConexionBD = new EConexionBD();
		oEConexionBD.dbHost = GetDBHost();
		oEConexionBD.dbPort = Integer.parseInt(GetDBPort());
		oEConexionBD.dbUser = GetDBUser();
		oEConexionBD.dbPassword = GetDBPassword();
			
		database.ConnectarBDSIC(oEConexionBD);
	}
	@Dado("^ingreso al sistema sic con usuario administrador Chile$")
	public void ingreso_al_sistema_sic_con_usuario_administrador_Chile() throws Exception {

		//String usuario_VPN = GetVPN_Usuario();
		//String password_VPN = GetVPN_Password();
		//System.out.println("usuario_VPN: "+usuario_VPN);
		//System.out.println("password_VPN: "+password_VPN);
		//loginPage.IngresarAutenticacion(usuario_VPN,password_VPN);
		
		String usuario = GetSICUsuarioAdministradorCL();
		String password = GetSICPasswordAdministradorCL();
		loginPage.AbrirSIC_CL();
		loginPage.Login(usuario, password);
		
		EConexionBD oEConexionBD = new EConexionBD();
		oEConexionBD.dbHost = GetDBHost_CL();
		oEConexionBD.dbPort = Integer.parseInt(GetDBPort_CL());
		oEConexionBD.dbUser = GetDBUser_CL();
		oEConexionBD.dbPassword = GetDBPassword_CL();
			
		database.ConnectarBDSIC_CL(oEConexionBD);
	}
	
	@Cuando("^ingreso al sistema IMS$")
	public void ingreso_al_sistema_IMS() throws Throwable {
		//String usuario = GetUsuarioIMS();
		//String password = GetPasswordIMS();
		loginPage.AbrirIMS();
	}
	@Cuando("^ingreso al sistema IMS Peru$")
	public void ingreso_al_sistema_IMS_Peru() throws Throwable {

		loginPage.AbrirIMS_pe();
	}

	//calculo stock IMS fin
	
	@Dado("^ingreso al sistema sic con perfil estandar$")
	public void ingreso_al_sistema_sic_con_perfil_estandar() throws Exception {
		String usuario = GetSICUsuarioEstandar();
		String password = GetSICPasswordEstandar();

		loginPage.AbrirSIC();
		loginPage.Login(usuario, password);
	}

	@Entonces("^se muestra la pantalla dashboard$")
	public void se_muestra_la_pantalla_dashboard() throws InterruptedException {
		Assert.assertTrue("No se muestra la pantalla Dashboard", loginPage.EsperarPantallaDashboard());
	}

	@Entonces("^se muestra la pantalla login$")
	public void se_muestra_la_pantalla_login() throws InterruptedException {
		Assert.assertTrue("No se muestra la pantalla Login", loginPage.EsperarPantallaLogin());
	}

	@Cuando("^cierro sesion$")
	public void cierro_sesion() throws InterruptedException, IOException {
		menuPage.ClickOpcionesUsuario();
		menuPage.ClickCerrarSesion();
	}

	@Dado("^ingreso al login$")
	public void ingreso_al_login() throws Exception {

		loginPage.AbrirSIC();
	}

	@Cuando("^ingreso usuario \"([^\"]*)\"$")
	public void ingreso_usuario(String usuario) throws InterruptedException, IOException {
		loginPage.IngresarUsuario(usuario);
	}

	@Cuando("^ingreso password \"([^\"]*)\"$")
	public void ingreso_password(String password) throws InterruptedException, IOException {
		loginPage.IngresarPassword(password);
	}

	@Cuando("^doy click a iniciar sesion$")
	public void doy_click_a_iniciar_sesion() throws InterruptedException, IOException {

		loginPage.ClickIniciarSesion();
	}

	@Cuando("^muestra mensaje de clave \"([^\"]*)\"$")
	public void muestra_mensaje_de_clave(String mensajeEsperado) throws InterruptedException, IOException {
		String mensajeObtenido = loginPage.ObtenerMensajeErrorPassword();
		String mensajeError = "El mensaje mostrado es incorrecto";
		Assert.assertTrue(mensajeError, mensajeEsperado.equals(mensajeObtenido));
	}

	@Entonces("^muestra mensaje de usuario \"([^\"]*)\"$")
	public void muestra_mensaje_de_usuario(String mensajeEsperado) throws InterruptedException, IOException {
		String mensajeObtenido = loginPage.ObtenerMensajeErrorUsuario();
		String mensajeError = "El mensaje mostrado es incorrecto";
		Assert.assertTrue(mensajeError, mensajeEsperado.equals(mensajeObtenido));
	}

	@Entonces("^muestra mensaje de validacion \"([^\"]*)\"$")
	public void muestra_mensaje_de_validacion(String mensajeEsperado) throws InterruptedException, IOException {
		String mensajeObtenido = loginPage.ObtenerMensajeValidacion();
		System.out.println("Mensaje obtenido de la web:"+ mensajeObtenido);
		String mensajeError = "El mensaje mostrado es incorrecto";
		Assert.assertTrue(mensajeError, mensajeObtenido.contains(mensajeEsperado));
	}

	@Entonces("^verifico que existe el campo usuario$")
	public void verifico_que_existe_el_campo_usuario() throws InterruptedException, IOException {
		loginPage.EsperarPantallaLogin();
		Assert.assertTrue("No se encontró el campo Usuario", loginPage.ExisteCampoUsuario());
	}

	@Entonces("^verifico que existe el campo password$")
	public void verifico_que_existe_el_campo_password() throws InterruptedException, IOException {
		Assert.assertTrue("No se encontró el campo Password", loginPage.ExisteCampoPassword());
	}

	@Entonces("^verifico que existe el boton iniciar sesion$")
	public void verifico_que_existe_el_boton_iniciar_sesion() throws InterruptedException, IOException {
		Assert.assertTrue("No se encontró el boton Iniciar Sesion", loginPage.ExisteBotonIniciarSesion());
	}

	@Cuando("^espero (\\d+) minutos$")
	public void espero_minutos(int arg1) throws Throwable {
	    
	}

	@Entonces("^se muestra mensaje de expiro se sesion$")
	public void se_muestra_mensaje_de_expiro_se_sesion() throws Throwable {
	    Assert.assertTrue("No se muestra mensaje de expiro de sesion", loginPage.EsperarMensajeSesionExpirada());
	}
}
