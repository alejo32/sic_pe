package corp.ripley.sicpe.stepdefinition;

import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import org.junit.Assert;
import corp.ripley.sicpe.util.DriverFactory;

import java.io.IOException;

public class ConfiguracionSteps extends DriverFactory {

	@Cuando("^doy click al combo nombre de sistema$")
	public void doy_click_al_combo_nombre_de_sistema() throws IOException, InterruptedException {
		configuracionPage.ClickComboNombreSistema();
	}

	@Cuando("^doy click en agregar$")
	public void doy_click_en_agregar() throws IOException, InterruptedException {
		configuracionPage.ClickAgregar();
	}

	@Cuando("^doy click en el combo tipo de transaccion$")
	public void doy_click_al_combo_tipo_de_transaccion() throws IOException, InterruptedException {
		configuracionPage.ClickComboTipoTransaccion();
	}

	@Cuando("^doy click en el combo tipo de inventario$")
	public void doy_click_al_combo_tipo_de_inventario() throws IOException, InterruptedException {
		configuracionPage.ClickComboTipoTransaccion();
	}

	@Entonces("^se verifica que se muestren todos los sistemas$")
	public void se_valida_que_se_muestren_todos_los_sistemas() throws IOException, InterruptedException {
		Assert.assertTrue("No se muestra la opción B2B", configuracionPage.ExisteSistemaOpcion("B2B"));
		Assert.assertTrue("No se muestra la opción BO", configuracionPage.ExisteSistemaOpcion("BO"));
		Assert.assertTrue("No se muestra la opción BGT", configuracionPage.ExisteSistemaOpcion("BGT"));
		Assert.assertTrue("No se muestra la opción SVC", configuracionPage.ExisteSistemaOpcion("SVC"));
		Assert.assertTrue("No se muestra la opción MTO", configuracionPage.ExisteSistemaOpcion("MTO"));
		Assert.assertTrue("No se muestra la opción PMM", configuracionPage.ExisteSistemaOpcion("PMM"));
	}

	@Entonces("^se verifica que se muestren todos los tipos de transaccion$")
	public void se_verifica_que_se_muestren_todos_los_tipos_de_transaccion() throws IOException, InterruptedException {

		Assert.assertTrue("No se muestra el tipo de transacción ADJUST",
				configuracionPage.ExisteTipoTransaccion("ADJUST"));
		Assert.assertTrue("No se muestra el tipo de transacción SFULL",
				configuracionPage.ExisteTipoTransaccion("SFULL"));
		Assert.assertTrue("No se muestra el tipo de transacción SDELTA",
				configuracionPage.ExisteTipoTransaccion("SDELTA"));
	}

	@Entonces("^se verifica que se muestren todos los tipos de inventario$")
	public void se_verifica_que_se_muestren_todos_los_tipos_de_inventario() throws IOException, InterruptedException {

		Assert.assertTrue("No se muestra el tipo de inventario Stock Disponible",
				configuracionPage.ExisteTipoInventario("1"));
		Assert.assertTrue("No se muestra el tipo de transacción Reservado para ventas",
				configuracionPage.ExisteTipoInventario("2"));
	}

	@Dado("^el boton consultar esta deshabilitado$")
	public void el_boton_consultar_esta_deshabilitado() throws IOException, InterruptedException {
		Assert.assertTrue("El botón Consultar esta habilitado",
				configuracionPage.ExisteBotonDeshabilitado("Consultar"));
	}

	@Dado("^el boton agregar esta deshabilitado$")
	public void el_boton_agregar_esta_deshabilitado() throws IOException, InterruptedException {
		Assert.assertTrue("El botón Agregar esta habilitado", configuracionPage.ExisteBotonDeshabilitado("Agregar"));
	}

	@Dado("^el boton eliminar esta deshabilitado$")
	public void el_boton_eliminar_esta_deshabilitado() throws IOException, InterruptedException {
		Assert.assertTrue("El botón Consultar esta habilitado",
				configuracionPage.ExisteBotonDeshabilitado("Eliminar"));
	}

	@Cuando("^selecciono el sistema \"([^\"]*)\"$")
	public void selecciono_el_sistema(String nombreSistema) throws IOException, InterruptedException {
		configuracionPage.SeleccionarSistema(nombreSistema);
	}

	@Cuando("^selecciono tipo de transaccion \"([^\"]*)\"$")
	public void selecciono_tipo_de_transaccion(String codigoTipoTransaccion) throws IOException, InterruptedException {
		configuracionPage.SeleccionarTipoTransaccion(codigoTipoTransaccion);
	}

	@Cuando("^selecciono tipo de inventario (\\d+)$")
	public void selecciono_tipo_de_inventario(String codigoTipoInventario) throws IOException, InterruptedException {
		configuracionPage.SeleccionarTipoInventario(codigoTipoInventario);
	}

	@Entonces("^se habilita el boton consultar$")
	public void se_habilita_el_boton_consultar() throws IOException, InterruptedException {
		Assert.assertTrue("El botón Consultar esta deshabilitado",
				configuracionPage.ExisteBotonHabilitado("Consultar"));
	}

	@Entonces("^se habilita el boton agregar$")
	public void se_habilita_el_boton_agregar() throws IOException, InterruptedException {
		Assert.assertTrue("El botón Agregar esta deshabilitado", configuracionPage.ExisteBotonHabilitado("Agregar"));
	}

	@Entonces("^se habilita el boton eliminar$")
	public void se_habilita_el_boton_elimimar(String nombreSistema) throws IOException, InterruptedException {
		Assert.assertTrue("El botón Consultar esta deshabilitado",
				configuracionPage.ExisteBotonHabilitado("Eliminar"));
	}

	@Cuando("^doy click en consultar$")
	public void doy_click_en_consultar() throws IOException, InterruptedException {
		configuracionPage.ClickConsultar();
	}

	@Cuando("^doy click en limpiar$")
	public void doy_click_en_limpiar() throws IOException, InterruptedException {
		configuracionPage.ClickLimpiar();
	}

	@Entonces("^los botones se deshabilitan a excepcion del boton limpiar$")
	public void los_botones_se_deshabilitan_a_excepcion_del_boton_limpiar() throws IOException, InterruptedException {
		Assert.assertTrue("El botón Consultar esta habilitado",
				configuracionPage.ExisteBotonDeshabilitado("Consultar"));
		Assert.assertTrue("El botón Eliminar esta habilitado",
				configuracionPage.ExisteBotonDeshabilitado("Eliminar"));
		Assert.assertTrue("El botón Agregar esta habilitado", configuracionPage.ExisteBotonDeshabilitado("Agregar"));
	}

	@Entonces("^se limpia el resultado$")
	public void se_limpia_el_resultado() throws IOException, InterruptedException {
		Assert.assertTrue("No se limpió el resultado", !configuracionPage.ExisteResultadoLimpio());
	}

	@Entonces("^muestra las configuraciones asociadas al sistema$")
	public void muestra_las_configuraciones_asociadas_al_sistema() throws IOException, InterruptedException {
		configuracionPage.ExisteCargaDeResultados();
	}

	@Entonces("^muestra mensaje de confirmacion \"([^\"]*)\"$")
	public void muestra_mensaje_de_confirmacion(String mensaje) throws IOException, InterruptedException {
		configuracionPage.ExisteMensajeConfirmacion(mensaje);
	}

	@Entonces("^al dar continuar muestra mensaje \"([^\"]*)\"$")
	public void al_dar_continuar_muestra_mensaje(String mensaje) throws IOException, InterruptedException {
		configuracionPage.ClickContinuar();
		configuracionPage.VerificarMensajeFinal(mensaje);
	}
}
