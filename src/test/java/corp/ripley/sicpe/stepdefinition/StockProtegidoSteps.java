package corp.ripley.sicpe.stepdefinition;

import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import org.junit.Assert;
import corp.ripley.sicpe.pageobject.BasePage;
import corp.ripley.sicpe.util.DriverFactory;

public class StockProtegidoSteps extends DriverFactory {

	@Cuando("^presiono el boton consultar stock protegido$")
	public void presiono_el_botón_Consultar_Stock_Protegido() throws Throwable {
		stockProtegidoPage.ClickBtnConsultar();
		BasePage.WaitSleep(1);
		BasePage.TakeScreenShot();
	}

	@Entonces("^se despliega la grilla con registros$")
	public void se_despliega_la_grilla_con_registros() throws Throwable {

		Assert.assertTrue("No se muestran registros para el filtro seleccionado", stockProtegidoPage.GrillaHasRows());
		BasePage.WaitSleep(1);
		BasePage.TakeScreenShot();
	}

	@Cuando("^presiono el boton nuevo evento$")
	public void se_presiona_el_Botón_Nuevo_Evento() throws Throwable {
		stockProtegidoPage.btnEvento();
		BasePage.WaitSleep(1);
	}

	@Cuando("^se despliega el formulario de configuracion$")
	public void se_despliega_el_formulario_de_configuracion() throws Throwable {
		Assert.assertTrue("No se muestra el formulario de configuracion", stockProtegidoPage.BtnCancelarIsDisplayed());
		stockProtegidoPage.MoveToBtnCancelar();
		BasePage.WaitSleep(1);
		BasePage.TakeScreenShot();
	}

	@Entonces("^ingreso una fecha de inicio correcta$")
	public void ingreso_una_fecha_de_inicio_correcta() throws Throwable {
		BasePage.WaitSleep(1);
		stockProtegidoPage.inpFecIni();
		BasePage.WaitSleep(1);
		stockProtegidoPage.SeleccionarInicioVigencia(0);
	}

	@Entonces("^ingreso una fecha de inicio igual a la actual$")
	public void ingreso_una_fecha_de_inicio_igual_a_la_actual() throws Throwable {
		stockProtegidoPage.inpFecIni();
		BasePage.WaitSleep(1);
		stockProtegidoPage.SeleccionarInicioVigencia(0);
	}

	@Entonces("^se muestra la fecha seleccionada$")
	public void se_muestra_la_fecha_seleccionada() throws Throwable {
		Assert.assertTrue("La fecha seleccionada no se muestra en el campo fecha inicio",
				stockProtegidoPage.TxtFecIniHasValue());
	}

	@Entonces("^ingreso una fecha de termino correcta$")
	public void ingreso_una_fecha_de_termino_válida() throws Throwable {
		BasePage.WaitSleep(1);
		stockProtegidoPage.inpFecFin();
		BasePage.WaitSleep(1);
		stockProtegidoPage.fechaFin();
	}

	@Entonces("^ingreso stock para el nivel uno$")
	public void ingreso_Stock_para_Nivel_Uno() throws Throwable {
		stockProtegidoPage.inpLevel1();
		BasePage.WaitSleep(1);
	}

	@Entonces("^ingreso stock para el nivel dos$")
	public void ingreso_Stock_para_Nivel_Dos() throws Throwable {
		stockProtegidoPage.inpLevel2();
		BasePage.WaitSleep(1);
	}

	@Entonces("^presiono el boton guardar$")
	public void presionamos_el_Botón_Guardar() throws Throwable {
		stockProtegidoPage.btnGuardar();
		BasePage.WaitSleep(1);
		BasePage.TakeScreenShot();
	}

	@Entonces("^Se despliega Pop Up con mensaje de alerta para Confirmar Stock$")
	public void se_despliega_Pop_Up_con_mensaje_de_alerta_para_Confirmar_Stock() throws Throwable {
		stockProtegidoPage.ClickBtnConfirmar();
		BasePage.WaitSleep(1);
	}

	@Entonces("^se despliega mensaje de alerta Confirmando$")
	public void se_despliega_mensaje_de_alerta_Confirmando() throws Throwable {
		BasePage.TakeScreenShot();
	}

	@Cuando("^Se presiona el Botón Editar$")
	public void se_presiona_el_Botón_Editar() throws Throwable {
		stockProtegidoPage.rdbEditar();
		BasePage.WaitSleep(1);
		stockProtegidoPage.btnEditar();
		BasePage.WaitSleep(1);
		BasePage.TakeScreenShot();
	}

	@Entonces("^Editamos stock para el nivel uno$")
	public void editamos_Stock_para_Nivel_Uno() throws Throwable {
		stockProtegidoPage.editLevel1();
		BasePage.WaitSleep(1);
	}

	@Entonces("^Editamos stock para el nivel dos$")
	public void editamos_Stock_para_Nivel_Dos() throws Throwable {
		stockProtegidoPage.editLevel2();
		BasePage.WaitSleep(1);
	}

	@Cuando("^selecciono el ultimo registro$")
	public void selecciono_el_ultimo_registro() throws Throwable {
		stockProtegidoPage.rdbEditar();
		BasePage.TakeScreenShot();
	}

	@Cuando("^presiono el boton eliminar$")
	public void se_presiona_el_Botón_Eliminar() throws Throwable {
	
		stockProtegidoPage.ClickBtnEliminar();
		BasePage.WaitSleep(1);
		BasePage.TakeScreenShot();
	}

	
	/*  */
	@Dado("^existe el campo departamento$")
	public void valido_la_existencia_de_campo_Departamento() throws Throwable {
		stockProtegidoPage.selDepartamento();
		BasePage.WaitSleep(1);
	}

	@Cuando("^el campo departamento se encuentra vacio$")
	public void campo_Departamento_se_encuentra_vacío() throws Throwable {
		stockProtegidoPage.ClickCbDepartamento();
		BasePage.WaitSleep(1);
		BasePage.TakeScreenShot();
	}

	@Entonces("^selecciono departamento \"([^\"]*)\"$")
	public void permite_seleccionar_un_Departamento_(String codigoDepartamento) throws Throwable {
		stockProtegidoPage.selDeptoEsc(codigoDepartamento);
		BasePage.WaitSleep(1);
		BasePage.TakeScreenShot();
	}

	@Entonces("^selecciono linea \"([^\"]*)\"$")
	public void selecciono_linea(String linea) throws Throwable {
		stockProtegidoPage.selLineaEsc(linea);
		BasePage.WaitSleep(1);
		BasePage.TakeScreenShot();
	}

	@Entonces("^selecciono sublinea \"([^\"]*)\"$")
	public void selecciono_sublinea(String subLinea) throws Throwable {
		stockProtegidoPage.selSubLineaEsc(subLinea);
		BasePage.WaitSleep(1);
		BasePage.TakeScreenShot();
	}

	@Entonces("^selecciono codigo venta \"([^\"]*)\"$")
	public void selecciono_codigo_venta(String codigoVenta) throws Throwable {
		stockProtegidoPage.IngresarCodigoVenta(codigoVenta);
		BasePage.WaitSleep(1);
		BasePage.TakeScreenShot();
	}

	@Entonces("^me permite seleccionar el departamento \"([^\"]*)\"$")
	public void me_permite_seleccionar_el_departamento(String codigoDepartamento) throws Throwable {
		stockProtegidoPage.selDeptoEsc(codigoDepartamento);

		BasePage.WaitSleep(1);
		BasePage.TakeScreenShot();
	}

	@Entonces("^ingreso el departamento \"([^\"]*)\"$")
	public void ingreso_el_departamento(String codigoDepartamento) throws Throwable {
		stockProtegidoPage.selDeptoEsc(codigoDepartamento);
		BasePage.WaitSleep(1);
		BasePage.TakeScreenShot();
	}

	@Dado("^valido la existencia del campo linea$")
	public void valido_la_existencia_del_campo_Línea() throws Throwable {
		stockProtegidoPage.txtLinea();
		BasePage.WaitSleep(1);
	}

	@Entonces("^no permite seleccionar un valor de linea$")
	public void no_permite_seleccionar_un_valor_de_Linea() throws Throwable {
		BasePage.TakeScreenShot();
	}

	@Dado("^valido la existencia del campo sub linea$")
	public void valido_la_existencia_del_campo_Sub_Línea() throws Throwable {
		stockProtegidoPage.txtSubLinea();
		BasePage.WaitSleep(1);
	}

	@Entonces("^no permite seleccionar un valor de sub linea$")
	public void no_permite_seleccionar_un_valor_de_Sub_Linea() throws Throwable {
		BasePage.TakeScreenShot();
	}

	@Dado("^valido la existencia del campo cod venta$")
	public void valido_la_existencia_del_campo_Cod_Venta() throws Throwable {
		stockProtegidoPage.txtCodVenta();
		BasePage.WaitSleep(1);
	}

	@Entonces("^no permite seleccionar un valor de cod venta$")
	public void no_permite_seleccionar_un_valor_de_Cod_Venta() throws Throwable {
		BasePage.TakeScreenShot();
	}

	@Dado("^valido la existencia del boton consultar$")
	public void valido_la_existencia_del_boton_Consultar() throws Throwable {
		Assert.assertTrue("No existe el botón consultar", stockProtegidoPage.btnConsultaIsDisplayed());
		BasePage.WaitSleep(1);
		BasePage.TakeScreenShot();
	}

	@Entonces("^el boton consultar se encuentra deshabilitado$")
	public void el_botón_Consultar_se_encuentra_deshabilitado() throws Throwable {
		Assert.assertTrue("El botón no se encuentra deshabilitado", stockProtegidoPage.btnConsultaIsDisabled());
		BasePage.WaitSleep(1);
		BasePage.TakeScreenShot();
	}

	@Entonces("^se habilita el combobox linea$")
	public void se_habilita_el_combobox_linea() throws Throwable {
		Assert.assertTrue("El combobox línea no se encuentra habilitado", stockProtegidoPage.cbLineaIsEnabled());
		BasePage.WaitSleep(1);
		BasePage.TakeScreenShot();
	}

	@Entonces("^se habilita campo codigo de venta$")
	public void se_habilita_campo_Cod_Venta() throws Throwable {
		Assert.assertTrue("El combobox línea no se encuentra habilitado", stockProtegidoPage.txtCodigoVentaIsEnabled());
		BasePage.WaitSleep(1);
		BasePage.TakeScreenShot();
	}

	@Entonces("^el boton nuevo evento se encuentra deshabilitado$")
	public void el_botón_Nuevo_Evento_se_encuentra_deshabilitado() throws Throwable {
		Assert.assertTrue("El botón no se encuentra deshabilitado", stockProtegidoPage.btnNuevoEventoIsDisabled());
		BasePage.TakeScreenShot();
	}

	@Entonces("^el boton editar se encuentra deshabilitado$")
	public void el_botón_Editar_se_encuentra_deshabilitado() throws Throwable {
		Assert.assertTrue("El boton no se encuentra deshabilitado", stockProtegidoPage.btnEditarIsDisabled());
		BasePage.TakeScreenShot();
	}

	@Entonces("^el boton eliminar se encuentra deshabilitado$")
	public void el_botón_Eliminar_se_encuentra_deshabilitado() throws Throwable {
		Assert.assertTrue("El boton no se encuentra deshabilitado", stockProtegidoPage.btnEliminarIsDisabled());
		BasePage.TakeScreenShot();
	}

	@Dado("^valido la existencia grilla configuraciones$")
	public void valido_la_existencia_Grilla_configuraciones() throws Throwable {
		Assert.assertTrue("No existe la grilla configuraciones", stockProtegidoPage.grillConfigIdDisplayed());
		BasePage.WaitSleep(1);
	}

	@Entonces("^la grilla se despliega sin registros$")
	public void la_grilla_se_despliega_sin_registros() throws Throwable {
		Assert.assertTrue("La grilla tiene registros", stockProtegidoPage.gillConfigIsEmpty());
		BasePage.TakeScreenShot();
	}

	@Dado("^valido la existencia del boton nuevo evento$")
	public void valido_la_existencia_de_boton_Nuevo_Evento() throws Throwable {
		Assert.assertTrue("No existe el botón nuevo evento", stockProtegidoPage.btnEventoIsDisplayed());
		BasePage.WaitSleep(1);
		BasePage.TakeScreenShot();
	}

	@Dado("^valido la existencia del boton editar$")
	public void valido_la_existencia_de_Botón_Editar() throws Throwable {
		Assert.assertTrue("No existe el botón editar", stockProtegidoPage.btnEditarIsDisplayed());
		BasePage.WaitSleep(1);
		BasePage.TakeScreenShot();
	}

	@Dado("^valido la existencia del boton eliminar$")
	public void valido_la_existencia_de_Botón_Eliminar() throws Throwable {
		Assert.assertTrue("No existe el botón editar", stockProtegidoPage.btnEliminarIsDisplayed());
		BasePage.WaitSleep(1);
	}

	@Dado("^selecciono por defecto tipo de producto ripley$")
	public void se_selecciona_por_defecto_Tipo_de_Producto_Ripley() throws Throwable {
		stockProtegidoPage.selProducto();
		BasePage.WaitSleep(1);
	}

	@Entonces("^se despliegan todos los departamentos existentes$")
	public void se_despliegan_todos_los_departamentos_existente() throws Throwable {
		BasePage.TakeScreenShot();
	}

	@Entonces("^el boton consultar se encuentra habilitado$")
	public void se_habilita_el_boton_Consultar() throws Throwable {
		Assert.assertTrue("El boton consultar esta deshabilitado", stockProtegidoPage.btnConsultarIsEnabled());
		BasePage.WaitSleep(1);
		stockProtegidoPage.selProducto();
		BasePage.WaitSleep(1);
		BasePage.TakeScreenShot();
	}

	@Entonces("^selecciono el combo departamento$")
	public void seleccionar_Combobox_Deptarmento() throws Throwable {
		stockProtegidoPage.ClickCbDepartamento();
		BasePage.WaitSleep(1);
		BasePage.TakeScreenShot();
	}

	@Entonces("^selecciono el combo linea$")
	public void selecciono_el_combobox_Linea() throws Throwable {
		stockProtegidoPage.ClickCbLinea();
		BasePage.WaitSleep(1);
		BasePage.TakeScreenShot();
	}

	@Entonces("^selecciono el combo sublinea$")
	public void selecciono_el_combobox_Sublinea() throws Throwable {
		stockProtegidoPage.ClickCbSubLinea();
		BasePage.WaitSleep(1);
		BasePage.TakeScreenShot();
	}

	@Entonces("^Se habilita campo Línea$")
	public void se_habilita_campo_Línea() throws Throwable {
		stockProtegidoPage.txtLinea();
		BasePage.WaitSleep(1);
	}

	@Entonces("^selecciono opción Botas$")
	public void selecciono_opción_Botas() throws Throwable {
		stockProtegidoPage.txtLineaProd();
		BasePage.WaitSleep(1);
		BasePage.TakeScreenShot();
	}

	@Entonces("^elimino departamento seleccionado$")
	public void elimino_departamento_seleccionado() throws Throwable {
		stockProtegidoPage.EliminarDepartamentoSeleccionado();
		BasePage.WaitSleep(1);
		BasePage.TakeScreenShot();
	}

	@Entonces("^elimino lineas seleccionadas$")
	public void elimino_Líneas_seleccionadas() throws Throwable {

		BasePage.WaitSleep(1);
		stockProtegidoPage.EliminarLineaSeleccionado();
		BasePage.WaitSleep(1);
		BasePage.TakeScreenShot();
	}

	@Entonces("^Ingreso más de un SKU$")
	public void ingreso_más_de_un_SKU() throws Throwable {
		stockProtegidoPage.txtSKUSel();
		BasePage.WaitSleep(1);
		BasePage.TakeScreenShot();
	}

	@Entonces("^presiono boton consultar$")
	public void presiono_boton_consultar() throws Throwable {
		stockProtegidoPage.ClickBtnConsultar();
		BasePage.WaitSleep(1);
		BasePage.TakeScreenShot();
	}

	@Entonces("^se despliega mensaje de alerta debe seleccionar una bodega para continuar$")
	public void se_despliega_mensaje_de_alerta_Debe_seleccionar_una_bodega_para_continuar() throws Throwable {
		stockProtegidoPage.msjAlerta();
		BasePage.WaitSleep(1);
		BasePage.TakeScreenShot();
	}

	@Cuando("^Ingreso un SKU$")
	public void ingreso_un_SKU() throws Throwable {
		stockProtegidoPage.txtSKUUnit();
		BasePage.WaitSleep(1);
		BasePage.TakeScreenShot();
	}

	@Entonces("^Mensaje en grilla No se encontraron resultados para los criterios de búsqueda ingresados$")
	public void mensaje_en_grilla_No_se_encontraron_resultados_para_los_criterios_de_búsqueda_ingresados()
			throws Throwable {
		stockProtegidoPage.msjGrilla();
		BasePage.WaitSleep(1);
		BasePage.TakeScreenShot();
	}

	@Cuando("^Permite seleccionar un Departamento con Configuración Activa$")
	public void permite_seleccionar_un_Departamento_con_Configuración_Activa() throws Throwable {
		stockProtegidoPage.selDeptoActiva();
		BasePage.WaitSleep(1);
		BasePage.TakeScreenShot();
	}

	@Cuando("^selecciono opción Carteras$")
	public void selecciono_opción_Carteras() throws Throwable {
		stockProtegidoPage.txtLineaActiva();
		BasePage.WaitSleep(1);
		BasePage.TakeScreenShot();
	}

	@Cuando("^selecciono opción Cuero S(\\d+)$")
	public void selecciono_opción_Cuero_S(int arg1) throws Throwable {
		stockProtegidoPage.txtSubLineaActiva();
		BasePage.WaitSleep(1);
		BasePage.TakeScreenShot();
	}

	@Cuando("^Ingreso un SKU con Configuración Activa$")
	public void ingreso_un_SKU_con_Configuración_Activa() throws Throwable {
		stockProtegidoPage.txtSKUActivo();
		BasePage.WaitSleep(1);
		BasePage.TakeScreenShot();
	}

	@Entonces("^ingreso stock para el nivel tres$")
	public void ingreso_Stock_para_Nivel_Tres() throws Throwable {
		stockProtegidoPage.inpLevel3();
	}

	@Entonces("^ingreso stock para el nivel cuatro$")
	public void ingreso_Stock_para_Nivel_Cuatro() throws Throwable {
		stockProtegidoPage.inpLevel4();
	}

	@Entonces("^ingreso stock para el nivel cinco$")
	public void ingreso_Stock_para_Nivel_Cinco() throws Throwable {
		stockProtegidoPage.inpLevel5();
	}

	@Entonces("^selecciono el campo inicio vigencia$")
	public void selecciono_el_campo_inicio_vigencia() throws Throwable {
		stockProtegidoPage.inpFecIni();
		BasePage.WaitSleep(1);
		BasePage.TakeScreenShot();
	}

	@Entonces("^sistema no permite ingresar fecha menor a la actual$")
	public void sistema_no_permite_ingresar_Fecha_menor_a_la_actual() throws Throwable {
		Assert.assertTrue("El sistema permite seleccionar una fecha menor a la actual",
				stockProtegidoPage.VerifyPrevDayDisable());
		BasePage.WaitSleep(1);
		BasePage.TakeScreenShot();
	}

	@Entonces("^ingreso una fecha de inicio mayor a la actual$")
	public void ingreso_una_fecha_de_inicio_mayor_a_la_actual() throws Throwable {
		stockProtegidoPage.inpFecIni();
		BasePage.WaitSleep(1);

		stockProtegidoPage.SeleccionarInicioVigencia(2);
		BasePage.WaitSleep(1);
	}

	@Entonces("^ingreso una fecha de termino menor a la fecha de inicio$")
	public void ingreso_una_fecha_de_termino_Menor_a_la_Fecha_de_Inicio() throws Throwable {
		stockProtegidoPage.inpFecFin();
		BasePage.WaitSleep(1);
		stockProtegidoPage.SeleccionarFinVigencia(0);
		BasePage.WaitSleep(1);
	}

	@Entonces("^ingreso una fecha de termino igual a la actual$")
	public void ingreso_una_fecha_de_termino_igual_a_la_actual() throws Throwable {
		BasePage.WaitSleep(1);
		stockProtegidoPage.inpFecFin();
		stockProtegidoPage.SeleccionarFinVigencia(0);
		BasePage.WaitSleep(1);
	}

	@Entonces("^selecciono la casilla permanente$")
	public void seleccionamos_Checkbox_Permanente() throws Throwable {
		stockProtegidoPage.chkPermanente();
		BasePage.WaitSleep(1);
		BasePage.TakeScreenShot();
	}

	@Entonces("^el campo fecha inicio se deshabilita$")
	public void el_campo_fecha_inicio_se_deshabilita() throws Throwable {
		stockProtegidoPage.TxtFechaInicioIsDisabled();
		BasePage.WaitSleep(1);
		BasePage.TakeScreenShot();
	}

	@Entonces("^el campo fecha de termino se deshabilita$")
	public void el_campo_fecha_de_termino_se_deshabilita() throws Throwable {
		stockProtegidoPage.TxtFechaInicioIsDisabled();
		BasePage.WaitSleep(1);
		BasePage.TakeScreenShot();
	}

	@Entonces("^Validamos mensaje de alerta Debe completar los campos obligatorios$")
	public void validamos_mensaje_de_alerta_Debe_completar_los_campos_obligatorios() throws Throwable {
		stockProtegidoPage.msjObligatorio();
		BasePage.WaitSleep(1);
		BasePage.TakeScreenShot();
	}

	@Entonces("^Se despliega mensaje de alerta para confirmar o cancelar la falta de niveles$")
	public void se_despliega_mensaje_de_alerta_para_confirmar_o_cancelar_la_falta_de_niveles() throws Throwable {
		stockProtegidoPage.msjNiveles();
		BasePage.WaitSleep(1);
		BasePage.TakeScreenShot();
	}

	@Entonces("^Se despliega mensaje de alerta indicando que ya existe una configuración$")
	public void se_despliega_mensaje_de_alerta_indicando_que_ya_existe_una_configuración() throws Throwable {
//	    stockProtegidoPage.msjConfig();
		BasePage.WaitSleep(1);
		BasePage.TakeScreenShot();
	}

	@Entonces("^presiono el boton cancelar$")
	public void presiono_el_boton_cancelar() throws Throwable {
		stockProtegidoPage.btnCancelar();
		BasePage.WaitSleep(1);
		BasePage.TakeScreenShot();
	}

	@Entonces("^presiono el boton confirmar$")
	public void presiono_el_boton_confirmar() throws Throwable {
		stockProtegidoPage.ClickBtnConfirmar();
		BasePage.WaitSleep(1);
		BasePage.TakeScreenShot();
	}

	@Entonces("^se despliega mensaje de alerta confirmando la selección$")
	public void se_despliega_mensaje_de_alerta_confirmando_la_selección() throws Throwable {
		stockProtegidoPage.msjEliminar();
		BasePage.TakeScreenShot();
	}

	@Entonces("^En mensaje emergente se cancela la eliminación$")
	public void en_mensaje_emergente_se_cancela_la_eliminación() throws Throwable {
		stockProtegidoPage.btnCancelaEliminar();
		BasePage.TakeScreenShot();
	}

	@Entonces("^Presionamos el Botón Cancelar$")
	public void presionamos_el_Botón_Cancelar() throws Throwable {
		stockProtegidoPage.btnCancelaEditar();
		BasePage.TakeScreenShot();
	}

	@Entonces("^se muestra mensaje flotante \"([^\"]*)\"$")
	public void se_muestra_mensaje_flotante_x(String mensaje) throws Throwable {
		stockProtegidoPage.ExisteMensajeFlotante(mensaje);
		BasePage.TakeScreenShot();
	}

	@Entonces("^se muestra mensaje de validacion \"([^\"]*)\"$")
	public void se_muestra_mensaje_de_validacion_x(String mensaje) throws Throwable {
		stockProtegidoPage.ExisteMensajeValidacion(mensaje);
		BasePage.TakeScreenShot();
	}

}
