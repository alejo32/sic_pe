package corp.ripley.sicpe.stepdefinition;

import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import corp.ripley.sicpe.model.ESku;
import corp.ripley.sicpe.model.sic.EContador;
import corp.ripley.sicpe.model.sic.EStockXBodega;
import corp.ripley.sicpe.model.sic.ETv_Publish_Products;
import corp.ripley.sicpe.pageobject.sic.JerarquiaPage;
import corp.ripley.sicpe.util.DriverFactory;

import java.util.ArrayList;
import java.util.List;


public class IMSCalculoSteps extends DriverFactory{

	//Calculo de stock ims inicio
	
		private List<EStockXBodega> oListaStockXBodega;
		private String[] respIMS;
		private static ETv_Publish_Products oTv_Publish_Products;
	    private static ESku oDepartamentoProducto;
		private static EContador oContador;

		
		public IMSCalculoSteps() {
			//oProductoStockXbodega = new EStockXBodega();
			oListaStockXBodega = new ArrayList<EStockXBodega>();
			oTv_Publish_Products = new ETv_Publish_Products();
			oContador = new EContador();
		}
		
	

		@Dado("^que el producto\"([^\"]*)\" exista en la tabla de publicados$")
		public void que_el_producto_exista_en_la_tabla_de_publicados(String codigo_venta) throws Throwable {
			oTv_Publish_Products =	database.ObtenerTV_PUBLISH_PRODUCTS(codigo_venta);
			oContador =	database.ObtenerTV_PUBLISH_PRODUCTS_Count(codigo_venta);
			database.ParametrizarPublicados(oTv_Publish_Products.TV_PUBLISH_PRODUCTS_ID,oContador.CONTADOR,codigo_venta);
		}
		
		@Dado("^que en la tabla de metodo de despacho tenga la configuracion requerida \"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\" para el producto\"([^\"]*)\" en la bodega \"([^\"]*)\"$")
		public void que_en_la_tabla_de_metodo_de_despacho_tenga_la_configuracion_requerida_para_el_producto_en_la_bodega(String sts, String dd, String sfs, String pu,String codigo_venta, String codigo_bodega) throws Throwable {
			oDepartamentoProducto =	database.ObtenerDepartamentoProducto(codigo_venta);
			database.ParametrizarAtributo(sts,dd,sfs,pu,oDepartamentoProducto.deptoCode, codigo_bodega);

		}

		
	/*		
		@Dado("^que el metodo de despacho es DD para el producto\"([^\"]*)\" en la bodega \"([^\"]*)\"$")
		public void que_el_metodo_de_despacho_para_el_producto_en_la_bodega_es_DD(String codigo_venta, String codigo_bodega) throws Throwable {
			database.ParametrizarAtributoDD(codigo_venta, codigo_bodega);
		}
		
		@Dado("^que el metodo de despacho es STS para el producto\"([^\"]*)\" en la bodega \"([^\"]*)\"$")
		public void que_el_metodo_de_despacho_es_STS_para_el_producto_en_la_bodega(String codigo_venta, String codigo_bodega) throws Throwable {
			database.ParametrizarAtributoSTS(codigo_venta, codigo_bodega);
		}

		@Dado("^que el metodo de despacho es SFS para el producto\"([^\"]*)\" en la bodega \"([^\"]*)\"$")
		public void que_el_metodo_de_despacho_es_SFS_para_el_producto_en_la_bodega(String codigo_venta, String codigo_bodega) throws Throwable {
			database.ParametrizarAtributoSFS(codigo_venta, codigo_bodega);
		}*/
		
		@Dado("^que el producto\"([^\"]*)\" exista en la tabla del whitelist en la bodega \"([^\"]*)\"$")
		public void que_el_producto_exista_en_la_tabla_del_whitelist_en_la_bodega(String codigo_venta, String codigo_bodega) throws Throwable {
			database.ParametrizarWhitelist(codigo_venta, codigo_bodega);
		}

	
		@Dado("^que el producto\"([^\"]*)\" en la bodega \"([^\"]*)\" tenga valor umbral \"([^\"]*)\" en la tabla de stock ripley$")
		public void que_el_producto_en_la_bodega_tenga_valor_umbral_en_la_tabla_de_stock_ripley(String codigo_venta, String codigo_bodega, String umbral) throws Throwable {
			//database.ParametrizarUmbralStockSkruRipley(codigo_venta,codigo_bodega,umbral);
		}

		

		@Dado("^que el producto\"([^\"]*)\" en la bodega \"([^\"]*)\" tenga valor umbral \"([^\"]*)\" y los metodos \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\",\"([^\"]*)\"  en la tabla de stock ripley$")
		public void que_el_producto_en_la_bodega_tenga_valor_umbral_y_los_metodos_en_la_tabla_de_stock_ripley(String codigo_venta, String codigo_bodega, String umbral, String sts, String dd, String sfs, String pu) throws Throwable {
			database.ParametrizarUmbralStockSkruRipley(codigo_venta,codigo_bodega,umbral,sts,dd,sfs,pu);
		}


	//calculo de stock ims fin
		
		
	
		@Dado("^obtener informacion del restos de bodegas para el producto\"([^\"]*)\"$")
		public void obtener_informacion_del_restos_de_bodegas_para_el_producto(String codigo_venta) throws Throwable {
			oListaStockXBodega = database.ObtenerInformacionStockProductoBD(codigo_venta);
					}
		@Cuando("^voy a la opcion sku_controller$")
		public void voy_a_la_opcion_sku_controller() throws Throwable {
			imsCalculoPage.ClickOpcionConsultaSKU();
		}
		
		@Cuando("^ingresar el producto\"([^\"]*)\" y las credenciales de IMS Peru$")
		public void ingresar_el_producto_y_las_credenciales_de_IMS_Peru(String codigo_venta) throws Throwable {
			imsCalculoPage.IngresarProducto(codigo_venta);
			imsCalculoPage.ClickTriItOut();
			respIMS = imsCalculoPage.ingresarAutenticacionIMS_pe(GetIMSUsuarioRipley_pe(), GeIMSPasswordRipley_pe(),codigo_venta);
		}
		
		
		@Cuando("^ingresar el producto\"([^\"]*)\" y le doy click en el boton TryItOut para enviar las credenciales de IMS$")
		public void ingresar_el_producto_y_le_doy_click_en_el_boton_TryItOut_para_enviar_las_credenciales_de_IMS(String codigo_venta) throws Throwable {
			imsCalculoPage.IngresarProducto(codigo_venta);
			imsCalculoPage.ClickTriItOut();
			respIMS = imsCalculoPage.ingresarAutenticacion(GetIMSUsuarioRipley_cl(), GeIMSPasswordRipley_cl(),codigo_venta);
		}
	
		@Cuando("^Valida que el  stock vendible de sic se haya informado a IMS$")
		public void valida_que_el_stock_vendible_de_sic_se_haya_informado_a_IMS() throws Throwable {
			imsCalculoPage.CompararCalculoStockSicIms(oListaStockXBodega,JerarquiaPage.stock_disponible,JerarquiaPage.stock_Seguridad, JerarquiaPage.stock_Activo,JerarquiaPage.stock_Reserva,JerarquiaPage.stock_vendible, respIMS);

		}
		

		@Cuando("^Valida que el  stock vendible de sic se haya informado a IMS Peru$")
		public void valida_que_el_stock_vendible_de_sic_se_haya_informado_a_IMS_Peru() throws Throwable {
			imsCalculoPage.CompararCalculoStockSicImsPeru(oListaStockXBodega, respIMS);
			
		}
		
		
}
