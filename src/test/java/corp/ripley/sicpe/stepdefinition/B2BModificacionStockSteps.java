package corp.ripley.sicpe.stepdefinition;

import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import corp.ripley.sicpe.pageobject.integraciones.B2BModifStockPage;
import corp.ripley.sicpe.pageobject.sic.JerarquiaPage;
import corp.ripley.sicpe.util.DriverFactory;

public class B2BModificacionStockSteps extends DriverFactory {
	
	@Dado("^ingreso a B2B con perfil Ripley con proveedor \"([^\"]*)\"$")
	public void ingreso_a_B_B_con_perfil_Ripley_con_proveedor(String valor) throws Throwable {			
			
		modificacionstockPage.AbrirB2B();
		modificacionstockPage.Login(GetB2BUsuarioRipley(), GetB2BPasswordRipley());
		modificacionstockPage.selectList_Proveedor(valor); 
		modificacionstockPage.ClickEntrarProv();		
	}

	@Dado("^ingreso a la funcionalidad Mantencion de stock comprometido via archivo$")
	public void ingreso_a_la_funcionalidad_Mantencion_de_stock_comprometido_via_archivo() throws Throwable {
		modificacionstockPage.IrMenuMantencion();

	}
	@Dado("^ingreso a la funcionalidad Modificacion de stock comprometido via archivo$")
	public void ingreso_a_la_funcionalidad_Modificacion_de_stock_comprometido_via_archivo() throws Throwable {
		modificacionstockPage.IrMenu();

	}

	@Cuando("^cargo un archivo XLS con productos que han cambiado de stock \"([^\"]*)\"$")
	public void cargo_un_archivo_XLS_con_productos_que_han_cambiado_de_stock(String ruta) throws Throwable {
		modificacionstockPage.CargarArchivo(ruta);
	}

	@Cuando("^doy click en enviar$")
	public void doy_click_en_enviar() throws Throwable {
		modificacionstockPage.ClickBotonEnviar();
	}
	
	@Entonces("^obtener data de carga$")
	public void optener_data_de_carga() throws Throwable {
		modificacionstockPage.OptenerDataB2B();
			
	}
	
	//Captura los datos de la grilla de B2B en variables publicas para consultar en Auditoria de SIC	
		@Cuando("^realizo la consulta de auditoria de la transacion en SIC$")
		public void realizo_la_consulta_de_auditoria_de_la_transacion_en_SIC() throws Throwable {
		
			auditoriaPage.ConsultarStockBaseEnSIC(JerarquiaPage.nrocod_venta);
			auditoriaPage.ClickConsultar();
			
		}
		
		@Cuando("^realizo la consulta de stock por jerarquia en SIC$")
		public void realizo_la_consulta_de_stock_por_jerarquia_en_SIC() throws Throwable {
			
			jerarquiaPage.ConsultarProductoBodega(B2BModifStockPage.vcod_articulo, B2BModifStockPage.vcod_bodega);
		
		}
		
	@Cuando("^realizo la consulta de auditoria de la transacion \"([^\"]*)\" y \"([^\"]*)\"$")
	public void realizo_la_consulta_de_auditoria_de_la_transacion(String codigo_venta, String bodega) throws Throwable {
		Thread.sleep(1000);
		menuPage.SeleccionarBodega(bodega);
		auditoriaPage.ConsultarStockBaseEnSIC(codigo_venta);
		auditoriaPage.ClickConsultar();
	//	Thread.sleep(1000);
	}


	
	@Entonces("^compara stock base \"([^\"]*)\" con stock disponible de SIC$")
	public void compara_stock_base_con_stock_disponible_de_SIC(String StockExcepcion) throws Throwable {
		modificacionstockPage.CompararStock(StockExcepcion);
	}
	
	@Entonces("^comparar stock base de B2B con stock disponible de SIC$")
	public void comparar_stock_base_de_B_B_con_stock_disponible_de_SIC() throws Throwable {
		modificacionstockPage.CompararStock(B2BModifStockPage.vStock_cantidad);
	}
	@Entonces("^muestra resultado \"([^\"]*)\"$")
	public void muestra_resultado(String mensaje) throws Throwable {
		modificacionstockPage.ValidarMensaje(mensaje);
	}
	
	@Dado("^ingreso a la funcionalidad Modificacion de stock comprometido$")
	public void ingreso_a_la_funcionalidad_Modificacion_de_stock_comprometido() throws Throwable {
		modificacionstockPage.IrMenuForm();
	}

	@Cuando("^busco un sku unico \"([^\"]*)\"$")
	public void busco_un_sku_unico(String codigo) throws Throwable {
		modificacionstockPage.BuscarSku(codigo);

	}
	
	@Cuando("^busco un sku excepcion \"([^\"]*)\" en la bodega \"([^\"]*)\"$")
	public void busco_un_sku_excepcion_en_la_bodega(String codigo, String bodega) throws Throwable {
		modificacionstockPage.BuscarSkuBodegaB2B(codigo, bodega);
	}
	
	

@Cuando("^ingreso una nueva cantidad base comprometida \"([^\"]*)\" y excepcion \"([^\"]*)\"$")
public void ingreso_una_nueva_cantidad_base_comprometida_y_excepcion(String StockBase, String StockExcepcion) throws Throwable {
	modificacionstockPage.IngresarCantBase(StockBase);
	modificacionstockPage.IngresarCantExcepcion(StockExcepcion);
}

	@Cuando("^ingreso una nueva cantidad base comprometida \"([^\"]*)\"$")
	public void ingreso_una_nueva_cantidad_base_comprometida(String cantidad) throws Throwable {
		modificacionstockPage.IngresarCantBase(cantidad);
	}
	
	@Entonces("^graba y muestra resultado \"([^\"]*)\"$")
	public void graba_y_muestra_resultado(String mensaje) throws Throwable {
		modificacionstockPage.irAGuardarYAceptarAlerta(mensaje);
		
	}

}
