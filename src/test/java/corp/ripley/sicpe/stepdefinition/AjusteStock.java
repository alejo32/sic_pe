package corp.ripley.sicpe.stepdefinition;

import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import corp.ripley.sicpe.model.EStock;
import org.junit.Assert;
import corp.ripley.sicpe.util.DriverFactory;

import java.awt.*;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AjusteStock extends DriverFactory {

	private EStock oProductoStockRipley;
	private List<EStock> oListProductoStockRipley;
	private String tipoActualizacion = "";
	private String nombreArchivo;

	public AjusteStock() {

		oListProductoStockRipley = new ArrayList<EStock>();
	}

	//calculo de stock ims inicio 
		
	@Dado("^realizo la consulta de stock disponible \"([^\"]*)\" actual del producto \"([^\"]*)\" en la bodega \"([^\"]*)\"$")
	public void realizo_la_consulta_de_stock_disponible_actual_del_producto_en_la_bodega(String tipo_inventario, String codigo_venta, String codigo_bodega) throws Throwable {
		menuPage.SeleccionarBodega(codigo_bodega);
		ajusteStockPage.ConsultarStock(codigo_venta,tipo_inventario);
		int stockActual = Integer.parseInt(ajusteStockPage.ObtenerStockActual());
		
	}

	@Dado("^realizo ajuste ingresando el stock a ajustar \"([^\"]*)\" en la bodega \"([^\"]*)\"$")
	public void realizo_ajuste_ingresando_el_stock_a_ajustar(String stockAjuste, String codigo_bodega) throws Throwable {
		//menuPage.SeleccionarBodega(codigo_bodega);
		ajusteStockPage.IngresarAjuste(stockAjuste);
		ajusteStockPage.IngresarObservacion("Prueba calculo de stock IMS automatizado=");
		ajusteStockPage.ClickActualizar();
		
   	
	}
	

	


	@Dado("^realizo la consulta de stock vendible por jerarquia en SIC para el producto \"([^\"]*)\"$")
	public void realizo_la_consulta_de_stock_vendible_por_jerarquia_en_SIC_para_el_producto(String codigo_venta) throws Throwable {
		jerarquiaPage.ConsultarStockVendible(codigo_venta);
	}
	
	//calculo de stock ims fin 
	@Cuando("^selecciono una bodega al azar$")
	public void selecciono_una_bodega_al_azar() throws IOException, InterruptedException, SQLException, AWTException {
		menuPage.SeleccionarBodega(database.ObtenerProductoStockRipley("1", "1").bodega);
	}

	@Dado("^tengo informacion de un producto con tipo de inventario \"([^\"]*)\"$")
	public void tengo_informacion_de_un_producto_con_tipo_de_inventario(String tipoInventario)
			throws IOException, InterruptedException, SQLException {
		oProductoStockRipley = database.ObtenerProductoStockRipley("1", tipoInventario);
	}

	@Dado("^tengo informacion de stock de \"([^\"]*)\" productos de la bodega \"([^\"]*)\"$")
	public void tengo_informacion_de_stock_de_x_productos(String cantidad, String bodega)
			throws IOException, InterruptedException, SQLException {
		oListProductoStockRipley = database.ObtenerListaProductoStockRipley(cantidad, bodega);
	}



	@Dado("^genero un archivo de carga masiva de ajuste de stock acotado$")
	public void genero_un_archivo_de_carga_masiva_de_ajuste_de_stock_acotado() throws Throwable {
		SetTipoAjuste("MDADJUST");

		String mdadjustPathLocal = GetRutaCarpetaLocalMDADJUST();

		System.out.println("Ruta local destino de achivo: " + mdadjustPathLocal);
		System.out.println("getListaProductoStockRipley CANTIDAD DE PRODUCTOS :" + oListProductoStockRipley.size());

		//oListaStockProductos = database.ObtenerListaProductoStockRipley("10");

		nombreArchivo = archivo.CrearArchivoMDADJUST(mdadjustPathLocal, ".csv", oListProductoStockRipley, true, true, true, true);
		System.out.println("NOMBRE ARCHIVO:" + nombreArchivo);
	}

	@Dado("^tengo informacion de un producto sin stock$")
	public void tengo_informacion_de_un_producto_sin_stock() throws IOException, InterruptedException, SQLException {
		oProductoStockRipley = database.ObtenerProductoSinStockRipley();
	}

	@Dado("^tengo informacion de un producto con stock negativo$")
	public void tengo_informacion_de_un_producto_con_stock_negativo()
			throws IOException, InterruptedException, SQLException {
		oProductoStockRipley = database.ObtenerProductoStockRipleyNegativo("1");
	}

	@Dado("^tengo informacion de un producto con stock positivo$")
	public void tengo_informacion_de_un_producto_con_stock_positivo()
			throws IOException, InterruptedException, SQLException {
		oProductoStockRipley = database.ObtenerProductoStockRipleyPositivo("1");
	}

	@Dado("^tengo informacion de un producto con stock cero$")
	public void tengo_informacion_de_un_producto_con_stock_cero()
			throws IOException, InterruptedException, SQLException {
		oProductoStockRipley = database.ObtenerProductoStockRipleyCero("1");
	}

	@Dado("^doy click a limpiar$")
	public void doy_click_a_limpiar() throws IOException, InterruptedException, SQLException {
		ajusteStockPage.Limpiar();
	}

	@Cuando("^ingreso codigo venta \"([^\"]*)\"$")
	public void ingreso_codigo_de_venta(String codigoVenta) throws AWTException, InterruptedException {
		ajusteStockPage.IngresarCodigoVenta(codigoVenta);
	}

	@Dado("^realizo la consulta de stock actual$")
	public void realizo_la_consulta_de_stock_actual() throws InterruptedException, AWTException {

		if (!oProductoStockRipley.bodega.equals(""))
			menuPage.SeleccionarBodega(oProductoStockRipley.bodega);
		ajusteStockPage.ConsultarStock(oProductoStockRipley.codigoVenta, oProductoStockRipley.tipoInventario);
	}

	@Dado("^se muestra el stock actual$")
	public void se_muestra_el_stock_actual() {

		int stockActual = Integer.parseInt(ajusteStockPage.ObtenerStockActual());

		Assert.assertTrue("Los valores no coinciden",
				ajusteStockPage.ValidarStock(stockActual, oProductoStockRipley.stock));
	}

	@Dado("^selecciono tipo de actualizacion \"([^\"]*)\"$")
	public void selecciono_tipo_de_actualizacion(String tipoActualizacion)
			throws IOException, InterruptedException, SQLException, AWTException {
		this.tipoActualizacion = tipoActualizacion;
		switch (tipoActualizacion) {
		case "Incrementar":
			ajusteStockPage.SeleccionarIncrementar();
			break;
		case "Disminuir":
			ajusteStockPage.SeleccionarDisminuir();
			break;
		case "Reemplazar":
			ajusteStockPage.SeleccionarReemplazar();
			break;
		}
	}

	@Cuando("^ingreso el stock a ajustar \"([^\"]*)\"$")
	public void ingreso_el_stock_a_ajustar(String stockAjuste) throws AWTException, InterruptedException {

		ajusteStockPage.IngresarAjuste(stockAjuste);
	}
	@Cuando("^ingreso la observacion \"([^\"]*)\"$")
	public void ingreso_la_observacion(String observacion) throws Throwable {
		ajusteStockPage.IngresarObservacion(observacion);
	}

	@Entonces("^se actualiza el valor del campo nuevo stock$")
	public void se_actualiza_el_valor_del_campo_nuevo_stock() {

		String operacion = tipoActualizacion;

		int stockActual = oProductoStockRipley.stock;
		int stockAjustar = Integer.parseInt(ajusteStockPage.ObtenerStockAjuste());
		int nuevoStockEsperado = operacion.equals("Incrementar") ? stockActual + stockAjustar
				: (operacion.equals("Disminuir") ? stockActual - stockAjustar : stockAjustar);

		int nuevoStockMostrado = Integer.parseInt(ajusteStockPage.ObtenerNuevoStock());
				System.out.println("Stock Actual: " + stockActual);
		        Assert.assertTrue("Los valores no coinciden",
				ajusteStockPage.ValidarStock(nuevoStockEsperado, nuevoStockMostrado));
	}

	@Entonces("^realizo click en Actualizar$")
	public void realizo_click_en_Actualizar() throws Throwable {
		ajusteStockPage.ClickActualizar();
	}

	@Cuando("^selecciono tipo de inventario \"([^\"]*)\"$")
	public void selecciono_tipo_inventario(String tipoInventario) throws InterruptedException, AWTException {
		ajusteStockPage.SeleccionarTipoInventario(tipoInventario);
	}

	@Cuando("^doy click a consultar$")
	public void doy_click_a_consultar() throws InterruptedException, AWTException {
		ajusteStockPage.Consultar();
	}

	@Cuando("^doy clic a actualizar$")
	public void doy_click_a_actualizar() throws InterruptedException {
		ajusteStockPage.ClickActualizar();
	}

	@Cuando("^no completo el campo ajuste$")
	public void no_completo_el_campo_ajuste() {
	}

	@Cuando("^completo el campo observacion$")
	public void completo_el_campo_observacion() throws AWTException, InterruptedException {
		ajusteStockPage.IngresarObservacion("QA test automation 01");
	}

	@Entonces("^se muestra mensaje \"([^\"]*)\"$")
	public void se_muestra_mensaje(String mensaje)
			throws IOException, InterruptedException, SQLException, AWTException {
		Assert.assertTrue("No se encontró el mensaje de error", ajusteStockPage.ExisteMensajeError(mensaje));
	}

	@Entonces("^el boton actualizar esta deshabilitado$")
	public void el_boton_consultar_esta_deshabilitado() throws IOException, InterruptedException {
		Assert.assertTrue("El botón Actualizar esta habilitado",
				ajusteStockPage.ExisteBotonDeshabilitado("Actualizar"));
	}

	@Entonces("^se marca en rojo el campo ajuste$")
	public void se_marca_en_rojo_el_campo_ajuste()
			throws IOException, InterruptedException, SQLException, AWTException {
		Assert.assertTrue("El campo ajuste no se marca en rojo", ajusteStockPage.ValidarCampoAjusteError());
	}

	@Entonces("^se marca en rojo el campo observacion$")
	public void se_marca_en_rojo_el_campo_observacion()
			throws IOException, InterruptedException, SQLException, AWTException {
		Assert.assertTrue("El campo observación no se marca en rojo", ajusteStockPage.ValidarCampoObservacionError());
	}

	@Entonces("^los campos vuelven a su valor inicial$")
	public void los_campos_vuelven_a_su_valor_inicial()
			throws IOException, InterruptedException, SQLException, AWTException {
		Assert.assertTrue("El campo Cod.Venta no limpió su valor", ajusteStockPage.ObtenerCodigoVenta().equals(""));
		Assert.assertTrue("El campo Ajuste no limpió su valor", ajusteStockPage.ObtenerStockAjuste().equals(""));
		Assert.assertTrue("El campo StockActual no limpió su valor", ajusteStockPage.ObtenerStockActual().equals("0"));
		Assert.assertTrue("El campo NuevoStock no limpió su valor", ajusteStockPage.ObtenerNuevoStock().equals("0"));

		String opcionTipoProductoSeleccionada = ajusteStockPage.ObtenerTextoComboTipoProducto();
		Assert.assertTrue("El combo TipoProducto no tiene por defecto la opcion 'Seleccione'",
				opcionTipoProductoSeleccionada.equals("1-Ripley"));

		String opcionTipoInventarioSeleccionada = ajusteStockPage.ObtenerTextoComboTipoInventario();
		Assert.assertTrue("El combo TipoProducto no tiene por defecto la opcion '1-Ripley'",
				opcionTipoInventarioSeleccionada.equals("Seleccione"));
	}

}
