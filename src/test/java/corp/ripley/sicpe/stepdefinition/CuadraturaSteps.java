package corp.ripley.sicpe.stepdefinition;

import io.cucumber.java.es.Cuando;
import io.cucumber.java.es.Entonces;
import org.junit.Assert;
import corp.ripley.sicpe.util.DriverFactory;

import java.awt.*;
import java.io.IOException;

public class CuadraturaSteps extends DriverFactory {

	@Cuando("^doy clic en el ultimo reporte de cuadratura$")
	public void doy_clic_en_el_ultimo_reporte_de_cuadratura() throws IOException, InterruptedException, AWTException {

		cuadraturaPage.DescargarUltimoReporte();
	}

	@Entonces("^se descarga el reporte$")
	public void se_descarga_el_reporte() throws IOException, InterruptedException, AWTException {
		String rutaDescargas = GetRutaCarpetaLocalDescargas();

		String fileName = cuadraturaPage.ObtenerNombreUltimoReporte();
		Thread.sleep(20000);
		Assert.assertTrue("No se encontró el archivo", archivo.ValidarDescargaArchivo(rutaDescargas, fileName));
	}

	@Entonces("^valido que se muestren los ultimos registros$")
	public void valido_que_se_muestren_los_x_ultimos_registros() throws InterruptedException, AWTException {
		int numeroPaginas = cuadraturaPage.ObtenerNumeroPaginas();
		System.out.println("NUMERO DE PAGINAS:" + numeroPaginas);
		int contadorReportes = 0;
		for (int i = 1; i <= numeroPaginas; i++) {
			cuadraturaPage.IrPagina(i);
			int numeroReportes = cuadraturaPage.ObtenerNumeroReportesPorPagina();
			System.out.println("# de reportes encontrados en la pagina " + i + " :" + numeroReportes);
			contadorReportes = contadorReportes + numeroReportes;
		}
		Assert.assertTrue("No se muestran los ultimos 30 registros", contadorReportes == 30);
	}

	
}
