package corp.ripley.sicpe.stepdefinition;

import io.cucumber.java.After;
import io.cucumber.java.AfterStep;
import io.cucumber.java.Before;
import corp.ripley.sicpe.util.DriverFactory;
import io.cucumber.java.Scenario;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriverException;

public class MasterHooks extends DriverFactory {

	public Scenario scenario;
	public static String nombreTest;

	@Before
	public void setup(Scenario scenario) {
		this.scenario = scenario;
		nombreTest = scenario.getName();
		driver = getDriver();
	}

	@After
	public void tearDown() throws InterruptedException {
		if(scenario.isFailed()) {
			try {
				scenario.log("Current Page URL is " + driver.getCurrentUrl());
				final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
				scenario.attach(screenshot, "image/png", nombreTest);
			} catch (WebDriverException somePlatformsDontSupportScreenshots) {
				System.err.println(somePlatformsDontSupportScreenshots.getMessage());
			}
		}

		try {
			if(driver != null) {
				driver.manage().deleteAllCookies();
				driver.quit();
				driver = null;
			}
		} catch (Exception e) {
			System.out.println("Fallo en ejecución: tearDown, Exception: " + e.getMessage());
		}
	}

	@AfterStep
	public void screenshot(Scenario scenario){
		scenario.log("Current Page URL is " + driver.getCurrentUrl());
		byte[] screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
		scenario.attach(screenshot, "image/png", nombreTest);
	}

}