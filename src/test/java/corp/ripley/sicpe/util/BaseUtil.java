package corp.ripley.sicpe.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class BaseUtil {

	private static String formatoFecha1 = Constant.FORMATO_FECHA1;

	public int GenerarRandom(int valorMaximo, int valorMinimo) {
		Random random = new Random();
		return random.nextInt(valorMaximo - valorMinimo) + valorMinimo;
	}

	public String GenerarCUD() {
		return "00040050" + ObtenerFechaActual(formatoFecha1);
	}

	public int GenerarRandom(int valor) {
		Random random = new Random();
		return random.nextInt(valor);
	}

	public String ObtenerFechaActual(String formatoFecha) {
		System.out.println("FORMATO FECHA:_____"+formatoFecha);
		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat(formatoFecha);
		return dateFormat.format(date);
	}

	public String GenerarNombreArchivo(String prefijo, String extension) {
		int numeroRandom = GenerarRandom(99999, 10000);
		String fechaActual = ObtenerFechaActual(formatoFecha1);

		return prefijo + fechaActual + String.valueOf(numeroRandom) + extension;
	}

	public String GenerarIDTRX(String prefijo) {
		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat(formatoFecha1);
		return prefijo + dateFormat.format(date);
	}

}
