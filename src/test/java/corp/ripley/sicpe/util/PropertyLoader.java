package corp.ripley.sicpe.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyLoader {

	public static String GetProperty(String propertyName) {
		try {
			Properties prop = new Properties();
			InputStream input = null;
			input = new FileInputStream("parametros.properties");
			prop.load(input);
			return prop.getProperty(propertyName).replace("\"", "").replace(";", "");

		} catch (IOException e) {
			e.printStackTrace();
		}
		return "";
	}

}
