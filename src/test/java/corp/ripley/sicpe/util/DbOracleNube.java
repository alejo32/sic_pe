package corp.ripley.sicpe.util;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;

import corp.ripley.sicpe.model.EConexionBD;
import corp.ripley.sicpe.model.ESku;

public class DbOracleNube {


	public void ConnectarTunelSSH(EConexionBD oEntidad) throws IOException, SQLException {
		System.out.println("-----------------------------------------------ConnectarTunelSSH");
		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat(Constant.FORMATO_FECHA2);
		System.out.println("Inicio: " + dateFormat.format(date));

		System.out.println("Conectandose a tunel SSH:" + oEntidad.sshUser + "| ********* |" + oEntidad.sshHost + "|"
				+ oEntidad.sshPort + "|" + oEntidad.dbHost + "|" + oEntidad.dbPort + "|" + oEntidad.dbUser
				+ "| *********");
		OracleHelper oracleHelper = new OracleHelper();
		OracleHelper.setConnection(
				oracleHelper.ConnectDbWithTunnel(oEntidad.sshUser, oEntidad.sshPassword, oEntidad.sshHost,
						oEntidad.sshPort, oEntidad.dbHost, oEntidad.dbPort, oEntidad.dbUser, oEntidad.dbPassword));
		
		System.out.println("OK_OKOKOKOKOKOKO");
	}

	public ESku ObtenerDatosPRUEBA(String codigoVenta, String codigoPais)
			throws IOException, InterruptedException, SQLException {

		ESku oEntidad = new ESku();
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;

			String sqlQuery = "";

			System.out.println(sqlQuery);
			rs = stmt.executeQuery(sqlQuery);
			while (rs.next()) {
				oEntidad.sku = rs.getString("SKU");
				
			}
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerDatosPRUEBA()");
			System.err.println(e.getMessage());
		}
		return oEntidad;
	}


	public Connection ConnectSIC() {
		return OracleHelper.getConnection();

	}

}
