package corp.ripley.sicpe.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class Zipper {

	File zip;
	ZipOutputStream output;

	public Zipper(File zip) throws FileNotFoundException {
		this.output = new ZipOutputStream(new FileOutputStream(zip));
	}

	private boolean zipFile(File file) {
		try {
			byte[] buf = new byte[1024];
			output.putNextEntry(new ZipEntry(file.getName()));
			FileInputStream fis = new FileInputStream(file);
			int len;
			while ((len = fis.read(buf)) > 0) {
				output.write(buf, 0, len);
			}
			fis.close();
			output.closeEntry();
			return true;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	private boolean zipDir(File file) {
		try {
			output.putNextEntry(new ZipEntry(file.getPath() + File.pathSeparator));
			output.closeEntry();
			return true;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	private boolean add(List<File> files) {
		for (File file : files) {
			zipFile(file);
		}
		return true;
	}

	public void zip(List<File> oLista) throws IOException {
		add(oLista);
		output.finish();
		output.close();
	}

}
