package corp.ripley.sicpe.util;

import corp.ripley.sicpe.pageobject.aws.SQSPage;
import corp.ripley.sicpe.pageobject.integraciones.B2BModifStockPage;
import corp.ripley.sicpe.pageobject.sic.*;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class DriverFactory {
	public static WebDriver driver;
	public static WebDriverWait wait;
	public static MenuPage menuPage;
	public static LoginPage loginPage;
	public static AuditoriaPage auditoriaPage;
	public static JerarquiaPage jerarquiaPage;
	public static CargaStockPage cargaStockPage;
	public static CuadraturaPage cuadraturaPage;
	public static AjusteStockPage ajusteStockPage;
	public static ConfiguracionPage configuracionPage;
	public static corp.ripley.sicpe.pageobject.aws.LoginPage awsLoginPage;
	public static SQSPage awsSQSPage;
	public static StockProtegidoPage	stockProtegidoPage;
	public static DatabaseUtil database;
	public static DbOracleNube oracleDb;
	public static JsonUtil tramaJson;
	public static ApiUtil servicio;
	public static ArchivosUtil archivo;
	public static SFTPClientUtil sftpClient;
	public static B2BModifStockPage modificacionstockPage;
	public static IMSCalculoPage imsCalculoPage;

	public WebDriver getDriver() {

		String webdriver = System.getProperty("browser", "chrome");

		try {

			switch (webdriver) {
				case "firefox":
					if (null == driver) {
						WebDriverManager.firefoxdriver().setup();
						//FirefoxOptions fo = new FirefoxOptions();
						//fo.addArguments("--headless");
						driver = new FirefoxDriver();
					}
					break;
				case "chrome":
					if (null == driver) {
						WebDriverManager.chromedriver().setup();
						ChromeOptions co = new ChromeOptions();
						//co.addArguments("--headless");
						co.addArguments("--disable-gpu");
						co.addArguments("--remote-debugging-port=9222");
						co.addArguments("--start-maximized");
						co.addArguments("--ignore-certificate-errors");
						co.addArguments("--disable-popup-blocking");
						co.addArguments("--window-size=1920,1080");
						co.addArguments("--incognito");
						driver = new ChromeDriver();
						driver.manage().window().maximize();
						driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
					}
					break;
				case "ie":
					if (null == driver) {
						// Todo
					}
					break;
				case "remote":
					if (null == driver) {
						String host = "10.0.149.90";
						String completeUrl = "http://" + host + ":4444";

						WebDriverManager.chromedriver().setup();
						ChromeOptions cop = new ChromeOptions();
						//cop.addArguments("--headless");
						cop.addArguments("--disable-gpu");
						cop.addArguments("--start-maximized");
						cop.addArguments("--no-sandbox");
						cop.addArguments("--ignore-certificate-errors");
						cop.addArguments("--disable-popup-blocking");
						//cop.addArguments("--window-size=1920,1080");
						cop.addArguments("--disable-dev-shm-usage");
						cop.addArguments("--incognito");

						try {
							driver = new RemoteWebDriver(new URL(completeUrl), cop);
							driver.manage().window().maximize();
							driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
						} catch (MalformedURLException e) {
							e.printStackTrace();
						}
					}
					break;
			}
		} catch (Exception e) {
			System.out.println("No se cargo browser:" + e.getMessage());
		} finally {
			driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
			
			wait =  new WebDriverWait(driver, 60);
			loginPage = PageFactory.initElements(driver, LoginPage.class);
			menuPage = PageFactory.initElements(driver, MenuPage.class);
			cuadraturaPage = PageFactory.initElements(driver, CuadraturaPage.class);
			auditoriaPage = PageFactory.initElements(driver, AuditoriaPage.class);
			jerarquiaPage = PageFactory.initElements(driver, JerarquiaPage.class);
			ajusteStockPage = PageFactory.initElements(driver, AjusteStockPage.class);
			stockProtegidoPage	= PageFactory.initElements(driver, StockProtegidoPage.class);
			cargaStockPage = PageFactory.initElements(driver, CargaStockPage.class);
//			cargaProtegidoPage	= PageFactory.initElements(driver, CargaProtegidoPage.class);
			configuracionPage = PageFactory.initElements(driver, ConfiguracionPage.class);
			modificacionstockPage = PageFactory.initElements(driver, B2BModifStockPage.class); 
			imsCalculoPage = PageFactory.initElements(driver, IMSCalculoPage.class); 
			awsLoginPage = PageFactory.initElements(driver, corp.ripley.sicpe.pageobject.aws.LoginPage.class);
			awsSQSPage = PageFactory.initElements(driver, SQSPage.class);
			database=new DatabaseUtil();
			oracleDb=new DbOracleNube();
			tramaJson=new JsonUtil();
			servicio= new ApiUtil();
			archivo= new ArchivosUtil();
			sftpClient= new SFTPClientUtil();
			
		}
		return driver;
	}

	public String GetBrowser() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("browser");
	}

	public String GetSICUrl() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("sic_url");
	}
	
	
	
	//Calculo stock IMS inicio
	public String GetSICUrl_CL() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("sic_url_cl");
	}
	
	public String GetIMSUrl_cl() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("ims_url_cl");
	}
	
	public String GetIMSUsuarioRipley_cl() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("ims_usuario_cl");
	}

	public String GeIMSPasswordRipley_cl() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("ims_password_cl");
	}
	
	
	public String GetIMSUrl_pe() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("ims_url_pe");
	}
	
	public String GetIMSUsuarioRipley_pe() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("ims_usuario_pe");
	}

	public String GeIMSPasswordRipley_pe() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("ims_password_pe");
	}
	//Calculo stock IMS fin
	
	public String GetAWSUrl() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("aws_url");
	}
	
	public String GetB2BUrl() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("b2b_url");
	}
	
	public String GetB2BUsuarioRipley() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("b2b_usuario_ripley");
	}

	public String GetB2BPasswordRipley() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("b2b_password_ripley");
	}
	
	public String GetSICUsuarioAdministrador() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("sic_usuario_administrador");
	}

	public String GetSICPasswordAdministrador() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("sic_password_administrador");
	}
	
	public String GetVPN_Usuario() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("VPN_Usuario");
	}
	public String GetVPN_Password() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("VPN_Pasword");
	}
	
	//Calculo de stock IMS inicio
	public String GetSICUsuarioAdministradorCL() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("sic_usuario_administrador_cl");
	}
	public String GetSICPasswordAdministradorCL() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("sic_password_administrador_cl");
	}
	public String GetUsuarioIMS() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("ims_usuario");
	}

	public String GetPasswordIMS() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("ims_password");
	}
	//calculo de stock IMS fin
	public String GetSICUsuarioEstandar() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("sic_usuario_estandar");
	}

	public String GetSICPasswordEstandar() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("sic_password_estandar");
	}

	public String GetSICCodigoPais() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("codigo_pais");
	}

	public String GetAWSAccountId() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("aws_accountId");
	}

	public String GetAWSUsuario() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("aws_username");
	}

	public String GetAWSPassword() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("aws_password");
	}

	public String GetAWSUrlMensajeSQSAjuste() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("aws_mensaje_SQS_ajuste");
	}

	public String GetAWSUrlMensajeSQSIncrementalProductos() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("aws_mensaje_SQS_incremental_productos");
	}

	public String GetSFTPHost() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("sftp_host");
	}

	public String GetSFTPPort() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("sftp_port");
	}

	public String GetSFTPUser() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("sftp_user");
	}

	public String GetSFTPPassword() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("sftp_password");
	}

	public String GetSSHUser() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("sic_ssh_user");
	}

	public String GetSSHPpk() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("sic_ssh_ppk");
	}

	public String GetSSHHost() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("sic_ssh_host");
	}

	public String GetSSHPort() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("sic_ssh_port");
	}

	public String GetDBHost() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("sic_db_host");
	}

	public String GetDBPort() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("sic_db_port");
	}

	public String GetDBUser() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("sic_db_user");
	}

	public String GetDBPassword() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("sic_db_password");
	}
	
	//Conexion a la base de datos de Chile para el Calculo IMS Inicio
	public String GetDBHost_CL() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("sic_db_host_cl");
	}

	public String GetDBPort_CL() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("sic_db_port_cl");
	}

	public String GetDBUser_CL() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("sic_db_user_cl");
	}

	public String GetDBPassword_CL() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("sic_db_password_cl");
	}
	//Conexion a la base de datos de Chile para el Calculo IMS Fin
	
	public String GetDBHostOracleNube() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("oracle_nube_db_host");
	}

	public String GetDBPortOracleNube() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("oracle_nube_db_port");
	}

	public String GetDBUserOracleNube() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("oracle_nube_db_user");
	}

	public String GetDBPasswordOracleNube() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("oracle_nube_db_password");
	}
	

	public String GetAPIConnectAjuste() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("url_api_connect_ajuste");
	}

	public String GetAPIWrapperAjuste() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("url_wrapper_ajuste");
	}

	public String GetAPIConnectSFULL() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("url_api_connect_sfull");
	}

	public String GetAPIWrapperSFULL() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("url_wrapper_sfull");
	}

	public String GetRutaCarpetaLocalDescargas() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("ruta_descarga_archivos");
	}

	public String GetRutaCarpetaLocalSFULL() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("sftp_sfull_path_local");
	}

	public String GetRutaCarpetaLocalMDADJUST() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("mdadjust_path_local");
	}
	public String GetRutaCarpetaLocalCMSP() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("cmsp_path_local");
	}

	public String GetRutaCarpetaRemotaSFULL() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("sftp_sfull_path_destino");
	}

	public String GetRutaCarpetaRemoto() {
		ReadConfigFile file = new ReadConfigFile();
		return file.GetProperty("mdadjust_path_remoto");
	}
	private String tipoAjuste;

	public String GetTipoAjuste() {
		return tipoAjuste;
	}

	public void SetTipoAjuste(String tipoAjuste) {
		this.tipoAjuste = tipoAjuste;
	}
}
