package corp.ripley.sicpe.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.JSchException;

public class MySqlDbHelper {

	private static Connection conn;
	private boolean done = false;

	public static void setConnection(Connection conn) {
		MySqlDbHelper.conn = conn;
	}

	public static Connection getConnection() {
		return conn;
	}

	public static void closeConnection() throws SQLException {
		conn.close();
	}

	
	public Connection ConnectDbWithTunnel(String strSshUser, String strSshPassword, String strSshHost, int nSshPort,
			String strRemoteHost, int nRemotePort, String strDbUser, String strDbPassword) throws SQLException {
		Connection conn = null;

		try {

//	      int nLocalPort = 3324;         
//	      try {
//	      doSshTunnel(strSshUser, strSshPassword, strSshHost, nSshPort, strRemoteHost, nLocalPort, nRemotePort);
//	      }catch(Exception ex) {
//	    	  System.out.println("OCURRIO UN ERROR: "+ex.getMessage());
//	      }
			done = true;
			Class.forName("com.mysql.cj.jdbc.Driver");
			String connectionString = "jdbc:mysql://" + strRemoteHost + ":" + nRemotePort + "/SIC";
			System.out.println("CONNECTION_STRING: " + connectionString);
			conn = DriverManager.getConnection(connectionString, strDbUser, strDbPassword);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return conn;
	}
//Conexion a la base de datos de Chile para Calculo IMS inicio
	
	public Connection ConnectBDSIC_CL(String strRemoteHost, int nRemotePort, String strDbUser, String strDbPassword) throws SQLException {
		Connection conn = null;

		try {
			done = true;
			Class.forName("com.mysql.cj.jdbc.Driver");
			String connectionString = "jdbc:mysql://" + strRemoteHost + ":" + nRemotePort + "/SIC";
			System.out.println("CONNECTION_STRING: " + connectionString);
			conn = DriverManager.getConnection(connectionString, strDbUser, strDbPassword);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return conn;
	}
	
	public Connection ConnectBDSIC(String strRemoteHost, int nRemotePort, String strDbUser, String strDbPassword) throws SQLException {
		Connection conn = null;

		try {
			done = true;
			Class.forName("com.mysql.cj.jdbc.Driver");
			String connectionString = "jdbc:mysql://" + strRemoteHost + ":" + nRemotePort + "/SIC";
			System.out.println("CONNECTION_STRING: " + connectionString);
			conn = DriverManager.getConnection(connectionString, strDbUser, strDbPassword);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return conn;
	}
//Conexion a la base de datos de Chile para Calculo IMS fin
	
	
	private void doSshTunnel(String strSshUser, String sshKeyFilePath, String strSshHost, int nSshPort,
			String strRemoteHost, int nLocalPort, int nRemotePort) throws JSchException {
		final JSch jsch = new JSch();
		Session session = jsch.getSession(strSshUser, strSshHost, 22);

		jsch.addIdentity(sshKeyFilePath);
		final Properties config = new Properties();
		config.put("StrictHostKeyChecking", "no");
		config.put("ConnectionAttempts", "3");
		session.setConfig(config);

		session.connect();
		session.setPortForwardingL(nLocalPort, strRemoteHost, nRemotePort);

	}

	public void CloseDbConnection(Connection conn) throws SQLException {
		try {
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			System.exit(0);
		}
	}

}
