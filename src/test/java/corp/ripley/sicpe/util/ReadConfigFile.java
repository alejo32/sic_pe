package corp.ripley.sicpe.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ReadConfigFile {
	protected InputStream input = null;
	protected Properties prop = null;

	public ReadConfigFile() {

	}

	public String GetProperty(String propertyName) {
		String propiedad="";
		try {
			Properties prop = new Properties();
			InputStream input = null;
			input = new FileInputStream(Constant.CONFIG_PROPERTIES_DIRECTORY);
			prop.load(input);
			propiedad = prop.getProperty(propertyName).replace("\"", "").replace(";", "");
			
		} catch (IOException ex) {
				
		}
		return propiedad;
	}

}
