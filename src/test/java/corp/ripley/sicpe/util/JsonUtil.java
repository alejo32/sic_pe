package corp.ripley.sicpe.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import corp.ripley.sicpe.model.ESku;
import corp.ripley.sicpe.model.ajusteinventario.EAjusteInventario;
import corp.ripley.sicpe.model.ajusteinventario.EAjusteInventarioSku;
import corp.ripley.sicpe.model.incrementalProducto.EIncrementalProducto;
import corp.ripley.sicpe.model.incrementalProducto.EIncrementalProductoMessageHeader;
import corp.ripley.sicpe.model.incrementalProducto.EIncrementalProductoSku;
import corp.ripley.sicpe.model.sincronizacionfull.ESFull;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class JsonUtil {

	BaseUtil util;

	public JsonUtil() {
		util = new BaseUtil();
	}

	public String ConvertirAJson(Object object) throws JsonProcessingException {
		ObjectMapper objectMapper = new ObjectMapper();
		String jsonString = objectMapper.writeValueAsString(object);
		System.out.println("-----------------------------------------------");
		System.out.println("TRAMA JSON GENERADA:");
		System.out.println(jsonString);
		System.out.println("-----------------------------------------------");
		return jsonString;
	}
	public String GenerarTramaJsonAjusteInventarioSkuNoExiste(List<ESku> productos, String sistemaOrigen, String countryCode,
												   String transactionType, String user, String operacion, String documentType, boolean stockIgualAlActual)
			throws JsonProcessingException {

		EAjusteInventario oCabecera = new EAjusteInventario();

		String fecha = util.ObtenerFechaActual(Constant.FORMATO_FECHA2);

		oCabecera.transactionId = util.GenerarIDTRX("AUT_ADJUST_");
		oCabecera.transactionType = transactionType;
		oCabecera.originSystem = sistemaOrigen;
		oCabecera.masterReport = "masterReport01";
		oCabecera.masterDetail = "masterDetail01";
		oCabecera.user = user;
		oCabecera.registration = fecha;
		oCabecera.country = countryCode;

		List<EAjusteInventarioSku> oDetalleList = new ArrayList<EAjusteInventarioSku>();
		int contadorDetalle = 0;
		for (ESku producto : productos) {
			EAjusteInventarioSku oDetalle = new EAjusteInventarioSku();
			oDetalle.idtrx = transactionType.equals("ADJUST") ? (util.GenerarIDTRX("idtrx") + contadorDetalle) : "";
			oDetalle.sku = "3330000091308";
			oDetalle.fulfillment = producto.bodega;
			oDetalle.inventoryType = producto.tipoInventario;
			oDetalle.stock = stockIgualAlActual ? producto.iStock : util.GenerarRandom(50, 10);
			oDetalle.operation =operacion;
			//oDetalle.operation = transactionType.equals("ADJUST")
			//		? (operacion.equals("") ? (producto.esImpar.equals("1") ? "A" : "S") : operacion)
			//		: "";
			oDetalle.cud = util.GenerarCUD();
			oDetalle.fixInventory = "N";
			oDetalle.transaction_doc = documentType;
			oDetalle.referenceId = "Reference01";
			oDetalle.referenceId2 = "Reference02";
			oDetalle.referenceId3 = "Reference03";
			oDetalle.originProcessDate = fecha;
			oDetalle.originDescription = "AJUSTE DE STOCK VIA SERVICIO SQS";
			oDetalle.comercialDate = fecha;
			oDetalleList.add(oDetalle);
			System.out.println("PRODUCTO ENCONTRADO:" + oDetalle.sku);
			contadorDetalle++;
		}

		oCabecera.skus = oDetalleList;

		return ConvertirAJson(oCabecera);
	}
	public String GenerarTramaJsonAjusteInventario(List<ESku> productos, String sistemaOrigen, String countryCode,
                                                   String transactionType, String user, String operacion, String documentType, boolean stockIgualAlActual)
			throws JsonProcessingException {

		EAjusteInventario oCabecera = new EAjusteInventario();

		String fecha = util.ObtenerFechaActual(Constant.FORMATO_FECHA2);

		oCabecera.transactionId = util.GenerarIDTRX("AUT_ADJUST_");
		oCabecera.transactionType = transactionType;
		oCabecera.originSystem = sistemaOrigen;
		oCabecera.masterReport = "masterReport01";
		oCabecera.masterDetail = "masterDetail01";
		oCabecera.user = user;
		oCabecera.registration = fecha;
		oCabecera.country = countryCode;

		List<EAjusteInventarioSku> oDetalleList = new ArrayList<EAjusteInventarioSku>();
		int contadorDetalle = 0;
		for (ESku producto : productos) {
			EAjusteInventarioSku oDetalle = new EAjusteInventarioSku();
			oDetalle.idtrx = transactionType.equals("ADJUST") ? (util.GenerarIDTRX("idtrx") + contadorDetalle) : "";
			oDetalle.sku = producto.sku;
			oDetalle.fulfillment = producto.bodega;
			oDetalle.inventoryType = producto.tipoInventario;
			oDetalle.stock = stockIgualAlActual ? producto.iStock : util.GenerarRandom(50, 10);
			oDetalle.operation =operacion;
			//oDetalle.operation = transactionType.equals("ADJUST")
			//		? (operacion.equals("") ? (producto.esImpar.equals("1") ? "A" : "S") : operacion)
			//		: "";
			oDetalle.cud = util.GenerarCUD();
			oDetalle.fixInventory = "N";
			oDetalle.transaction_doc = documentType;
			oDetalle.referenceId = "Reference01";
			oDetalle.referenceId2 = "Reference02";
			oDetalle.referenceId3 = "Reference03";
			oDetalle.originProcessDate = fecha;
			oDetalle.originDescription = "AJUSTE DE STOCK VIA SERVICIO SQS";
			oDetalle.comercialDate = fecha;
			oDetalleList.add(oDetalle);
			System.out.println("PRODUCTO ENCONTRADO:" + oDetalle.sku);
			contadorDetalle++;
		}

		oCabecera.skus = oDetalleList;

		return ConvertirAJson(oCabecera);
	}

	public String GenerarTramaJsonIncrementalProducto(List<ESku> productos, String sistemaOrigen, String countryCode)
			throws JsonProcessingException {

		EIncrementalProducto oCabecera = new EIncrementalProducto();
		List<EIncrementalProductoSku> oListaSku = new ArrayList<EIncrementalProductoSku>();

		for (ESku producto : productos) {
			EIncrementalProductoSku oEntidad = new EIncrementalProductoSku();
			oEntidad.itemId = producto.sku;
			oEntidad.codVenta = producto.codigoVenta;
			oEntidad.style = producto.style;
			oEntidad.productSubClass = producto.sublineCode;
			oEntidad.productClass = producto.lineCode;
			oEntidad.departmentNumber = producto.deptoCode;
			oEntidad.description = producto.description;
			oListaSku.add(oEntidad);
		}

		EIncrementalProductoMessageHeader oMessageHeader = new EIncrementalProductoMessageHeader();
		oMessageHeader.country = countryCode;
		oMessageHeader.source = sistemaOrigen;

		oCabecera.skus = oListaSku;
		oCabecera.messageHeader = oMessageHeader;

		return ConvertirAJson(oCabecera);
	}

	public String GenerarTramaJsonAjusteInventario(List<ESku> productos, String sistemaOrigen, String countryCode,
			String transactionType, String user, String operacion, String documentType, boolean sinTipoTransaccion,
			boolean sinUsuario, boolean sinIDTRX, boolean sinBodega, boolean sinTipoInventario, boolean sinOperacion)
			throws JsonProcessingException {

		EAjusteInventario oCabecera = new EAjusteInventario();

		String fecha = util.ObtenerFechaActual(Constant.FORMATO_FECHA2);

		oCabecera.transactionId = util.GenerarIDTRX("AUT_ADJUST_");
		oCabecera.transactionType = sinTipoTransaccion ? "" : transactionType;
		oCabecera.originSystem = sistemaOrigen;
		oCabecera.masterReport = "masterReport01";
		oCabecera.masterDetail = "masterDetail01";
		oCabecera.user = sinUsuario ? "" : user;
		oCabecera.registration = fecha;
		oCabecera.country = countryCode;

		List<EAjusteInventarioSku> oDetalleList = new ArrayList<EAjusteInventarioSku>();
		int contadorDetalle = 0;
		for (ESku producto : productos) {
			EAjusteInventarioSku oDetalle = new EAjusteInventarioSku();
			oDetalle.idtrx = transactionType.equals("ADJUST")
					? (sinIDTRX ? "" : (util.GenerarIDTRX("idtrx") + contadorDetalle))
					: "";
			oDetalle.sku = producto.sku;
			oDetalle.fulfillment = sinBodega ? "" : producto.bodega;
			oDetalle.inventoryType = sinTipoInventario ? "" : producto.tipoInventario;
			oDetalle.stock = util.GenerarRandom(50, 10);
			oDetalle.operation = transactionType.equals("ADJUST")
					? (sinOperacion ? ""
							: (operacion.equals("") ? (producto.esImpar.equals("1") ? "A" : "S") : operacion))
					: "";
			oDetalle.cud = util.GenerarCUD();
			oDetalle.fixInventory = "N";
			oDetalle.transaction_doc = documentType;
			oDetalle.referenceId = "Reference01";
			oDetalle.referenceId2 = "Reference02";
			oDetalle.referenceId3 = "Reference03";
			oDetalle.originProcessDate = fecha;
			oDetalle.originDescription = "AJUSTE DE STOCK VIA SERVICIO REST";
			oDetalle.comercialDate = fecha;
			oDetalleList.add(oDetalle);
			System.out.println("PRODUCTO ENCONTRADO:" + oDetalle.sku);
			contadorDetalle++;
		}

		oCabecera.skus = oDetalleList;

		return ConvertirAJson(oCabecera);
	}
	public String GenerarTramaJsonAjusteInventarioConIdDetalleDuplicado(List<ESku> productos, String sistemaOrigen,
																		String countryCode, String transactionType, String user) throws JsonProcessingException {

		EAjusteInventario oCabecera = new EAjusteInventario();

		String fecha = util.ObtenerFechaActual(Constant.FORMATO_FECHA2);
		String idTrx = util.GenerarIDTRX("idtrx");

		oCabecera.transactionId = transactionType.equals("ADJUST") ? util.GenerarIDTRX("AUT_ADJUST_")
				: util.GenerarIDTRX("AUT_SDELTA");
		oCabecera.transactionType = transactionType;
		oCabecera.originSystem = sistemaOrigen;
		oCabecera.masterReport = "masterReport01";
		oCabecera.masterDetail = "masterDetail01";
		oCabecera.user = user;
		oCabecera.registration = fecha;
		oCabecera.country = countryCode;

		List<EAjusteInventarioSku> oDetalleList = new ArrayList<EAjusteInventarioSku>();
		for (ESku producto : productos) {
			EAjusteInventarioSku oDetalle = new EAjusteInventarioSku();
			oDetalle.idtrx = transactionType.equals("ADJUST") ? idTrx : "";
			oDetalle.sku = producto.sku;
			oDetalle.fulfillment = producto.bodega;
			oDetalle.inventoryType = producto.tipoInventario;
			oDetalle.stock = util.GenerarRandom(50, 10);
			oDetalle.operation = transactionType.equals("ADJUST") ? (producto.esImpar.equals("1") ? "A" : "S") : "";
			oDetalle.cud = util.GenerarCUD();
			oDetalle.fixInventory = "N";
			oDetalle.transaction_doc = "NTD";
			oDetalle.referenceId = "Reference01";
			oDetalle.referenceId2 = "Reference02";
			oDetalle.referenceId3 = "Reference03";
			oDetalle.originProcessDate = fecha;
			oDetalle.originDescription = "AJUSTE DE STOCK VIA SERVICIO SQS";
			oDetalle.comercialDate = fecha;
			oDetalleList.add(oDetalle);
			System.out.println("PRODUCTO ENCONTRADO:" + oDetalle.sku);
		}

		oCabecera.skus = oDetalleList;

		return ConvertirAJson(oCabecera);
	}



	public String GenerarTramaJsonAjusteInventarioConIdDetalleDuplicado_DOS(List<ESku> productos, String sistemaOrigen,
			String countryCode, String transactionType, String user) throws JsonProcessingException {

		EAjusteInventario oCabecera = new EAjusteInventario();

		String fecha = util.ObtenerFechaActual(Constant.FORMATO_FECHA2);
		String idTrx = util.GenerarIDTRX("idtrx");

		oCabecera.transactionId = transactionType.equals("ADJUST") ? util.GenerarIDTRX("AUT_ADJUST_DUPLE_")
				: util.GenerarIDTRX("AUT_SDELTA");
		oCabecera.transactionType = transactionType;
		oCabecera.originSystem = sistemaOrigen;
		oCabecera.masterReport = "masterReport01";
		oCabecera.masterDetail = "masterDetail01";
		oCabecera.user = user;
		oCabecera.registration = fecha;
		oCabecera.country = countryCode;

		List<EAjusteInventarioSku> oDetalleList = new ArrayList<EAjusteInventarioSku>();
		for (ESku producto : productos) {
			EAjusteInventarioSku oDetalle = new EAjusteInventarioSku();
			oDetalle.idtrx = transactionType.equals("ADJUST") ? idTrx : "";
			oDetalle.sku = producto.sku;
			oDetalle.fulfillment = producto.bodega;
			oDetalle.inventoryType = producto.tipoInventario;
			oDetalle.stock = util.GenerarRandom(50, 10);
			oDetalle.operation = transactionType.equals("ADJUST") ? (producto.esImpar.equals("1") ? "A" : "S") : "";
			oDetalle.cud = util.GenerarCUD();
			oDetalle.fixInventory = "N";
			oDetalle.transaction_doc = "NTD";
			oDetalle.referenceId = "Reference01";
			oDetalle.referenceId2 = "Reference02";
			oDetalle.referenceId3 = "Reference03";
			oDetalle.originProcessDate = fecha;
			oDetalle.originDescription = "AJUSTE DE STOCK VIA SERVICIO SQS";
			oDetalle.comercialDate = fecha;
			oDetalleList.add(oDetalle);
			System.out.println("PRODUCTO ENCONTRADO:" + oDetalle.sku);
		}

		oCabecera.skus = oDetalleList;

		return ConvertirAJson(oCabecera);
	}

	public String GenerarTramaJsonAjusteInventarioNegativo(List<ESku> productos, String sistemaOrigen,
														   String countryCode) throws JsonProcessingException {

		EAjusteInventario oCabecera = new EAjusteInventario();

		String fecha = util.ObtenerFechaActual(Constant.FORMATO_FECHA2);

		oCabecera.transactionId = util.GenerarIDTRX("AUT_ADJUST_");
		oCabecera.transactionType = "ADJUST";
		oCabecera.originSystem = sistemaOrigen;
		oCabecera.masterReport = "masterReport01";
		oCabecera.masterDetail = "masterDetail01";
		oCabecera.user = "UserQA";
		oCabecera.registration = fecha;
		oCabecera.country = countryCode;

		List<EAjusteInventarioSku> oDetalleList = new ArrayList<EAjusteInventarioSku>();
		int contadorDetalle = 0;
		for (ESku producto : productos) {
			EAjusteInventarioSku oDetalle = new EAjusteInventarioSku();
			oDetalle.idtrx = (util.GenerarIDTRX("idtrx") + contadorDetalle);
			oDetalle.sku = producto.sku;
			oDetalle.fulfillment = producto.bodega;
			oDetalle.inventoryType = producto.tipoInventario;
			oDetalle.stock = util.GenerarRandom(50, 10);
			oDetalle.operation = "S";
			oDetalle.cud = util.GenerarCUD();
			oDetalle.fixInventory = "N";
			oDetalle.transaction_doc = "NTD";
			oDetalle.referenceId = "Reference01";
			oDetalle.referenceId2 = "Reference02";
			oDetalle.referenceId3 = "Reference03";
			oDetalle.originProcessDate = fecha;
			oDetalle.originDescription = "AJUSTE DE STOCK VIA SERVICIO SQS";
			oDetalle.comercialDate = fecha;
			oDetalleList.add(oDetalle);
			System.out.println("PRODUCTO ENCONTRADO:" + oDetalle.sku);
			contadorDetalle++;
		}

		oCabecera.skus = oDetalleList;

		return ConvertirAJson(oCabecera);
	}

	public String GenerarTramaJsonAjusteRemplazar(List<ESku> productos, String sistemaOrigen,
														   String countryCode) throws JsonProcessingException {

		EAjusteInventario oCabecera = new EAjusteInventario();

		String fecha = util.ObtenerFechaActual(Constant.FORMATO_FECHA2);

		oCabecera.transactionId = util.GenerarIDTRX("AUT_ADJUST_");
		oCabecera.transactionType = "ADJUST";
		oCabecera.originSystem = sistemaOrigen;
		oCabecera.masterReport = "masterReport01";
		oCabecera.masterDetail = "masterDetail01";
		oCabecera.user = "UserQA";
		oCabecera.registration = fecha;
		oCabecera.country = countryCode;

		List<EAjusteInventarioSku> oDetalleList = new ArrayList<EAjusteInventarioSku>();
		int contadorDetalle = 0;
		for (ESku producto : productos) {
			EAjusteInventarioSku oDetalle = new EAjusteInventarioSku();
			oDetalle.idtrx = (util.GenerarIDTRX("idtrx") + contadorDetalle);
			oDetalle.sku = producto.sku;
			oDetalle.fulfillment = producto.bodega;
			oDetalle.inventoryType = producto.tipoInventario;
			oDetalle.stock = util.GenerarRandom(50, 10);
			oDetalle.operation = "R";
			oDetalle.cud = util.GenerarCUD();
			oDetalle.fixInventory = "N";
			oDetalle.transaction_doc = "NTD";
			oDetalle.referenceId = "Reference01";
			oDetalle.referenceId2 = "Reference02";
			oDetalle.referenceId3 = "Reference03";
			oDetalle.originProcessDate = fecha;
			oDetalle.originDescription = "AJUSTE DE STOCK VIA SERVICIO SQS";
			oDetalle.comercialDate = fecha;
			oDetalleList.add(oDetalle);
			System.out.println("PRODUCTO ENCONTRADO:" + oDetalle.sku);
			contadorDetalle++;
		}

		oCabecera.skus = oDetalleList;

		return ConvertirAJson(oCabecera);
	}
	public String GenerarTramaJsonAjusteDelta(List<ESku> productos, String sistemaOrigen,String operacion,
			String countryCode) throws JsonProcessingException {

		EAjusteInventario oCabecera = new EAjusteInventario();

		String fecha = util.ObtenerFechaActual(Constant.FORMATO_FECHA2);

		oCabecera.transactionId = util.GenerarIDTRX("AUT_ADJUST_");
		oCabecera.transactionType = "ADJUST";
		oCabecera.originSystem = sistemaOrigen;
		oCabecera.masterReport = "masterReport01";
		oCabecera.masterDetail = "masterDetail01";
		oCabecera.user = "UserQA";
		oCabecera.registration = fecha;
		oCabecera.country = countryCode;

		List<EAjusteInventarioSku> oDetalleList = new ArrayList<EAjusteInventarioSku>();
		int contadorDetalle = 0;
		for (ESku producto : productos) {
			EAjusteInventarioSku oDetalle = new EAjusteInventarioSku();
			oDetalle.idtrx = (util.GenerarIDTRX("idtrx") + contadorDetalle);
			oDetalle.sku = producto.sku;
			oDetalle.fulfillment = producto.bodega;
			oDetalle.inventoryType = producto.tipoInventario;
			oDetalle.stock = util.GenerarRandom(50, 10);
			oDetalle.operation = operacion;
			oDetalle.cud = util.GenerarCUD();
			oDetalle.fixInventory = "N";
			oDetalle.transaction_doc = "NTD";
			oDetalle.referenceId = "Reference01";
			oDetalle.referenceId2 = "Reference02";
			oDetalle.referenceId3 = "Reference03";
			oDetalle.originProcessDate = fecha;
			oDetalle.originDescription = "AJUSTE DE STOCK VIA SERVICIO SQS";
			oDetalle.comercialDate = fecha;
			oDetalleList.add(oDetalle);
			System.out.println("PRODUCTO ENCONTRADO:" + oDetalle.sku);
			contadorDetalle++;
		}

		oCabecera.skus = oDetalleList;

		return ConvertirAJson(oCabecera);
	}

	public String GenerarTramaJsonAjusteInventarioPositivo(List<ESku> productos, String sistemaOrigen,
			String countryCode) throws JsonProcessingException {

		EAjusteInventario oCabecera = new EAjusteInventario();

		String fecha = util.ObtenerFechaActual(Constant.FORMATO_FECHA2);

		oCabecera.transactionId = util.GenerarIDTRX("AUT_ADJUST_");
		oCabecera.transactionType = "ADJUST";
		oCabecera.originSystem = sistemaOrigen;
		oCabecera.masterReport = "masterReport01";
		oCabecera.masterDetail = "masterDetail01";
		oCabecera.user = "UserQA";
		oCabecera.registration = fecha;
		oCabecera.country = countryCode;

		Random random = new Random();

		List<EAjusteInventarioSku> oDetalleList = new ArrayList<EAjusteInventarioSku>();
		int contadorDetalle = 0;
		for (ESku producto : productos) {
			EAjusteInventarioSku oDetalle = new EAjusteInventarioSku();
			oDetalle.idtrx = (util.GenerarIDTRX("idtrx") + contadorDetalle);
			oDetalle.sku = producto.sku;
			oDetalle.fulfillment = producto.bodega;
			oDetalle.inventoryType = producto.tipoInventario;
			oDetalle.stock = (random.nextInt(50 - 10) + 10);
			oDetalle.operation = "A";
			oDetalle.cud = util.GenerarCUD();
			oDetalle.fixInventory = "N";
			oDetalle.transaction_doc = "NTD";
			oDetalle.referenceId = "Reference01";
			oDetalle.referenceId2 = "Reference02";
			oDetalle.referenceId3 = "Reference03";
			oDetalle.originProcessDate = fecha;
			oDetalle.originDescription = "AJUSTE DE STOCK VIA SERVICIO REST";
			oDetalle.comercialDate = fecha;
			oDetalleList.add(oDetalle);
			System.out.println("PRODUCTO ENCONTRADO:" + oDetalle.sku);
			contadorDetalle++;
		}

		oCabecera.skus = oDetalleList;

		return ConvertirAJson(oCabecera);
	}

	public String GenerarTramaJsonSincronizacionFull(String sfullFileName, String sistemaOrigen, String countryCode,
			String userName) throws JsonProcessingException {

		String fecha = util.ObtenerFechaActual(Constant.FORMATO_FECHA2);

		ESFull entidad = new ESFull();
		entidad.transactionId = util.GenerarIDTRX("AUT_SFULL_");
		entidad.transactionType = "SFULL";
		entidad.originSystem = sistemaOrigen;
		entidad.masterReport = "MasterReport01";
		entidad.masterDetail = "MasterDetail01";
		entidad.user = userName;
		entidad.registration = fecha;
		entidad.fileName = sfullFileName;
		entidad.country = countryCode;

		System.out.println("COUNTRY_CODE: " + entidad.country);

		return ConvertirAJson(entidad);
	}

}
