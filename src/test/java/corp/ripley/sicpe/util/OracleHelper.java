package corp.ripley.sicpe.util;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.JSchException;


public class OracleHelper {
		
	private static Connection conn;
	private boolean done=false;    
	
	public static void setConnection(Connection conn) {
		OracleHelper.conn = conn;
	}


	public static Connection getConnection(){
		return conn;
	}

	public static void closeConnection() throws SQLException{
		conn.close();
	}
		
	public Connection ConnectDbWithTunnel(String strSshUser,
											String strSshPassword,
											String strSshHost,
											int nSshPort,
											String strRemoteHost,
											int nRemotePort,
											String strDbUser,
											String strDbPassword
											) throws SQLException {		
		Connection conn = null;
		
		try
	    {				
			
			
	      int nLocalPort = 3324;         
	      //int nLocalPort = ThreadLocalRandom.current().nextInt(1, 99999);// local port number use to bind SSH tunnel
	      try {
	      doSshTunnel(strSshUser, strSshPassword, strSshHost, nSshPort, strRemoteHost, nLocalPort, nRemotePort);
	      }catch(Exception ex) {
	    	  System.out.println("OCURRIO UN ERROR: "+ex.getMessage());
	      }

	      done = true;
	      conn = OpenConnection("jdbc:oracle:thin:@//localhost:1531/dbinteg", strDbUser,strDbPassword);	      
	      
	    }
	    catch( Exception e )
	    {
	      e.printStackTrace();
	    }
		
	    	return conn;
	}
	
	public Connection OpenConnection(String dbUrl,String dbUsuario,String dbPassword) throws SQLException {
		return DriverManager.getConnection(dbUrl,dbUsuario,dbPassword);
	}
	public void CloseConnection(Connection Conn) throws SQLException {
		Conn.close();		
	}
	
	
	
	private void doSshTunnel( String strSshUser, String sshKeyFilePath, String strSshHost, int nSshPort, String strRemoteHost, int nLocalPort, int nRemotePort ) throws JSchException
	  {
	    final JSch jsch = new JSch();
	    Session session = jsch.getSession( strSshUser, strSshHost, 22 );
	    
        jsch.addIdentity(sshKeyFilePath);
	    final Properties config = new Properties();
	    config.put( "StrictHostKeyChecking", "no" );
        config.put("ConnectionAttempts", "3");
        session.setConfig(config);
	     
	    session.connect();
	    session.setPortForwardingL(nLocalPort, strRemoteHost, nRemotePort);
	    
	  }
	
	public void CloseDbConnection(Connection conn) throws SQLException {
		try
	    {
			conn.close();
	    }
	    catch( Exception e )
	    {
	      e.printStackTrace();
	    }
	    finally
	    {
	      System.exit(0);
	    }
	}
	
	

}
