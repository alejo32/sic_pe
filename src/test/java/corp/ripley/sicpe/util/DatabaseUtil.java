package corp.ripley.sicpe.util;

import corp.ripley.sicpe.model.*;
import corp.ripley.sicpe.model.sic.EContador;
import corp.ripley.sicpe.model.sic.EGrillaAuditoria;
import corp.ripley.sicpe.model.sic.EStockXBodega;
import corp.ripley.sicpe.model.sic.ETv_Publish_Products;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class DatabaseUtil {

	//Se listan algunas de las bodegas que se visualizan en el combo bodega,
	// esto hasta que se tenga información de Roles y Perfiles
	String bodegasHabilitadas = "'20036','20037','20052','20054','20044','2095','20062','20022','20058','20063','20042','20065','20039','20061','20048','20050','20059','29004','20049','20026','20041','20069','20021','20034','20045','20014','20015','20013','20016','20017','20011','29024','29092','29091','29089','29088','29090','20005','20093','20003','2044','20072','20080','20071','20025','20047','29017','20024','20030','29021','29022','20094','20064','20077','20066','20029','29023','20086','20043','20079','29018','20074','20027','20028','20073','20006','20010','20060','20040','29019','29020','20012','20098'";

	public void ConnectarTunelSSH(EConexionBD oEntidad) throws IOException, SQLException {
		//YT//	System.out.println("-----------------------------------------------ConnectarTunelSSH");
		//YT//Date date = new Date();
		//YT//SimpleDateFormat dateFormat = new SimpleDateFormat(utils.Constant.FORMATO_FECHA2);
		//YT//System.out.println("Inicio: " + dateFormat.format(date));

		//YT//System.out.println("Conectandose a tunel SSH:" + oEntidad.sshUser + "| ********* |" + oEntidad.sshHost + "|"
		//YT//		+ oEntidad.sshPort + "|" + oEntidad.dbHost + "|" + oEntidad.dbPort + "|" + oEntidad.dbUser
		//YT//		+ "| *********");
		MySqlDbHelper mySqlDbHelper = new MySqlDbHelper();
		MySqlDbHelper.setConnection(
				mySqlDbHelper.ConnectDbWithTunnel(oEntidad.sshUser, oEntidad.sshPassword, oEntidad.sshHost,
						oEntidad.sshPort, oEntidad.dbHost, oEntidad.dbPort, oEntidad.dbUser, oEntidad.dbPassword));
	}
    //Conexion a la base de datos de Chile para Calculo de IMS inicio
	
	public void ConnectarBDSIC_CL(EConexionBD oEntidad) throws IOException, SQLException {
	
		MySqlDbHelper mySqlDbHelper = new MySqlDbHelper();
		MySqlDbHelper.setConnection(
				mySqlDbHelper.ConnectBDSIC_CL(oEntidad.dbHost, oEntidad.dbPort, oEntidad.dbUser, oEntidad.dbPassword));
	}
	
	public void ConnectarBDSIC(EConexionBD oEntidad) throws IOException, SQLException {
		
		MySqlDbHelper mySqlDbHelper = new MySqlDbHelper();
		MySqlDbHelper.setConnection(
				mySqlDbHelper.ConnectBDSIC(oEntidad.dbHost, oEntidad.dbPort, oEntidad.dbUser, oEntidad.dbPassword));
	}
	//Conexion a la base de datos de Chile para Calculo de IMS fin
	
	public boolean HabilitarConfiguracionMovimientoInventario(String codigoPais, String tipoTransaccion,
			String codigoSistema, String tipoInventario) {
		int affectedRows = 0;
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();

			String sqlQuery = "UPDATE RIPLEY_TRX_TYPE " + "SET STATUS='A' "
					+ "WHERE TRANSACTION_TYPE_CODE='[TIPO_TRANSACCION]' " + "AND SYSTEM_CODE='[CODIGO_SISTEMA]' "
					+ "AND COUNTRY_CODE='[CODIGO_PAIS]' " + "AND INVENTORY_TYPE= [INVENTORY_TYPE];";
			sqlQuery = sqlQuery.replace("[TIPO_TRANSACCION]", tipoTransaccion);
			sqlQuery = sqlQuery.replace("[CODIGO_SISTEMA]", codigoSistema);
			sqlQuery = sqlQuery.replace("[CODIGO_PAIS]", codigoPais);
			sqlQuery = sqlQuery.replace("[INVENTORY_TYPE]", tipoInventario);

			 System.out.println("HabilitarConfiguracionMovimientoInventario : " +sqlQuery);
			affectedRows = stmt.executeUpdate(sqlQuery);

			// conn.close();
		} catch (Exception e) {
			System.err.println("ERROR - No se actualizó la configuración de movimiento de inventario. Revisar");
			System.err.println(e.getMessage());
		}
		return affectedRows > 0;
	}

	public boolean DeshabilitarConfiguracionMovimientoInventario(String codigoPais, String codigoSistema,
			String tipoTransaccion, String tipoInventario) {
		int affectedRows = 0;
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();

			String sqlQuery = "UPDATE RIPLEY_TRX_TYPE " + "SET STATUS='I' "
					+ "WHERE TRANSACTION_TYPE_CODE='[TIPO_TRANSACCION]' " + "AND SYSTEM_CODE='[CODIGO_SISTEMA]' "
					+ "AND COUNTRY_CODE='[CODIGO_PAIS]' " + "AND INVENTORY_TYPE= [INVENTORY_TYPE];";
			sqlQuery = sqlQuery.replace("[TIPO_TRANSACCION]", tipoTransaccion);
			sqlQuery = sqlQuery.replace("[CODIGO_SISTEMA]", codigoSistema);
			sqlQuery = sqlQuery.replace("[CODIGO_PAIS]", codigoPais);
			sqlQuery = sqlQuery.replace("[INVENTORY_TYPE]", tipoInventario);

			// System.out.println("SQL: " +sqlQuery);
			affectedRows = stmt.executeUpdate(sqlQuery);

			// conn.close();
		} catch (Exception e) {
			System.err.println("ERROR - No se actualizó la configuración de movimiento de inventario. Revisar");
			System.err.println(e.getMessage());
		}
		return affectedRows > 0;
	}

	public List<ESku> ObtenerInformacionProductos(String codigoPais, int cantidad)
			throws IOException, InterruptedException, SQLException {
		List<ESku> oLista = new ArrayList<ESku>();
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;

			String sqlQuery = "\n"
					+ "select COD_VENTA, SELLER, FULFILLMENT_CODE,INVENTORY_TYPE, QTY, IF(RIGHT(COD_VENTA, 1) % 2 <> 0, true, false) ES_IMPAR from (\n"
					+ "SELECT t1.COD_VENTA,t1.SELLER, t2.FULFILLMENT_CODE,t2.INVENTORY_TYPE, t2.QTY FROM SKU t1\n"
					+ "INNER JOIN SKU_STOCK_RIPLEY t2\n" + "ON t1.SKU_ID = t2.SKU_ID\n"
					+ "WHERE t2.FULFILLMENT_CODE IN ([BODEGAS_HABILITADAS]) AND t1.COUNTRY_CODE='[CODIGO_PAIS]' AND LENGTH(t1.SKU)=8 AND LENGTH(t1.COD_VENTA)=13\n"
					+ "LIMIT 100000\n" + ") A \n" + "WHERE QTY>0\n" + "ORDER BY RAND()\n" + "LIMIT [CANTIDAD]";
			sqlQuery = sqlQuery.replace("[CODIGO_PAIS]", codigoPais);
			sqlQuery = sqlQuery.replace("[CANTIDAD]", String.valueOf(cantidad));
			sqlQuery = sqlQuery.replace("[BODEGAS_HABILITADAS]", bodegasHabilitadas);

			 System.out.println(sqlQuery);
			rs = stmt.executeQuery(sqlQuery);
			while (rs.next()) {
				ESku oEntidad = new ESku();
				oEntidad.sku = rs.getString("COD_VENTA");
				oEntidad.seller = rs.getString("SELLER");
				oEntidad.bodega = rs.getString("FULFILLMENT_CODE");
				oEntidad.tipoInventario = rs.getString("INVENTORY_TYPE");
				oEntidad.stock = rs.getString("QTY");
				oEntidad.iStock = rs.getInt("QTY");
				oEntidad.esImpar = rs.getString("ES_IMPAR");

				oLista.add(oEntidad);
			}
			// conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerInformacionProductos");
			System.err.println(e.getMessage());
		}
		return oLista;
	}

	public List<ESku> ObtenerInformacionProductos(String codigoPais, int cantidad, String tipoInventario)
			throws IOException, InterruptedException, SQLException {
		List<ESku> oLista = new ArrayList<ESku>();
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;

			String sqlQuery = "\n"
					+ "select COD_VENTA, SELLER, FULFILLMENT_CODE,INVENTORY_TYPE, QTY, IF(RIGHT(COD_VENTA, 1) % 2 <> 0, true, false) ES_IMPAR from (\n"
					+ "SELECT t1.COD_VENTA,t1.SELLER, t2.FULFILLMENT_CODE,t2.INVENTORY_TYPE, t2.QTY FROM SKU t1\n"
					+ "INNER JOIN SKU_STOCK_RIPLEY t2\n" + "ON t1.SKU_ID = t2.SKU_ID\n"
					+ "WHERE t2.FULFILLMENT_CODE IN ([BODEGAS_HABILITADAS]) AND INVENTORY_TYPE='[INVENTORY_TYPE]' AND t1.COUNTRY_CODE='[CODIGO_PAIS]' AND LENGTH(t1.SKU)=8 AND LENGTH(t1.COD_VENTA)=13\n"
					+ "LIMIT 100000\n" + ") A \n" + "WHERE QTY>0\n" + "ORDER BY RAND()\n" + "LIMIT [CANTIDAD]";
			sqlQuery = sqlQuery.replace("[CODIGO_PAIS]", codigoPais);
			sqlQuery = sqlQuery.replace("[CANTIDAD]", String.valueOf(cantidad));
			sqlQuery = sqlQuery.replace("[BODEGAS_HABILITADAS]", bodegasHabilitadas);
			sqlQuery = sqlQuery.replace("[INVENTORY_TYPE]", tipoInventario);

			System.out.println(sqlQuery);
			rs = stmt.executeQuery(sqlQuery);
			while (rs.next()) {
				ESku oEntidad = new ESku();
				oEntidad.sku = rs.getString("COD_VENTA");
				oEntidad.seller = rs.getString("SELLER");
				oEntidad.bodega = rs.getString("FULFILLMENT_CODE");
				oEntidad.tipoInventario = rs.getString("INVENTORY_TYPE");
				oEntidad.stock = rs.getString("QTY");
				oEntidad.iStock = rs.getInt("QTY");
				oEntidad.esImpar = rs.getString("ES_IMPAR");

				oLista.add(oEntidad);
			}
			// conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerInformacionProductos");
			System.err.println(e.getMessage());
		}
		return oLista;
	}

	public List<ESku> ObtenerInformacionProductosNoExiste(String codigoPais, int cantidad)
			throws IOException, InterruptedException, SQLException {
		List<ESku> oLista = new ArrayList<ESku>();
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;

			String sqlQuery = "\n"
					+ "select COD_VENTA, SELLER, FULFILLMENT_CODE,INVENTORY_TYPE, QTY, IF(RIGHT(COD_VENTA, 1) % 2 <> 0, true, false) ES_IMPAR from (\n"
					+ "SELECT t1.COD_VENTA,t1.SELLER, t2.FULFILLMENT_CODE,t2.INVENTORY_TYPE, t2.QTY FROM SKU t1\n"
					+ "INNER JOIN SKU_STOCK_RIPLEY t2\n" + "ON t1.SKU_ID = t2.SKU_ID\n"
					+ "WHERE t2.FULFILLMENT_CODE IN ([BODEGAS_HABILITADAS]) AND t1.COUNTRY_CODE='[CODIGO_PAIS]'\n"
					+ "LIMIT 100000\n" + ") A \n" + "WHERE QTY>0\n" + "ORDER BY RAND()\n" + "LIMIT [CANTIDAD]";
			sqlQuery = sqlQuery.replace("[CODIGO_PAIS]", codigoPais);
			sqlQuery = sqlQuery.replace("[CANTIDAD]", String.valueOf(cantidad));
			sqlQuery = sqlQuery.replace("[BODEGAS_HABILITADAS]", bodegasHabilitadas);

			Random random = new Random();
			System.out.println(sqlQuery);
			rs = stmt.executeQuery(sqlQuery);
			while (rs.next()) {
				ESku oEntidad = new ESku();
				oEntidad.sku = "300011211" + (random.nextInt(9999 - 1000) + 1000);
				oEntidad.seller = rs.getString("SELLER");
				oEntidad.bodega = rs.getString("FULFILLMENT_CODE");
				oEntidad.tipoInventario = rs.getString("INVENTORY_TYPE");
				oEntidad.stock = rs.getString("QTY");
				oEntidad.iStock = rs.getInt("QTY");
				oEntidad.esImpar = rs.getString("ES_IMPAR");

				oLista.add(oEntidad);
			}
			// conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerInformacionProductosNoExiste");
			System.err.println(e.getMessage());
		}
		return oLista;
	}

	public List<ESku> ObtenerInformacionProductosSinStock(String codigoPais, int cantidad, String bodega)
			throws IOException, InterruptedException, SQLException {
		List<ESku> oLista = new ArrayList<ESku>();
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;

			String sqlQuery = "\n"
					+ "SELECT t1.COD_VENTA,t1.SELLER,\"[FULFILLMENT_CODE]\" FULFILLMENT_CODE, 1 INVENTORY_TYPE, 1 QTY, 1 ES_IMPAR FROM SKU t1\n"
					+ "LEFT JOIN SKU_STOCK_RIPLEY t2\n" + "ON t1.SKU_ID=t2.SKU_ID\n"
					+ "WHERE t1.COUNTRY_CODE='[CODIGO_PAIS]'\n" + "AND t2.FULFILLMENT_CODE IS NULL\n"
					+ "AND LENGTH(t1.COD_VENTA)=13\n" + "LIMIT [CANTIDAD]";
			sqlQuery = sqlQuery.replace("[FULFILLMENT_CODE]", bodega);
			sqlQuery = sqlQuery.replace("[CODIGO_PAIS]", codigoPais);
			sqlQuery = sqlQuery.replace("[CANTIDAD]", String.valueOf(cantidad));

//			System.out.println(sqlQuery);
			rs = stmt.executeQuery(sqlQuery);
			while (rs.next()) {
				ESku oEntidad = new ESku();
				oEntidad.sku = rs.getString("COD_VENTA");
				oEntidad.seller = rs.getString("SELLER");
				oEntidad.bodega = rs.getString("FULFILLMENT_CODE");
				oEntidad.tipoInventario = rs.getString("INVENTORY_TYPE");
				oEntidad.stock = rs.getString("QTY");
				oEntidad.iStock = rs.getInt("QTY");
				oEntidad.esImpar = rs.getString("ES_IMPAR");

				oLista.add(oEntidad);
			}
			// conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerInformacionProductosSinStock");
			System.err.println(e.getMessage());
		}
		return oLista;
	}

	public List<ESku> ObtenerInformacionProductosConStock0(String codigoPais, int cantidad)
			throws IOException, InterruptedException, SQLException {
		List<ESku> oLista = new ArrayList<ESku>();
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;

			String sqlQuery = "\n"
					+ "select COD_VENTA, SELLER, FULFILLMENT_CODE,INVENTORY_TYPE, QTY, IF(RIGHT(COD_VENTA, 1) % 2 <> 0, true, false) ES_IMPAR from (\n"
					+ "SELECT t1.COD_VENTA,t1.SELLER, t2.FULFILLMENT_CODE,t2.INVENTORY_TYPE, t2.QTY FROM SKU t1\n"
					+ "INNER JOIN SKU_STOCK_RIPLEY t2\n" + "ON t1.SKU_ID = t2.SKU_ID\n"
					+ "WHERE t2.FULFILLMENT_CODE IN ([BODEGAS_HABILITADAS]) AND  t1.COUNTRY_CODE='[CODIGO_PAIS]'\n"
					+ "LIMIT 100000\n" + ") A \n" + "WHERE QTY=0\n" + "ORDER BY RAND()\n" + "LIMIT [CANTIDAD]";
			sqlQuery = sqlQuery.replace("[CODIGO_PAIS]", codigoPais);
			sqlQuery = sqlQuery.replace("[CANTIDAD]", String.valueOf(cantidad));
			sqlQuery = sqlQuery.replace("[BODEGAS_HABILITADAS]", bodegasHabilitadas);

			System.out.println("Consulta Stock cero " + sqlQuery);
			rs = stmt.executeQuery(sqlQuery);
			while (rs.next()) {
				ESku oEntidad = new ESku();
				oEntidad.sku = rs.getString("COD_VENTA");
				oEntidad.seller = rs.getString("SELLER");
				oEntidad.bodega = rs.getString("FULFILLMENT_CODE");
				oEntidad.tipoInventario = rs.getString("INVENTORY_TYPE");
				oEntidad.stock = rs.getString("QTY");
				oEntidad.iStock = rs.getInt("QTY");
				oEntidad.esImpar = rs.getString("ES_IMPAR");

				oLista.add(oEntidad);
			}
			// conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerInformacionProductosConStock0");
			System.err.println(e.getMessage());
		}
		return oLista;
	}

	int intentos = 1;

	public ETransaction EsperarProcesamientoTransaccion(String idTrxHeader) throws InterruptedException {
		ETransaction oEntidad = ObtenerResultadoTransaccion(idTrxHeader);
		System.out.println("estado::::: " + oEntidad.status);
		System.out.println("Intento:");
		try {
			while (oEntidad == null || !oEntidad.status.equals("F") && !oEntidad.status.equals("CF")
					&& !oEntidad.status.equals("C") && intentos < 540) {
				intentos++;
				System.out.print(".." + intentos);
				Thread.sleep(20000);

				oEntidad = ObtenerResultadoTransaccion(idTrxHeader);
			}
		} catch (Exception ex) {
			intentos++;
			System.out.print(".." + intentos);
			Thread.sleep(5000);
			System.out.println("EsperarProcesamientoTransaccion()");
			if (intentos > 20)
				return null;
			else
				return EsperarProcesamientoTransaccion(idTrxHeader);
		}
		return oEntidad;
	}

	public ETransaction ObtenerResultadoTransaccion(String idTrxHeader) {
		ETransaction entidad = null;
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;
			String sqlQuery = "SELECT TRANSACCION_ID,\r\n" + "		FILENAME, \r\n" + "		STATUS, \r\n"
					+ "		TOTAL_ITEMS, \r\n" + "		SUCCESS_ITEMS, \r\n" + "		DUPLICATED_ITEMS, \r\n"
					+ "		UNSENT_ITEMS, \r\n" + "		ERROR_ITEM, \r\n" + "		UNCHANGED_ITEMS, \r\n"
					+ "		COUNTRY_CODE, \r\n" + "		COMMENTS \r\n" + "FROM `TRANSACTION` \r\n"
					+ "WHERE IDTRX_HEADER='" + idTrxHeader + "'";

			rs = stmt.executeQuery(sqlQuery);

			entidad = new ETransaction();
			while (rs.next()) {
				// System.out.println("RESULTADO TRANSACCION: "+ entidad.status);
				entidad.transactionId = rs.getString("TRANSACCION_ID");
				entidad.fileName = rs.getString("FILENAME");
				entidad.status = rs.getString("STATUS");
				entidad.totalItems = rs.getString("TOTAL_ITEMS");
				entidad.successItems = rs.getString("SUCCESS_ITEMS");
				entidad.duplicatedItems = rs.getString("DUPLICATED_ITEMS");
				entidad.unsentItems = rs.getString("UNSENT_ITEMS");
				entidad.errorItems = rs.getString("ERROR_ITEM");
				entidad.unchangedItems = rs.getString("UNCHANGED_ITEMS");
				entidad.countryCode = rs.getString("COUNTRY_CODE");
				entidad.comments = rs.getString("COMMENTS");
			}
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerResultadoTransaccion ");
			System.err.println(e.getMessage());
		}

		return entidad;
	}

	public ETransaction ObtenerResultadoTransaccionByTransactionId(String transactionId) {
		ETransaction entidad = null;
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;
			String sqlQuery = "SELECT TRANSACCION_ID,\r\n" + "		FILENAME, \r\n" + "		STATUS, \r\n"
					+ "		TOTAL_ITEMS, \r\n" + "		SUCCESS_ITEMS, \r\n" + "		DUPLICATED_ITEMS, \r\n"
					+ "		UNSENT_ITEMS, \r\n" + "		ERROR_ITEM, \r\n" + "		UNCHANGED_ITEMS, \r\n"
					+ "		COUNTRY_CODE, \r\n" + "		COMMENTS \r\n" + "FROM `TRANSACTION` \r\n"
					+ "WHERE TRANSACCION_ID='" + transactionId + "'";

			rs = stmt.executeQuery(sqlQuery);

			entidad = new ETransaction();
			while (rs.next()) {
				 System.out.println("RESULTADO TRANSACCION: "+ entidad.status);
				entidad.transactionId = rs.getString("TRANSACCION_ID");
				entidad.fileName = rs.getString("FILENAME");
				entidad.status = rs.getString("STATUS");
				entidad.totalItems = rs.getString("TOTAL_ITEMS");
				entidad.successItems = rs.getString("SUCCESS_ITEMS");
				entidad.duplicatedItems = rs.getString("DUPLICATED_ITEMS");
				entidad.unsentItems = rs.getString("UNSENT_ITEMS");
				entidad.errorItems = rs.getString("ERROR_ITEM");
				entidad.unchangedItems = rs.getString("UNCHANGED_ITEMS");
				entidad.countryCode = rs.getString("COUNTRY_CODE");
				entidad.comments = rs.getString("COMMENTS");
				System.out.println("RESULTADO TRANSACCION  YULI: "+ entidad.transactionId);
				System.out.println("RESULTADO TRANSACCION  YULI: "+ entidad.fileName);
				System.out.println("RESULTADO TRANSACCION  YULI: "+ entidad.status);
				System.out.println("RESULTADO TRANSACCION  YULI: "+ entidad.totalItems);
				System.out.println("RESULTADO TRANSACCION  YULI: "+ entidad.successItems);
				System.out.println("RESULTADO TRANSACCION  YULI: "+ entidad.duplicatedItems);
				System.out.println("RESULTADO TRANSACCION  YULI: "+ entidad.unsentItems);
				System.out.println("RESULTADO TRANSACCION  YULI: "+ entidad.errorItems);
				System.out.println("RESULTADO TRANSACCION  YULI: "+ entidad.unchangedItems);
				System.out.println("RESULTADO TRANSACCION  YULI: "+ entidad.countryCode);
				System.out.println("RESULTADO TRANSACCION  YULI: "+ entidad.comments);
			}
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerResultadoTransaccion ");
			System.err.println(e.getMessage());
		}

		return entidad;
	}

	public String ObtenerNombreBodega(String codigoBodega) {
		String nombreBodega = "";
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;
			String sqlQuery = "SELECT NAME FROM LOCATION WHERE COUNTRY_CODE='PE' AND CODE='[CODIGO_VENTA]';";
			sqlQuery = sqlQuery.replace("[CODIGO_VENTA]", codigoBodega);

			rs = stmt.executeQuery(sqlQuery);
			while (rs.next()) {
				nombreBodega = rs.getString("NAME");

			}
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerNombreBodega");
			System.err.println(e.getMessage());
		}

		return nombreBodega;
	}

	public List<ETransactionDetail> ObtenerResultadoDetalleTransaccion(String transactionId) {

		List<ETransactionDetail> lista = null;
		boolean first = true;
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;
			String sqlQuery = "SELECT IDTRX,\r\n" + "		SKU,\r\n" + "		FULFILLMENT_CODE,\r\n"
					+ "		INVENTORY_TYPE,\r\n" + "		STOCK,\r\n" + "		PREV_STOCK,\r\n"
					+ "		OPERATION,\r\n" + "		CUD,\r\n" + "		STATUS,\r\n" + "		DETAIL\r\n"
					+ "FROM TRANSACTION_ITEM_LINE_RIPLEY WHERE TRANSACCION_ID=[TRANSACTION_ID]";
					//+ "FROM TRANSACTION_ITEM_LINE_MASSIVE_RIPLEY WHERE TRANSACCION_ID=[TRANSACTION_ID]";

			sqlQuery = sqlQuery.replace("[TRANSACTION_ID]", transactionId);

			System.out.println("Obteniendo detalle de transaccionId:" + transactionId);
			System.out.println("YULIN QUERY:" + sqlQuery);
			rs = stmt.executeQuery(sqlQuery);

			while (rs.next()) {
				if (first)
					lista = new ArrayList<ETransactionDetail>();
				first = false;
				ETransactionDetail entidad = new ETransactionDetail();
				entidad.idTrx = rs.getString("IDTRX");
				entidad.sku = rs.getString("SKU");
				entidad.fulfillmentCode = rs.getString("FULFILLMENT_CODE");
				entidad.inventoryType = rs.getString("INVENTORY_TYPE");
				entidad.stock = rs.getString("STOCK");
				entidad.prevStock = rs.getString("PREV_STOCK");
				entidad.operation = rs.getString("OPERATION");
				entidad.cud = rs.getString("CUD");
				entidad.status = rs.getString("STATUS");
				entidad.detail = rs.getString("DETAIL");

				lista.add(entidad);
			}
			// conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerResultadoDetalleTransaccion");
			System.err.println(e.getMessage());
		}

		return lista;
	}

	public EStock ObtenerStockProductoBD(String codigoVenta, String bodega, String tipoInventario) {
		EStock oEntidad = new EStock();
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;
			String sqlQuery = "SELECT t2.FULFILLMENT_CODE, t2.INVENTORY_TYPE, t2.QTY, t2.USERNAME FROM SKU t1\n"
					+ "		INNER JOIN SKU_STOCK_RIPLEY t2\n" + "		ON t1.SKU_ID=t2.SKU_ID\n"
					+ "		WHERE t1.COUNTRY_CODE='PE'\n" + "		AND t1.COD_VENTA = '[COD_VENTA]'\n"
					+ "		AND t2.FULFILLMENT_CODE='[FULFILLMENT_CODE]'\n"
					+ "		AND t2.INVENTORY_TYPE='[INVENTORY_TYPE]'";

			sqlQuery = sqlQuery.replace("[COD_VENTA]", codigoVenta);
			sqlQuery = sqlQuery.replace("[FULFILLMENT_CODE]", bodega);
			sqlQuery = sqlQuery.replace("[INVENTORY_TYPE]", tipoInventario);

			 System.out.println("Consulta a la BD Yulin:" + sqlQuery);
			rs = stmt.executeQuery(sqlQuery);

			while (rs.next()) {
				oEntidad.bodega = rs.getString("FULFILLMENT_CODE");
				oEntidad.tipoInventario = rs.getString("INVENTORY_TYPE");
				oEntidad.stock = rs.getInt("QTY");
				oEntidad.username = rs.getString("USERNAME");
			}
			// conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerStockProductoBD");
			System.err.println(e.getMessage());
		}

		return oEntidad;

	}

	public EStockJerarquia ObtenerInformacionStockJerarquiaProductoMayorCero(String codigoPais) {
		EStockJerarquia oEntidad = new EStockJerarquia();
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;
			String sqlQuery = "SELECT  t2.COD_VENTA,\n" + "		t1.FULFILLMENT_CODE,\n" + "		t2.DEPTO_CODE,\n"
					+ "		t2.LINE_CODE,\n" + "		t2.SUBLINE_CODE,\n" + "		t2.`STYLE`,\n" + "		t2.NAME,\n"
					+ "		t1.QTY,\n" + "		t1.INVENTORY_TYPE\n" + "FROM SKU_STOCK_RIPLEY t1\n"
					+ "INNER JOIN SKU t2 ON t1.SKU_ID=t2.SKU_ID\n"
					+ "WHERE  t1.FULFILLMENT_CODE IN ([BODEGAS_HABILITADAS]) AND t2.COUNTRY_CODE='[CODIGO_PAIS]'\n"
					+ "AND t1.QTY>0\n" + "AND LENGTH(t2.COD_VENTA)=13\n" + "LIMIT 1\n" + "";

			sqlQuery = sqlQuery.replace("[CODIGO_PAIS]", codigoPais);
			sqlQuery = sqlQuery.replace("[BODEGAS_HABILITADAS]", bodegasHabilitadas);

			System.out.println(sqlQuery);
			rs = stmt.executeQuery(sqlQuery);

			while (rs.next()) {
				oEntidad.codigoVenta = rs.getString("COD_VENTA");
				oEntidad.fulfillmentCode = rs.getString("FULFILLMENT_CODE");
				oEntidad.deptoCode = rs.getString("DEPTO_CODE");
				oEntidad.lineCode = rs.getString("LINE_CODE");
				oEntidad.subLineCode = rs.getString("SUBLINE_CODE");
				oEntidad.style = rs.getString("STYLE");
				oEntidad.name = rs.getString("NAME");
				oEntidad.qty = rs.getString("QTY");
				oEntidad.inventoryType = rs.getString("INVENTORY_TYPE");
			}
			// conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerInformacionStockJerarquiaProductoMayorCero");
			System.err.println(e.getMessage());
		}
		return oEntidad;
	}

	public EStockJerarquia ObtenerInformacionStockJerarquiaProducto(String codigoPais, String cantidad) {
		EStockJerarquia oEntidad = new EStockJerarquia();
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;
			String sqlQuery = "SELECT * FROM  (SELECT STYLE,COD_VENTA,SUBLINE_CODE,LINE_CODE,DEPTO_CODE,NAME\n"
					+ "FROM SKU WHERE COUNTRY_CODE='PE' AND LENGTH(COD_VENTA)=13 \n" + "LIMIT 100000) a\n"
					+ "order by RAND()\n" + "LIMIT [CANTIDAD]";

			sqlQuery = sqlQuery.replace("[CODIGO_PAIS]", codigoPais);
			sqlQuery = sqlQuery.replace("[CANTIDAD]", cantidad);

			rs = stmt.executeQuery(sqlQuery);

			while (rs.next()) {
				oEntidad.style = rs.getString("STYLE");
				oEntidad.codigoVenta = rs.getString("COD_VENTA");
				oEntidad.subLineCode = rs.getString("SUBLINE_CODE");
				oEntidad.lineCode = rs.getString("LINE_CODE");
				oEntidad.deptoCode = rs.getString("DEPTO_CODE");
				oEntidad.name = rs.getString("NAME");
			}
			// conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerInformacionStockJerarquiaProducto");
			System.err.println(e.getMessage());
		}

		return oEntidad;

	}

	public EStockJerarquia ObtenerInformacionStockJerarquiaProducto(String codigoPais, String cantidad,
			String tipoInventario) {
		EStockJerarquia oEntidad = new EStockJerarquia();
		
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;
			String sqlQuery = "select * from (\n"
					+ "SELECT t1.STYLE,t1.COD_VENTA,t1.SUBLINE_CODE,t1.LINE_CODE,t1.DEPTO_CODE,t1.NAME,t2.FULFILLMENT_CODE,t2.INVENTORY_TYPE, t2.QTY FROM SKU t1\n"
					+ "INNER JOIN SKU_STOCK_RIPLEY t2\n" + "ON t1.SKU_ID = t2.SKU_ID\n"
					+ "WHERE t2.FULFILLMENT_CODE='20027' AND  INVENTORY_TYPE='[TIPO_INVENTARIO]' AND t1.COUNTRY_CODE='[CODIGO_PAIS]' AND LENGTH(t1.SKU)=8 AND LENGTH(t1.COD_VENTA)=13\n"
					+ "LIMIT 100000\n" + ") A \n" + "ORDER BY RAND()\n" + "LIMIT [CANTIDAD]";

			sqlQuery = sqlQuery.replace("[CODIGO_PAIS]", codigoPais);
			sqlQuery = sqlQuery.replace("[CANTIDAD]", cantidad);
			sqlQuery = sqlQuery.replace("[TIPO_INVENTARIO]", tipoInventario);
			System.out.println(sqlQuery);
			rs = stmt.executeQuery(sqlQuery);

			while (rs.next()) {
				System.out.println("Ingresa al While ?:si");
				oEntidad.style = rs.getString("STYLE");
				oEntidad.codigoVenta = rs.getString("COD_VENTA");
				oEntidad.subLineCode = rs.getString("SUBLINE_CODE");
				oEntidad.lineCode = rs.getString("LINE_CODE");
				oEntidad.deptoCode = rs.getString("DEPTO_CODE");
				oEntidad.name = rs.getString("NAME");
				oEntidad.fulfillmentCode = rs.getString("FULFILLMENT_CODE");
				oEntidad.inventoryType = rs.getString("INVENTORY_TYPE");
				oEntidad.qty = rs.getString("QTY");
			}
			// conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerInformacionStockJerarquiaProducto");
			System.err.println(e.getMessage());
		}
		return oEntidad;
	}

	//Calculo de stock ims inicio

	
	
	/*public void ParametrizarPublicados(String oTv_Publish_Products_id ,String codigo_venta) {
		
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			int nuevo_id;
			String Tv_Publish_Products_id;
			
			//nuevo_id = Integer.parseUnsignedInt(oTv_Publish_Products_id);
			nuevo_id =  Integer.parseInt(oTv_Publish_Products_id);
			nuevo_id =nuevo_id+1;
			Tv_Publish_Products_id = Integer.toString(nuevo_id);
		  	String sqlQuery = "INSERT INTO SIC.TV_PUBLISH_PRODUCTS (TV_PUBLISH_PRODUCTS_ID, SYSTEM_CODE, COD_VENTA, LASTUPDATE)\n"
					+ "VALUES([NUEVO_ID],'TV','[COD_VENTA]','2021-09-09 02:54:22')\n";
			
		  	sqlQuery = sqlQuery.replace("[NUEVO_ID]", Tv_Publish_Products_id);
			sqlQuery = sqlQuery.replace("[COD_VENTA]", codigo_venta);
			System.out.println(sqlQuery);
			stmt.executeUpdate(sqlQuery);
			

		} catch (Exception e) {
			System.err.println("Got an exception! - ParametrizarPublicados");
			System.err.println(e.getMessage());
		}
		
	}*/
	
	public void ParametrizarPublicados(String oTv_Publish_Products_id ,String nroregistro, String codigo_venta) {
		
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			int nuevo_id;
			String Tv_Publish_Products_id;
			System.out.println("Valor de max="+oTv_Publish_Products_id);
			System.out.println("Valor de count="+nroregistro);
		
			nuevo_id =  Integer.parseInt(oTv_Publish_Products_id);
			nuevo_id =nuevo_id+1;

			Tv_Publish_Products_id = Integer.toString(nuevo_id);
			System.out.println("Nuevo Correlativo="+Tv_Publish_Products_id);
		  	String sqlQuery = "INSERT INTO SIC.PUBLISHED_PRODUCTS (PUBLISHED_PRODUCTS_ID, SYSTEM_CODE, COD_VENTA,PUBLISHED, LASTUPDATE)\n"
					+ "VALUES([NUEVO_ID],'TV','[COD_VENTA]',1,'2021-09-09 02:54:22')\n";
			
		  	sqlQuery = sqlQuery.replace("[NUEVO_ID]", Tv_Publish_Products_id);
			sqlQuery = sqlQuery.replace("[COD_VENTA]", codigo_venta);
			if (Integer.parseInt(nroregistro)==0)
			{
			System.out.println(sqlQuery);
			stmt.executeUpdate(sqlQuery);
			}

		} catch (Exception e) {
			System.err.println("Got an exception! - ParametrizarPublicados");
			System.err.println(e.getMessage());
		}
		
	}
	
	

	public void ParametrizarWhitelist(String codigo_venta, String codigo_bodega) {
			
	try {
		Connection conn = ConnectSIC();
		Statement stmt = conn.createStatement();
				
	  	String sqlQuery = "INSERT INTO SIC.SKU_STOCK_IMS_WHITELIST (FULFILLMENT_CODE, TYPE_CODE, CODE, STATUS)\n"
				+ "VALUES('[COD_BODEGA]','SKU','[COD_VENTA]',1)\n";
		
		sqlQuery = sqlQuery.replace("[COD_VENTA]", codigo_venta);
		sqlQuery = sqlQuery.replace("[COD_BODEGA]", codigo_bodega);
		System.out.println(sqlQuery);
		stmt.executeUpdate(sqlQuery);
        		

	} catch (Exception e) {
		System.err.println("Got an exception! - ParametrizarWhitelist");
		System.err.println(e.getMessage());
	}
	
}
	
	
	
	public void ParametrizarUmbralStockSkruRipley(String codigo_venta, String codigo_bodega, String umbral, String sts,String dd,String sfs,String pu) {
		
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
		
			String sqlQuery = "UPDATE SIC.SKU_STOCK_RIPLEY SET STS='[STS]', DD='[DD]', SFS='[SFS]', PROTEGIDO=0, PUBLICADO_TV=1, PU='[PU]', UMBRAL='[UMBRAL]'\n"
					+ "WHERE COD_VENTA='[COD_VENTA]' AND INVENTORY_TYPE='1' AND FULFILLMENT_CODE='[COD_BODEGA]'\n";
			
			sqlQuery = sqlQuery.replace("[COD_VENTA]", codigo_venta);
			sqlQuery = sqlQuery.replace("[COD_BODEGA]", codigo_bodega);
			sqlQuery = sqlQuery.replace("[UMBRAL]", umbral);
			sqlQuery = sqlQuery.replace("[STS]", sts);
			sqlQuery = sqlQuery.replace("[DD]", dd);
			sqlQuery = sqlQuery.replace("[SFS]", sfs);
			sqlQuery = sqlQuery.replace("[PU]", pu);
			System.out.println(sqlQuery);
			stmt.executeUpdate(sqlQuery);
	
	
		} catch (Exception e) {
			System.err.println("Got an exception! - ParametrizarUmbralStockSkruRipley");
			System.err.println(e.getMessage());
		}
	
	}
	
	
	
public void ParametrizarAtributoSFS(String codigo_venta, String codigo_bodega) {
		
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
						
		  	String sqlQuery = "INSERT INTO SIC.PRODUCT_HIERARCHY_ATTRIBUTES (FULFILLMENT, CODE, TYPE, NAME, STS, DD, SFS, PU, MARK, ORDEN)\n"
					+ "VALUES('[COD_BODEGA]','[COD_VENTA]', 'SKU', 'INTERFAZ', 0, 0, 1, 1, 1, 1)\n";
			
			
			sqlQuery = sqlQuery.replace("[COD_VENTA]", codigo_venta);
			sqlQuery = sqlQuery.replace("[COD_BODEGA]", codigo_bodega);
			System.out.println(sqlQuery);
		
			stmt.executeUpdate(sqlQuery);

	
		} catch (Exception e) {
			System.err.println("Got an exception! - ParametrizarAtributoSFS");
			System.err.println(e.getMessage());
		}
	
	}
	
	public void ParametrizarAtributoSTS(String codigo_venta, String codigo_bodega) {
		
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
						
		  	String sqlQuery = "INSERT INTO SIC.PRODUCT_HIERARCHY_ATTRIBUTES (FULFILLMENT, CODE, TYPE, NAME, STS, DD, SFS, PU, MARK, ORDEN)\n"
					+ "VALUES('[COD_BODEGA]','[COD_VENTA]', 'SKU', 'INTERFAZ', 1, 0, 0, 1, 1, 1)\n";
			
			
			sqlQuery = sqlQuery.replace("[COD_VENTA]", codigo_venta);
			sqlQuery = sqlQuery.replace("[COD_BODEGA]", codigo_bodega);
			System.out.println(sqlQuery);
		
			stmt.executeUpdate(sqlQuery);

	
		} catch (Exception e) {
			System.err.println("Got an exception! - ParametrizarAtributoSTS");
			System.err.println(e.getMessage());
		}
	
	}
	
	public void ParametrizarAtributo(String sts,String dd, String sfs,String pu,String DEPTO, String codigo_bodega) {
		
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
						
		  	String sqlQuery = "INSERT INTO SIC.PRODUCT_HIERARCHY_ATTRIBUTES (FULFILLMENT, CODE, TYPE, NAME, STS, DD, SFS, PU, MARK, ORDEN)\n"
					+ "VALUES('[COD_BODEGA]','[COD_DEPTO]','DEPTO','INTERFAZ','[STS]','[DD]','[SFS]','[PU]', 1, 1)\n";
			
			
			sqlQuery = sqlQuery.replace("[COD_DEPTO]", DEPTO);
			sqlQuery = sqlQuery.replace("[COD_BODEGA]", codigo_bodega);
			sqlQuery = sqlQuery.replace("[STS]", sts);
			sqlQuery = sqlQuery.replace("[DD]", dd);
			sqlQuery = sqlQuery.replace("[SFS]", sfs);
			sqlQuery = sqlQuery.replace("[PU]", pu);
			System.out.println(sqlQuery);
		
			stmt.executeUpdate(sqlQuery);

	
		} catch (Exception e) {
			System.err.println("Got an exception! - ParametrizarAtributo");
			System.err.println(e.getMessage());
		}
	
	}
	
	
	public void ParametrizarAtributoDD(String codigo_venta, String codigo_bodega) {
    			
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
						
		  	String sqlQuery = "INSERT INTO SIC.PRODUCT_HIERARCHY_ATTRIBUTES (FULFILLMENT, CODE, TYPE, NAME, STS, DD, SFS, PU, MARK, ORDEN)\n"
					+ "VALUES('[COD_BODEGA]','[COD_VENTA]', 'SKU', 'INTERFAZ', 0, 1, 0, 0, 1, 1)\n";
			
			
			sqlQuery = sqlQuery.replace("[COD_VENTA]", codigo_venta);
			sqlQuery = sqlQuery.replace("[COD_BODEGA]", codigo_bodega);
			System.out.println(sqlQuery);
		
			stmt.executeUpdate(sqlQuery);

	
		} catch (Exception e) {
			System.err.println("Got an exception! - ParametrizarAtributoDD");
			System.err.println(e.getMessage());
		}
	
	}
	
	public List<EStockXBodega> ObtenerInformacionStockProductoBD(String codigo_venta) {
		//EStockXBodega oEntidad = new EStockXBodega();
		List<EStockXBodega> oLista = new ArrayList<EStockXBodega>();
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;
		  
			String sqlQuery = "SELECT t2.FULFILLMENT_CODE,t2.QTY,t2.DISPONIBLE,t2.PROTEGIDO,t2.PUBLICADO_TV,t2.UMBRAL, t2.STS,t2.DD,t2.SFS,t2.PU FROM SKU t1\n"
					+ "INNER JOIN SKU_STOCK_RIPLEY t2\n" 
					+ "ON t1.SKU_ID = t2.SKU_ID\n"
					+ "WHERE t2.COD_VENTA='[COD_VENTA]' AND LENGTH(t1.COD_VENTA)=13\n";
			sqlQuery = sqlQuery.replace("[COD_VENTA]", codigo_venta);
			System.out.println(sqlQuery);
            rs = stmt.executeQuery(sqlQuery);
	
			while (rs.next()) {
				EStockXBodega oEntidad = new EStockXBodega();
				oEntidad.FULFILLMENT_CODE = rs.getString("FULFILLMENT_CODE");
				oEntidad.QTY = rs.getString("QTY");	
				oEntidad.DISPONIBLE = rs.getString("DISPONIBLE");	
				oEntidad.PROTEGIDO = rs.getString("PROTEGIDO");	
				oEntidad.PUBLICADO_TV = rs.getString("PUBLICADO_TV");
				oEntidad.UMBRAL = rs.getString("UMBRAL");	
				oEntidad.STS = rs.getString("STS");
				oEntidad.DD = rs.getString("DD");	
				oEntidad.SFS = rs.getString("SFS");
				oEntidad.PU = rs.getString("PU");
				oLista.add(oEntidad);

           			}
		
			// conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerInformacionStockJerarquiaProducto");
			System.err.println(e.getMessage());
		}
		return oLista;
	}
	//Caluclo de stock ims fin

	public ESku ObtenerDepartamentoProducto(String codigo_venta)
	{	ESku oEntidad = new ESku();

		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;

			String sqlQuery = "SELECT  DEPTO_CODE from SKU WHERE COD_VENTA='[COD_VENTA]';\n";
			sqlQuery = sqlQuery.replace("[COD_VENTA]", codigo_venta);

			System.out.println(sqlQuery);
			rs = stmt.executeQuery(sqlQuery);

			while (rs.next()) {

				oEntidad.deptoCode= rs.getString("DEPTO_CODE");
				System.err.println("resultado 1 : "+ oEntidad.deptoCode);
			}
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerDepartamentoProducto");
			System.err.println(e.getMessage());
		}

		return oEntidad;
	}

	public ETv_Publish_Products ObtenerTV_PUBLISH_PRODUCTS(String codigo_venta)
	{	ETv_Publish_Products oEntidad = new ETv_Publish_Products();

		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;

			String sqlQuery = "SELECT MAX(PUBLISHED_PRODUCTS_ID) AS TV_PUBLISH_PRODUCTS_ID FROM PUBLISHED_PRODUCTS\n";
			System.out.println(sqlQuery);
			rs = stmt.executeQuery(sqlQuery);

			while (rs.next()) {

				oEntidad.TV_PUBLISH_PRODUCTS_ID = rs.getString("TV_PUBLISH_PRODUCTS_ID");
				System.err.println("resultado 1 : "+ oEntidad.TV_PUBLISH_PRODUCTS_ID);
			}
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerTV_PUBLISH_PRODUCTS");
			System.err.println(e.getMessage());
		}

		return oEntidad;
	}

	public EContador ObtenerTV_PUBLISH_PRODUCTS_Count(String codigo_venta)
	{	EContador oEntidad = new EContador();
		
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;	
			
		  	String sqlQuery = "SELECT COUNT(1) AS NROREGISTRO FROM PUBLISHED_PRODUCTS WHERE COD_VENTA='[COD_VENTA]';\n";
		  	sqlQuery = sqlQuery.replace("[COD_VENTA]", codigo_venta);		  	
		  	System.out.println(sqlQuery);
			rs = stmt.executeQuery(sqlQuery);
			
			   while (rs.next()) {
					
				oEntidad.CONTADOR = rs.getString("NROREGISTRO");
			
					} 	
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerTV_PUBLISH_PRODUCTS_Count");
			System.err.println(e.getMessage());
		}
		             
        return oEntidad;	
	}
	
	
	public EStockJerarquia ObtenerInformacionStockJerarquiaProductoMenorCero(String codigoPais) {
		EStockJerarquia oEntidad = new EStockJerarquia();
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;
			String sqlQuery = "SELECT  t2.COD_VENTA,\n" + "		t1.FULFILLMENT_CODE,\n" + "		t2.DEPTO_CODE,\n"
					+ "		t2.LINE_CODE,\n" + "		t2.SUBLINE_CODE,\n" + "		t2.`STYLE`,\n" + "		t2.NAME,\n"
					+ "		t1.QTY,\n" + "		t1.INVENTORY_TYPE\n" + "FROM SKU_STOCK_RIPLEY t1\n"
					+ "INNER JOIN SKU t2 ON t1.SKU_ID=t2.SKU_ID\n"
					+ "WHERE  t1.FULFILLMENT_CODE IN ([BODEGAS_HABILITADAS]) AND t2.COUNTRY_CODE='[CODIGO_PAIS]'\n"
					+ "AND t1.QTY<0\n" + "AND LENGTH(t2.COD_VENTA)=13\n" + "LIMIT 1\n" + "";

			sqlQuery = sqlQuery.replace("[CODIGO_PAIS]", codigoPais);
			sqlQuery = sqlQuery.replace("[BODEGAS_HABILITADAS]", bodegasHabilitadas);

			// System.out.println(sqlQuery);
			rs = stmt.executeQuery(sqlQuery);

			while (rs.next()) {
				oEntidad.codigoVenta = rs.getString("COD_VENTA");
				oEntidad.fulfillmentCode = rs.getString("FULFILLMENT_CODE");
				oEntidad.deptoCode = rs.getString("DEPTO_CODE");
				oEntidad.lineCode = rs.getString("LINE_CODE");
				oEntidad.subLineCode = rs.getString("SUBLINE_CODE");
				oEntidad.style = rs.getString("STYLE");
				oEntidad.name = rs.getString("NAME");
				oEntidad.qty = rs.getString("QTY");
				oEntidad.inventoryType = rs.getString("INVENTORY_TYPE");
			}
			// conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerInformacionStockJerarquiaProductoMayorCero");
			System.err.println(e.getMessage());
		}

		return oEntidad;

	}

	public EStockJerarquia ObtenerInformacionStockJerarquiaProductoIgualCero(String codigoPais) {
		EStockJerarquia oEntidad = new EStockJerarquia();
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;
			String sqlQuery = "SELECT  t2.COD_VENTA,\n" + "		t1.FULFILLMENT_CODE,\n" + "		t2.DEPTO_CODE,\n"
					+ "		t2.LINE_CODE,\n" + "		t2.SUBLINE_CODE,\n" + "		t2.`STYLE`,\n" + "		t2.NAME,\n"
					+ "		t1.QTY,\n" + "		t1.INVENTORY_TYPE\n" + "FROM SKU_STOCK_RIPLEY t1\n"
					+ "INNER JOIN SKU t2 ON t1.SKU_ID=t2.SKU_ID\n"
					+ "WHERE  t1.FULFILLMENT_CODE IN ([BODEGAS_HABILITADAS]) AND t2.COUNTRY_CODE='[CODIGO_PAIS]'\n"
					+ "AND t1.QTY=0\n" + "AND LENGTH(t2.COD_VENTA)=13\n" + "LIMIT 1\n" + "";

			sqlQuery = sqlQuery.replace("[CODIGO_PAIS]", codigoPais);
			sqlQuery = sqlQuery.replace("[BODEGAS_HABILITADAS]", bodegasHabilitadas);

			 System.out.println(sqlQuery);
			rs = stmt.executeQuery(sqlQuery);

			while (rs.next()) {
				oEntidad.codigoVenta = rs.getString("COD_VENTA");
				oEntidad.fulfillmentCode = rs.getString("FULFILLMENT_CODE");
				oEntidad.deptoCode = rs.getString("DEPTO_CODE");
				oEntidad.lineCode = rs.getString("LINE_CODE");
				oEntidad.subLineCode = rs.getString("SUBLINE_CODE");
				oEntidad.style = rs.getString("STYLE");
				oEntidad.name = rs.getString("NAME");
				oEntidad.qty = rs.getString("QTY");
				oEntidad.inventoryType = rs.getString("INVENTORY_TYPE");
			}
			// conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerInformacionStockJerarquiaProductoMayorCero");
			System.err.println(e.getMessage());
		}

		return oEntidad;

	}

	public EStockJerarquia ObtenerInformacionStockJerarquiaProductoMayorIgualA(String codigoPais, String stock) {
		EStockJerarquia oEntidad = new EStockJerarquia();
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;
			String sqlQuery = "SELECT  t2.COD_VENTA,\n" + "		t1.FULFILLMENT_CODE,\n" + "		t2.DEPTO_CODE,\n"
					+ "		t2.LINE_CODE,\n" + "		t2.SUBLINE_CODE,\n" + "		t2.`STYLE`,\n" + "		t2.NAME,\n"
					+ "		t1.QTY,\n" + "		t1.INVENTORY_TYPE\n" + "FROM SKU_STOCK_RIPLEY t1\n"
					+ "INNER JOIN SKU t2 ON t1.SKU_ID=t2.SKU_ID\n"
					+ "WHERE  t1.FULFILLMENT_CODE IN ([BODEGAS_HABILITADAS]) AND t2.COUNTRY_CODE='[CODIGO_PAIS]'\n"
					+ "AND t1.QTY>=[STOCK]\n" + "AND LENGTH(t2.COD_VENTA)=13\n" + "LIMIT 1\n" + "";

			sqlQuery = sqlQuery.replace("[CODIGO_PAIS]", codigoPais);
			sqlQuery = sqlQuery.replace("[STOCK]", stock);
			sqlQuery = sqlQuery.replace("[BODEGAS_HABILITADAS]", bodegasHabilitadas);

			// System.out.println(sqlQuery);
			rs = stmt.executeQuery(sqlQuery);

			while (rs.next()) {
				oEntidad.codigoVenta = rs.getString("COD_VENTA");
				oEntidad.fulfillmentCode = rs.getString("FULFILLMENT_CODE");
				oEntidad.deptoCode = rs.getString("DEPTO_CODE");
				oEntidad.lineCode = rs.getString("LINE_CODE");
				oEntidad.subLineCode = rs.getString("SUBLINE_CODE");
				oEntidad.style = rs.getString("STYLE");
				oEntidad.name = rs.getString("NAME");
				oEntidad.qty = rs.getString("QTY");
				oEntidad.inventoryType = rs.getString("INVENTORY_TYPE");
			}
			// conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerInformacionStockJerarquiaProductoMayorCero");
			System.err.println(e.getMessage());
		}

		return oEntidad;

	}

	public EStockJerarquia ObtenerInformacionStockJerarquiaProductoMenorIgualA(String codigoPais, String stock) {
		EStockJerarquia oEntidad = new EStockJerarquia();
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;
			String sqlQuery = "SELECT  t2.COD_VENTA,\n" + "		t1.FULFILLMENT_CODE,\n" + "		t2.DEPTO_CODE,\n"
					+ "		t2.LINE_CODE,\n" + "		t2.SUBLINE_CODE,\n" + "		t2.`STYLE`,\n" + "		t2.NAME,\n"
					+ "		t1.QTY,\n" + "		t1.INVENTORY_TYPE\n" + "FROM SKU_STOCK_RIPLEY t1\n"
					+ "INNER JOIN SKU t2 ON t1.SKU_ID=t2.SKU_ID\n"
					+ "WHERE  t1.FULFILLMENT_CODE IN ([BODEGAS_HABILITADAS]) AND t2.COUNTRY_CODE='[CODIGO_PAIS]'\n"
					+ "AND t1.QTY<=[STOCK]\n" + "AND LENGTH(t2.COD_VENTA)=13\n" + "LIMIT 1\n" + "";

			sqlQuery = sqlQuery.replace("[CODIGO_PAIS]", codigoPais);
			sqlQuery = sqlQuery.replace("[STOCK]", stock);
			sqlQuery = sqlQuery.replace("[BODEGAS_HABILITADAS]", bodegasHabilitadas);

			// System.out.println(sqlQuery);
			rs = stmt.executeQuery(sqlQuery);

			while (rs.next()) {
				oEntidad.codigoVenta = rs.getString("COD_VENTA");
				oEntidad.fulfillmentCode = rs.getString("FULFILLMENT_CODE");
				oEntidad.deptoCode = rs.getString("DEPTO_CODE");
				oEntidad.lineCode = rs.getString("LINE_CODE");
				oEntidad.subLineCode = rs.getString("SUBLINE_CODE");
				oEntidad.style = rs.getString("STYLE");
				oEntidad.name = rs.getString("NAME");
				oEntidad.qty = rs.getString("QTY");
				oEntidad.inventoryType = rs.getString("INVENTORY_TYPE");
			}
			// conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerInformacionStockJerarquiaProductoMayorCero");
			System.err.println(e.getMessage());
		}

		return oEntidad;

	}

	public EStockJerarquia ObtenerInformacionStockJerarquiaProductoDiferenteA(String codigoPais, String stock) {
		EStockJerarquia oEntidad = new EStockJerarquia();
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;
			String sqlQuery = "SELECT  t2.COD_VENTA,\n" + "		t1.FULFILLMENT_CODE,\n" + "		t2.DEPTO_CODE,\n"
					+ "		t2.LINE_CODE,\n" + "		t2.SUBLINE_CODE,\n" + "		t2.`STYLE`,\n" + "		t2.NAME,\n"
					+ "		t1.QTY,\n" + "		t1.INVENTORY_TYPE\n" + "FROM SKU_STOCK_RIPLEY t1\n"
					+ "INNER JOIN SKU t2 ON t1.SKU_ID=t2.SKU_ID\n"
					+ "WHERE  t1.FULFILLMENT_CODE IN ([BODEGAS_HABILITADAS]) AND t2.COUNTRY_CODE='[CODIGO_PAIS]'\n"
					+ "AND t1.QTY<>[STOCK]\n" + "AND LENGTH(t2.COD_VENTA)=13\n" + "LIMIT 1\n" + "";

			sqlQuery = sqlQuery.replace("[CODIGO_PAIS]", codigoPais);
			sqlQuery = sqlQuery.replace("[STOCK]", stock);
			sqlQuery = sqlQuery.replace("[BODEGAS_HABILITADAS]", bodegasHabilitadas);

			System.out.println(sqlQuery);
			rs = stmt.executeQuery(sqlQuery);

			while (rs.next()) {
				oEntidad.codigoVenta = rs.getString("COD_VENTA");
				oEntidad.fulfillmentCode = rs.getString("FULFILLMENT_CODE");
				oEntidad.deptoCode = rs.getString("DEPTO_CODE");
				oEntidad.lineCode = rs.getString("LINE_CODE");
				oEntidad.subLineCode = rs.getString("SUBLINE_CODE");
				oEntidad.style = rs.getString("STYLE");
				oEntidad.name = rs.getString("NAME");
				oEntidad.qty = rs.getString("QTY");
				oEntidad.inventoryType = rs.getString("INVENTORY_TYPE");
			}
			// conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerInformacionStockJerarquiaProductoMayorCero");
			System.err.println(e.getMessage());
		}

		return oEntidad;

	}

	public EStock ObtenerProductoStockRipley(String cantidad, String tipoInventario) {
		EStock oEntidad = new EStock();
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;
			String sqlQuery = "SELECT t1.COD_VENTA,t2.FULFILLMENT_CODE, t2.INVENTORY_TYPE,t2.QTY,t2.USERNAME FROM SKU t1\n"
					+ "INNER JOIN SKU_STOCK_RIPLEY t2\n" + "ON t1.SKU_ID=t2.SKU_ID\n" + "WHERE t2.QTY>0\n"
					+ "AND t1.COUNTRY_CODE='PE'\n" + "AND LENGTH(t1.COD_VENTA)>12\n"
					+ "AND t2.INVENTORY_TYPE='[TIPO_INVENTARIO]'   AND t2.FULFILLMENT_CODE IN ([BODEGAS_HABILITADAS]) \n"
					+ "LIMIT [LIMIT]";

			sqlQuery = sqlQuery.replace("[LIMIT]", cantidad);
			sqlQuery = sqlQuery.replace("[TIPO_INVENTARIO]", tipoInventario);
			sqlQuery = sqlQuery.replace("[BODEGAS_HABILITADAS]", bodegasHabilitadas);

			System.out.println(sqlQuery);
			rs = stmt.executeQuery(sqlQuery);

			while (rs.next()) {
				oEntidad.codigoVenta = rs.getString("COD_VENTA");
				oEntidad.bodega = rs.getString("FULFILLMENT_CODE");
				oEntidad.tipoInventario = rs.getString("INVENTORY_TYPE");
				oEntidad.stock = rs.getInt("QTY");
				oEntidad.username = rs.getString("USERNAME");
			}
			// conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerProductoStockRipley");
			System.err.println(e.getMessage());
		}

		return oEntidad;

	}

	public List<EStock> ObtenerListaProductoStockRipley(String cantidad) {

		List<EStock> oListaProductos = new ArrayList<EStock>();
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;
			String sqlQuery = "SELECT * FROM(\n"
					+ "	SELECT t1.COD_VENTA,t2.FULFILLMENT_CODE, t2.INVENTORY_TYPE,t2.QTY FROM SKU t1\n"
					+ "	INNER JOIN SKU_STOCK_RIPLEY t2\n" + "	ON t1.SKU_ID=t2.SKU_ID\n" + "	WHERE t2.QTY>0\n"
					+ "	AND t1.COUNTRY_CODE='PE'\n" + "	AND LENGTH(t1.COD_VENTA)>12\n"
					+ "	AND t2.INVENTORY_TYPE='[TIPO_INVENTARIO]' \n" + "	LIMIT 61000)A \n" + "	ORDER BY RAND() \n"
					+ "	LIMIT [LIMIT]";

			sqlQuery = sqlQuery.replace("[LIMIT]", cantidad);
			sqlQuery = sqlQuery.replace("[TIPO_INVENTARIO]", "1");

			System.out.println(sqlQuery);
			rs = stmt.executeQuery(sqlQuery);

			while (rs.next()) {
				System.out.println("obteniendo objeto");
				EStock oEntidad = new EStock();
				oEntidad.codigoVenta = rs.getString("COD_VENTA");
				oEntidad.bodega = rs.getString("FULFILLMENT_CODE");
				oEntidad.tipoInventario = rs.getString("INVENTORY_TYPE");
				oEntidad.stock = rs.getInt("QTY");
				System.out.println("PRODUCTO ENCONTRADO: " + oEntidad.codigoVenta);
				oListaProductos.add(oEntidad);

			}
			//conn.close();
			//stmt.close();
			//rs.close();
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerProductoStockRipley");
			System.err.println(e.getMessage());
		}

		return oListaProductos;
	}

	public List<EStock> ObtenerListaProductoStockRipley(String cantidad, String bodega) {

		List<EStock> oListaProductos = new ArrayList<EStock>();
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;
			String sqlQuery = "SELECT * FROM(\n"
					+ "	SELECT t1.COD_VENTA,t2.FULFILLMENT_CODE, t2.INVENTORY_TYPE,t2.QTY FROM SKU t1\n"
					+ "	INNER JOIN SKU_STOCK_RIPLEY t2\n" + "	ON t1.SKU_ID=t2.SKU_ID\n" + "	WHERE t2.QTY>0\n"
					+ "	AND t1.COUNTRY_CODE='PE'\n" + "	AND LENGTH(t1.COD_VENTA)>12\n"
					+ "	AND t2.INVENTORY_TYPE='[TIPO_INVENTARIO]' AND t2.FULFILLMENT_CODE IN ([BODEGAS_HABILITADAS]) \n"
					+ "	LIMIT 10000)A \n" + "	ORDER BY RAND() \n" + "	LIMIT [LIMIT]";

			sqlQuery = sqlQuery.replace("[LIMIT]", cantidad);
			sqlQuery = sqlQuery.replace("[BODEGAS_HABILITADAS]", "'" + bodega + "'");
			sqlQuery = sqlQuery.replace("[TIPO_INVENTARIO]", "1");

			 System.out.println(sqlQuery);
			rs = stmt.executeQuery(sqlQuery);

			while (rs.next()) {
				EStock oEntidad = new EStock();
				oEntidad.codigoVenta = rs.getString("COD_VENTA");
				oEntidad.bodega = rs.getString("FULFILLMENT_CODE");
				oEntidad.tipoInventario = rs.getString("INVENTORY_TYPE");
				oEntidad.stock = rs.getInt("QTY");
				System.out.println("PRODUCTO ENCONTRADO: " + oEntidad.codigoVenta);
				oListaProductos.add(oEntidad);

			}
			// conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerProductoStockRipley");
			System.err.println(e.getMessage());
		}

		return oListaProductos;
	}

	public EStock ObtenerProductoStockRipleyNegativo(String tipoInventario) {
		EStock oEntidad = new EStock();
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;
			String sqlQuery = "SELECT t1.COD_VENTA,t2.FULFILLMENT_CODE, t2.INVENTORY_TYPE,t2.QTY,t2.USERNAME FROM SKU t1\n"
					+ "INNER JOIN SKU_STOCK_RIPLEY t2\n" + "ON t1.SKU_ID=t2.SKU_ID\n" + "WHERE t2.QTY<0\n"
					+ "AND t1.COUNTRY_CODE='PE'\n" + "AND LENGTH(t1.COD_VENTA)>12\n"
					+ "AND t2.INVENTORY_TYPE='[TIPO_INVENTARIO]'  AND t2.FULFILLMENT_CODE IN ([BODEGAS_HABILITADAS]) \n"
					+ "LIMIT 1";

			sqlQuery = sqlQuery.replace("[TIPO_INVENTARIO]", tipoInventario);
			sqlQuery = sqlQuery.replace("[BODEGAS_HABILITADAS]", bodegasHabilitadas);

			System.out.println(sqlQuery);
			rs = stmt.executeQuery(sqlQuery);

			while (rs.next()) {
				oEntidad.codigoVenta = rs.getString("COD_VENTA");
				oEntidad.bodega = rs.getString("FULFILLMENT_CODE");
				oEntidad.tipoInventario = rs.getString("INVENTORY_TYPE");
				oEntidad.stock = rs.getInt("QTY");
				oEntidad.username = rs.getString("USERNAME");
			}
			// conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerProductoStockRipley");
			System.err.println(e.getMessage());
		}

		return oEntidad;

	}

	public EStock ObtenerProductoStockRipleyPositivo(String tipoInventario) {
		EStock oEntidad = new EStock();
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;
			String sqlQuery = "SELECT t1.COD_VENTA,t2.FULFILLMENT_CODE, t2.INVENTORY_TYPE,t2.QTY,t2.USERNAME FROM SKU t1\n"
					+ "INNER JOIN SKU_STOCK_RIPLEY t2\n" + "ON t1.SKU_ID=t2.SKU_ID\n" + "WHERE t2.QTY>0\n"
					+ "AND t1.COUNTRY_CODE='PE'\n" + "AND LENGTH(t1.COD_VENTA)>12\n"
					+ "AND t2.INVENTORY_TYPE='[TIPO_INVENTARIO]'  AND t2.FULFILLMENT_CODE IN ([BODEGAS_HABILITADAS]) \n"
					+ "LIMIT 1";

			sqlQuery = sqlQuery.replace("[TIPO_INVENTARIO]", tipoInventario);
			sqlQuery = sqlQuery.replace("[BODEGAS_HABILITADAS]", bodegasHabilitadas);

			System.out.println(sqlQuery);
			rs = stmt.executeQuery(sqlQuery);

			while (rs.next()) {
				oEntidad.codigoVenta = rs.getString("COD_VENTA");
				oEntidad.bodega = rs.getString("FULFILLMENT_CODE");
				oEntidad.tipoInventario = rs.getString("INVENTORY_TYPE");
				oEntidad.stock = rs.getInt("QTY");
				oEntidad.username = rs.getString("USERNAME");
			}
			// conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerProductoStockRipley");
			System.err.println(e.getMessage());
		}

		return oEntidad;

	}

	public EStock ObtenerProductoStockRipleyCero(String tipoInventario) {
		EStock oEntidad = new EStock();
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;
			String sqlQuery = "SELECT t1.COD_VENTA,t2.FULFILLMENT_CODE, t2.INVENTORY_TYPE,t2.QTY,t2.USERNAME FROM SKU t1\n"
					+ "INNER JOIN SKU_STOCK_RIPLEY t2\n" + "ON t1.SKU_ID=t2.SKU_ID\n" + "WHERE t2.QTY=0\n"
					+ "AND t1.COUNTRY_CODE='PE'\n" + "AND LENGTH(t1.COD_VENTA)>12\n"
					+ "AND t2.INVENTORY_TYPE='[TIPO_INVENTARIO]' AND t2.FULFILLMENT_CODE IN ([BODEGAS_HABILITADAS]) \n"
					+ "LIMIT 1";

			sqlQuery = sqlQuery.replace("[TIPO_INVENTARIO]", tipoInventario);
			sqlQuery = sqlQuery.replace("[BODEGAS_HABILITADAS]", bodegasHabilitadas);

			System.out.println(sqlQuery);
			rs = stmt.executeQuery(sqlQuery);

			while (rs.next()) {
				oEntidad.codigoVenta = rs.getString("COD_VENTA");
				oEntidad.bodega = rs.getString("FULFILLMENT_CODE");
				oEntidad.tipoInventario = rs.getString("INVENTORY_TYPE");
				oEntidad.stock = rs.getInt("QTY");
				oEntidad.username = rs.getString("USERNAME");
			}
			// conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerProductoStockRipley");
			System.err.println(e.getMessage());
		}

		return oEntidad;

	}

	public EStock ObtenerProductoSinStockRipley() {
		EStock oEntidad = new EStock();
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;
			String sqlQuery = "SELECT t1.COD_VENTA,'' FULFILLMENT_CODE, '1' INVENTORY_TYPE,'' QTY, '' USERNAME FROM SKU t1\n"
					+ "LEFT JOIN SKU_STOCK_RIPLEY t2\n" + "ON t1.SKU_ID=t2.SKU_ID\n" + "where LENGTH(t1.COD_VENTA)>12\n"
					+ "and t2.SKU_ID is NULL\n" + "LIMIT 1";

			System.out.println(sqlQuery);
			rs = stmt.executeQuery(sqlQuery);

			while (rs.next()) {
				oEntidad.codigoVenta = rs.getString("COD_VENTA");
				oEntidad.bodega = rs.getString("FULFILLMENT_CODE");
				oEntidad.tipoInventario = rs.getString("INVENTORY_TYPE");
				oEntidad.stock = rs.getInt("QTY");
				oEntidad.username = rs.getString("USERNAME");
			}
			// conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerProductoSinStockRipley");
			System.err.println(e.getMessage());
		}

		return oEntidad;

	}

	public List<ESku> ObtenerCodigoVenta(String codigoPais, int cantidad)
			throws IOException, InterruptedException, SQLException {
		List<ESku> oLista = new ArrayList<ESku>();
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;

			String sqlQuery = "SELECT COD_VENTA FROM SKU WHERE COUNTRY_CODE='" + codigoPais
					+ "'  AND LENGTH(COD_VENTA)>11 ORDER BY RAND() LIMIT " + cantidad;

			// System.out.println("Obteniendo " + cantidad + " de "+ codigoPais + " Sku");
			rs = stmt.executeQuery(sqlQuery);
			while (rs.next()) {
				ESku oEntidad = new ESku();
				oEntidad.sku = rs.getString("COD_VENTA");
				oLista.add(oEntidad);
			}
			// conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerCodigoVenta");
			System.err.println(e.getMessage());
		}
		return oLista;
	}

	public List<EBodega> ObtenerBodegas(String codigoPais, int cantidad)
			throws IOException, InterruptedException, SQLException {
		List<EBodega> oLista = new ArrayList<EBodega>();
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;

			String sqlQuery = "SELECT CODE, NAME FROM LOCATION WHERE COUNTRY_CODE='" + codigoPais + "' LIMIT "
					+ cantidad;

//			System.out.println("Obteniendo " + cantidad + " de "+ codigoPais + " Sku");
			rs = stmt.executeQuery(sqlQuery);
			while (rs.next()) {
				EBodega oEntidad = new EBodega();
				oEntidad.codigoBodega = rs.getString("CODE");
				oEntidad.nombre = rs.getString("NAME");
				oEntidad.codigoPais = codigoPais;
				oLista.add(oEntidad);
			}
			// conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerBodegas");
			System.err.println(e.getMessage());
		}
		return oLista;
	}

	public List<ESku> ObtenerSkuConStockActual(String codigoPais, int cantidad)
			throws IOException, InterruptedException, SQLException {

		List<ESku> oLista = new ArrayList<ESku>();
		try {

			String bodega = ObtenerBodegaAlAzar(codigoPais, cantidad);
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;

			String sqlQuery = "SELECT  t1.SKU, t2.FULFILLMENT_CODE, t1.SELLER, t2.INVENTOR"
					+ "Y_TYPE, t2.QTY FROM SKU t1 \r\n" + "INNER JOIN SKU_STOCK_RIPLEY t2 ON t1.SKU_ID=t2.SKU_ID\r\n"
					+ "WHERE t1.COUNTRY_CODE='" + codigoPais + "'\r\n" + "AND t2.FULFILLMENT_CODE='" + bodega + "' \r\n"
					+ "AND LENGTH(t1.SELLER)>0\r\n" + "AND LENGTH(t2.INVENTORY_TYPE)>0 ORDER BY RAND()\r\n" + "LIMIT "
					+ cantidad;

			System.out.println("Obteniendo " + cantidad + " de " + codigoPais + " Sku con Stock actual");
			rs = stmt.executeQuery(sqlQuery);
			while (rs.next()) {
				ESku oEntidad = new ESku();
				oEntidad.sku = rs.getString("SKU");
				oEntidad.bodega = rs.getString("FULFILLMENT_CODE");
				oEntidad.seller = rs.getString("SELLER");
				oEntidad.tipoInventario = rs.getString("INVENTORY_TYPE");
				oEntidad.stock = rs.getString("QTY");
				oEntidad.stockEsDiferente = "0";
				oLista.add(oEntidad);
			}
			// conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerSkuConStockActual");
			System.err.println(e.getMessage());
		}

		return oLista;

	}

	public List<ESku> ObtenerSkuConStockActualYDiferente(String codigoPais, int cantidad)
			throws IOException, InterruptedException, SQLException {

		List<ESku> oLista = new ArrayList<ESku>();
		try {

			String bodega = ObtenerBodegaAlAzar(codigoPais, cantidad);
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;

			String sqlQuery = "SELECT t2.COD_VENTA, t1.FULFILLMENT_CODE, t2.SELLER, t1.INVENTORY_TYPE, \r\n"
					+ "		t1.QTY,IF(RIGHT(t2.COD_VENTA, 1) % 2 <> 0, true, false) STOCK_DIFERENTE\r\n"
					+ "FROM SKU_STOCK_RIPLEY t1\r\n" + "INNER JOIN SKU t2 on t1.SKU_ID=t2.SKU_ID\r\n"
					+ "WHERE t2.COUNTRY_CODE='" + codigoPais + "'\r\n" + "AND LENGTH(t2.SELLER)>0\r\n"
					+ "AND t1.FULFILLMENT_CODE='" + bodega + "'\r\n" + "LIMIT 100";

			System.out.println(
					"Obteniendo " + cantidad + "sku's con stock de la bodega " + bodega + " del pais " + codigoPais);
			rs = stmt.executeQuery(sqlQuery);
			while (rs.next()) {
				ESku oEntidad = new ESku();
				oEntidad.sku = rs.getString("SKU");
				oEntidad.bodega = rs.getString("FULFILLMENT_CODE");
				oEntidad.seller = rs.getString("SELLER");
				oEntidad.tipoInventario = rs.getString("INVENTORY_TYPE");
				oEntidad.stock = rs.getString("QTY");
				oEntidad.stockEsDiferente = rs.getString("STOCK_DIFERENTE");
				oLista.add(oEntidad);
			}
			// conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! ");
			System.err.println(e.getMessage());
		}

		return oLista;

	}

	private String ObtenerBodegaAlAzar(String codigoPais, int cantidadRegistros) {

		String bodega = "20027";
//		try {
//			Connection conn = ConnectSIC();
//			Statement stmt = conn.createStatement();
//			ResultSet rs;
//
//			String sqlQuery = "SELECT FULFILLMENT_CODE FROM (\r\n"
//					+ "SELECT  t2.FULFILLMENT_CODE, COUNT(t2.FULFILLMENT_CODE) cantidad FROM SKU t1 \r\n"
//					+ "INNER JOIN SKU_STOCK_RIPLEY t2 ON t1.SKU_ID=t2.SKU_ID\r\n"
//					+ "INNER JOIN LOCATION t3 ON t2.FULFILLMENT_CODE=t3.CODE\r\n" + "WHERE t3.COUNTRY_CODE='"
//					+ codigoPais + "'\r\n" + "AND LENGTH(t2.FULFILLMENT_CODE)>0\r\n" + "AND LENGTH(t1.SELLER)>0\r\n"
//					+ "AND t3.NAME NOT LIKE '%NO USAR%' AND LENGTH(t2.INVENTORY_TYPE)>0\r\n"
//					+ "GROUP BY t2.FULFILLMENT_CODE\r\n" + "ORDER BY RAND()\r\n" + ") A\r\n" + "WHERE cantidad>"
//					+ cantidadRegistros + "\r\n" + "LIMIT 1";
//			System.out.println("ObtenerBodegaAlAzar: " + sqlQuery);
//			rs = stmt.executeQuery(sqlQuery);
//			while (rs.next()) {
//				bodega = rs.getString("FULFILLMENT_CODE");
//			}
//
//			if (bodega.equals("")) {
//
//				List<EBodega> oBodegaList = new ArrayList<EBodega>();
//				oBodegaList = ObtenerBodegas(codigoPais, 1);
//				bodega = oBodegaList.get(0).codigoBodega;
//				if (bodega.equals("")) {
//					oBodegaList = ObtenerBodegas(codigoPais, 1);
//				}
//				System.out.println("Obteniendo bodegaaa : " + bodega);
//			}
//			System.out.println(bodega);
//			// conn.close();
//		} catch (Exception e) {
//			System.err.println("Got an exception! - ObtenerBodegaAlAzar ");
//			System.err.println(e.getMessage());
//		}
//		System.out.println("Obteniendo bodega : " + bodega);
//
////		if (bodega == null || bodega.equals("")) {
////			bodega = ObtenerBodegaAlAzar(codigoPais, 1);
////		}

		return bodega;

	}

	public EGrillaAuditoria ObtenerTransaccionBy(String codigoPais, String idTrxHeader, String tipoInventario,
                                                 String sistemaOrigen, String estadoTransaccion, String estadoRegistro, String tipoTransaccion)
			throws IOException, InterruptedException, SQLException {

		EGrillaAuditoria oEGrillaAuditoria = new EGrillaAuditoria();
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;

			String sqlQuery = "SELECT t1.TRANSACCION_ID, \n" + "		t1.IDTRX_HEADER, \n"
					+ "		t1.SYSTEM_CODE, \n" + "		t2.FULFILLMENT_CODE, \n" + "		t2.SKU, \n"
					+ "		t1.TRANSACTION_TYPE_CODE, \n" + "		t2.TRANSACTION_DOC,\n" + "		t1.USERNAME,\n"
					+ "		t1.STATUS STATUS_CABECERA,\n" + "		t2.STATUS STATUS_DETALLE,\n"
					+ "		t2.DETAIL,\n" + "		t2.INVENTORY_TYPE,\n"
					+ "		DATE_FORMAT(PROCESS_START, '%d-%m-%Y') PROCESS_START,\n"
					+ "		DATE_FORMAT(DATE_ADD(PROCESS_START,INTERVAL -1 DAY), '%d-%m-%Y') PROCESS_PREV,\n"
					+ "		DATE_FORMAT(DATE_ADD(PROCESS_START,INTERVAL 1 DAY), '%d-%m-%Y') PROCESS_NEXT,  \n"
					+ "		t2.PREV_STOCK,\n" + "		t2.STOCK,\n" + "		t2.OPERATION \n"
					+ "FROM TRANSACTION t1\n" + "INNER JOIN TRANSACTION_ITEM_LINE_RIPLEY t2\n"
					+ "ON t1.TRANSACCION_ID=t2.TRANSACCION_ID\n" + "WHERE t2.INVENTORY_TYPE=[INVENTORY_TYPE]\n"
					+ "AND t1.STATUS=[STATUS_HEADER]\n" + "AND t2.STATUS=[STATUS_DETAIL]\n"
					+ "AND t1.COUNTRY_CODE='PE'\n" + "AND TRANSACTION_TYPE_CODE=[TRANSACTION_TYPE_CODE]\n"
					+ "AND SYSTEM_CODE=[SYSTEM_CODE]\n" + "AND t1.IDTRX_HEADER=[IDTRX_HEADER]\n"
					+ "order by 1 desc LIMIT 1";

			sqlQuery = sqlQuery.replace("[INVENTORY_TYPE]",
					tipoInventario.equals("") ? "t2.INVENTORY_TYPE" : tipoInventario);

			sqlQuery = sqlQuery.replace("[IDTRX_HEADER]",
					idTrxHeader.equals("") ? "t1.IDTRX_HEADER" : "'" + idTrxHeader + "'");

			sqlQuery = sqlQuery.replace("[SYSTEM_CODE]",
					sistemaOrigen.equals("") ? "t1.SYSTEM_CODE" : "'" + sistemaOrigen + "'");

			sqlQuery = sqlQuery.replace("[STATUS_HEADER]",
					estadoTransaccion.equals("") ? "t1.STATUS" : "'" + estadoTransaccion + "'");
			sqlQuery = sqlQuery.replace("[STATUS_DETAIL]",
					estadoRegistro.equals("") ? "t2.STATUS" : "'" + estadoRegistro + "'");
			sqlQuery = sqlQuery.replace("[TRANSACTION_TYPE_CODE]",
					tipoTransaccion.equals("") ? "TRANSACTION_TYPE_CODE" : "'" + tipoTransaccion + "'");
			System.out.println(sqlQuery);
			rs = stmt.executeQuery(sqlQuery);
			while (rs.next()) {
				oEGrillaAuditoria.transaccionId = rs.getString("TRANSACCION_ID");
				oEGrillaAuditoria.idtrxHeader = rs.getString("IDTRX_HEADER");
				oEGrillaAuditoria.codigoSistema = rs.getString("SYSTEM_CODE");
				oEGrillaAuditoria.codigoBodega = rs.getString("FULFILLMENT_CODE");
				oEGrillaAuditoria.codigoVenta = rs.getString("SKU");
				oEGrillaAuditoria.codigoTipoTransaccion = rs.getString("TRANSACTION_TYPE_CODE");
				oEGrillaAuditoria.documentoTransaccion = rs.getString("TRANSACTION_DOC");
				oEGrillaAuditoria.username = rs.getString("USERNAME");
				oEGrillaAuditoria.estadoCabecera = rs.getString("STATUS_CABECERA");
				oEGrillaAuditoria.estadoDetalle = rs.getString("STATUS_DETALLE");
				oEGrillaAuditoria.mensajeDetalle = rs.getString("DETAIL");
				oEGrillaAuditoria.tipoInventario = rs.getString("INVENTORY_TYPE");
				oEGrillaAuditoria.processStart = rs.getString("PROCESS_START");
				oEGrillaAuditoria.processPrev = rs.getString("PROCESS_PREV");
				oEGrillaAuditoria.processNext = rs.getString("PROCESS_NEXT");
				oEGrillaAuditoria.stockAntes = rs.getString("PREV_STOCK");
				oEGrillaAuditoria.stockDespues = rs.getString("STOCK");
				oEGrillaAuditoria.operacion = rs.getString("OPERATION");
			}
			// conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerSkuConStockActual");
			System.err.println(e.getMessage());
		}

		return oEGrillaAuditoria;
	}

	public List<EGrillaAuditoria> ObtenerTransaccionBy(String codigoPais, String idTrxHeader)
			throws IOException, InterruptedException, SQLException {

		List<EGrillaAuditoria> oListGrillaAuditoria = new ArrayList<EGrillaAuditoria>();
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;

			String sqlQuery = "SELECT t1.TRANSACCION_ID, \n" + "		t1.IDTRX_HEADER, \n"
					+ "		t1.SYSTEM_CODE, \n" + "		t2.FULFILLMENT_CODE, \n" + "		t2.SKU, \n"
					+ "		t1.TRANSACTION_TYPE_CODE, \n" + "		t2.TRANSACTION_DOC,\n" + "		t1.USERNAME,\n"
					+ "		t1.STATUS STATUS_CABECERA,\n" + "		t2.STATUS STATUS_DETALLE,\n"
					+ "		t2.DETAIL,\n" + "		t2.INVENTORY_TYPE,\n"
					+ "		DATE_FORMAT(PROCESS_START, '%d-%m-%Y') PROCESS_START,\n"
					+ "		DATE_FORMAT(DATE_ADD(PROCESS_START,INTERVAL -1 DAY), '%d-%m-%Y') PROCESS_PREV,\n"
					+ "		DATE_FORMAT(DATE_ADD(PROCESS_START,INTERVAL 1 DAY), '%d-%m-%Y') PROCESS_NEXT,  \n"
					+ "		t2.PREV_STOCK,\n" + "		t2.STOCK,\n" + "		t2.OPERATION \n"
					+ "FROM TRANSACTION t1\n" + "INNER JOIN TRANSACTION_ITEM_LINE_RIPLEY t2\n "
					+ "ON t1.TRANSACCION_ID=t2.TRANSACCION_ID\n"
					+ "WHERE t1.COUNTRY_CODE='[PAIS]' AND t1.IDTRX_HEADER=[IDTRX_HEADER]\n" + "order by 1 desc";
			sqlQuery = sqlQuery.replace("[IDTRX_HEADER]",
					idTrxHeader.equals("") ? "t1.IDTRX_HEADER" : "'" + idTrxHeader + "'");
			sqlQuery = sqlQuery.replace("[PAIS]", idTrxHeader.equals("") ? "t1.COUNTRY_CODE" : codigoPais);

			System.out.println(sqlQuery);
			rs = stmt.executeQuery(sqlQuery);
			while (rs.next()) {
				EGrillaAuditoria oEGrillaAuditoria = new EGrillaAuditoria();
				oEGrillaAuditoria.transaccionId = rs.getString("TRANSACCION_ID");
				oEGrillaAuditoria.idtrxHeader = rs.getString("IDTRX_HEADER");
				oEGrillaAuditoria.codigoSistema = rs.getString("SYSTEM_CODE");
				oEGrillaAuditoria.codigoBodega = rs.getString("FULFILLMENT_CODE");
				oEGrillaAuditoria.codigoVenta = rs.getString("SKU");
				oEGrillaAuditoria.codigoTipoTransaccion = rs.getString("TRANSACTION_TYPE_CODE");
				oEGrillaAuditoria.documentoTransaccion = rs.getString("TRANSACTION_DOC");
				oEGrillaAuditoria.username = rs.getString("USERNAME");
				oEGrillaAuditoria.estadoCabecera = rs.getString("STATUS_CABECERA");
				oEGrillaAuditoria.estadoDetalle = rs.getString("STATUS_DETALLE");
				oEGrillaAuditoria.mensajeDetalle = rs.getString("DETAIL");
				oEGrillaAuditoria.tipoInventario = rs.getString("INVENTORY_TYPE");
				oEGrillaAuditoria.processStart = rs.getString("PROCESS_START");
				oEGrillaAuditoria.processPrev = rs.getString("PROCESS_PREV");
				oEGrillaAuditoria.processNext = rs.getString("PROCESS_NEXT");
				oEGrillaAuditoria.stockAntes = rs.getString("PREV_STOCK");
				oEGrillaAuditoria.stockDespues = rs.getString("STOCK");
				oEGrillaAuditoria.operacion = rs.getString("OPERATION");
				oListGrillaAuditoria.add(oEGrillaAuditoria);
			}
			// conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerSkuConStockActual");
			System.err.println(e.getMessage());
		}

		return oListGrillaAuditoria;
	}

	public Connection ConnectSIC() {
		return MySqlDbHelper.getConnection();

	}

	public List<ESku> ObtenerDatosIncrementalProducto(String codigoPais, String cantidad)
			throws IOException, InterruptedException, SQLException {

		List<ESku> oLista = new ArrayList<ESku>();
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;

			String sqlQuery = "SELECT * FROM(SELECT SKU,COD_VENTA,STYLE,SUBLINE_CODE,LINE_CODE,DEPTO_CODE, NAME,COUNTRY_CODE\n"
					+ "			  FROM SKU WHERE LENGTH(COD_VENTA)=13 AND COUNTRY_CODE='[CODIGO_PAIS]' LIMIT 10000 ) A\n"
					+ "ORDER BY RAND()\n" + "LIMIT [CANTIDAD]";

			sqlQuery = sqlQuery.replace("[CODIGO_PAIS]", codigoPais);
			sqlQuery = sqlQuery.replace("[CANTIDAD]", cantidad);
			System.out.println(sqlQuery);
			rs = stmt.executeQuery(sqlQuery);
			while (rs.next()) {
				ESku oEntidad = new ESku();
				oEntidad.sku = rs.getString("SKU");
				oEntidad.codigoVenta = rs.getString("COD_VENTA");
				oEntidad.style = rs.getString("STYLE");
				oEntidad.sublineCode = rs.getString("SUBLINE_CODE");
				oEntidad.lineCode = rs.getString("LINE_CODE");
				oEntidad.deptoCode = rs.getString("DEPTO_CODE");
				oEntidad.description = rs.getString("NAME");
				oEntidad.countryCode = rs.getString("COUNTRY_CODE");
				oLista.add(oEntidad);
			}
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerDatosIncrementalProducto()");
			System.err.println(e.getMessage());
		}
		return oLista;

	}

	public ESku ObtenerInformacionJerarquiaProducto(String codigoVenta, String codigoPais)
			throws IOException, InterruptedException, SQLException {

		ESku oEntidad = new ESku();
		try {
			Connection conn = ConnectSIC();
			Statement stmt = conn.createStatement();
			ResultSet rs;

			String sqlQuery = "SELECT SKU,COD_VENTA,STYLE,SUBLINE_CODE,LINE_CODE,DEPTO_CODE, NAME,COUNTRY_CODE\n"
					+ "			  FROM SKU WHERE COD_VENTA='[COD_VENTA]' AND COUNTRY_CODE='[CODIGO_PAIS]'";

			sqlQuery = sqlQuery.replace("[COD_VENTA]", codigoVenta);
			sqlQuery = sqlQuery.replace("[CODIGO_PAIS]", codigoPais);

			System.out.println(sqlQuery);
			rs = stmt.executeQuery(sqlQuery);
			while (rs.next()) {
				oEntidad.sku = rs.getString("SKU");
				oEntidad.codigoVenta = rs.getString("COD_VENTA");
				oEntidad.style = rs.getString("STYLE");
				oEntidad.sublineCode = rs.getString("SUBLINE_CODE");
				oEntidad.lineCode = rs.getString("LINE_CODE");
				oEntidad.deptoCode = rs.getString("DEPTO_CODE");
				oEntidad.description = rs.getString("NAME");
				oEntidad.countryCode = rs.getString("COUNTRY_CODE");
			}
		} catch (Exception e) {
			System.err.println("Got an exception! - ObtenerInformacionJerarquiaProducto()");
			System.err.println(e.getMessage());
		}
		return oEntidad;
	}

}
