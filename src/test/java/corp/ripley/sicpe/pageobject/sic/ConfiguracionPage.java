package corp.ripley.sicpe.pageobject.sic;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import corp.ripley.sicpe.pageobject.BasePage;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ConfiguracionPage extends BasePage {

	public ConfiguracionPage() throws IOException {
		super();
		// TODO Auto-generated constructor stub
	}

	@FindBy(xpath = "//a[contains(@class,'main_button') and text()='Consultar']")
	private WebElement btnConsultar;

	@FindBy(xpath = "//a[contains(@class,'main_button') and text()='Limpiar']")
	private WebElement btnLimpiar;

	@FindBy(xpath = "//a[contains(@class,'main_button') and text()='Eliminar']")
	private WebElement btnEliminar;

	@FindBy(xpath = "//a[contains(@class,'main_button') and text()='Agregar']")
	private WebElement btnAgregar;

	@FindBy(xpath = "//p[text()='Nombre de Sistema ']/../div/select")
	private WebElement cbNombreSistema;

	@FindBy(xpath = "//p[text()='Tipo de Transacción ']/../div/select")
	private WebElement cbTipoTransaccion;

	@FindBy(xpath = "//p[text()='Tipo de Inventario ']/../div/select")
	private WebElement cbTipoInventario;

	@FindBy(xpath = "//button[text()='Continuar']")
	private WebElement btnContinuar;

	@FindBy(xpath = "//input[@type='checkbox']")
	private WebElement chkResultado;

	// Select selOpcion = new Select(cbNombreSistema);
	//	selOpcion.selectByValue(codigoSistema);

	String xpathCheckBoxResultado = "//input[@type='checkbox']";
	String xpathSistemaOpcion = "//p[text()='Nombre de Sistema ']/../div/select/option[@value='[CODIGO_SISTEMA]']";
	String xpathBtnDeshabilitado = "//a[@class='main_button disabled' and text()='[NOMBRE_BOTON]']";
	String xpathBtnHabilitado = "//a[@class='main_button' and text()='Consultar']";
	String xpathTipoTransaccionOpcion = "//p[text()='Tipo de Transacción ']/../div/select/option[@value='[CODIGO_TRANSACCION]']";
	String xpathTipoInventarioOpcion = "//p[text()='Tipo de Inventario ']/../div/select/option[@value='[CODIGO_INVENTARIO]']";
	String xpathColumnaGrilla = "//th[text()='[NOMBRE_COLUMNA]']";
	String xpathMensajeConfirmacion = "//p[text()='Existe una configuración inactiva, ¿desea reactivarla?']";
	String xpathMensajeFinal = "//p[text()='[MENSAJE]']";

	public void ClickConsultar() throws InterruptedException {
		WaitSleep(1);
		WaitAndClickElement(btnConsultar);
	}

	public void EsperarConsulta() {
		WaitUntilWebElementIsVisible(chkResultado);
	}

	public void ClickLimpiar() throws InterruptedException {
		WaitAndClickElement(btnLimpiar);
	}

	public void ClickEliminar() throws InterruptedException {
		WaitAndClickElement(btnEliminar);
	}

	public void ClickAgregar() throws InterruptedException {
		WaitAndClickElement(btnAgregar);
	}

	public void ClickContinuar() throws InterruptedException {
		WaitAndClickElement(btnContinuar);
	}

	public void ClickComboNombreSistema() throws InterruptedException {
		WaitAndClickElement(cbNombreSistema);
	}

	public void ClickComboTipoTransaccion() throws InterruptedException {
		WaitAndClickElement(cbTipoTransaccion);
	}

	public void ClickComboTipoInventario() throws InterruptedException {
		WaitAndClickElement(cbTipoInventario);
	}

	public boolean ExisteSistemaOpcion(String codigoSistema) {
		xpathSistemaOpcion = xpathSistemaOpcion.replace("[CODIGO_SISTEMA]", codigoSistema);

		return ElementExist(xpathSistemaOpcion);

	}

	public boolean ExisteTipoTransaccion(String codigoTipoTransaccion) {
		xpathTipoTransaccionOpcion = xpathTipoTransaccionOpcion.replace("[CODIGO_TRANSACCION]", codigoTipoTransaccion);
		return ElementExist(xpathTipoTransaccionOpcion);
	}

	public boolean ExisteTipoInventario(String codigoTipoInventario) {
		xpathTipoInventarioOpcion = xpathTipoInventarioOpcion.replace("[CODIGO_INVENTARIO]", codigoTipoInventario);
		return ElementExist(xpathTipoInventarioOpcion);
	}

	public boolean ExisteColumnaGrilla(String nombreColumnaGrilla) {
		xpathColumnaGrilla = xpathColumnaGrilla.replace("[NOMBRE_COLUMNA]", nombreColumnaGrilla);
		return ElementExist(xpathColumnaGrilla);
	}

	public boolean ExisteBotonDeshabilitado(String nombreBoton) {
		xpathBtnDeshabilitado = xpathBtnDeshabilitado.replace("[NOMBRE_BOTON]", nombreBoton);
		return ElementExist(xpathBtnDeshabilitado);
	}

	public boolean ExisteBotonHabilitado(String nombreBoton) {
		xpathBtnHabilitado = xpathBtnHabilitado.replace("[NOMBRE_BOTON]", nombreBoton);
		return ElementExist(xpathBtnHabilitado);
	}

	public boolean ExisteMensajeConfirmacion(String mensaje) {

		return ElementExist(xpathMensajeConfirmacion);
	}

	public boolean VerificarMensajeFinal(String mensaje) throws InterruptedException {
		xpathMensajeFinal = xpathMensajeFinal.replace("[MENSAJE]", mensaje);
		WebDriverWait wait = new WebDriverWait(getDriver(), 20);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpathMensajeFinal)));
		return true;
	}

	public boolean ExisteResultadoLimpio() {
		return ElementExist(xpathCheckBoxResultado);
	}

	public boolean ExisteCargaDeResultados() {
		return ElementExist(xpathCheckBoxResultado);
	}

	public void SeleccionarSistema(String nombreSistema) throws InterruptedException {
		WaitAndClickElement(cbNombreSistema);
		WaitSleep(1);
		xpathSistemaOpcion = xpathSistemaOpcion.replace("[CODIGO_SISTEMA]", nombreSistema);
		WaitAndClickXpath(xpathSistemaOpcion);
		WaitSleep(1);
	}

	public void SeleccionarTipoTransaccion(String codigoTipoTransaccion) throws InterruptedException {
		WaitAndClickElement(cbTipoTransaccion);
		WaitSleep(1);
		xpathTipoTransaccionOpcion = xpathTipoTransaccionOpcion.replace("[CODIGO_TRANSACCION]", codigoTipoTransaccion);
		WaitAndClickXpath(xpathTipoTransaccionOpcion);
		WaitSleep(1);
	}

	public void SeleccionarTipoInventario(String codigoTipoInventario) throws InterruptedException {
		WaitAndClickElement(cbTipoInventario);
		WaitSleep(1);
		xpathTipoInventarioOpcion = xpathTipoInventarioOpcion.replace("[CODIGO_INVENTARIO]", codigoTipoInventario);
		WaitAndClickXpath(xpathTipoInventarioOpcion);
		WaitSleep(1);
	}

}
