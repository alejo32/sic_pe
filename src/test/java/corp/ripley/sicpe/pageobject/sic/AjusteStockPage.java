package corp.ripley.sicpe.pageobject.sic;

import java.awt.AWTException;
import java.io.IOException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import corp.ripley.sicpe.pageobject.BasePage;

public class AjusteStockPage extends BasePage {

	public AjusteStockPage() throws IOException {
		super();
	}

	@FindBy(xpath = "//p[contains(text(),'Cod. Venta')]/../input")
	private WebElement txtCodigoVenta;

	@FindBy(xpath = "//p[contains(text(),'Tipo de Inventario')]/../div/select")
	private WebElement cbTipoInventario;

	@FindBy(id = "sku-stock-demo")
	private WebElement txtStockActual;

	@FindBy(xpath = "//a[text()='Consultar']")
	private WebElement btnConsultar;

	@FindBy(xpath = "//a[text()='Limpiar']")
	private WebElement btnLimpiar;

	//@FindBy(xpath = "//a[text()='Actualizar']")
	@FindBy(xpath = "//*[@id='form-footer-adjust']/div")
	private WebElement btnActualizar;

	@FindBy(xpath = "//div[contains(@class,'open')]")
	private WebElement lblMensajeError;

	@FindBy(id = "incrementar")
	private WebElement chkIncrementar;

	@FindBy(id = "disminuir")
	private WebElement chkDisminuir;

	@FindBy(id = "reemplazar")
	private WebElement chkReemplazar;

	@FindBy(xpath = "//*[contains(text(),'Tipo de Producto')]/../div/select")
	private WebElement cbTipoProducto;

	@FindBy(xpath = "//p[contains(text(),'Ajuste')]/../input")
	private WebElement txtStockAjuste;

	@FindBy(xpath = "//p[contains(text(),'Observaci')]/../textarea")
	private WebElement taObservacion;

	@FindBy(xpath = "//p[contains(text(),'Nuevo Stock')]/../input")
	private WebElement txtNuevoStock;

	String xpathImgLoading = "//div[@class='prelod-adjus-stok']/img";
	String xpathMensajeFlotante = "//*[contains(text(),'[MENSAJE]')]";
	String xpathAjusteError = "//input[contains(@class,'input-sic input-error')]";
	String xpathObservacionError = "//textarea[contains(@class,'input-sic text-area-adjust input-error')]";
	String xpathTipoInventarioOpcion = "//p[contains(text(),'Tipo de Inventario')]/../div/select/option[@value=[TIPO_INVENTARIO]]";

	public boolean ExisteMensajeFlotanteError(String mensaje) {

		xpathMensajeFlotante = xpathMensajeFlotante.replace("[MENSAJE]", mensaje);
		WaitUntilXpathIsVisible(xpathMensajeFlotante, 10);
		return true;
	}

	public boolean ExisteAjusteStockError() {
		WaitUntilXpathIsVisible(xpathAjusteError, 10);
		return true;
	}

	public boolean ExisteObservacionError() {
//		WebDriverWait wait = new WebDriverWait(getDriver(), 10);
//		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpathObservacionError)));
		return true;
	}

	public boolean ExisteMensajeError(String mensaje) throws InterruptedException {
		WaitSleep(1);
		WaitUntilWebElementDissapears(xpathImgLoading);
		WaitSleep(1);
		return GetElementText(lblMensajeError).contains(mensaje);
	}

	public void IngresarCodigoVenta(String codigoVenta) throws AWTException, InterruptedException {
		SendKeysToWebElement(txtCodigoVenta, codigoVenta);
	}

	public void IngresarObservacion(String observacion) throws AWTException, InterruptedException {
		SendKeysToWebElement(taObservacion, observacion);
	}

	public void IngresarAjuste(String stockAjuste) throws AWTException, InterruptedException {
		SendKeysToWebElement(txtStockAjuste, stockAjuste);
	}
	
	String xpathBtnDeshabilitado = "//a[@class='main_button disabled' and text()='[NOMBRE_BOTON]']";

	public boolean ExisteBotonDeshabilitado(String nombreBoton) {
		xpathBtnDeshabilitado = xpathBtnDeshabilitado.replace("[NOMBRE_BOTON]", nombreBoton);
		return ElementExist(xpathBtnDeshabilitado);
	}


	public void SeleccionarTipoInventario(String tipoInventario) throws InterruptedException, AWTException {
		if (!tipoInventario.equals("")) {
			WaitAndClickElement(cbTipoInventario);
			WaitSleep(1);
			xpathTipoInventarioOpcion = xpathTipoInventarioOpcion.replace("[TIPO_INVENTARIO]", tipoInventario);
			WaitAndClickXpath(xpathTipoInventarioOpcion);
		}
	}

	public void Limpiar() throws InterruptedException {
		WaitAndClickElement(btnLimpiar);
	}

	public void ClickConsultar() throws InterruptedException {
		WaitAndClickElement(btnConsultar);
		WaitUntilWebElementDissapears("//div[@class='prelod-adjus-stok']/img");
		WaitSleep(1);
	}

	public void ClickActualizar() {
		WaitAndClickElement(btnActualizar);
	    WaitSleep(10);  // le damos timpo para que se refresque el ajuste en la BD
		btnActualizar.click();
	}

	public void SeleccionarIncrementar() {
		WaitAndClickElement(chkIncrementar);
	}

	public void SeleccionarDisminuir() {
		WaitAndClickElement(chkDisminuir);
	}

	public void SeleccionarReemplazar() {
		WaitAndClickElement(chkReemplazar);
	}

	public void ConsultarStock(String codigoVenta, String tipoInventario) throws AWTException, InterruptedException {
		SeleccionarTipoInventario(tipoInventario);
		IngresarCodigoVenta(codigoVenta);
		ClickConsultar();
	}

	public boolean ValidarStock(int nuevoStockEsperado, int nuevoStockMostrado) {
		System.out.println("Stock Esperado: " + nuevoStockEsperado + " - Stock Visualizado: " + nuevoStockMostrado);
		//return 1==1;
		return nuevoStockEsperado == nuevoStockMostrado;

	}

	//public void Actualizar() {
	//	ClickActualizar();
		
	//}

	public void Consultar() throws InterruptedException, AWTException {
		ClickConsultar();
	}

	public boolean ValidarCampoAjusteError() throws InterruptedException, AWTException {
		return ExisteAjusteStockError();
	}

	public boolean ValidarCampoObservacionError() throws InterruptedException, AWTException {
		return ExisteObservacionError();
	}

	public String ObtenerTextoComboTipoProducto() {
		return GetComboboxText(cbTipoProducto);
	}

	public String ObtenerTextoComboTipoInventario() {
		return GetComboboxText(cbTipoInventario);
	}

	public String ObtenerStockActual() {
		return GetElementText(txtStockActual);
	}

	public String ObtenerStockAjuste() {
		return GetElementText(txtStockAjuste);
	}

	public String ObtenerNuevoStock() {
		return GetElementText(txtNuevoStock);
	}

	public String ObtenerCodigoVenta() {
		return GetElementText(txtCodigoVenta);
	}

	
}