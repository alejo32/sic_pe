package corp.ripley.sicpe.pageobject.sic;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.junit.Assert;

import com.google.gson.Gson;

import corp.ripley.sicpe.model.sic.EStockXBodega;
import corp.ripley.sicpe.model.sic.ResponseIMS;
import corp.ripley.sicpe.pageobject.BasePage;



public class IMSCalculoPage extends BasePage {

	public static String bodegasPorStock_v1;
	public static String bodegasPorStock_v2;
	public static String bodegasPorStock_v3;
	public static String bodegasPorStock_v4;
	public static String bodegasPorStock_v5;
	
	public static String bodegasPorStock_Bodega1;
	public static String bodegasPorStock_Stock1;
	public static String bodegasPorStock_Bodega2;
	public static String bodegasPorStock_Stock2;
	public static String bodegasPorStock_Bodega3;
	public static String bodegasPorStock_Stock3;
	public static String bodegasPorStock_Bodega4;
	public static String bodegasPorStock_Stock4;
	public static String bodegasPorStock_Bodega5;
	public static String bodegasPorStock_Stock5;
	
	public IMSCalculoPage() throws IOException {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	@FindBy(xpath = "//*[@id=\"resource_sku-controller\"]/div/h2/a")
	private WebElement link_sku_controller;
	@FindBy(xpath = "//*[@id=\"sku-controller_getSkuByIdUsingGET\"]/div[1]/h3/span[1]/a")
	private WebElement btGet;
	@FindBy(xpath = "/html/body/div[3]/div[2]/ul/li[6]/ul/li[2]/ul/li/div[2]/form/table[1]/tbody/tr/td[2]/input")
	private WebElement txtCodigoVenta;
	@FindBy(xpath = "/html/body/div[3]/div[2]/ul/li[6]/ul/li[2]/ul/li/div[2]/form/div[3]/input")
	private WebElement btTriItOutAutentication;
	@FindBy(xpath = "/html/body/div[3]/div[2]/ul/li[6]/ul/li[2]/ul/li/div[2]/div[2]/div[5]/pre")
	private WebElement txtResponseCode;
	@FindBy(xpath = "/html/body/div[3]/div[2]/ul/li[7]/div/h2/a")
	private WebElement Stock_controler_peruba;
	@FindBy(xpath = "/html/body")
	private WebElement ResultadoBody;
	@FindBy(xpath = "/html/body/pre")
	private WebElement Resultadopre;
	
	public void ClickOpcionConsultaSKU() throws IOException, InterruptedException {
		WaitAndClickElement(link_sku_controller);	
		WaitAndClickElement(btGet);
		Thread.sleep(1000);			
	}
	

	public void IngresarProducto(String codigoVenta) throws IOException, InterruptedException {
	
		SendKeysToWebElement(txtCodigoVenta, codigoVenta);

	}
	
	public void ClickTriItOut() throws InterruptedException {
		WaitAndClickElement(btTriItOutAutentication);
		btTriItOutAutentication.click();
	
	}
	
	
	
	
	public String[] ingresarAutenticacionIMS_pe(String usuario, String clave, String codigo_venta)throws InterruptedException, IOException
	{
		System.out.println("Ingresando a la pagina ims Perú con la credenciales");
		System.out.println("usuario ims:"+usuario);
		System.out.println("password ims:"+clave);
		
		String urlAutentica="https://imsperupub.ripleyqa.com/sku/";
		String urlAutenticacion=urlAutentica.substring(0,8)+usuario+":"+clave+"@"+urlAutentica.substring(8)+codigo_venta;
		//String urlAutenticacion="https://administrador:admin@imspub.ripleyqa.com/sku/2064060970008";
	    System.out.println("nueva url:" + urlAutenticacion);
	    WaitSleep(145);
		getDriver().get(urlAutenticacion);
		
				
		String CadenaBodyFull = ResultadoBody.getText();
		System.out.println("El resultado:" +CadenaBodyFull);
		
	    Gson gson = new Gson(); 
   
        ResponseIMS response = gson.fromJson(CadenaBodyFull, ResponseIMS.class); 
     
        
        String[] CadenaBodyJson = response.getBodegasPorStock().split(",");
        
        for(int i=0; CadenaBodyJson.length>i; i++)
        {
     
        	String[] llaveValor = CadenaBodyJson[i].split(":");
        	String bodega = llaveValor[0];
        	String qty = llaveValor[1];
               
        }        
        return CadenaBodyJson;	
	}

	public String[] ingresarAutenticacion(String usuario, String clave, String codigo_venta)throws InterruptedException, IOException
	{
		System.out.println("Ingresando a la pagina ims con la credenciales");
		System.out.println("usuario ims:"+usuario);
		System.out.println("password ims:"+clave);
		
		String urlAutentica="https://imspub.ripleyqa.com/sku/";
		String urlAutenticacion=urlAutentica.substring(0,8)+usuario+":"+clave+"@"+urlAutentica.substring(8)+codigo_venta;
		//String urlAutenticacion="https://administrador:admin@imspub.ripleyqa.com/sku/2064060970008";
	    System.out.println("nueva url:" + urlAutenticacion);
	    WaitSleep(65);		
		getDriver().get(urlAutenticacion);
		
				
		String CadenaBodyFull = ResultadoBody.getText();
		System.out.println("El resultado:" +CadenaBodyFull);
		
		// Creating a Gson Object 
        Gson gson = new Gson(); 
  
        // Converting json to object 
        // first parameter should be prpreocessed json 
        // and second should be mapping class 
        ResponseIMS response = gson.fromJson(CadenaBodyFull, ResponseIMS.class); 
        //System.out.println(" RESULTADO!!: " + response.getBodegasPorStock());
        
        String[] CadenaBodyJson = response.getBodegasPorStock().split(",");
        
        for(int i=0; CadenaBodyJson.length>i; i++)
        {
        	//System.out.println("FOR:::: " + CadenaBodyJson[i]);
        	String[] llaveValor = CadenaBodyJson[i].split(":");
        	String bodega = llaveValor[0];
        	String qty = llaveValor[1];
        	//System.out.println("BODEGA: "+ bodega);
        	//System.out.println("qty: "+ qty);
        
        }        
        return CadenaBodyJson;	
	}
	
	
	
	public void CompararCalculoStockSicImsPeru(List<EStockXBodega> lista, String[] respIMS )throws InterruptedException, IOException {

		System.out.println("cantidad de registro " + lista.size());
		int StockBodegaVirtual = 0;
		int StockSTS = 0;
		int StockSFS = 0;
		int Stock = 0;
		int BodegaVirtual = 20061;

		if (lista != null) {
			for (EStockXBodega entidad : lista) {
				if (entidad.DISPONIBLE == null) //Se usa stk Disponible(QTY) si el activo(DISPONIBLE) es null
				{
					if (Integer.parseInt(entidad.PUBLICADO_TV) == 1) {
						if ((Integer.parseInt(entidad.QTY) - Integer.parseInt(entidad.PROTEGIDO)) <= Integer.parseInt(entidad.UMBRAL)) {
							System.out.println("paso01:");
							System.out.println("DD:" + Integer.parseInt(entidad.DD) );
							System.out.println("STS:" + Integer.parseInt(entidad.STS));
							System.out.println("SFS:" + Integer.parseInt(entidad.SFS));
							System.out.println("PU:" + Integer.parseInt(entidad.PU));

							if (Integer.parseInt(entidad.DD) == 1 || Integer.parseInt(entidad.SFS) == 1) {
								System.out.println("paso02:");
								System.out.println("lontiud de la bodega:" + entidad.FULFILLMENT_CODE.length());

								if (Integer.parseInt(entidad.DD) == 1 || entidad.FULFILLMENT_CODE.length() == '5') {
									System.out.println("lontiud de la bodega:" + entidad.FULFILLMENT_CODE.length());

									StockSTS = StockSTS + (Integer.parseInt(entidad.QTY) - Integer.parseInt(entidad.PROTEGIDO));
								}
									StockBodegaVirtual = StockBodegaVirtual + (Integer.parseInt(entidad.QTY) - Integer.parseInt(entidad.PROTEGIDO));
							 }
							}
						}
						//END DEL IF
				} else if (Integer.parseInt(entidad.DISPONIBLE) >= 0) {
						//for (EStockXBodega entidad : lista)  for duplicado
						//{
						if (Integer.parseInt(entidad.PUBLICADO_TV) == 1) {
							if ((Integer.parseInt(entidad.QTY) - Integer.parseInt(entidad.PROTEGIDO)) <= Integer.parseInt(entidad.UMBRAL)) {
								System.out.println("paso02:Se calcula con el Stock Activo");
								if (Integer.parseInt(entidad.DD) == 1 || Integer.parseInt(entidad.SFS) == 1) {
									if (Integer.parseInt(entidad.DD) == 1) {
										StockSTS = StockSTS + Integer.parseInt(entidad.DISPONIBLE);
									}
									StockBodegaVirtual = StockBodegaVirtual + Integer.parseInt(entidad.DISPONIBLE);
								}
							}
						}
						//}del for duplicado
					}//ELSIF

				}// del FOR

			}//DEL IF PRINCIPAL


			System.out.println("STOCK BODEGA VIRTUAL :" + StockBodegaVirtual);

			System.out.println("STOCK STS:" + StockSTS);

//
		if (lista != null) {
				System.out.println("paso03:Comparar con el resultado del Json");
				//if (stock_Activo == null)
				//{	System.out.println("paso03:");

				for (EStockXBodega entidad : lista) {//System.out.println("paso02:");
					if (entidad.DISPONIBLE == null) {    //System.out.println("paso03:");

						if (Integer.parseInt(entidad.PUBLICADO_TV) == 1) {
							if ((Integer.parseInt(entidad.QTY) - Integer.parseInt(entidad.PROTEGIDO)) <= Integer.parseInt(entidad.UMBRAL)) {//System.out.println("paso04:");
								StockSFS = 0;
								Stock = 0;

								for (int i = 0; respIMS.length > i; i++) {
									String[] llaveValor = respIMS[i].split(":");
									//System.out.println("llavevalor i =:"+llaveValor[i]);
									String bodega = llaveValor[0];
									String qty = llaveValor[1];
									System.out.println("llavevalor 0 =:" + llaveValor[0]);
									System.out.println("llavevalor 1 =:" + llaveValor[1]);
									System.out.println("Bodega:" + bodega);
									System.out.println("entidad.FULFILLMENT_CODE =" + entidad.FULFILLMENT_CODE);
									System.out.println("entidad.STS=" + entidad.STS);
									System.out.println("entidad.SFS=" + entidad.SFS);
									System.out.println("Json qty=" + qty);

									System.out.println("StockSTS=" + StockSTS);
									System.out.println("StockBodegaVirtual=" + StockBodegaVirtual);


									if (Integer.parseInt(bodega) == Integer.parseInt(entidad.FULFILLMENT_CODE) && Integer.parseInt(entidad.STS) == 1 && Integer.parseInt(entidad.FULFILLMENT_CODE) != 20061) {
										System.out.println("===============================");
										System.out.println("stock obteniedo del json es Yulinnn= " + qty);
										StockSFS = (Integer.parseInt(entidad.QTY) - Integer.parseInt(entidad.PROTEGIDO)) + StockSTS;
										System.out.println("stock calculcado STS de la BD es= " + StockSFS);
										Assert.assertEquals(Integer.parseInt(qty), StockSFS);
										System.out.println("SIC envia Stock STS de : " + StockSFS + " unidades a la bodega:" + bodega);
									} else if (Integer.parseInt(entidad.FULFILLMENT_CODE) == 20026 && Integer.parseInt(bodega) == BodegaVirtual) //DD
									//else if (Integer.parseInt(bodega)==BodegaVirtual) //DD
									{
										System.out.println("===============================");
										System.out.println("stock obteniedo del json= " + qty);
										System.out.println("stock calculcado DD de la BD es=  " + StockBodegaVirtual);
										Assert.assertEquals(Integer.parseInt(qty), StockBodegaVirtual);
										System.out.println("El Calculo de STOCK de SIC fue enviado a IMS en la bodega virtual: " + BodegaVirtual + ", con el stock: " + qty);
									} else if (Integer.parseInt(entidad.FULFILLMENT_CODE) != 20026 && Integer.parseInt(bodega) == BodegaVirtual && Integer.parseInt(entidad.SFS) == 1) //STS bodega virtual
									{
										System.out.println("===============================");
										System.out.println("stock obteniedo del json= " + qty);
										System.out.println("stock calculcado SFS de la BD es ?=  " + StockBodegaVirtual);
										Assert.assertEquals(Integer.parseInt(qty), StockBodegaVirtual);
										System.out.println("El Calculo de STOCK de SIC fue enviado a IMS en la bodega virtual: " + BodegaVirtual + ", con el stock: " + qty);
									} else if (Integer.parseInt(entidad.FULFILLMENT_CODE) != 20026 && Integer.parseInt(bodega) == Integer.parseInt(entidad.FULFILLMENT_CODE) && Integer.parseInt(entidad.SFS) == 1)//SFS
									{
										System.out.println("===============================");
										Stock = Integer.parseInt(entidad.QTY) - Integer.parseInt(entidad.PROTEGIDO);
										System.out.println("stock obteniedo del json=" + qty);
										System.out.println("stock calculcado SFS de la BD= " + Stock);
										Assert.assertEquals(Integer.parseInt(qty), Stock);
										System.out.println("SIC envia Stock de : " + Stock + " a la bodega:" + bodega);

									}

								}//for interno

							}
						}
					}// IF
				} // FOR ENTIDAD
			} // IF PRINCIPAL
		}// DEL VOID

	
	
	public void CompararCalculoStockSicIms(List<EStockXBodega> lista,String stock_disponible,String stock_Seguridad,String stock_Activo,String stock_Reserva,String stock_vendible, String[] respIMS )throws InterruptedException, IOException
	{
		System.out.println("stock_Seguridad:"+ stock_Seguridad);
		System.out.println("stock_Activo:"+ stock_Activo);
		System.out.println("stock_Reserva:"+ stock_Reserva);
		System.out.println("stock_vendible:"+ stock_vendible);
	    //Assert.assertEquals(bodegasPorStock_Stock1,stock_vendible);
	    //System.out.println("cantidad de registro "+lista.size());	
	    int StockBodegaVirtual=0;
	    int StockSTS=0;
	    int StockSFS=0;
	    int BodegaVirtual=10039;
	    
	    if (lista != null) 
	    { 	//if (stock_Activo == null || Integer.parseInt(stock_Activo)==0) . El Producto Owner indico que solo cosidera S.Disponible si el activo es null
	    	if (stock_Activo == null)
	    	{
	    		for (EStockXBodega entidad : lista) 
	    		{System.out.println("UMBRAL="+Integer.parseInt(entidad.UMBRAL));
	    		 System.out.println("PUBLICADO="+Integer.parseInt(entidad.PUBLICADO_TV));
	    			if (Integer.parseInt(entidad.PUBLICADO_TV)==1)
	 				{ 
	    			if (Integer.parseInt(stock_vendible)<= Integer.parseInt(entidad.UMBRAL))
	 					{
	 						System.out.println("paso01:");	 						 					 					
		 					if (Integer.parseInt(entidad.DD)==1 || Integer.parseInt(entidad.SFS)==1 )
		 					{if (Integer.parseInt(entidad.DD)==1 )
		 						{
		 						StockSTS=StockSTS+Integer.parseInt(entidad.QTY);
		 						}		 						
		 						StockBodegaVirtual=StockBodegaVirtual+Integer.parseInt(entidad.QTY);
		 					}		 					
	 					}
	 				}
	 			}
	 		}else if (Integer.parseInt(stock_Activo) >=0)
	    	{
	 			for (EStockXBodega entidad : lista) 
	    		{if (Integer.parseInt(entidad.PUBLICADO_TV)==1)
	 				{ if (Integer.parseInt(stock_vendible)<= Integer.parseInt(entidad.UMBRAL))
	 					{System.out.println("paso01:");	 						 					 					
		 					if (Integer.parseInt(entidad.DD)==1 || Integer.parseInt(entidad.SFS)==1 )
		 					{if (Integer.parseInt(entidad.DD)==1 )
		 						{
		 						StockSTS=StockSTS+Integer.parseInt(entidad.DISPONIBLE);
		 						}		 						
		 						StockBodegaVirtual=StockBodegaVirtual+Integer.parseInt(entidad.DISPONIBLE);
		 					}		 					
	 					}
	 				}
	 			}
	    	}
	    	
	  	}
	    
		System.out.println("STOCK BODEGA VIRTUAL :"+ StockBodegaVirtual);
	    System.out.println("STOCK STS:"+ StockSTS);
	    
//
	    if (lista != null) {
	    	System.out.println("paso02:");
	     	if (stock_Activo == null)
	    	{	System.out.println("paso03:");
	    		for (EStockXBodega entidad : lista) 
	    		{if (Integer.parseInt(entidad.PUBLICADO_TV)==1)
	    			{if (Integer.parseInt(stock_vendible)<= Integer.parseInt(entidad.UMBRAL))
 		    			{System.out.println("paso04:");
 		    			 StockSFS=0;
		 				 for(int i=0; respIMS.length>i; i++) 
		 				 { 	String[] llaveValor = respIMS[i].split(":");
		 		        	String bodega = llaveValor[0].substring(1, llaveValor[0].length());
		 		        	String qty = llaveValor[1];
		 		        	if (Integer.parseInt(bodega)==Integer.parseInt(entidad.FULFILLMENT_CODE)&& Integer.parseInt(entidad.STS)==1 && Integer.parseInt(entidad.FULFILLMENT_CODE)!=10039 )
		 			    	{System.out.println("stock obteniedo del json= "+qty);
 		         			StockSFS = Integer.parseInt(entidad.QTY)+StockSTS;
 		         	    	System.out.println("stock calculcado de la BD= "+StockSFS);
			    			Assert.assertEquals(Integer.parseInt(qty),StockSFS);
			    			System.out.println("SIC envia Stock de : "+ StockSFS+" a la bodega:"+bodega);		    			
		 			    	}
		 		 		   	else if (Integer.parseInt(entidad.FULFILLMENT_CODE)==10095 && Integer.parseInt(bodega)==BodegaVirtual)
		 			    	{  		
			 		         	System.out.println("stock obteniedo del json= "+qty);
			 		         	System.out.println("stock calculcado de la BD=  "+StockBodegaVirtual);
			 		        	Assert.assertEquals(Integer.parseInt(qty),StockBodegaVirtual);
			 			    	System.out.println("El Calculo de STOCK de SIC fue enviado a IMS en la bodega virtual: "+BodegaVirtual+", con el stock: "+qty);	
 			    	    	}
		 			    	else if (Integer.parseInt(bodega)==Integer.parseInt(entidad.FULFILLMENT_CODE))
		 			    	{
		 	 		         	System.out.println("stock obteniedo del json="+qty);
		 	 		         	System.out.println("stock calculcado de la BD= "+entidad.QTY);
		 			    		Assert.assertEquals(qty,entidad.QTY);
		 			    		System.out.println("SIC envia Stock de : "+ entidad.QTY +" a la bodega:"+bodega);
		 			    	}
		 				 }
 		    			}  
	    			}
	    		}
	    	} 
		} 
	}

}
