package corp.ripley.sicpe.pageobject.sic;

import java.io.IOException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;import corp.ripley.sicpe.pageobject.BasePage;

public class CuadraturaPage extends BasePage{

	public CuadraturaPage() throws IOException {
		super();
		// TODO Auto-generated constructor stub
	}

	@FindBy(xpath = "//input[@type='text' and @placeholder='Ingrese Cod. de venta']")
	private WebElement txtSku;

	@FindBy(xpath = "//p[contains(text(),'Desde')]/../div/div/input")
	private WebElement txtDesde;

	@FindBy(xpath = "//ul[@item='[object Object]' and @index=0]/li/a")
	private WebElement lnkUltimoReporte;

	private String xpathPaginacion = "//a[text()='[NUMERO_PAGINA]']/../..";
	private String xpathLnkReportes="//div[@class='sic-reporte-list']/ul/li/a";
	private String xpathPaginas="//a[contains(@class,'paginate')]";

	public int ObtenerNumeroPaginas() throws InterruptedException {
		return GetElementsSize(xpathPaginas) - 4;
	}

	public int ObtenerNumeroReportesPorPagina() {
		return GetElementsSize(xpathLnkReportes);
	}

	public void DescargarUltimoReporte() {
		System.out.println("DescargarUltimoReporte()");
		WaitAndClickElement(lnkUltimoReporte);
		System.out.println("DescargarUltimoReporte() - FIN");
	}

	public String ObtenerHrefUltimoReporte() {		
		String fullpath = GetElementAttribute(lnkUltimoReporte,"href");
		return fullpath;
	}

	public void IrPagina(int numeroPagina) throws InterruptedException {
		System.out.println(xpathPaginacion);
		String xpathPaginacionMod = xpathPaginacion.replace("[NUMERO_PAGINA]", String.valueOf(numeroPagina));
		WaitAndClickXpath(xpathPaginacionMod);
		WaitSleep(2);
	}

	public String ObtenerNombreUltimoReporte() {
		String fullpath = ObtenerHrefUltimoReporte();
		return fullpath.replace("https://sicm-qa-bucket-files.s3.amazonaws.com/", "");
	}
	
}