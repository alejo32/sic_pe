package corp.ripley.sicpe.pageobject.sic;

import static org.testng.Assert.assertTrue;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import corp.ripley.sicpe.pageobject.BasePage;

public class StockProtegidoPage extends BasePage {

	public @FindBy(xpath = "//*[@id=\'system-mss\']/option[1]") WebElement sel_Producto;
	public @FindBy(xpath = "//p[contains(text(),'Departamento')]/../div/div/div/span[2]/input[@placeholder='Seleccione']") WebElement cbDepartamento;
	public @FindBy(xpath = "//p[contains(text(),'Línea')]/../div/div/div/span[2]/input[@placeholder='Seleccione']") WebElement cbLinea;
	public @FindBy(xpath = "//p[contains(text(),'Sublínea')]/../div/div/div/span[2]/input[1]") WebElement cbSubLinea;
	public @FindBy(xpath = "//p[contains(text(),'Cod.')]/../div/div/div/span[2]/input[1]") WebElement txtCodigoVenta;
	public @FindBy(xpath = "(//input[@id='incrementar'])[last()]") WebElement rbUltimoRegistro;
	public @FindBy(id = "search-protec-setting") WebElement btnConsultar;
	public @FindBy(xpath = "//a[text()='Nuevo Evento']") WebElement btnNuevoEvento;
	public @FindBy(xpath = "//a[text()='Editar']") WebElement btnEditar;
	public @FindBy(xpath = "//a[text()='Eliminar']") WebElement btnEliminar;
	public @FindBy(xpath = "//button[text()='Confirmar']") WebElement btnConfirmar;
	public @FindBy(xpath = "//button[text()='Cancelar']") WebElement btnCancelarPopup;
	public @FindBy(xpath = "//*[@id=\'form-foo-sic-protected\']/div[4]/a[1]") WebElement btn_Guardar;
	public @FindBy(className = "sic-scroll-table") WebElement scroll_Config;
	public @FindBy(id = "form-foo-sic-protected") WebElement scroll_Stock;
	public @FindBy(xpath = "/html/body/div[2]/div") WebElement pop_up;
	public @FindBy(id = "date-date-sic") WebElement txtFechaInicio;
	public @FindBy(id = "date-end-sic") WebElement txtFechaTermino;
	public @FindBy(id = "stock-sic-level1") WebElement inp_Level1;
	public @FindBy(id = "stock-sic-level2") WebElement inp_Level2;
	public @FindBy(id = "stock-sic-level3") WebElement inp_Level3;
	public @FindBy(id = "stock-sic-level4") WebElement inp_Level4;
	public @FindBy(id = "stock-sic-level5") WebElement inp_Level5;
	public @FindBy(id = "Permanente") WebElement chk_Permanente;
	//
	public @FindBy(xpath = "/html/body/div[2]/div/div[3]/button[1]") WebElement btn_CancelaEliminar;
	public @FindBy(xpath = "//*[@id=\'form-foo-sic-protected\']/div[4]/a[2]") WebElement btn_CancelarEditar;
	public @FindBy(id = "system-mss") WebElement sel_TipoProducto;
	public @FindBy(xpath = "//*[@id=\'line-prot-sic\']") WebElement txt_Linea;
	public @FindBy(xpath = "//*[@id=\'line-dropdown-js\']/div/div/span[2]/input[1]") WebElement txt_LineaProd;
	public @FindBy(xpath = "//*[@id=\'sub-line-dropdown-js\']") WebElement txt_SubLinea;
	public @FindBy(xpath = "//*[@id=\'sub-line-dropdown-js\']/div/div/span[2]/input[1]") WebElement txt_SubLineaProd;
	public @FindBy(xpath = "//*[@id=\'sic-filter-container-proct\']/div[3]/p") WebElement click_Out;
	public @FindBy(xpath = "//*[@id=\'dpto-dropdown-js\']/div/div/span[1]/span/span[2]") WebElement del_Depto;
	public @FindBy(xpath = "//*[@id=\'line-dropdown-js\']/div/div/span[1]/span/span[2]") WebElement del_Linea;
	public @FindBy(xpath = "//*[@id=\'sub-sku-dropdown-js\']") WebElement txt_SKU;
	public @FindBy(xpath = "//*[@id=\'sub-sku-dropdown-js\']/div/div/span[2]/input[1]") WebElement txt_SkuInp;
	public @FindBy(xpath = "/html/body/div[2]") WebElement div_Mensaje;
	public @FindBy(xpath = "/html/body/div[2]/div/div[2]/div[1]/p[1]") WebElement msj_Alerta;
	public @FindBy(xpath = "/html/body/div/section/div[3]/div[2]/div[2]/form/div[2]/div[2]/div/div[2]/p") WebElement msj_Grilla;
	public @FindBy(xpath = "/html/body/div/section/div[3]/div[2]/div[2]/form/div[2]/div[2]/div/div[2]") WebElement grilla_Cont;
	public @FindBy(xpath = "/html/body/div/section/div[3]/div[2]/div[2]/form/div[3]/div[3]/p") WebElement msj_Obligatorio;
	public @FindBy(xpath = "/html/body/div[2]/div/div[2]/div[1]/p[1]") WebElement msj_Niveles;
	public @FindBy(xpath = "/html/body/div[2]/div/div[2]/div[1]/p[1]") WebElement msj_COnfig;
	public @FindBy(xpath = "/html/body/div[2]/div/div[2]/div[1]/p[1]") WebElement msj_Eliminar;

	public @FindBy(id = "sub-line-prot-sic") WebElement txt_Sublinea;
	public @FindBy(id = "sku-prot-sic") WebElement txt_CodVenta;
	public @FindBy(id = "search-protec-setting") WebElement btnConsulta;
	public @FindBy(xpath = "//a[text()='Cancelar']") WebElement btnCancelar;
	public @FindBy(xpath = "//a[text()='Consultar' and @class='main_button disabled']") WebElement btnConsultaDisabled;
	public @FindBy(xpath = "//*[@id=\'app\']/section/div[3]/div[2]/div[2]/form/div[2]/div[2]/div") WebElement grilla_Config;
	public String xpathLoading = "//div[@id='prelod-login-stokc-sku']/img";

	public StockProtegidoPage() throws IOException {
		super();
		// TODO Auto-generated constructor stub
	}

	public void selTipoProd() throws Exception {
		sel_TipoProducto.isSelected();

	}

	public void selTipoProducto() throws Exception {
		assertTrue(isElementClickable(sel_TipoProducto));

	}

	public void selDepartamento() throws Exception {
		cbDepartamento.isEnabled();

	}

	public void txtLinea() throws Exception {
		txt_Linea.isEnabled();

	}

	public void txtLineaClick() throws Exception {
		txt_Linea.click();

	}

	public void txtLineaSel() throws Exception {
		txt_Linea.sendKeys(Keys.ENTER);
		BasePage.WaitSleep(2);

	}

	public void txtLineaProd() throws Exception {
		txt_LineaProd.sendKeys("000162");
		BasePage.WaitSleep(1);
		txt_LineaProd.sendKeys(Keys.ENTER);

	}

	public void txtLineaActiva() throws Exception {
		txt_LineaProd.sendKeys("500536");
		BasePage.WaitSleep(1);
		txt_LineaProd.sendKeys(Keys.ENTER);

	}

	public void txtSubLineaSel() throws Exception {
		click_Out.click();
		BasePage.WaitSleep(1);
		txt_SubLineaProd.click();
		BasePage.WaitSleep(2);

	}

	public void txtSubLineaProd() throws Exception {
		txt_SubLineaProd.sendKeys("S001121");
		BasePage.WaitSleep(1);
		txt_SubLineaProd.sendKeys(Keys.ENTER);

	}

	public void txtSubLineaActiva() throws Exception {
		txt_SubLineaProd.sendKeys("S502038");
		BasePage.WaitSleep(1);
		txt_SubLineaProd.sendKeys(Keys.ENTER);

	}

	public void txtSubLinea() throws Exception {
		txt_Sublinea.isEnabled();

	}

	public void txtCodVenta() throws Exception {
		txt_CodVenta.isEnabled();

	}

	public void txtCodVentaSel() throws Exception {
		txt_CodVenta.click();

	}

	public boolean btnConsultaIsDisplayed() throws Exception {
		return btnConsulta.isDisplayed();
	}

	public boolean BtnCancelarIsDisplayed() throws Exception {
		return btnCancelar.isDisplayed();
	}

	public void MoveToBtnCancelar() throws Exception {
		ActionMoveToElement(btnCancelar);
	}

	public boolean btnConsultaIsDisabled() throws Exception {
		return btnConsultaDisabled.isDisplayed();
	}

	public boolean cbLineaIsEnabled() throws Exception {
		return driver
				.findElements(By
						.xpath("//p[contains(text(),'Línea')]/../div/div/div/span[2]/input[@placeholder='Seleccione']"))
				.size() > 0;
	}

	public boolean txtCodigoVentaIsEnabled() throws Exception {
		return driver.findElements(By.xpath("//p[contains(text(),'Cod. Venta')]/../div/div/div/span[2]/input[1]"))
				.size() > 0;
	}

	public boolean btnNuevoEventoIsDisabled() throws Exception {
		return HasClass(btnNuevoEvento, "disabled");
	}

	public boolean TxtFechaInicioIsDisabled() throws Exception {
		return !txtFechaInicio.isEnabled();
	}

	public boolean TxtFechaTerminoIsDisabled() throws Exception {
		return !txtFechaTermino.isEnabled();
	}

	public boolean btnEditarIsDisabled() throws Exception {
		return HasClass(btnEditar, "disabled");
	}

	public boolean btnEliminarIsDisabled() throws Exception {
		return HasClass(btnEliminar, "disabled");
	}

	public boolean grillConfigIdDisplayed() throws Exception {
		return grilla_Config.isDisplayed();
	}

	public boolean gillConfigIsEmpty() throws Exception {

		return driver.findElements(By.id("empty-row-protec")).size() > 0;
	}

	public boolean btnEventoIsDisplayed() throws Exception {
		return btnNuevoEvento.isDisplayed();
	}

	public boolean btnEditarIsDisplayed() throws Exception {
		return btnEditar.isDisplayed();
	}

	public boolean btnEliminarIsDisplayed() throws Exception {
		return btnEliminar.isDisplayed();
	}

	public void btnEliminarDis() throws Exception {
		btnEliminar.isEnabled();
	}

	public void selProducto() throws IOException {
		sel_Producto.click();
	}

	public boolean btnConsultarIsEnabled() throws IOException {
		return btnConsulta.isEnabled();
	}

	public void EliminarDepartamentoSeleccionado() throws IOException {

		String xpathBtnXDepartamento = "//p[contains(text(),'Departamento')]/../div/div/div/span/span/span[2]";
		List<WebElement> oListaElementos = driver.findElements(By.xpath(xpathBtnXDepartamento));

		for (WebElement oElemento : oListaElementos) {
			oElemento.click();
			WaitSleep(1);
		}

		driver.findElement(By.xpath("//p[contains(text(),'Departamento')]")).click();

	}

	public void EliminarLineaSeleccionado() throws IOException {

		WaitUntilWebElementDissapears(xpathLoading);

		String xpathBtnXDepartamento = "//p[contains(text(),'Línea')]/../div/div/div/span/span/span[2]";
		List<WebElement> oListaElementos = driver.findElements(By.xpath(xpathBtnXDepartamento));

		for (WebElement oElemento : oListaElementos) {
			WaitAndClickElement(oElemento);
			WaitSleep(1);
		}

		driver.findElement(By.xpath("//p[contains(text(),'Departamento')]")).click();

	}

	public void delLinea() throws IOException {
		del_Linea.click();
		BasePage.WaitSleep(1);
		click_Out.click();

	}

	public void clickOut() throws IOException {
		click_Out.click();

	}

	public void txtSKU() throws IOException {
		txt_SKU.isEnabled();

	}

	public void txtSKUClick() throws IOException {
		txt_SKU.click();

	}

	public void txtSKUSel() throws IOException {
		txt_SkuInp.sendKeys("2026050220005");
		txt_SkuInp.sendKeys(Keys.ENTER);
		BasePage.WaitSleep(1);
		txt_SkuInp.sendKeys("2026050240003");
		txt_SkuInp.sendKeys(Keys.ENTER);

	}

	public void divMensaje() throws IOException {
		div_Mensaje.click();

	}

	public void msjAlerta() throws IOException {
		Assert.assertEquals("Debe seleccionar una bodega para continuar.", msj_Alerta.getText());

	}

	public void txtSKUUnit() throws IOException {
		txt_SkuInp.sendKeys("2026050220005");
		txt_SkuInp.sendKeys(Keys.ENTER);

	}

	public void txtSKUActivo() throws IOException {
		txt_SkuInp.sendKeys("2012352050006");
		txt_SkuInp.sendKeys(Keys.ENTER);

	}

	public void msjGrilla() throws IOException {
		grilla_Cont.click();
		BasePage.WaitSleep(1);
		Assert.assertEquals("No se encontraron resultados para los criterios de búsqueda ingresados",
				msj_Grilla.getText());
	}

	
	public void chkPermanente() throws IOException {
		chk_Permanente.click();

	}

	public void msjObligatorio() throws IOException {
		Assert.assertEquals("Debe completar los campos obligatorios", msj_Obligatorio.getText());

	}

	public void msjNiveles() throws IOException {
		Assert.assertEquals("Hay niveles de protección sin completar, éstos quedarán sin valores en dichos niveles.",
				msj_Niveles.getText());

	}

	public void msjConfig() throws IOException {
		System.out.println("Mensaje: " + msj_COnfig.getText());
		BasePage.WaitSleep(10);
		Assert.assertEquals("Ya existe una configuración para la jerarquía \n\n seleccionada en esas fechas",
				msj_COnfig.getText());

	}

	public void btnCancelar() throws IOException {
		btnCancelarPopup.click();

	}

	public void msjEliminar() throws IOException {
		Assert.assertEquals("¿Está seguro que desea eliminar la configuración seleccionada?", msj_Eliminar.getText());

	}

	public void btnCancelaEliminar() throws IOException {
		btn_CancelaEliminar.click();

	}

	public void btnCancelaEditar() throws IOException {
		btn_CancelarEditar.click();

	}

	public boolean ExisteMensajeFlotante(String mensaje) throws IOException {
		String xpathMensajeFlotante = "//p[contains(text(),'[MENSAJE]')]";
		return driver.findElements(By.xpath(xpathMensajeFlotante.replace("[MENSAJE]", mensaje))).size() > 0;
	}

	public boolean ExisteMensajeValidacion(String mensaje) throws IOException {
		String xpathMensajeValidacion = "//p[contains(text(),'[MENSAJE]')]";
		return driver.findElements(By.xpath(xpathMensajeValidacion.replace("[MENSAJE]", mensaje))).size() > 0;
	}

	public void ClickCbDepartamento() throws Exception {
		cbDepartamento.click();
	}

	public void ClickCbLinea() throws Exception {
		cbLinea.click();
	}

	public void ClickCbSubLinea() throws Exception {
		cbSubLinea.click();
	}

	public void selDeptoEsc(String codigoDepartamento) throws Exception {
		cbDepartamento.sendKeys(codigoDepartamento);
		cbDepartamento.sendKeys(Keys.ENTER);
		driver.findElement(By.xpath("//p[contains(text(),'Departamento')]")).click();
		WaitSleep(1);
	}

	public void selLineaEsc(String linea) throws Exception {
		cbLinea.sendKeys(linea);
		cbLinea.sendKeys(Keys.ENTER);
		driver.findElement(By.xpath("//p[contains(text(),'Departamento')]")).click();
		WaitSleep(1);
	}

	public void selSubLineaEsc(String subLinea) throws Exception {
		cbSubLinea.sendKeys(subLinea);
		cbSubLinea.sendKeys(Keys.ENTER);
		driver.findElement(By.xpath("//p[contains(text(),'Departamento')]")).click();
		WaitSleep(1);
		WaitUntilWebElementDissapears(xpathLoading);
	}

	public void IngresarCodigoVenta(String codigoVenta) throws Exception {
		txtCodigoVenta.sendKeys(codigoVenta);
		txtCodigoVenta.sendKeys(Keys.ENTER);
		driver.findElement(By.xpath("//p[contains(text(),'Departamento')]")).click();
		WaitSleep(1);
	}

	public void selDeptoActiva() throws Exception {
		cbDepartamento.sendKeys("D101");
		cbDepartamento.sendKeys(Keys.ENTER);
	}

	public void ClickBtnConsultar() throws Exception {
		btnConsultar.click();
		WaitUntilWebElementDissapears(xpathLoading);
	}

	public void btnEvento() throws Exception {
		btnNuevoEvento.click();
	}

	public void scrollConfig() throws Exception {
		scrollToElementByWebElementLocator(scroll_Config);
	}

	public boolean GrillaHasRows() {
		return driver.findElements(By.id("incrementar")).size() > 0;
	}

	public void scrollStock() throws Exception {
		scrollToElementByWebElementLocator(scroll_Stock);
	}

	public void inpFecIni() throws Exception {
		txtFechaInicio.click();
	}

	public boolean TxtFecIniHasValue() throws Exception {
		return !GetElementText(txtFechaInicio).equals("");
	}

	public boolean VerifyPrevDayDisable() throws Exception {
		String day = GetDayNumber(-1);
		String xpathDatePickerDay = "//p[text()='Inicio Vigencia:']/../div/div/div/span[text()='[DAY]']";
		WebElement element = driver.findElement(By.xpath(xpathDatePickerDay.replace("[DAY]", day)));
		return HasClass(element, "disabled");
	}

	public void SeleccionarInicioVigencia(int dateAdd) throws Exception {
		String day = GetDayNumber(dateAdd);
		String xpathDatePickerDay = "//p[text()='Inicio Vigencia:']/../div/div/div/span[text()='[DAY]']";
		xpathDatePickerDay = xpathDatePickerDay.replace("[DAY]", day);
		System.out.println("XPATH: " + xpathDatePickerDay);

		ActionMoveAndClick(driver.findElement(By.xpath(xpathDatePickerDay)));

	}

	public void SeleccionarFinVigencia(int dateAdd) throws Exception {
		String day = GetDayNumber(dateAdd);
		String xpathDatePickerDay = "//p[text()='Fin Vigencia:']/../div/div/div/span[text()='[DAY]']";
		WaitAndClickXpath(xpathDatePickerDay.replace("[DAY]", day));
	}

	public void fechaInicio() throws IOException {
		Calendar fecha = Calendar.getInstance();
		int dia = fecha.get(Calendar.DAY_OF_MONTH);
		WebElement fechaIniOK = null;

		for (int i = 1; i <= 41; i++) {
			WebElement fecInicio = driver.findElement(
					By.xpath("//*[@id='form-protected-stock-js']/div[1]/div[1]/div[1]/div/div[2]/div/span[" + i + "]"));
			String fechaIni = fecInicio.getText();
			try {
				if (dia == Integer.parseInt(fechaIni)) {
					fechaIniOK = driver.findElement(By.xpath(
							"//*[@id='form-protected-stock-js']/div[1]/div[1]/div[1]/div/div[2]/div/span[" + i + "]"));
				} else {
					System.out.println("");
				}
			} catch (Exception e) {
				e.getMessage();
			}
		}
		fechaIniOK.click();
	}

	public void inpFecFin() throws Exception {
		txtFechaTermino.click();

	}

	public void fechaFin() throws IOException {
		Calendar fecha = Calendar.getInstance();
		int dia = fecha.get(Calendar.DAY_OF_MONTH);
		int diaMas = dia + 1;
		WebElement fechaIniOK = null;

		for (int i = 1; i <= 41; i++) {
			WebElement fecInicio = driver.findElement(By
					.xpath("//*[@id=\'form-protected-stock-js\']/div[1]/div[1]/div[2]/div/div[2]/div/span[" + i + "]"));
			String fechaIni = fecInicio.getText();
			try {
				if (diaMas == Integer.parseInt(fechaIni)) {
					fechaIniOK = driver.findElement(
							By.xpath("//*[@id=\'form-protected-stock-js\']/div[1]/div[1]/div[2]/div/div[2]/div/span["
									+ i + "]"));
				} else {
					System.out.println("");
				}
			} catch (Exception e) {
				e.getMessage();
			}
		}
		fechaIniOK.click();

	}

	public void fechaIniMayor() throws IOException {
		Calendar fecha = Calendar.getInstance();
		int dia = fecha.get(Calendar.DAY_OF_MONTH);
		int diaMas = dia + 1;
		WebElement fechaIniOK = null;

		for (int i = 1; i <= 41; i++) {
			WebElement fecInicio = driver.findElement(
					By.xpath("//*[@id='form-protected-stock-js']/div[1]/div[1]/div[1]/div/div[2]/div/span[" + i + "]"));
			String fechaIni = fecInicio.getText();
			try {
				if (diaMas == Integer.parseInt(fechaIni)) {
					fechaIniOK = driver.findElement(By.xpath(
							"//*[@id='form-protected-stock-js']/div[1]/div[1]/div[1]/div/div[2]/div/span[" + i + "]"));
				} else {
					System.out.println("");
				}
			} catch (Exception e) {
				e.getMessage();
			}
		}
		fechaIniOK.click();

	}

	public void fechaFinMenor() throws IOException {
		Calendar fecha = Calendar.getInstance();
		int dia = fecha.get(Calendar.DAY_OF_MONTH);
		WebElement fechaIniOK = null;

		for (int i = 1; i <= 41; i++) {
			WebElement fecInicio = driver.findElement(By
					.xpath("//*[@id=\'form-protected-stock-js\']/div[1]/div[1]/div[2]/div/div[2]/div/span[" + i + "]"));
			String fechaIni = fecInicio.getText();
			try {
				if (dia == Integer.parseInt(fechaIni)) {
					fechaIniOK = driver.findElement(
							By.xpath("//*[@id=\'form-protected-stock-js\']/div[1]/div[1]/div[2]/div/div[2]/div/span["
									+ i + "]"));
				} else {
					System.out.println("");
				}
			} catch (Exception e) {
				e.getMessage();
			}
		}
		fechaIniOK.click();
	}

	public void popUp() throws Exception {
		pop_up.click();
	}

	public void btnGuardar() throws Exception {
		btn_Guardar.click();
		WaitUntilWebElementDissapears(xpathLoading);
	}

	public void inpLevel1() throws Exception {
		inp_Level1.sendKeys("2");

	}

	public void inpLevel2() throws Exception {
		inp_Level2.sendKeys("2");

	}

	public void inpLevel3() throws Exception {
		inp_Level3.sendKeys("2");

	}

	public void inpLevel4() throws Exception {
		inp_Level4.sendKeys("2");

	}

	public void inpLevel5() throws Exception {
		inp_Level5.sendKeys("2");

	}

	public void ClickBtnConfirmar() throws Exception {
		btnConfirmar.click();

	}

	public void rdbEditar() throws Exception {
		rbUltimoRegistro.click();

	}
	

	public void SeleccionarUltimoRegistro() throws Exception {
		rbUltimoRegistro.click();

	}
	

	public void btnEditar() throws Exception {
		btnEditar.click();

	}

	public void ClickBtnEliminar() throws Exception {
		btnEliminar.click();

	}

	public void editLevel1() throws Exception {
		inp_Level1.clear();
		inp_Level1.sendKeys("3");

	}

	public void editLevel2() throws Exception {
		inp_Level2.clear();
		inp_Level2.sendKeys("3");

	}

}
