package corp.ripley.sicpe.pageobject.sic;

import java.awt.AWTException;
import java.io.IOException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import corp.ripley.sicpe.pageobject.BasePage;

public class JerarquiaPage extends BasePage {
	
	public static String nrocod_venta;
	public static String stock_disponible;
	public static String stock_Activo;
	public static String stock_Reserva; 
	public static String stock_Seguridad; 		
	public static String stock_vendible;

	public JerarquiaPage() throws IOException {
		super();
		// TODO Auto-generated constructor stub
	}

	@FindBy(xpath = "//input[@type='text' and @placeholder='SKU o Cod Venta']")
	private WebElement txtCodigoVenta;

	@FindBy(xpath = "//input[@type='number']")
	private WebElement txtStock;

	@FindBy(xpath = "//a[@class='main_button' and text()='Limpiar']")
	private WebElement btnLimpiar;

	@FindBy(xpath = "//a[@class='main_button' and text()='Consultar']")
	private WebElement btnConsultar;

	@FindBy(xpath = "//a[@class='main_button' and text()='Exportar']")
	private WebElement btnExportar;

	@FindBy(xpath = "//p[text()='Tipo de Producto ']/../div/select")
	private WebElement cbTipoProducto;

	@FindBy(xpath = "//div[@id='quantityType-hirch']/div/select")
	private WebElement cbRangoStock;

	@FindBy(xpath = "//*[@id='checkbox']")
	private WebElement checkSinFiltro;

	@FindBy(xpath = "//input[@id='ddDepartment']/../input[1]")
	private WebElement txtDepartamento;

	@FindBy(xpath = "//input[@id='ddLine']/../input[1]")
	private WebElement txtLinea;

	@FindBy(xpath = "//input[@id='ddSubline']/../input[1]")
	private WebElement txtSubLinea;

	@FindBy(xpath = "//p[text()='Tipo de Inventario:']/../div/select")
	private WebElement cbTipoInventario;

	@FindBy(xpath = "//table/tr/td[1]")
	private WebElement tdBodega;

	@FindBy(xpath = "//table/tr/td[2]")
	private WebElement tdDepartamento;

	@FindBy(xpath = "//table/tr/td[3]")
	private WebElement tdLinea;

	@FindBy(xpath = "//table/tr/td[4]")
	private WebElement tdSublinea;

	@FindBy(xpath = "//table/tr/td[5]")
	private WebElement tdEstilo;

	@FindBy(xpath = "//table/tr[2]/td[6]")
	private WebElement tdCodigoVenta;

	@FindBy(xpath = "//table/tr/td[8]")
	private WebElement tdtipoInventario;
	
	@FindBy(xpath = "//table/tr/td[7]")
	private WebElement tdStock;
	//CALCULO STOCK IMS INICIO
	//@FindBy(xpath = "//table/tr/td[9]") //posiion 9 para peru para chile es 12
	@FindBy(xpath = "//table/tr/td[8]")
	private WebElement tdStockActivo;
	@FindBy(xpath = "//table/tr/td[9]")
	private WebElement tdStocReserva;
	@FindBy(xpath = "//table/tr/td[10]") 
	private WebElement tdStockSeguridad;
	@FindBy(xpath = "//table/tr/td[11]") 
	private WebElement tdStockVendible;
	//CALCULO STOCK IMS FIN
	@FindBy(id = "information") private WebElement menu_informacion;

	String xpathStock = "//option[text()='Stock mayor a:']/..";
	String xpathStockOpcion = "//option[text()='Stock mayor a:']/../option[text()='[TIPO_STOCK]']";
	String xpathStockOpciones = "//option[text()='Stock mayor a:']/../option";
	String xpathBtnDeshabilitado = "//a[@class='main_button disabled' and text()='[NOMBRE_BOTON]']";
	String xpathBtnHabilitado = "//a[@class='main_button' and text()='[NOMBRE_BOTON]']";
	String xpathLineaSeleccionada = "//span[text()='[NOMBRE_LINEA]']";
	String xpathSubLineaSeleccionada = "//span[text()='[NOMBRE_SUBLINEA]']";
	String xpathTipoInventario = "//p[text()='Tipo de Inventario:']/../div/select";
	String xpathTipoInventarioOpcion = "//p[text()='Tipo de Inventario:']/../div/select/option[@value=[CODIGO]]";
	String xpathLimiteStockOpcion = "//div[@id='quantityType-hirch']/div/select/option[text()='[NOMBRE]']";
	String xpathImgLoading = "//div[@class='prelod-adjus-stok']/img";


	public String ObtenerCeldaStockVendible() {
		return GetElementText(tdStock);
	}
	
	public String ObtenerCeldaBodega() {
		return GetElementText(tdBodega);
	}

	public String ObtenerCeldaDepartamento() {
		return GetElementText(tdDepartamento);
	}

	public String ObtenerCeldaLinea() {
		return GetElementText(tdLinea);
	}

	public String ObtenerCeldaSubLinea() {
		return GetElementText(tdSublinea);
	}

	public String ObtenerCeldaEstilo() {
		return GetElementText(tdEstilo);
	}

	public String ObtenerCeldaCodigoVenta() {
		return GetElementText(tdCodigoVenta);
	}

	public String ObtenerCeldaStock() {
		return GetElementText(tdStock);
	}

	public String ObtenerCeldaTipoInventario() {
		return GetElementText(tdtipoInventario);
	}

	public void IngresarCodigoVenta(String codigoVenta) throws InterruptedException {
		SendKeysToWebElement(txtCodigoVenta, codigoVenta);
		ClickConsultar();//esto funciona junto con el el consulta fuera de este metodo individual no
	}

	public void IngresarStock(String stock) throws InterruptedException {
		SendKeysToWebElement(txtStock, stock);
	}

	public void SeleccionarTipoStock(int stockEnBD) throws InterruptedException {

		if (stockEnBD == 0) {
			WaitAndClickXpath(xpathStock);
			xpathStockOpcion = xpathStockOpcion.replace("[TIPO_STOCK]", "Stock igual a:");
			WaitAndClickXpath(xpathStockOpcion);
		}
		if (stockEnBD < 0) {
			WaitAndClickXpath(xpathStock);
			xpathStockOpcion = xpathStockOpcion.replace("[TIPO_STOCK]", "Stock menor a:");
			WaitAndClickXpath(xpathStockOpcion);
		}

		if (stockEnBD > 0) {
			WaitAndClickXpath(xpathStock);
			xpathStockOpcion = xpathStockOpcion.replace("[TIPO_STOCK]", "Stock mayor a:");
			WaitAndClickXpath(xpathStockOpcion);
		}
	}

	public void SeleccionarTipoStockMayorIgualA() throws InterruptedException {
		WaitAndClickXpath(xpathStock);
		xpathStockOpcion = xpathStockOpcion.replace("[TIPO_STOCK]", "Stock mayor o igual a:");
		WaitAndClickXpath(xpathStockOpcion);
	}

	public void seleccionarTipoInventario(String tipoInventario) throws InterruptedException {
		WaitAndClickXpath(xpathTipoInventario);
		xpathTipoInventarioOpcion = xpathTipoInventarioOpcion.replace("[CODIGO]", tipoInventario);
		WaitAndClickXpath(xpathTipoInventarioOpcion);
	}
	
	
	public void SeleccionarTipoStockMenorIgualA() throws InterruptedException {
		WaitAndClickXpath(xpathStock);
		xpathStockOpcion = xpathStockOpcion.replace("[TIPO_STOCK]", "Stock menor o igual a:");
		WaitAndClickXpath(xpathStockOpcion);
	}

	public void SeleccionarTipoStockDistintoA() throws InterruptedException {
		WaitAndClickXpath(xpathStock);
		xpathStockOpcion = xpathStockOpcion.replace("[TIPO_STOCK]", "Stock distinto a:");
		WaitAndClickXpath(xpathStockOpcion);
	}


	public void ClickConsultar() throws InterruptedException {
		WaitAndClickElement(btnConsultar);
		
	}

	public void ClickSinFiltro() throws InterruptedException {
		WaitAndClickElement(checkSinFiltro);

	}

	public void ClickComboRangoStock() throws InterruptedException {
		WaitAndClickElement(cbRangoStock);
	}

	public void ClickComboTipoInventario() throws InterruptedException {
		WaitAndClickElement(cbTipoInventario);
	}

	public void ClickLimpiar() throws InterruptedException {
		WaitAndClickElement(btnLimpiar);
	}

	public void ClickExportar() throws InterruptedException {
		WaitAndClickElement(btnExportar);
	}

	public int ObtenerStockSIC() throws InterruptedException {
		return Integer.parseInt(GetElementText(tdStock));

	}

	public String ObtenerTextoComboTipoProducto() {
		return GetComboboxText(cbTipoProducto);
	}

	public String ObtenerTextoComboRangoStock() {
		return GetComboboxText(cbRangoStock);
	}

	public boolean ExisteBotonDeshabilitado(String nombreBoton) {
		xpathBtnDeshabilitado = xpathBtnDeshabilitado.replace("[NOMBRE_BOTON]", nombreBoton);
		return ElementExist(xpathBtnDeshabilitado);
	}

	public boolean ExisteLineaSeleccionada(String nombre) {
		xpathLineaSeleccionada = xpathLineaSeleccionada.replace("[NOMBRE_LINEA]", nombre);
		return ElementExist(xpathLineaSeleccionada);
	}

	public boolean ExisteSubLineaSeleccionada(String nombre) {
		xpathSubLineaSeleccionada = xpathSubLineaSeleccionada.replace("[NOMBRE_SUBLINEA]", nombre);
		return ElementExist(xpathSubLineaSeleccionada);
	}

	public boolean ExisteBotonHabilitado(String nombreBoton) {
		xpathBtnHabilitado = xpathBtnHabilitado.replace("[NOMBRE_BOTON]", nombreBoton);
		return ElementExist(xpathBtnHabilitado);
	}

	public boolean ExisteTipoInventarioOpcion(String codigo) {
		xpathTipoInventarioOpcion = xpathTipoInventarioOpcion.replace("[CODIGO]", codigo);
		return ElementExist(xpathTipoInventarioOpcion);
	}

	public boolean ExisteRangoStockOpcion(String nombre) {
		xpathLimiteStockOpcion = xpathLimiteStockOpcion.replace("[NOMBRE]", nombre);
		return ElementExist(xpathLimiteStockOpcion);
	}
	
	
	public int ObtenerCantidadOpcionesRangoStock() {
		return GetElementsSize(xpathStockOpciones);
	}

	public void SeleccionarDepartamento(String nombre) throws InterruptedException, AWTException {
		SendKeysToWebElement(txtDepartamento, nombre);
		SendDownKey();
		SendEnterKey();
	}

	public void ClickTxtLinea() {
		String xpathLinea = "//input[@id='ddLine']/../input[1][@aria-disabled='false']";
		WaitAndClickXpath(xpathLinea);
	}

	public void ClickTxtSubLinea() {
		String xpathLinea = "//input[@id='ddSubline']/../input[1][@aria-disabled='false']";
		WaitAndClickXpath(xpathLinea);
	}

	public void IngresarLinea(String nombre) {
		SendKeysToWebElement(txtLinea, nombre);
	}

	public void IngresarSubLinea(String nombre) {
		SendKeysToWebElement(txtSubLinea, nombre);
	}

	public void SeleccionarLinea(String nombre) throws AWTException, InterruptedException {
		SendKeysToWebElement(txtLinea, nombre);
		SendDownKey();
		SendEnterKey();
	}

	public void SeleccionarSubLinea(String nombre) throws AWTException, InterruptedException {
		SendKeysToWebElement(txtSubLinea, nombre);
		SendDownKey();
		SendEnterKey();
	}

	public boolean VerificarTipoInventario(String actual, String esperado) {
		return actual.equals(esperado);
	}

	public boolean VerificarSubLineaSeleccionada(String nombre) {
		return ExisteSubLineaSeleccionada(nombre);
	}

	public void ConsultarProducto(String codigoVenta) throws InterruptedException {
		IngresarCodigoVenta(codigoVenta);
		ClickSinFiltro();
		ClickConsultar();
	}
	
	public void ConsultarProductoBodega(String codigoArticulo, String codigoBodega) throws InterruptedException {
		menuPage.SeleccionarBodega(codigoBodega);
		IngresarCodigoVenta(codigoArticulo);
		ClickConsultar();
		WaitUntilWebElementIsVisible(tdCodigoVenta);
		String vcod_venta = GetElementText(tdCodigoVenta);
		String[] variable =vcod_venta.split("-");
		nrocod_venta = variable [0]; 
		System.out.println("nrocod_venta:"+ nrocod_venta);
		menu_informacion.click();
		
	}

	//CALCULO STOCK IMS INICIO
	public void ConsultarStockVendible(String codigoArticulo) throws InterruptedException {
		IngresarCodigoVenta(codigoArticulo);
		ClickConsultar();
		WaitUntilWebElementIsVisible(tdCodigoVenta);
		Thread.sleep(3000);
		stock_disponible = GetElementText(tdStock);	
		stock_Activo = GetElementText(tdStockActivo);		
		stock_Reserva = GetElementText(tdStocReserva);		
		stock_Seguridad = GetElementText(tdStockSeguridad);			
		stock_vendible = GetElementText(tdStockVendible);
	
		System.out.println("Stock_Disponible:"+ stock_disponible);
		System.out.println("Stock_Activo ?:"+ stock_Activo);
		System.out.println("Stock_Reserva:"+ stock_Reserva);
		System.out.println("Stock_Seguridad:"+ stock_Seguridad);
		System.out.println("Stock_Vendible:"+ stock_vendible);

	}
	//CALCULO STOCK IMS FIN
	
	public void ConsultarProducto(String codigoVenta, int stockEnBD) throws InterruptedException {
		IngresarCodigoVenta(codigoVenta);
		SeleccionarTipoStock(stockEnBD);
		ClickConsultar();

	}

	public boolean VerificarLineaSeleccionada(String nombre) {
		return ExisteLineaSeleccionada(nombre);
	}

	public boolean VerificarBodega(String actual, String esperado) {
		return actual.equals(esperado);
	}

	public boolean VerificarDepartamento(String actual, String esperado) {
		return actual.contains(esperado);
	}

	public boolean VerificarLinea(String actual, String esperado) {
		return actual.contains(esperado);
	}

	public boolean VerificarSubLinea(String actual, String esperado) {
		return actual.contains(esperado);
	}

	public boolean VerificarEstilo(String actual, String esperado) {
		return actual.equals(esperado);
	}

	public boolean VerificarCodigoVenta(String actual, String esperado) {
		System.out.println(actual);
		System.out.println(esperado);
		return actual.equals(esperado);
	}

	public boolean VerificarStock(String actual, String esperado) {
		return actual.equals(esperado);
	}

	public boolean ValidarStockActual(int stockActual, int stockEsperado) throws Exception {

		return stockActual != stockEsperado;
	}

	public boolean ValidarNuevoStock(int stockActual, int stockEsperado) throws Exception {
		
		return stockActual != stockEsperado;
	}
	
	public void EsperarCarga() throws InterruptedException {
		WaitSleep(1);
		WaitUntilWebElementDissapears(xpathImgLoading);
		WaitSleep(1);
	}
}