package corp.ripley.sicpe.pageobject.integraciones;

import java.io.IOException;
import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import corp.ripley.sicpe.pageobject.BasePage;

public class B2BModifStockPage extends BasePage{
	
	public static String vcod_bodega;
	public static String vcod_articulo;
	public static String vStock_cantidad;
	
	public B2BModifStockPage() throws IOException{
		super();
	}
	
	@FindBy(id = "txtCodUsuario")
	private WebElement txtUsuario;	
	@FindBy (id = "txtPassword")
	private WebElement txtPassword; 
    @FindBy (id = "btnLogin")
	private WebElement btnEntrar;
		
	//iframe Proveedor
	@FindBy (xpath ="/html/frameset/frame")
	private WebElement frmProv;
	
	@FindBy (name="cboProveedor")
	private WebElement selectList_Proveedor; 
	
	@FindBy (id ="btnSubmit")
	private WebElement btnEntrarProveedor;
	
	//iframe menú Mantención	
	@FindBy (xpath="//frame[@src='/b2bWeb/portal/setProveedor.do']")
	private WebElement frmMenu;
	
	//@FindBy (xpath ="/html/body/table/tbody/tr[1]/td/div[1]/table/tbody/tr/td[2]/a[3]")
	@FindBy (xpath="/html/body/div/div[1]/div[1]/table/tbody/tr/td[4]/a[4]")
	private WebElement menuMantencion;
	
	@FindBy (linkText ="Stock Comprometido")
	private WebElement stockComprometido;
	
	@FindBy (linkText ="Modificación de stock comprometido vía archivo (nvo)")
	private WebElement modificarStock;
	
	@FindBy (linkText ="Modificación de stock comprometido (nvo)")
	private WebElement modificarStockForm;
	
	//Cargar Archivo 
	@FindBy (name ="archivo")
	private WebElement seleccionarArchivo;
	
	@FindBy (name ="btnEnviar")
	private WebElement enviarArchivo;
	
	@FindBy (xpath ="//td[@class='NormalText']") private WebElement lbl_mensaje;
	//tabla
	
	@FindBy (xpath ="//td[2][@class='ReportBody']") private WebElement cod_bodega;
	@FindBy (xpath ="//td[3][@class='ReportBody']") private WebElement cod_articulo;
	@FindBy (xpath ="//td[@class='ReportBody'][4]") private WebElement Stock_cantidad;
	
	//SIC
	@FindBy (xpath ="//td[@index='19']") private WebElement Stock_Disponible;
	
	//Formulario B2B
	
	
	String xpathBodegaB2B = "//select[@name='bodega']";
	String xpathBodegaOpcionB2B = "//select[@name='bodega']/option[@value='[CODIGO_BODEGA]']";
	
	@FindBy (name = "codRipley") private WebElement codskuRipley;
	@FindBy (xpath = "//input[@value = 'Buscar']") private WebElement btnBuscar;
	@FindBy (id = "StockMinimoNew.0") private WebElement txtCantBase; 
	@FindBy (id = "UnidadesStockExcepcionNew.0") private WebElement txtCantExcepcion; 
	@FindBy (xpath = "//input[@onclick='javascript: guardarArticulos(document.forms[0]);']") private WebElement btnGuardarAlert; 
	
	
	public void AbrirB2B() throws Exception {
		String url = GetB2BUrl();
		loadUrl(url);
	}

	public void IngresarUsuario(String usuario) throws IOException, InterruptedException {
		SendKeysToWebElement(txtUsuario, usuario);
	}

	public void IngresarPassword(String password) throws IOException, InterruptedException {
		SendKeysToWebElement(txtPassword, password);
	}

	public void ClickEntrar() throws InterruptedException {
		WaitAndClickElement(btnEntrar);
	}
	
	public void Login(String usuarioprov, String passwordprov) throws InterruptedException, IOException {
		modificacionstockPage.IngresarUsuario(usuarioprov);
		modificacionstockPage.IngresarPassword(passwordprov);
		modificacionstockPage.ClickEntrar();
	}
		
	public String selectList_Proveedor(String valor){
		driver.switchTo().frame(frmProv);

		Select selectList = new Select(selectList_Proveedor); 
		selectList.selectByVisibleText(valor);
		return GetElementText(selectList.getFirstSelectedOption());	
		
	}	
	
	public void ClickEntrarProv() throws InterruptedException {
		WaitAndClickElement(btnEntrarProveedor);	
		driver.switchTo().defaultContent(); 
	}
	
	public void IrMenu() throws InterruptedException {
		driver.switchTo().frame(frmMenu);
		WaitAndClickElement(menuMantencion);
		WaitAndClickElement(stockComprometido);
		WaitAndClickElement(modificarStock);
	}
	public void IrMenuMantencion() throws InterruptedException {
		driver.switchTo().frame(frmMenu);
		WaitAndClickElement(menuMantencion);
		WaitAndClickElement(stockComprometido);
		WaitAndClickElement(modificarStock);
	}

	public void CargarArchivo(String ruta) throws InterruptedException {
		driver.switchTo().frame("app");
		seleccionarArchivo.sendKeys(ruta);
	}
	
	public void ClickBotonEnviar() throws InterruptedException{
		WaitAndClickElement(enviarArchivo);
	}
		
	
	public void OptenerDataB2B()  throws InterruptedException{
	
		vcod_bodega=GetElementText(cod_bodega);
		vcod_articulo=GetElementText(cod_articulo);
		vStock_cantidad=GetElementText(Stock_cantidad);
		System.out.println("vcod_bodega:"+vcod_bodega);
		System.out.println("vcod_articulo:"+vcod_articulo);
		System.out.println("stock_cantidad:"+vStock_cantidad);
	
	}
	
	
	public void CompararStock(String StockBaase)  throws InterruptedException{
		WaitUntilWebElementIsVisible(Stock_Disponible);
		Assert.assertEquals(StockBaase, GetElementText(Stock_Disponible));
		System.out.println("Stock Base B2B:"+StockBaase);
		System.out.println("Stock disponible SIC:"+GetElementText(Stock_Disponible));
	}
	
	public void CompararStockExcepcion(String StockExcepcion)  throws InterruptedException{
		Assert.assertEquals(StockExcepcion, GetElementText(Stock_Disponible));
		System.out.println("Stock Excepcion B2B:"+StockExcepcion);
		System.out.println("Stock disponible SIC:"+GetElementText(Stock_Disponible));
		//driver.switchTo().defaultContent();
	}
	public void ValidarMensaje(String mensaje)  throws InterruptedException{
		WaitUntilWebElementIsVisible(lbl_mensaje);
		Assert.assertEquals(mensaje, GetElementText(lbl_mensaje));
		//driver.switchTo().defaultContent();
	}

	public void IrMenuForm() throws InterruptedException {
		driver.switchTo().frame(frmMenu);
		WaitAndClickElement(menuMantencion);
		WaitAndClickElement(stockComprometido);
		WaitAndClickElement(modificarStockForm);
	}
	
	public void BuscarSku(String codigo) throws InterruptedException {
		driver.switchTo().frame("app");
		SendKeysToWebElement(codskuRipley,codigo); 
		WaitAndClickElement(btnBuscar);
		
	}
	
	public void ClickComboBodegaB2B() throws InterruptedException {
		WaitAndClickXpath(xpathBodegaB2B);
	}

	public void ClickComboBodegaOpcionB2B(String codigoBodega) throws InterruptedException {
		String xpathBodegaOpcionMod = xpathBodegaOpcionB2B.replace("[CODIGO_BODEGA]", codigoBodega);
		System.out.println("RUTA XPATH:" + xpathBodegaOpcionMod);
		WaitAndClickXpath(xpathBodegaOpcionMod);
	}
	

	public void SeleccionarBodegaB2B(String codigoBodega) throws InterruptedException {
		modificacionstockPage.ClickComboBodegaB2B();
		System.out.println("CODIGO BODEGA:" + codigoBodega);
		menuPage.WaitSleep(1);
		modificacionstockPage.ClickComboBodegaOpcionB2B(codigoBodega);
		menuPage.WaitSleep(1);
		modificacionstockPage.ClickComboBodegaB2B();
	}

	public void BuscarSkuBodegaB2B(String codigo,String bodega) throws InterruptedException {
		driver.switchTo().frame("app");
		SendKeysToWebElement(codskuRipley,codigo);
		modificacionstockPage.SeleccionarBodegaB2B(bodega);
		WaitAndClickElement(btnBuscar);
		
	}
	
	public void IngresarCantBase(String cantidad) {
		SendKeysToWebElement(txtCantBase,cantidad); 
	}
	
	public void IngresarCantExcepcion(String StockExcepcion) {
		SendKeysToWebElement(txtCantExcepcion,StockExcepcion); 
	}
	public void irAGuardarYAceptarAlerta(String mensaje) {
		btnGuardarAlert.click();
	
		Assert.assertEquals(mensaje, getDriver().switchTo().alert().getText());
		
		getDriver().switchTo().alert().accept();
		driver.switchTo().defaultContent();
	}
	

}
