package corp.ripley.sicpe.pageobject.aws;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import corp.ripley.sicpe.pageobject.BasePage;

public class LoginPage extends BasePage {

	public LoginPage() throws IOException {
		super();
		// TODO Auto-generated constructor stub
	}

	@FindBy(xpath = "//input[@id='resolving_input']")
	private WebElement txtEmail;

	@FindBy(id = "next_button")
	private WebElement btnNext;

	@FindBy(id = "account")
	private WebElement txtAccount;

	@FindBy(id = "username")
	private WebElement txtUsername;

	@FindBy(id = "password")
	private WebElement txtPassword;

	@FindBy(id = "signin_button")
	private WebElement btnIniciarSesion;

	@FindBy(id = "user-options")
	private WebElement imgUser;

	@FindBy(xpath = "//input[@placeholder='Introducir texto...']")
	private WebElement txtIntroducirTexto;
	
	@FindBy(xpath = "//td[@role='menuitem' and text()='Enviar un mensaje']")
	private WebElement opcEnviarMensaje;

	@FindBy(xpath = "//div[text()='ID de grupo de mensajes']/../../td/input")
	private WebElement opcionFiltrada;

	private String elementToAppear = "//div[text()='ID de grupo de mensajes']/../../td/input";

	public void IngresarEmail(String accountId) {
		SendKeysToWebElement(txtEmail, accountId);
	}

	public void IngresarAccountId(String accountId) {
		SendKeysToWebElement(txtAccount, accountId);
	}

	public void IngresarUsuario(String usuario) {
		SendKeysToWebElement(txtUsername, usuario);
	}

	public void IngresarPassword(String password) {
		SendKeysToWebElement(txtPassword, password);
	}

	public void IngresarFiltro(String texto) {
		SendKeysToWebElement(txtIntroducirTexto,texto);
	}
	

	public void SeleccionarFiltro(String texto) {
		WaitSleep(2);
		String xpath = "//div[text()='"+texto+".fifo']";
		WaitAndClickXpath(xpath);
	}

	public void EnviarMensaje(String texto) {
		String xpath = "//div[text()='"+texto+".fifo']";
	
		Actions oAction = new Actions(driver);
		oAction.moveToElement(driver.findElement(By.xpath(xpath)));
		oAction.contextClick(driver.findElement(By.xpath(xpath))).build().perform();
		WaitAndClickElement(opcEnviarMensaje);
		WaitUntilXpathIsVisible(elementToAppear);
	}

	public void ObtenerHTMLPagina() {
		String str = getDriver().getPageSource();
		System.out.println(str);
	}
	
	public void ClickNext() {
		WaitAndClickElement(btnNext);
	}

	public void ClickIniciarSesion() throws InterruptedException {
		WaitAndClickElement(btnIniciarSesion);
		WaitSleep(5);
//		WaitAndClickElement(txtIntroducirTexto);
	}

	public void IrUrl(String url) throws IOException, InterruptedException {
		driver.navigate().to(url);
	}

	public void GoTo(String url) throws IOException, InterruptedException {
		driver.get(url);
	}

	public void EsperarVentanaMensajeSQS() {
		WaitUntilXpathIsVisible(elementToAppear);
		WaitSleep(5);

	}

	public void Logout() throws IOException, InterruptedException {
		driver.close();
	}

	public void AbrirAWS() throws Exception {
		String url = GetAWSUrl();
		driver.get(url);
	}

	
	public void Login(String accountId, String usuario, String password) throws IOException, InterruptedException {
		IngresarEmail(accountId);
		ClickNext();
		IngresarAccountId(accountId);
		IngresarUsuario(usuario);
		IngresarPassword(password);

		ClickIniciarSesion();
	}

}