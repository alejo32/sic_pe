package corp.ripley.sicpe.pageobject.aws;

import java.io.IOException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import corp.ripley.sicpe.pageobject.BasePage;
import corp.ripley.sicpe.util.BaseUtil;

public class SQSPage extends BasePage {

	BaseUtil util;
	public SQSPage() throws IOException {
		super();

		util = new BaseUtil();
	}


	//@FindBy(xpath = "//textarea")
	@FindBy(xpath = "//*[@id='awsui-textarea-0']")
	private WebElement taCuerpoMensaje;

	//@FindBy(xpath = "//div[text()='ID de grupo de mensajes']/../../td/input")
	@FindBy(xpath = "//input[@placeholder='Introduzca el ID del grupo de mensajes']")
	private WebElement txtIdGrupoMensaje;

	//@FindBy(xpath = "//div[text()='ID de deduplicación de mensajes']/../../td/input")
	@FindBy(xpath = "//input[@placeholder='Enter message deduplication id']")
	private WebElement txtIdDuplicacionMensaje;

	//@FindBy(xpath = "//button[text()='Enviar mensaje']")
	@FindBy(xpath = "//button/span[text()='Enviar mensaje']")
	private WebElement btnEnviarMensaje;

	@FindBy(xpath = "//button[text()='Enviar otro mensaje']")
	private WebElement btnEnviarOtroMensaje;

	public void IngresarDatosColaFifo(String jsonString) throws IOException, InterruptedException {

		SendKeysToWebElement(taCuerpoMensaje, jsonString);
		WaitSleep(5);
		SendKeysToWebElement(txtIdGrupoMensaje, String.valueOf(util.GenerarRandom(500, 1)));
		SendKeysToWebElement(txtIdDuplicacionMensaje, String.valueOf(util.GenerarRandom(500, 1)));
	}
	public void IngresarDatos(String jsonString) throws IOException, InterruptedException {

		SendKeysToWebElement(taCuerpoMensaje, jsonString);
		WaitSleep(5);
		//SendKeysToWebElement(txtIdGrupoMensaje, String.valueOf(com.util.GenerarRandom(500, 1)));
		//SendKeysToWebElement(txtIdDuplicacionMensaje, String.valueOf(com.util.GenerarRandom(500, 1)));
	}
	public void EnviarMensajeSQS() {
		WaitAndClickElement(btnEnviarMensaje);
		WaitSleep(5);
	}

	public void EnviarOtroMensaje() {
		WaitAndClickElement(btnEnviarOtroMensaje);
		WaitUntilWebElementIsVisible(taCuerpoMensaje);
	}
	

	
}