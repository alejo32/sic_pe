package corp.ripley.sicpe.pageobject;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import corp.ripley.sicpe.util.DriverFactory;

public class BasePage extends DriverFactory {
	protected WebDriverWait wait;
	protected JavascriptExecutor jsExecutor;

	public BasePage() throws IOException {
		this.wait = new WebDriverWait(driver, 60);
		jsExecutor = ((JavascriptExecutor) driver);
	}

	/* Método genera pausa en el proceso de ejecución */
	public static void WaitSleep(int second) {
		long millis = second * 1000;
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public boolean isElementClickable(WebElement element) {
		try {
			this.wait.until(ExpectedConditions.elementToBeClickable(element));
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/* Métodos JS y JS SCROLL */
	public void scrollToElementByWebElementLocator(WebElement element) {
		try {
			this.wait.until(ExpectedConditions.visibilityOf(element)).isEnabled();
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);
			((JavascriptExecutor) driver).executeScript("window.scrollBy(0, -400)");
		} catch (Exception e) {
			Assert.fail("No es posible realizar scroll a WebElement, Exception: " + e.getMessage());
		}
	}

	/* Método que realiza la captura de pantalla */
	public static void TakeScreenShot() throws IOException {
		DateFormat hora = new SimpleDateFormat("dd-MM-yyyy HHmmss");
		Date date = new Date();
		String nombrearchivo = "imagen_" + hora.format(date) + ".png";
		String ruta = "./Screenshot/";

		File imagen = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		//FileUtils.copyFile(imagen, new File(ruta + nombrearchivo));
	}

	/* Método que coloca imagen de la ejecución */
	public static void UltimoScreen() throws IOException {
		File carpeta = new File("./Screenshot/");
		String[] listado = carpeta.list();
		String nomArchivo = null;
		if (listado == null || listado.length == 0) {
			System.out.println("No hay elementos dentro de la carpeta actual");
			return;
		} else {
			for (int i = 0; i < listado.length; i++) {
				nomArchivo = listado[i++];
			}
			//test.addScreenCaptureFromPath("./Screenshot/" + nomArchivo);
		}
	}

	/* Métodos que ejecutan CLICK */
	public void WaitAndClickElement(WebElement element) {
		boolean clicked = false;
		int attempts = 0;
		while (!clicked && attempts < 10) {
			try {
				this.wait.until(ExpectedConditions.elementToBeClickable(element)).click();
				clicked = true;
			} catch (Exception e) {
				Assert.fail("No es posible clickear en elemento usando locator, element: " + "<" + element.toString()
						+ ">");
			}
			attempts++;
		}
	}

	/* Métodos que ejecutan CLICK */
	public void WaitAndClickXpath(String xpath) {
		boolean clicked = false;
		int attempts = 0;
		while (!clicked && attempts < 10) {
			try {
				this.wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath(xpath)))).click();
				clicked = true;
			} catch (Exception e) {
				Assert.fail("No es posible clickear en elemento usando locator, element: " + "<" + xpath + ">");
			}
			attempts++;
		}
	}

	public void WaitAndClickElementsUsingByLocator(By by) {
		boolean clicked = false;
		int attempts = 0;
		while (!clicked && attempts < 10) {
			try {
				this.wait.until(ExpectedConditions.elementToBeClickable(by)).click();
				clicked = true;
			} catch (Exception e) {
				Assert.fail("No es posible clickear en elemento usando locator, element: " + "<" + by.toString() + ">");
			}
			attempts++;
		}
	}

	public void ClickOnTextFromDropdownList(WebElement list, String textToSearchFor) {
		Wait<WebDriver> tempWait = new WebDriverWait(driver, 30);
		try {
			tempWait.until(ExpectedConditions.elementToBeClickable(list)).click();
			list.sendKeys(textToSearchFor);
			WaitSleep(2);
			list.sendKeys(Keys.ENTER);
		} catch (Exception e) {
			Assert.fail("No es posible seleccionar la opción solicitada, Exception: " + e.getMessage());
		}
	}

	public void ClickOnElementUsingCustomTimeout(WebElement locator, WebDriver driver, int timeout) {
		try {
			final WebDriverWait customWait = new WebDriverWait(driver, timeout);
			customWait.until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(locator)));
			locator.click();
		} catch (Exception e) {
			Assert.fail("No es posible realizar click en WebElement, Exception: " + e.getMessage());
		}
	}

	/* Métodos que ejecutan una ACCION */
	public void ActionMoveToElement(WebElement element) {
		Actions ob = new Actions(driver);
		try {
			this.wait.until(ExpectedConditions.elementToBeClickable(element)).isEnabled();
			ob.moveToElement(element).build().perform();
		} catch (StaleElementReferenceException elementUpdated) {
			WebElement elementToClick = element;
			Boolean elementPresent = wait.until(ExpectedConditions.elementToBeClickable(elementToClick)).isEnabled();
			if (elementPresent == true) {
				ob.moveToElement(elementToClick).build().perform();
			}
		} catch (Exception e) {
			Assert.fail("No es posible realizar Action Move en WebElement, Exception: " + e.getMessage());
		}
	}

	/* Métodos que ejecutan una ACCION */
	public void ActionMoveAndClick(WebElement element) {
		Actions ob = new Actions(driver);
		try {
			this.wait.until(ExpectedConditions.elementToBeClickable(element)).isEnabled();
			ob.moveToElement(element).click().build().perform();
		} catch (StaleElementReferenceException elementUpdated) {
			WebElement elementToClick = element;
			Boolean elementPresent = wait.until(ExpectedConditions.elementToBeClickable(elementToClick)).isEnabled();
			if (elementPresent == true) {
				ob.moveToElement(elementToClick).click().build().perform();
			}
		} catch (Exception e) {
			Assert.fail("No es posible realizar Action Move y Click en WebElement, Exception: " + e.getMessage());
		}
	}

	public void ActionMoveAndClickByLocator(By element) {
		Actions ob = new Actions(driver);
		try {
			Boolean elementPresent = wait.until(ExpectedConditions.elementToBeClickable(element)).isEnabled();
			if (elementPresent == true) {
				WebElement elementToClick = driver.findElement(element);
				ob.moveToElement(elementToClick).click().build().perform();
			}
		} catch (StaleElementReferenceException elementUpdated) {
			WebElement elementToClick = driver.findElement(element);
			ob.moveToElement(elementToClick).click().build().perform();
		} catch (Exception e) {
			Assert.fail("No es posible realizar Action Move y Click en WebElement usando locator, Exception: "
					+ e.getMessage());
		}
	}

	/* Métodos Sendkeys */
	public void SendKeysToWebElement(WebElement element, String textToSend) {
		try {
			if (!textToSend.equals("")) {
				this.WaitUntilWebElementIsVisible(element);
				element.clear();
				element.sendKeys(textToSend);
			}
		} catch (Exception e) {
			Assert.fail("No es posible enviar keys a WebElement, Exception: " + e.getMessage());
		}
	}

	/* Métodos JS y JS SCROLL */
	public void ScrollToElementByWebElementLocator(WebElement element) {
		try {
			this.wait.until(ExpectedConditions.visibilityOf(element)).isEnabled();
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);
			((JavascriptExecutor) driver).executeScript("window.scrollBy(0, -400)");
		} catch (Exception e) {
			Assert.fail("No es posible realizar scroll a WebElement, Exception: " + e.getMessage());
		}
	}

	public void JsPageScroll(int numb1, int numb2) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("scroll(" + numb1 + "," + numb2 + ")");
		} catch (Exception e) {
			Assert.fail("No es posible realizar scroll a WebElement, Exception: " + e.getMessage());
		}
	}

	public void JsClick(WebElement element) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", element);
	}

	/* Metodos de espera para elementos cuando se encuentren visibles */
	public boolean WaitUntilWebElementIsVisible(WebElement element) {
		try {
			this.wait.until(ExpectedConditions.visibilityOf(element));
			return true;
		} catch (Exception e) {
			Assert.fail("WebElement no esta visible, Exception: " + e.getMessage());
			return false;
		}
	}

	/* Metodos de espera para elementos cuando se encuentren visibles */
	public boolean WaitUntilXpathIsVisible(String xpath) {
		try {
			this.wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
			return true;
		} catch (Exception e) {
			Assert.fail("WebElement no esta visible, Exception: " + e.getMessage());
			WaitSleep(40);
			return false;
		}
	}

	public void WaitUntilXpathIsVisible(String xpath, int segundos) {
		try {
			WebDriverWait wait2 = new WebDriverWait(driver, segundos);
			wait2.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
		} catch (Exception e) {
			String mensaje = "EsperarElemento() - No se encontr� el elemento: [XPATH] | [ERROR]";
			mensaje = mensaje.replace("[XPATH]", xpath);
			mensaje = mensaje.replace("[ERROR]", e.getMessage());
			Assert.fail(mensaje);
		}
	}

	public boolean WaitUntilWebElementIsVisibleUsingByLocator(By element) {
		try {
			this.wait.until(ExpectedConditions.visibilityOfElementLocated(element));
			return true;
		} catch (Exception e) {
			Assert.fail("WebElement no esta visible, Exception: " + e.getMessage());
			return false;
		}
	}

	public boolean IsElementClickable(WebElement element) {
		try {
			this.wait.until(ExpectedConditions.elementToBeClickable(element));
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean WaitUntilWebElementDissapears(String xpath) {
		return this.wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(xpath)));
	}

	public String GetCurrentURL() {
		try {
			String url = driver.getCurrentUrl();

			return url;
		} catch (Exception e) {
			return e.getMessage();
		}
	}

	public String GetElementText(WebElement elemento) {
		String textoObtenido = "";
		try {
			Actions action = new Actions(driver);
			action.moveToElement(elemento).perform();

			textoObtenido = elemento.getText();

		//if (textoObtenido.equals(""))
		//		textoObtenido = elemento.getAttribute("value");

		} catch (Exception e) {
			String mensaje = "ObtenerTexto() - Error al obtener texto de elemento: [ELEMENTO] | [ERROR]";
			mensaje = mensaje.replace("[ELEMENTO]", elemento.getText());
			mensaje = mensaje.replace("[ERROR]", e.getMessage());
			Assert.fail(mensaje);
		}
		System.out.println("VALOR OBTENIDO DEL ELEMENTO:" + textoObtenido);
		return textoObtenido;
	}

	public String GetComboboxText(WebElement comboElement) {
		Select combo = new Select(comboElement);
		return combo.getFirstSelectedOption().getText();
	}

	public boolean ElementExist(String xpath) {
		try {
			Actions ob = new Actions(driver);
			ob.moveToElement(driver.findElement(By.xpath(xpath))).build().perform();
		} catch (Exception ex) {
			return false;
		}
		return driver.findElements(By.xpath(xpath)).size() > 0;
	}

	public int GetElementsSize(String xpath) {
		return driver.findElements(By.xpath(xpath)).size();
	}

	public void SendEnterKey() {
		WaitSleep(1);
		try {
			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.delay(20);
			robot.keyRelease(KeyEvent.VK_ENTER);
			WaitSleep(1);
		} catch (Exception e) {
			String mensaje = "EnviarEnter() - No se envi� la tecla ENTER | [ERROR]";
			mensaje = mensaje.replace("[ERROR]", e.getMessage());
			Assert.fail(mensaje);
		}
	}

	public void SendDownKey() {
		try {
			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_DOWN);
			robot.delay(20);
			robot.keyRelease(KeyEvent.VK_DOWN);
			WaitSleep(1);
		} catch (Exception e) {
			String mensaje = "EnviarEnter() - No se envi� la tecla ENTER | [ERROR]";
			mensaje = mensaje.replace("[ERROR]", e.getMessage());
			Assert.fail(mensaje);
		}
	}

	public String GetElementAttribute(WebElement element, String attribute) {
		return element.getAttribute(attribute);
	}

	/* Métodos para URL y Páginas */
	public void loadUrl(String url) throws Exception {
		System.out.println("Cargando url:" + url);
		driver.get(url);
	}

	public boolean HasClass(WebElement element, String className) {
		String classes = element.getAttribute("class");
		for (String c : classes.split(" ")) {
			if (c.equals(className)) {
				return true;
			}
		}

		return false;
	}

	public static void BajarHTML() throws IOException {
		String HTML = driver.getPageSource();
		Date date = new Date();
		SimpleDateFormat DateFormat = new SimpleDateFormat("ddMMyyyyHHmmss");
		String RutaRepo = "D:\\PageSources\\SIC_PAGESOURCE_" + DateFormat.format(date);
		String RutaHtml = RutaRepo + ".html";
		FileWriter ArchRepo = new FileWriter(RutaHtml);
		BufferedWriter Salida = new BufferedWriter(ArchRepo);
		Salida.write(HTML);
		Salida.close();
	}

	public static String GetDayNumber(int dayAdd) throws IOException {
		DateFormat dateFormat = new SimpleDateFormat("dd");
		Date currentDate = new Date();

		// convert date to calendar
		Calendar c = Calendar.getInstance();
		c.setTime(currentDate);
		c.add(Calendar.DAY_OF_MONTH, dayAdd);
		Date currentDatePlusOne = c.getTime();

		String dayNumber = dateFormat.format(currentDatePlusOne);
		System.out.println(" FECHA MODIFICADA: " + dayNumber);

		return dayNumber;

	}

}
